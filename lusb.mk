# makefile для включения в проект.
#
# на входе принимет параметры:
#  TARGET_CPU (или может быть переопределен через LUSB_TARGET) - определяет,
#              какой порт будет использоваться. Должен быть задан один
#              из поддерживаемых портов
#
# устанавливает следующие параметры:
#  LUSB_SRC         - исходные файлы на C, за исключением файла с дескрипторами
#  LUSB_SRC_DESCR   - исходные файлы с дескрипторами USB. Могут быть включены
#                     в проект или использоваться свои. Требуют специальных
#                     флагов для компиляции для перевода строк в UTF-16
#                     (возвращаются в переменной LUSB_SRC_DESCR_CPFLAGS)
#  LUSB_SRC_DESCR_CPFLAGS - дополнительные флаги компилятора, необходимые
#                     для сборки файлов с дескрипторами USB
#  LUSB_INC_DIRS    - директории с заголовками, которые должны быть добавлены
#                     в список директорий для поиска заголовков проекта


LUSB_MAKEFILE = $(abspath $(lastword $(MAKEFILE_LIST)))
LUSB_DIR := $(dir $(LUSB_MAKEFILE))
LUSB_TARGET ?= $(TARGET_CPU)

LUSB_SRC := $(LUSB_DIR)/src/lusb_eps.c \
            $(LUSB_DIR)/src/lusb_core.c \
            $(LUSB_DIR)/src/lusb_ctrlreq.c \
            $(LUSB_DIR)/src/lusb_activity_indication.c \
            $(LUSB_DIR)/src/lusb_soft_dd.c \
            $(LUSB_DIR)/src/lusb_msft_os_descr.c \
            $(LUSB_DIR)/src/classes/msc/lusb_msc.c

LUSB_SRC_DESCR := $(LUSB_DIR)/src/lusb_descriptors.c

LUSB_SRC_DESCR_CPFLAGS := -fwide-exec-charset=UTF-16LE

LUSB_INC_DIRS := $(LUSB_DIR)/src


#append port file to sources
ifeq ($(LUSB_TARGET), lpc17xx)
    LUSB_SRC += $(LUSB_DIR)/src/ports/lpc17xx/lusb_ll_lpc17xx.c
    LUSB_INC_DIRS += $(LUSB_DIR)/src/ports/lpc17xx
else ifeq ($(LUSB_TARGET), lpc43xx)
    LUSB_SRC += $(LUSB_DIR)/src/ports/lpc43xx/lusb_ll_lpc43xx.c
    LUSB_INC_DIRS += $(LUSB_DIR)/src/ports/lpc43xx    
else ifeq ($(LUSB_TARGET), lpc11u6x)
    LUSB_SRC += $(LUSB_DIR)/src/ports/lpc11u6x/lusb_ll_lpc11u6x.c
    LUSB_INC_DIRS += $(LUSB_DIR)/src/ports/lpc11u6x
else ifeq ($(LUSB_TARGET), stm32f0xx)
    LUSB_SRC += $(LUSB_DIR)/src/ports/stm32f0xx/lusb_ll_stm32f0xx.c
    LUSB_INC_DIRS += $(LUSB_DIR)/src/ports/stm32f0xx
else
    $(error lusb err: port architecture should be specified in LUSB_TARGET or TARGET_CPU variable)
endif
