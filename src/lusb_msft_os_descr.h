#ifndef LUSB_MSFT_OS_DESCR_H
#define LUSB_MSFT_OS_DESCR_H

/* Файл с поддержкой Microsoft OS Descriptors. Разрешается через параметр
   конфигурации LUSB_USE_MSFT_OS_DESCR.
   Может использоваться для указания дополнительных ID совместимости для
   привязки к драйверу (а не по VID/PID), в частности позволяет устанавливать
   WinUSB без .inf на Windows 8 и выше (для чего можно дополнительно определить
   LUSB_MSFT_OS_USE_COMPATID_WINUSB).

   Для получения этих дополнительных дексприторов используется код Vendor-запроса,
   который может быть каждым устройством назначен свой через определение
   LUSB_MSFT_OS_VENDOR_REQ_CODE (этот код не должен пересекатся с vendor-запросами
   самого устройства).

   Этот код ОС получает, предварительно запросив строку с заданным индексом. Если
   это строка нужного формата, то из нее извлекается код запроса, который уже
   используется для получения Compat-ID или Extended Properties дескрипторов. */


#include "lusb_usbdefs.h"
#include "lusb_config_params.h"

#define LUSB_MSFT_OS_STRING_DESCR_IDX       0xEE

#define LUSB_MSFT_OS_STRING_DESCR_LENGTH    0x12

#define LUSB_MSFT_OS_FEATURE_GENRE          0x1
#define LUSB_MSFT_OS_FEATURE_EX_COMPAT_ID   0x4
#define LUSB_MSFT_OS_FEATURE_EX_PROPERTIES  0x5


#define LUSB_MSFT_OS_COMPATID_WINUSB "WINUSB"


#ifdef LUSB_MSFT_OS_USE_COMPATID_WINUSB
    #define LUSB_MSFT_OS_COMPATID LUSB_MSFT_OS_COMPATID_WINUSB
#endif


#define LUSB_MSFT_COMPID_FUNC_CNT  LUSB_INTERFACE_CNT

#ifndef LUSB_MSFT_EXPROP_CNT
    #define LUSB_MSFT_EXPROP_CNT 0
    #define LUSB_MSFT_EXPROP_BLOB_SIZE 0
#endif


#include "lcspec_pack_start.h"
typedef struct {
    uint32_t dwLength;
    uint16_t bcdVersion;
    uint16_t wIndex;
    uint8_t  bCount;
    uint8_t  Reserved[7];
} LATTRIBUTE_PACKED t_lusb_msft_os_compid_hdr;

typedef struct {
    uint8_t bFirstInterfaceNumber;
    uint8_t Reserved1;
    char compatibleID[8];
    char subCompatibleID[8];
    uint8_t Reserved2[6];
} LATTRIBUTE_PACKED t_lusb_msft_os_compid_func;

typedef struct {
    t_lusb_msft_os_compid_hdr  hdr;
    t_lusb_msft_os_compid_func func[LUSB_MSFT_COMPID_FUNC_CNT];
} t_lusb_msft_comp_id_descr;

typedef struct {
    uint32_t dwLength;
    uint16_t bcdVersion;
    uint16_t wIndex;
    uint16_t wCount;
} t_lusb_msft_os_exprop_hdr;


typedef struct {
    t_lusb_msft_os_exprop_hdr hdr;
    uint8_t data[LUSB_MSFT_EXPROP_BLOB_SIZE];
} t_lusb_msft_os_exprop_descr;



#include "lcspec_pack_restore.h"





extern const uint8_t lusb_msft_os_str_descr[];
const uint8_t *lusb_msft_os_get_feature_descr(const t_lusb_req *req, int* length, uint8_t *tx_buf);

#endif // LUSB_MSFT_OS_DESCR_H
