/*
 * lusb_user_cb.h
 *
 *  Created on: 13.07.2010
 *      Author: borisov
 *
 *  Файл содержит определения callback-функций,
 *  которые может реализовать пользовательское приложение
 *  (константы для включения функций объявляются в lusb_config.h)
 */

#ifndef __LUSB_USER_CB_H__
#define __LUSB_USER_CB_H__

#include "lusb_config_params.h"
#include "lusb_usbdefs.h"

#if defined LUSB_APPL_CB_CUSTOM_CTRLREQ_RX && !defined LUSB_EP0_RX_PARTIAL
    int lusb_appl_cb_custom_ctrlreq_rx(const t_lusb_req *req, uint8_t* buf);
#endif

#if defined LUSB_APPL_CB_CUSTOM_CTRLREQ_TX && !defined LUSB_EP0_TX_PARTIAL
    const void* lusb_appl_cb_custom_ctrlreq_tx(const t_lusb_req *req, int* length, uint8_t *tx_buf);
#endif

#ifdef LUSB_EP0_RX_PARTIAL
    int lusb_appl_cb_custom_ctrlreq_partial_rx(t_lusb_req* req, uint8_t* buf,
                uint16_t offset, uint16_t length);
#endif

#ifdef LUSB_EP0_TX_PARTIAL
    int lusb_appl_cb_custom_ctrlreq_partial_tx(t_lusb_req* req, uint16_t offset);
#endif

#ifdef LUSB_APPL_CB_BUS_RESET
    void lusb_appl_cb_bus_reset(void);
#endif

#ifdef LUSB_APPL_CB_CONNECT_CHANGED
    void lusb_appl_cb_conch(uint8_t con);
#endif

#ifdef LUSB_APPL_CB_SUSPEND_CHANGED
    void lusb_appl_cb_suspch(uint8_t susp);
#endif

#ifdef LUSB_APPL_CB_ENTER_TEST_MODE
    int lusb_appl_cb_enter_test_mode(uint8_t test_selector);
#endif

#ifdef LUSB_APPL_CB_DMA_EVENT
    #error This function is obsolte! use LUSB_APPL_CB_DD_EVENT and lusb_appl_cb_dd_event callback!
#endif

#ifdef LUSB_APPL_CB_DD_EVENT
    void lusb_appl_cb_dd_event(uint8_t iface, uint8_t ep_idx, t_lusb_dd_event event, const t_lusb_dd_status *pDD);
#endif

#ifdef LUSB_APPL_CB_DEVSTATE_CHANGED
    void lusb_appl_cb_devstate_ch(uint8_t old_state, uint8_t new_state);
#endif

#ifdef LUSB_APPL_CB_INTF_ALTSET_CHANGED
    void lusb_appl_cb_intf_alteset_ch(uint8_t intf, uint8_t old_altset, uint8_t new_altset);
#else
    #define lusb_appl_cb_intf_alteset_ch(...)
#endif

#ifdef LUSB_APPL_CB_EP_EVENT
    void lusb_appl_cb_ep_event(uint8_t iface, uint8_t ep_idx, t_lusb_ep_event event);
#else
    #define lusb_appl_cb_ep_event(...)
#endif



#ifdef LUSB_APPL_CB_ACTIVITY_IND
    void lusb_appl_cb_activity_start(void);
    void lusb_appl_cb_activity_end(void);
#endif



#endif /* LUSB_USER_CB_H_ */
