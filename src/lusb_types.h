/*
 * lusb_typedefs.h
 *
 *  Created on: 20.07.2010
 *      Author: borisov
 *
 *      Файл содержит определение типов, используемых стеком LUSB
 */

#ifndef __LUSB_TYPES_H_
#define __LUSB_TYPES_H_

#include "lusb_usbdefs.h"
#include "lusb_config_params.h"


//коды ошибок стека
#define LUSB_ERR_SUCCESS                0
#define LUSB_ERR_UNSUPPORTED_REQ       -1
#define LUSB_ERR_MODULE_TST            -2
#define LUSB_ERR_DEV_UNCONFIGURED      -3
#define LUSB_ERR_EP_INDEX             -10
#define LUSB_ERR_EP_DIR               -11
#define LUSB_ERR_EP_BUSY              -12
#define LUSB_ERR_EP_DISABLED          -13
#define LUSB_ERR_NO_FREE_DESCR        -20
#define LUSB_ERR_DESCR_NOT_FOUND      -21
#define LUSB_ERR_TRANSF_SIZE          -22
#define LUSB_ERR_PARAMETERS           -23



//состояние устройства
#define LUSB_DEVSTATE_ATTACHED       (0x00000001)
#define LUSB_DEVSTATE_POWERED        (0x00000002)
#define LUSB_DEVSTATE_DEFAULT        (0x00000004)
#define LUSB_DEVSTATE_ADDRESS        (0x00000008)
#define LUSB_DEVSTATE_CONFIGURED     (0x00000010)
#define LUSB_DEVSTATE_SUSPENDED      (0x00000020)

//флаги при приеме данных
#define LUSB_PKTFLAGS_SETUP            (1UL << 0) /* признак, что идет чтение SETUP пакета */
#define LUSB_PKTFLAGS_STATUS           (1UL << 8) /* признак, что идет сдадия передачи статуса, а не данных */
#define LUSB_PKTFLAGS_LAST_DATA        (1UL << 9) /* признак, что это запрос на обмен последнего пакета стадии данных */


//состояние дескриптора DMA
#define LUSB_DDSTATUS_NOT_VALID         0x0
#define LUSB_DDSTATUS_NOT_SERVICED      0x1
#define LUSB_DDSTATUS_IN_PROGRESS       0x2
#define LUSB_DDSTATUS_CPL_SUCCESS       0x3
#define LUSB_DDSTATUS_CPL_UNDERRUN      0x4
#define LUSB_DDSTATUS_CPL_OVERRUN       0x5
#define LUSB_DDSTATUS_SYSTEM_ERR        0x6


//флаги при добавлении буфера dma
#define LUSB_ADDBUF_FLG_REINIT              0x4


typedef enum {
    LUSB_DD_EVENT_EOT                  = 0x1,  /**< завершена пердача данных по дескр. */
    LUSB_DD_EVENT_NODD                 = 0x2,  /**< не найден требуемый дескрптор */
    LUSB_DD_EVENT_ERR                  = 0x3   /**< системная ошибка */
} t_lusb_dd_event;

typedef enum {
    LUSB_EP_EVENT_ENABLED              = 1, /**< конечная точка разрешена */
    LUSB_EP_EVENT_DISABLED             = 2, /**< конечная точка запрещена */
    LUSB_EP_EVENT_HALTED               = 3, /**< переведена в остановленное состояние */
    LUSB_EP_EVENT_HALT_CLR             = 4, /**< сброс остановленного состояния со сбросом кт */
} t_lusb_ep_event;

typedef enum {
    LUSB_DD_FLAG_ADD_ZEROLEN_PACKET        = 1 << 0, /* Автоматическое добавление zero-len пакета, если передача кратна максимальному пакету */
} t_lusb_dd_flags;

typedef uint16_t t_lusb_dd_cnt;

/* используемая скорость подключения */
#define LUSB_SPEED_UNKNOWN 0
#define LUSB_SPEED_LOW     1
#define LUSB_SPEED_FULL    2
#define LUSB_SPEED_HIGH    3
#define LUSB_SPEED_SUPER   4


/** Состояние выполнения дескриптора по передачи данных */
typedef struct {
    uint16_t status; /**< Cостояние передачи - значение из LUSB_DDSTATUS_XXX
                            (LUSB_DDSTATUS_CPL_SUCCESS - успешно) */
    uint16_t flags;  /**< Флаги (резерв) */
    uint32_t last_addr; /**< Адрес последнего переданного слова +1 */
    uint32_t trans_cnt; /**< Количество переданных байт */
    uint32_t length;    /**< Сколько данных требовалось передать */
} t_lusb_dd_status;


/** состояние конечной точки (кроме 0-ой) */
typedef struct {
    const t_usb_endpoint_descriptor* descr; /**< Указатель на дескриптор конечной точки */
    uint8_t halted; /**< Признак, что кт в состоянии останова */
    t_lusb_dd_cnt dd_req_cnt;  /**< Счетчик поставленных запросов на обмен */
    t_lusb_dd_cnt dd_cpl_cnt;  /**< Счетчик завершенных запросов на обмен */
} t_lusb_ep_info;

#endif /* LUSB_TYPEDEFS_H_ */
