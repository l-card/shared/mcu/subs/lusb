/*
 * lusb_usb_defs.h
 *
 *  Created on: 12.07.2010
 *      Author: borisov
 *
 *
 *  Файл содержит стандартные определения USB интерфейса,
 *  которые не зависят от реализации стека
 */

#ifndef __LUSB_USB_DEFS_H__
#define __LUSB_USB_DEFS_H__

#include <stdint.h>
#include <stdbool.h>
#include "lcspec.h"
#include "lusb_chardefs.h"

#define USB_EP0_MAX_SIZE          64
#define USB_SETUP_PACKET_SIZE     8

#define USB_FS_EP_BULK_MAX_PACKET_SIZE   64
#define USB_HS_EP_BULK_MAX_PACKET_SIZE   512
#define USB_FS_EP_ISO_MAX_PACKET_SIZE    1024






/* USB Device Classes */
#define USB_DEVICE_CLASS_RESERVED              0x00
#define USB_DEVICE_CLASS_AUDIO                 0x01
#define USB_DEVICE_CLASS_COMMUNICATIONS        0x02
#define USB_DEVICE_CLASS_HID                   0x03
#define USB_DEVICE_CLASS_MONITOR               0x04
#define USB_DEVICE_CLASS_PHYSICAL_INTERFACE    0x05
#define USB_DEVICE_CLASS_POWER                 0x06
#define USB_DEVICE_CLASS_PRINTER               0x07
#define USB_DEVICE_CLASS_MASS_STORAGE          0x08
#define USB_DEVICE_CLASS_HUB                   0x09
#define USB_DEVICE_CLASS_MISCELLANEOUS         0xEF
#define USB_DEVICE_CLASS_VENDOR_SPECIFIC       0xFF

#define USB_INTERFACE_PROTOCOL_VENDOR_SPECIFIC 0xFF



//коды стандартных USB-запросов
#define USB_REQ_GET_STATUS         (0)
#define USB_REQ_CLEAR_FEATURES     (1)
#define USB_REQ_SET_FEATURES       (3)
#define USB_REQ_SET_ADDRESS        (5)
#define USB_REQ_GET_DESCRIPTOR     (6)
#define USB_REQ_SET_DESCRIPTOR     (7)
#define USB_REQ_GET_CONFIGURATION  (8)
#define USB_REQ_SET_CONFIGURATION  (9)
#define USB_REQ_GET_INTERFACE      (10)
#define USB_REQ_SET_INTERFACE      (11)
#define USB_REQ_SYNCH_FRAME        (12)

/* стандартные типы дескрипторов USB */
#define USB_DESCRTYPE_DEVICE                        (1)
#define USB_DESCRTYPE_CONFIGURATION                 (2)
#define USB_DESCRTYPE_STRING                        (3)
#define USB_DESCRTYPE_INTERFACE                     (4)
#define USB_DESCRTYPE_ENDPOINT                      (5)
#define USB_DESCRTYPE_DEVICE_QUALIFIER              (6)
#define USB_DESCRTYPE_OTHER_SPEED_CONFIGURATION     (7)
#define USB_DESCRTYPE_INTERFACE_POWER               (8)
#define USB_DESCRTYPE_OTG                           (9)
#define USB_DESCRTYPE_DEBUG                         (10)
#define USB_DESCRTYPE_INTERFACE_ASSOCIATION         (11)
/* стандартные дескрипторы, введенные в Wireless USB */
#define USB_DESCRTYPE_SECURITY                      (12)
#define USB_DESCRTYPE_KEY                           (13)
#define USB_DESCRTYPE_ENCRYPTION_TYPE               (14)
#define USB_DESCRTYPE_BOS                           (15)
#define USB_DESCRTYPE_DEVICE_CAPABILITY             (16)
#define USB_DESCRTYPE_WIRELESS_ENDPOINT_COMPANION   (17)
/* введенные в 3.0 */
#define USB_DESCRTYPE_SS_USB_ENDPOINT_COMPANION     (48)
#define USB_DESCRTYPE_SS_ISO_ENDPOINT_COMPANION     (49)




/* коды для Device Capability */
#define USB_DEVICE_CAP_WIRELESS_USB                 (1)
#define USB_DEVICE_CAP_WIRELESS_USB_EXT             (2)
#define USB_DEVICE_CAP_USB2_0_EXT                   (2)


//lang id
#define USB_LANGID_ENG_USA    0x0409
#define USB_LANGID_RUS        0x0419



/***********************************************************************************
 *     структура USB-запроса и маски полей
 ************************************************************************************/

struct st_lusb_req {
    uint8_t req_type;
    uint8_t request; //код запроса
    union {
        uint16_t val;
        struct {
            uint8_t val_l;
            uint8_t val_h;
        };
    };
    uint16_t index;
    uint16_t length; //длина данных
};
typedef struct st_lusb_req t_lusb_req;


//биты поля req_type
#define USB_REQ_REQTYPE_DIR              (0x80) //маска направления передачи
#define USB_REQ_REQTYPE_DIR_IN           (0x80) //device to host
#define USB_REQ_REQTYPE_DIR_OUT          (0x00) //host to device

#define USB_REQ_REQTYPE_TYPE             (0x60) //маска типа запроса
#define USB_REQ_REQTYPE_TYPE_STD         (0x00) //стандартный
#define USB_REQ_REQTYPE_TYPE_CLASS       (0x20) //класса
#define USB_REQ_REQTYPE_TYPE_VENDOR      (0x40) //производителя

#define USB_REQ_REQTYPE_RECIPIENT            (0x1F) //адресат запроса
#define USB_REQ_REQTYPE_RECIPIENT_DEVICE     (0x00) //device
#define USB_REQ_REQTYPE_RECIPIENT_INTERFACE  (0x01) //interface
#define USB_REQ_REQTYPE_RECIPIENT_ENDPOINT   (0x02) //ep
#define USB_REQ_REQTYPE_RECIPIENT_OTHER      (0x03) //other









/*******************************************************************************************
 *                                       маски для usb запросов                            *
 *******************************************************************************************/
//поля данных для запрса GET_STATUS
#define USB_DEVSTATUS_SELFPOW              (0x1)
#define USB_DEVSTATUS_RWU                  (0x2)

#define USB_EPSTATUS_HALT                  (0x1)

#define USB_DEVICE_STATUS_LENGTH           (0x2)
#define USB_INTERFACE_STATUS_LENGTH        (0x2)
#define USB_ENDPOINT_STATUS_LENGTH         (0x2)

//типы features для GET/SET_FEATURES
#define USB_FEATURES_ENDPOINT_HALT         (0x0)
#define USB_FEATURES_DEVICE_RWU            (0x1)
#define USB_FEATURES_DEVICE_TESTMODE       (0x2)

#define USB_GETCONFIG_REQ_LENGTH           (0x1)
#define USB_GET_INTERFACE_REQ_LENGTH       (0x1)




/***********************************************************************************
 *     дескрипторы, маски и константы для их полей
 ************************************************************************************/
#include "lcspec_pack_start.h"
struct st_usb_common_descriptor {
    uint8_t bLength;
    uint8_t bDescriptorType;
} LATTRIBUTE_PACKED;
typedef struct st_usb_common_descriptor t_usb_common_descriptor;


struct st_usb_device_descriptor {
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint16_t bcdUSB;
    uint8_t bDeviceClass;
    uint8_t bDeviceSubClass;
    uint8_t bDeviceProtocol;
    uint8_t bMaxPacketSize0;
    uint16_t idVendor;
    uint16_t idProduct;
    uint16_t bcdDevice;
    uint8_t iManufacturer;
    uint8_t iProduct;
    uint8_t iSerialNumber;
    uint8_t bNumConfigurations;
} LATTRIBUTE_PACKED;
typedef struct st_usb_device_descriptor t_usb_device_descriptor;

#define USB_DEVICE_DESCRIPTOR_LENGTH   sizeof(t_usb_device_descriptor) // (18)


struct st_usb_device_qualifier_descriptor {
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint16_t bcdUSB;
    uint8_t bDeviceClass;
    uint8_t bDeviceSubClass;
    uint8_t bDeviceProtocol;
    uint8_t bMaxPacketSize0;
    uint8_t bNumConfigurations;
    uint8_t bReserved;
} LATTRIBUTE_PACKED;
typedef struct st_usb_device_qualifier_descriptor t_usb_device_qualifier_descriptor;

#define USB_DEVICE_QUALIFIER_DESCRIPTOR_LENGTH    sizeof(t_usb_device_qualifier_descriptor)//(10)



#define USB_CONFIG_ATTR_SELFPOW                (0x40)
#define USB_CONFIG_ATTR_REMWU                  (0x20)
#define USB_CONFIG_ATTR_DEFAULT                (0x80)



struct st_usb_configuration_descriptor {
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint16_t wTotalLength;        //полная длина (cfg + int + все ep)
    uint8_t bNumInterfaces;       //кол-во интерфейсов
    uint8_t bConfigurationValue;  //номер конфигурации
    uint8_t iConfiguration;
    uint8_t bmAttributes;
    uint8_t bMaxPower;
} LATTRIBUTE_PACKED;
typedef struct st_usb_configuration_descriptor t_usb_configuration_descriptor;

#define USB_CONFIGURATION_DESCRIPTOR_LENGTH    sizeof(t_usb_configuration_descriptor) //(9)



struct st_usb_interface_descriptor {
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint8_t bInterfaceNumber;    //номер интерфейса (с нуля)
    uint8_t bAlternateSetting;   //номер альтернативных настроек (с нуля)
    uint8_t bNumEndpoints;       //кол-во EP
    uint8_t bInterfaceClass;
    uint8_t bInterfaceSubClass;
    uint8_t bInterfaceProtocol;
    uint8_t iInterface;
} LATTRIBUTE_PACKED;

typedef struct st_usb_interface_descriptor t_usb_interface_descriptor;

#define USB_INTERFACE_DESCRIPTOR_LENGTH    sizeof(t_usb_interface_descriptor) //9


#define USB_EPDIR                          (0x80)
#define USB_EPDIR_IN                       (0x80)
#define USB_EPDIR_OUT                      (0x00)
#define USB_EP_IN(ep)                      (USB_EPDIR_IN  | ep)
#define USB_EP_OUT(ep)                     (USB_EPDIR_OUT | ep)
#define USB_EPNUM_MSK                      (0xF)

//типы конечной точки
#define USB_EPTYPE_CTRL                        (0x0)
#define USB_EPTYPE_ISO                         (0x1)
#define USB_EPTYPE_BULK                        (0x2)
#define USB_EPTYPE_INT                         (0x3)

//тип синхронизации для изохронных ep
#define USB_EPISO_NOSYNC                   (0x0)
#define USB_EPISO_ASYNC                    (0x4)
#define USB_EPISO_ADAPTIVE                 (0x8)
#define USB_EPISO_SYNC                     (0xC)

//usage type для изохронных ep
#define USB_EPISO_TYPE_DATA                (0x00)
#define USB_EPISO_TYPE_FEEDBACK            (0x10)
#define USB_EPISO_TYPE_INPL_FEEDBACK       (0x20)

struct st_usb_endpoint_descriptor {
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint8_t bEndpointAddress;
    uint8_t bmAttributes;
    uint16_t wMaxPacketSize;
    uint8_t bInterval;
} LATTRIBUTE_PACKED;


#include "lcspec_pack_restore.h"

typedef struct st_usb_endpoint_descriptor t_usb_endpoint_descriptor;

#define USB_ENDPOINT_DESCRIPTOR_LENGTH     sizeof (t_usb_endpoint_descriptor) //(7)

#define USB_STRING_DESCR_TYPEDEF(_type, _string)  struct _type##_st { \
                                                    uint8_t bLength; \
                                                    uint8_t bDescriptorType; \
                                                    uint16_t str[sizeof(_string) + 1]; \
                                                    }; \
                                                    typedef struct _type##_st _type;                                                   
                                                

#endif /* LUSB_USB_DEFS_H_ */
