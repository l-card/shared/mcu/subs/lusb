/*
 * lusb_ctrl_transf.c
 *
 *  Created on: 06.07.2010
 *      Author: borisov
 *
 *  Файл содержит функции ядра usb-стека, определяющие алгоритм
 *  его работы, не зависящий от аппаратной части
 *  Здесь находятся callback-функции, вызываемые портом,
 *  так же здесь содержится логика работы 0-ой контрольной точки
 */


#include "lusb_config_params.h"
#include "lusb_types.h"
#include "lusb_usbdefs.h"
#include "lusb_ctrlreq.h"
#include "lusb_appl_cb.h"
#include "lusb.h"

#include "lusb_ll.h"
#include "lusb_core_cb.h"
#include "lusb_activity_indication.h"

#include <string.h>


#ifdef LUSB_USE_SOFT_DD
    #include "lusb_soft_dd.h"
#endif

#ifdef LUSB_USE_MSFT_OS_DESCR
    #include "lusb_msft_os_descr.h"
#endif


typedef enum { LUSB_EP0_STAGE_IDLE,              /* ожидаем прихода нового запроса */
              LUSB_EP0_STAGE_RD_DATA,           /* принимаем данные запроса Host->Dev */
              LUSB_EP0_STAGE_RD_DATA_PART_WT,   /* все данные приняты, но не все части обработаны */
              LUSB_EP0_STAGE_SETUP_RD_PROC,     /* все данные приняты, нужно обработать запрос */
              LUSB_EP0_STAGE_SETUP_WR_PROC,     /* принят запрос Dev->Host - нужно обработать его */
              LUSB_EP0_STAGE_DATA_WR,           /* передача данных Dev->Host */
              LUSB_EP0_STAGE_DATA_WR_DONE,
              LUSB_EP0_STAGE_STATUS_RD,         /* Ожидание статуса подтверждения запроса Dev->Host) */
              LUSB_EP0_STAGE_STATUS_WR          /* Необходимо передать статус подтверждения запроса Host->Device */
             } lusb_ep0_stage_t;

t_lusb_state g_lusb_state;


//общие переменные, используемые для обмена по 0-ой конечной точке

#define LSPEC_ALIGNMENT 4
#include "lcspec_align.h"
static uint8_t f_ctrl_buf[LUSB_EP0_BUF_SIZE]; /* буфер приема/передачи для управляющей кт */
static t_lusb_req f_ctrl_req;  //управляющий запрос

static volatile lusb_ep0_stage_t f_ctrl_stage; /* состояние автомата обработки управляющих запросов */

static volatile uint16_t f_ctrl_trans_rem_len; /* оставшаяся для передачи длина управляющего запроса */
static volatile uint16_t f_ctrl_buf_pos;       /* позиция в буфере для сохранения следующих данных */
static volatile bool f_ctrl_zpacket_req;     /* признак необходимости передачи нулевого пакета в конце стадии данных */
static volatile bool f_ctrl_back_work;      /* признак, что есть работа для выполнения в фоне (в lusb_progress) */
static volatile bool f_ctrl_buf_lock;       /* признак, что буфер и данные управляющего запроса
                                                  используются приложением и их нельзя переписывать */
static const uint8_t* f_ctrl_tx_buf;           /* указатель на буфер для передачи */

static volatile uint16_t f_deffered_rx_pack_cnt; /* количество пакето, которые приняты, но не были прочитаны */
static volatile int f_flags;


static void f_ep0_proc_rx_packet(int flags);
static void f_ep0_proc_tx_packet(void);
static void f_ep0_cb_rx(int flags);

static const void* f_ep0_ctrlreq_tx_process(const t_lusb_req *req, int* length, uint8_t* tx_buf);
static int f_ep0_ctrlreq_rd_process(const t_lusb_req *req, uint8_t *buf);





static void f_ctrl_ep_stall(void) {
    f_ctrl_buf_lock = false;
    lusb_ll_ep0_stall();
    f_ctrl_stage = LUSB_EP0_STAGE_IDLE;
}

static void f_ep0_start_back_work(bool irq_en) {
    f_ctrl_buf_lock = true;
    f_ctrl_back_work = false;
    lusb_ll_usbirq_en(irq_en);
}

static int f_has_new_setup(void) {
    return (f_deffered_rx_pack_cnt!=0) && (f_flags & LUSB_PKTFLAGS_SETUP);
}

/****************************************************************************************
 * инициализация USB-контроллера - обязательно должна вызываться в перед использованием
 * других функций
 * **************************************************************************************/
int lusb_init(void) {
    memset(&g_lusb_state, 0, sizeof(g_lusb_state));
    f_flags = 0;
    f_ctrl_stage = LUSB_EP0_STAGE_IDLE;
    f_deffered_rx_pack_cnt = 0;
    f_ctrl_back_work = f_ctrl_buf_lock = false;
    return lusb_ll_init();
}

int lusb_close(void) {
    lusb_ll_usbirq_en(false);
    lusb_connect(false);
    return lusb_ll_close();
}

int lusb_has_background_work(void) {
    return !!(f_ctrl_back_work || f_deffered_rx_pack_cnt);
}

#ifdef LUSB_EP0_RX_PARTIAL
int lusb_ctrl_rx_packet_done(void) {
    bool int_en = lusb_ll_usbirq_en(false);
    if (g_lusb_state.wrk_flgs & LUSB_WRKFLAGS_EP0_BUFFER_LOCK) {
        g_lusb_state.wrk_flgs &= ~LUSB_WRKFLAGS_EP0_BUFFER_LOCK;
        f_ctrl_wr_pos = 0;
    }
    lusb_ll_usbirq_en(int_en);
    return 0;
}
#endif

#ifdef LUSB_EP0_TX_PARTIAL
int lusb_ctrl_tx_packet(const uint8_t* buff, uint16_t size) {
    bool int_en = lusb_ll_usbirq_en(false);
    f_ctrl_tx_buf = buff;
    f_ctrl_tx_pos = 0;
    f_ctrl_tx_size = size;

    //нулевой размер - признак, что шлем нулевой пакет
    if (size == 0) {
        g_lusb_state.wrk_flgs |=
                (LUSB_WRKFLAGS_EP0_PACKET0_SEND | LUSB_WRKFLAGS_EP0_FULL_BUF_PREPARED);
    }

    //если предыдущий пакет уже передан - сразу шлем пакет
    if (g_lusb_state.wrk_flgs & LUSB_WRKFLAGS_EP0_WAIT_PREPARE_BUF) {
        g_lusb_state.wrk_flgs &= ~LUSB_WRKFLAGS_EP0_WAIT_PREPARE_BUF;
        lusb_core_ep0_txdone(0);
    }

    lusb_ll_usbirq_en(int_en);
    return 0;
}
#endif




/************************************************************************************
 * Функция выполняет фоновые действия USB-стека
 * обслуживаются запросы нулевой точки (если не установлен LUSB_CTLREQ_PROC_IN_INT),
 * и обмен по конечным точкам без DMA,
 * а также действия USB-стека...
 ************************************************************************************/
void lusb_progress(void) {
    /* обслуживаение запросов по нулевой конечной точке */
    bool int_en;

    if (lusb_has_background_work()) {
        /** @todo возможно запрет только управляющих прерываний */
        int_en = lusb_ll_usbirq_en(false);

#ifdef LUSB_EP0_RX_PARTIAL
        /** @todo проверка работы частичной обработки пакетов */
        if (((f_ctrl_stage = LUSB_EP0_STAGE_RD_DATA) || (f_ctrl_stage == LUSB_EP0_STAGE_RD_DATA_PART_WT))
            && ((f_ctrl_req.req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_VENDOR)
            && (f_ctrl_partial_pos==0)) {
                unsigned rem_size = f_ctrl_trans_rem_len;                
                f_ctrl_partial_pos = f_ctrl_buf_pos

                f_ep0_start_back_work(int_en);
                if (lusb_appl_cb_custom_ctrlreq_partial_rx(&f_ctrl_req,
                        f_ctrl_buf,  rem_size, f_ctrl_partial_pos)
                        == LUSB_ERR_UNSUPPORTED_REQ) {
                    lusb_ctrl_rx_packet_done(); /** @todo ?? */
                    f_ctl_ep_stall();
                }
            }
        }
#endif
#ifdef LUSB_EP0_TX_PARTIAL
        if ((f_ctrl_req.req_type & USB_REQ_REQTYPE_TYPE)
                            == USB_REQ_REQTYPE_TYPE_VENDOR)
        {
            lusb_ll_usbirq_en(int_en);
            f_ctrl_tx_buf = NULL;
            if (lusb_appl_cb_custom_ctrlreq_partial_tx(&f_ctrl_req, f_ctrl_wr_length)
                == LUSB_ERR_UNSUPPORTED_REQ)
            {
                f_ctrl_tx_size = LUSB_ERR_UNSUPPORTED_REQ;
                lusb_ll_stall_ctrl_ep();
            }
        }
#endif
        if (!f_ctrl_buf_lock) {
            /* обработка принятого запроса */
            if (f_ctrl_stage == LUSB_EP0_STAGE_SETUP_RD_PROC) {
                int res;
                f_ep0_start_back_work(int_en);
                res = f_ep0_ctrlreq_rd_process(&f_ctrl_req, f_ctrl_buf);
                int_en = lusb_ll_usbirq_en(false);
                f_ctrl_buf_lock = false;
                /* при приходе нового запроса должны прекратить обработку старого */
                if (f_has_new_setup()) {
                    f_ctrl_stage = LUSB_EP0_STAGE_IDLE;
                } else {
                    /* запрос не поддерживается */
                    if (res != LUSB_ERR_SUCCESS) {
                        f_ctrl_ep_stall();
                    } else {
                        /* посылаем подтвеждение */
                        f_ctrl_stage = LUSB_EP0_STAGE_STATUS_WR;
                        lusb_ll_ep0_set_state(LUSB_LL_EP0_STATE_STATUS_TX, 0);
                        /* посылка завершающей статуса - пакет 0-ой длины */
                        if (lusb_ll_ep0_wr_packet(NULL, LUSB_PKTFLAGS_STATUS, 0) != 0) {
                            f_ctrl_ep_stall();
                        }
                    }
                }
            } else if (f_ctrl_stage == LUSB_EP0_STAGE_SETUP_WR_PROC) {
                int res = LUSB_EP0_BUF_SIZE;
                f_ep0_start_back_work(int_en);
                f_ctrl_tx_buf = f_ep0_ctrlreq_tx_process(&f_ctrl_req, &res, f_ctrl_buf);
                int_en = lusb_ll_usbirq_en(false);
                f_ctrl_buf_lock = false;
                if ((f_ctrl_tx_buf == NULL) && (res != 0))
                    res = LUSB_ERR_UNSUPPORTED_REQ;

                if (f_has_new_setup()) {
                    f_ctrl_stage = LUSB_EP0_STAGE_IDLE;
                } else {
                    /* запрос не поддерживается */
                    if (res < 0) {
                        f_ctrl_ep_stall();
                    } else {
                        /* размер - минимум из того, что может передать устройство
                           и тем, что запрашивается */
                        f_ctrl_trans_rem_len = MIN(res, f_ctrl_req.length);
                        f_ctrl_stage         = LUSB_EP0_STAGE_DATA_WR;
                        /* передача нулевого пакета необхадима только в случае, если
                         * реальный передаваемый размер меньше запрошенного и
                         * передаваемый размер кратен размеру пакета нулевой конечной точки.
                         * Если кратен, но равен, то хост и так знает момент окончания
                         * и сразу переходит к обмену статусом без ожидания нулевого пакета */
                        f_ctrl_zpacket_req = (f_ctrl_trans_rem_len != f_ctrl_req.length) &&
                                ((f_ctrl_trans_rem_len & (LUSB_EP0_PACKET_SIZE - 1)) == 0);
                        f_ctrl_buf_pos       = 0;
                        f_ep0_proc_tx_packet();
                    }
                }
            } else if (f_deffered_rx_pack_cnt!=0){
                int flags = f_flags;
                f_deffered_rx_pack_cnt--;
                f_flags = 0;
                f_ep0_proc_rx_packet(flags);
            }
        }

        lusb_ll_usbirq_en(int_en);
    }


#ifdef LUSB_USE_MSC_INTERFACE
    lusb_msc_progress();
#endif

    lusb_activity_pull();
    lusb_ll_progress();

}


/*********************************************************************************
 *   callback функция, вызываемая портом при сбросе шины
 *********************************************************************************/
void lusb_core_cb_reset(void) {
    lusb_printf(LUSB_LOG_LVL_INFO, "lusb: bus reset!\n");

#ifdef LUSB_APPL_CB_DEVSTATE_CHANGED
    uint8_t old_state = g_lusb_state.dev.state;
#endif
    //модуль переводится в состояние DEFAULT
    g_lusb_state.dev.state = LUSB_DEVSTATE_DEFAULT | LUSB_DEVSTATE_POWERED | LUSB_DEVSTATE_ATTACHED;

#ifdef LUSB_APPL_CB_DEVSTATE_CHANGED
    if (old_state != g_lusb_state.dev.state)
        lusb_appl_cb_devstate_ch(old_state, g_lusb_state.dev.state);
#endif

#ifdef LUSB_APPL_CB_BUS_RESET
    lusb_appl_cb_bus_reset();
#endif
}

/*********************************************************************************
 *   callback функция, вызываемая портом изменении состояния подключения
 *   usb-модуля к шине
 *   con = 0 - дисконнект
 *         1 - подключен
 *********************************************************************************/
void lusb_core_cb_con_changed(uint8_t con) {
    uint8_t old_state = g_lusb_state.dev.state;

    if (con) {
        g_lusb_state.dev.state |= LUSB_DEVSTATE_ATTACHED;
    } else {
        if (old_state & LUSB_DEVSTATE_CONFIGURED)
            lusb_ll_configure(0);
        g_lusb_state.dev.state = 0;
        if(old_state!=0) {
            lusb_printf(LUSB_LOG_LVL_INFO, "lusb: connection lost!\n");
        }
    }

#ifdef LUSB_APPL_CB_DEVSTATE_CHANGED
    if (old_state!=g_lusb_state.dev.state)
        lusb_appl_cb_devstate_ch(old_state, g_lusb_state.dev.state);
#endif

#ifdef LUSB_APPL_CB_CONNECT_CHANGED
    lusb_appl_cb_conch(con);
#endif
}



/*********************************************************************************
 *   callback функция, вызываемая портом при входе/выходи в режим suspend
 *   susp = 0 - пробуждение из suspend
 *          1 - переход в suspend
 *********************************************************************************/
void lusb_core_cb_susp_changed(uint8_t susp) {
#ifdef LUSB_APPL_CB_DEVSTATE_CHANGED
    uint8_t old_state = g_lusb_state.dev.state;
#endif
    if (susp) {
        g_lusb_state.dev.state |= LUSB_DEVSTATE_SUSPENDED;
    } else {
        g_lusb_state.dev.state &= ~LUSB_DEVSTATE_SUSPENDED;
    }
#ifdef LUSB_APPL_CB_DEVSTATE_CHANGED    
    lusb_appl_cb_devstate_ch(old_state, g_lusb_state.dev.state);
#endif

#ifdef LUSB_APPL_CB_SUSPEND_CHANGED
    lusb_appl_cb_suspch(susp);
#endif
}




/*********************************************************************************
 *   callback функция, вызываемая портом при приеме пакета
 *   какой-либо конченой точкой
 *   ep_addr - адрес конечной точки, для которой пришел пакет
 *   flags  - флаги (пока используется только LUSB_PKTFLAGS_SETUP для CONTROL EP)
 *********************************************************************************/
void lusb_core_cb_packet_rx(uint8_t ep_addr, int flags) {
    lusb_activity_indication();

    if ((ep_addr & USB_EPNUM_MSK) == 0) {
        f_ep0_cb_rx(flags);
#ifdef LUSB_USE_SOFT_DD
    } else {
        lusb_soft_dd_packet_rx_cb(ep_addr, flags);
#endif
    }
}

/*********************************************************************************
 *   callback функция, вызываемая портом при завершениии передачи пакета
 *   какой-либо конченой точкой
 *   ep_addr - адрес конечной точки, для которой пришел пакет
 *   flags  - флаги (резерв)
 *********************************************************************************/
void lusb_core_cb_packet_tx(uint8_t ep_addr, int flags) {
    lusb_activity_indication();
    if ((ep_addr & USB_EPNUM_MSK) == 0) {
        f_ep0_proc_tx_packet();
#ifdef LUSB_USE_SOFT_DD
    } else {
        lusb_soft_dd_packet_tx_cb(ep_addr, flags);
#endif
    }
}





static void f_ep0_proc_rx_packet(int flags) {
    unsigned size;
    /* приняли SETUP флаг => SETUP STAGE управляющей транзакции */
    if (flags & LUSB_PKTFLAGS_SETUP) {
        f_ctrl_buf_pos = 0;
        f_deffered_rx_pack_cnt = 0;
        /* читаем управляющий пакет для последующего разбора */
        size = lusb_ll_ep0_rd_packet((uint8_t*)&f_ctrl_req, flags, USB_SETUP_PACKET_SIZE);
        /* управляющий пакет всегда читаем за один раз */
        if (size == USB_SETUP_PACKET_SIZE) {
            lusb_printf(LUSB_LOG_LVL_DETAIL, "lusb: ep0 setup: 0x%02X, %02X, %04X, %04X, %04X!\n",
                        f_ctrl_req.req_type, f_ctrl_req.request, f_ctrl_req.val, f_ctrl_req.index, f_ctrl_req.length);
            /* если длина = 0 => запрос не требует фазы данных */
            if (f_ctrl_req.length == 0) {
                f_ctrl_stage = LUSB_EP0_STAGE_SETUP_RD_PROC;
                f_ctrl_back_work = true;
                f_ctrl_trans_rem_len = 0;
            } else if ((f_ctrl_req.req_type & USB_REQ_REQTYPE_DIR) ==
                    USB_REQ_REQTYPE_DIR_IN) {
                /* запрос с данными Device -> Host */
                lusb_ll_ep0_set_state(LUSB_LL_EP0_STATE_DATA_TX, f_ctrl_req.length);
                f_ctrl_stage = LUSB_EP0_STAGE_SETUP_WR_PROC;
                f_ctrl_back_work = true;
            } else {
#ifndef LUSB_EP0_RX_PARTIAL
                if (f_ctrl_req.length <= LUSB_EP0_BUF_SIZE) {
#endif
                    f_ctrl_stage = LUSB_EP0_STAGE_RD_DATA;
                    f_ctrl_trans_rem_len = f_ctrl_req.length;
                    lusb_ll_ep0_set_state(LUSB_LL_EP0_STATE_DATA_RX, f_ctrl_req.length);
#ifndef LUSB_EP0_RX_PARTIAL
                } else {
                    f_ctrl_ep_stall();
                }
#endif
            }
        } else {
            lusb_printf(LUSB_LOG_LVL_WARN, "lusb: ep0 - invalid setup packet size!\n");
            f_ctrl_ep_stall();
        }
    } else {
        /* приняли пакет данных */
        if (f_ctrl_stage == LUSB_EP0_STAGE_RD_DATA) {
            if ((LUSB_EP0_BUF_SIZE - f_ctrl_buf_pos) >= LUSB_EP0_PACKET_SIZE) {
                size = lusb_ll_ep0_rd_packet(&f_ctrl_buf[f_ctrl_buf_pos],
                                             f_ctrl_trans_rem_len <= LUSB_EP0_PACKET_SIZE ? LUSB_PKTFLAGS_LAST_DATA : 0,
                                             f_ctrl_trans_rem_len);
                if (size == f_ctrl_trans_rem_len) {
                    /* завершили прием данных по запросу */
                    f_ctrl_trans_rem_len = 0;
                    f_ctrl_stage = LUSB_EP0_STAGE_SETUP_RD_PROC;
                    f_ctrl_back_work = true;
                } else if ((size < f_ctrl_trans_rem_len) && (size == LUSB_EP0_PACKET_SIZE)) {
                    /* если это не последний пакет, то должен быть размером LUSB_EP0_PACKET_SIZE */
                    f_ctrl_trans_rem_len -= size;
                    f_ctrl_buf_pos += size;
#ifdef LUSB_EP0_RX_PARTIAL
                    f_ctrl_back_work = true;
#endif
                } else {
                    lusb_printf(LUSB_LOG_LVL_WARN, "lusb: ep0 - rx packet invalid size!\n");
                    f_ctrl_ep_stall();
                }
            } else {
                f_deffered_rx_pack_cnt++;
            }
        } else if (f_ctrl_stage == LUSB_EP0_STAGE_STATUS_RD) {
            /* завершили прием статуса */
            size = lusb_ll_ep0_rd_packet(f_ctrl_buf, LUSB_PKTFLAGS_STATUS, 0);
            f_ctrl_stage = LUSB_EP0_STAGE_IDLE;
        } else {
            size = lusb_ll_ep0_rd_packet(f_ctrl_buf, 0, 0);
            lusb_printf(LUSB_LOG_LVL_WARN, "lusb: ep0 - rx unexpected packet! (stage = %d, size = %d)\n", f_ctrl_stage, size);
            f_ctrl_ep_stall();
        }
    }
}



/*****************************************************************************
 *   callback на принятие нулевой конечной точки пакета данных
 *   флаг указывает на принятие управляющего пакета
 *   (мб другие флаги, пока не используются...)
 ******************************************************************/
static void f_ep0_cb_rx(int flags) {
    if (f_ctrl_buf_lock) {
        if (flags & LUSB_PKTFLAGS_SETUP) {
            lusb_printf(LUSB_LOG_LVL_WARN, "lusb: ep0 - setup packet overrun!\n");
            f_deffered_rx_pack_cnt = 1;
            f_flags = flags;
        } else {
            f_deffered_rx_pack_cnt++;
            lusb_printf(LUSB_LOG_LVL_DETAIL, "lusb: ep0 - rx packet overrun!\n");
        }
    } else {
        f_flags = 0;
        f_ep0_proc_rx_packet(flags);
    }
}


/*************************************************************
 *   передача данных по 0-ой конечной точке
 *   вызывается в начале передачи и по завершению передачи предыдущего пакета.
 *   (!) Должна вызываться при запрещенных прерываниях или из прерывания
 ************************************************************/
static void f_ep0_proc_tx_packet(void) {
    if (f_ctrl_stage == LUSB_EP0_STAGE_DATA_WR) {
#ifdef LUSB_EP0_TX_PARTIAL
        /** @todo */
#endif
        /* определяем, последний ли это передаваемый пакет.
         * пакет последний, если он меньше полного, а также если полный, но
         * не нужен нулевой */
        bool tx_last = (f_ctrl_trans_rem_len < LUSB_EP0_PACKET_SIZE)
                || ((f_ctrl_trans_rem_len == LUSB_EP0_PACKET_SIZE) && !f_ctrl_zpacket_req);
        int tx_size = lusb_ll_ep0_wr_packet(&f_ctrl_tx_buf[f_ctrl_buf_pos],
                                            tx_last ? LUSB_PKTFLAGS_LAST_DATA : 0,
                                            f_ctrl_trans_rem_len);
        if (tx_size >= f_ctrl_trans_rem_len) {
            f_ctrl_trans_rem_len = 0;
            if (tx_last) {
                f_ctrl_stage = LUSB_EP0_STAGE_DATA_WR_DONE;
            }
        } else if ((tx_size % LUSB_EP0_PACKET_SIZE) == 0) {
            /* иначе передачи должны быть кратны размеру пакета нулевой точки */
            f_ctrl_trans_rem_len -= tx_size;
            f_ctrl_buf_pos += tx_size;
        } else {
            f_ctrl_ep_stall();
        }
    } else if (f_ctrl_stage == LUSB_EP0_STAGE_DATA_WR_DONE) {
        f_ctrl_stage = LUSB_EP0_STAGE_STATUS_RD;
        lusb_ll_ep0_set_state(LUSB_LL_EP0_STATE_STATUS_RX, 0);
    } else if (f_ctrl_stage == LUSB_EP0_STAGE_STATUS_WR) {
        /* завершена передача статусного пакета */

        /* запрос SET_ADDRESS выполняется только по завершению передачи статусного пакета */
        if ((f_ctrl_req.request == USB_REQ_SET_ADDRESS) &&
                ((f_ctrl_req.req_type & USB_REQ_REQTYPE_TYPE)
                == USB_REQ_REQTYPE_TYPE_STD)) {
            lusb_ll_set_addr(f_ctrl_req.val_l);
            if (f_ctrl_req.val_l)
                g_lusb_state.dev.state |= LUSB_DEVSTATE_ADDRESS;
            else
                g_lusb_state.dev.state &= ~(LUSB_DEVSTATE_ADDRESS | LUSB_DEVSTATE_CONFIGURED);
        }
        f_ctrl_stage = LUSB_EP0_STAGE_IDLE;
    }
}


/***************************************************************************************
 * Функция поготавливает данные для передачи в ответ на запрос по 0 контрольной точке
 * Входные параметры:
 *         req - параметры usb запроса
 * Выходные параметры:
 *         length: если >= 0 - размер данных в байтах для передачи
 *                 если = -1 - запрос не поддерживается
 * Возвращаемое значение:
 *         Указатель на буфер с данными (если length <= 0 - не имеет значения)
 ****************************************************************************************/
static const void *f_ep0_ctrlreq_tx_process(const t_lusb_req* req, int* length, uint8_t* tx_buf) {
    const uint8_t *wr_buf = NULL;
    *length = LUSB_ERR_UNSUPPORTED_REQ;

    /* обрабатываем стандартные запросы */
    if ((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_STD) {
        switch (req->request) {
            case USB_REQ_GET_DESCRIPTOR:
                wr_buf = lusb_ctrlreq_get_descr(req, length, tx_buf);
                break;
            case USB_REQ_GET_STATUS:
                wr_buf = lusb_ctrlreq_get_status(req, length, tx_buf);
                break;
            case USB_REQ_GET_CONFIGURATION:
                wr_buf = lusb_ctrlreq_get_configuration(req, length, tx_buf);
                break;
            case USB_REQ_GET_INTERFACE:
                wr_buf = lusb_ctrlreq_get_interface(req, length, tx_buf);
                break;
            case USB_REQ_SYNCH_FRAME:
                wr_buf = lusb_ctrlreq_synch_frame(req, length);                
                break;
        }
    }

#ifdef LUSB_USE_MSC_INTERFACE
    if (((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_CLASS)
            && ((req->req_type & USB_REQ_REQTYPE_RECIPIENT) == USB_REQ_REQTYPE_RECIPIENT_INTERFACE)
            && (req->index == LUSB_MSC_INTERFACE_NUM)) {
        wr_buf = lusb_msc_req_tx(req, length);
    }
#endif

#ifdef LUSB_USE_MSFT_OS_DESCR
    /* Запрос на получение данных со специальным определенным кодом пользователя
       используется для чтения специальных Mircrosoft OS дескрипторов */
    if (((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_VENDOR)
            && (req->request == LUSB_MSFT_OS_VENDOR_REQ_CODE)) {
        wr_buf = lusb_msft_os_get_feature_descr(req, length, tx_buf);
    }
#endif

#if defined LUSB_APPL_CB_CUSTOM_CTRLREQ_TX && !defined LUSB_EP0_TX_PARTIAL
    if (*length == LUSB_ERR_UNSUPPORTED_REQ) {
        wr_buf = lusb_appl_cb_custom_ctrlreq_tx(req, length, tx_buf);
    }
#endif
    return wr_buf;
}




/***************************************************************************************
 * Функция определяет реакцию на прием запроса usb по 0 контрольной точке
 * Входные параметры:
 *         req - параметры usb запроса
 *         buf - массив с принятыми данными
 * Возвращаемое значение:
 *         LUSB_ERR_SUCCESS - запрос обработан
 *         LUSB_ERR_UNSUPPORTED_REQ - запрос не поддерживается
 ****************************************************************************************/
static int f_ep0_ctrlreq_rd_process(const t_lusb_req* req, uint8_t* buf) {
    int res = LUSB_ERR_UNSUPPORTED_REQ;

    /* обслуживаем только стандартные запросы */
    if ((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_STD) {
        switch (req->request) {
            case USB_REQ_SET_ADDRESS:
                /* в отличии от дрегих запросов SET_ADDRESS
                   обслуживается в status стадии -
                   после завершения отправки завершающего пакета... */
                res = LUSB_ERR_SUCCESS;
                break;
            case USB_REQ_SET_CONFIGURATION:
                res = lusb_ctrlreq_set_config(req->val_l);
                break;
            case USB_REQ_CLEAR_FEATURES:
            case USB_REQ_SET_FEATURES:
                res = lusb_ctrlreq_features(req);
                break;
            case USB_REQ_SET_DESCRIPTOR:
                res = lusb_ctrlreq_set_descr(req, buf);
                break;
            case USB_REQ_SET_INTERFACE:
                res = lusb_ctrlreq_set_interface(req);
                break;
        }
    }

#ifdef LUSB_USE_MSC_INTERFACE
    if (((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_CLASS)
            && ((req->req_type & USB_REQ_REQTYPE_RECIPIENT) == USB_REQ_REQTYPE_RECIPIENT_INTERFACE)
            && (req->index == LUSB_MSC_INTERFACE_NUM)) {
        res = lusb_msc_req_rx(req, buf);
    }
#endif

#if defined LUSB_APPL_CB_CUSTOM_CTRLREQ_RX && !defined LUSB_EP0_RX_PARTIAL
    if (res == LUSB_ERR_UNSUPPORTED_REQ) {
        res = lusb_appl_cb_custom_ctrlreq_rx(req, buf);
    }
#endif
    return res;
}


