/*
 * lusb_descriptors.h
 *
 *  Created on: 06.07.2010
 *      Author: borisov
 */

#ifndef __LUSB_DESCRIPTORS_H__
#define __LUSB_DESCRIPTORS_H__

#include "lusb_usbdefs.h"
#include "lusb_config_params.h"
#include "lcspec.h"



#ifndef LUSB_DEV_DESCR_PTR
    extern LUSB_DESCR_SPEC_H t_usb_device_descriptor g_lusb_dev_descr;
    #define LUSB_DEV_DESCR_PTR &g_lusb_dev_descr
#endif

#ifdef LUSB_SUPPORT_HIGH_SPEED
    #ifndef LUSB_QUAL_DESCR_PTR
        extern LUSB_DESCR_SPEC_H t_usb_device_qualifier_descriptor g_lusb_qual_descr;
        #define LUSB_QUAL_DESCR_PTR &g_lusb_qual_descr
    #endif

    #ifndef LUSB_CONF_HS_DESCR_PTR
        extern LUSB_DESCR_SPEC_H t_usb_configuration_descriptor *g_lusb_cfgs_hs_ptr;
        #define LUSB_CONF_HS_DESCR_PTR g_lusb_cfgs_hs_ptr
    #endif

    #ifndef LUSB_CONF_HS_OTHER_DESCR_PTR
        extern LUSB_DESCR_SPEC_H t_usb_configuration_descriptor *g_lusb_cfgs_hs_other_ptr;
        #define LUSB_CONF_HS_OTHER_DESCR_PTR g_lusb_cfgs_hs_other_ptr
    #endif

    #ifndef LUSB_CONF_FS_DESCR_PTR
        extern LUSB_DESCR_SPEC_H t_usb_configuration_descriptor *g_lusb_cfgs_fs_ptr;
        #define LUSB_CONF_FS_DESCR_PTR g_lusb_cfgs_fs_ptr
    #endif

    #ifndef LUSB_CONF_FS_OTHER_DESCR_PTR
        extern LUSB_DESCR_SPEC_H t_usb_configuration_descriptor *g_lusb_cfgs_fs_other_ptr;
        #define LUSB_CONF_FS_OTHER_DESCR_PTR g_lusb_cfgs_fs_other_ptr
    #endif
#else
    #ifndef LUSB_CONF_DESCR_PTR
        extern LUSB_DESCR_SPEC_H t_usb_configuration_descriptor *g_lusb_cfgs_ptr;
        #define LUSB_CONF_DESCR_PTR g_lusb_cfgs_ptr
    #endif
#endif

//extern
extern LUSB_DESCR_SPEC_H uint8_t* g_lusb_string_descrs[LUSB_LANGID_CNT][LUSB_STRDESCR_CNT+1];
extern LUSB_DESCR_SPEC_H uint16_t* g_lusb_langids;


#ifdef LUSB_DEF_UTF16_STRING
#include <wchar.h>

// макросы для добавления L к строке (через 2 этапа - чтобы раскрыть define)
#define LSTR(__str) _LSTR(__str)
#define _LSTR(__str) L##__str


#define LUSB_STRING_GEN_LEN(__name, __str, __len) struct st_##__name { \
    uint8_t len; \
    uint8_t type; \
    wchar_t str[__len]; \
    } LATTRIBUTE_PACKED; \
    static LUSB_DESCR_SPEC_C struct st_##__name __name = {2*(__len)+2, USB_DESCRTYPE_STRING, LSTR(__str)};

#define LUSB_STRING_GEN(__name, __str) LUSB_STRING_GEN_LEN(__name, __str, sizeof(__str)-1)

#endif

#endif /* LUSB_DESCRIPTORS_H_ */
