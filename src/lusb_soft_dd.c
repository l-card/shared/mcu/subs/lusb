#include "lusb.h"

#ifdef LUSB_USE_SOFT_DD

#include "lusb_core_cb.h"

#include <string.h>


typedef struct {
    t_lusb_dd_status dd;
    uint32_t started_cnt;
    uint8_t *buf;
} t_soft_dd_info;


typedef struct {
    t_soft_dd_info  dd_info[LUSB_DMA_EP_DESCR_CNT];
    const t_usb_endpoint_descriptor *ep_descr;
    t_lusb_dd_cnt dd_set_pos;
    t_lusb_dd_cnt dd_start_pos;
    t_lusb_dd_cnt dd_get_pos;
} t_soft_dd_ep_info;


static t_soft_dd_ep_info f_rx_eps[LUSB_EP_NONCTL_CNT];
static t_soft_dd_ep_info f_tx_eps[LUSB_EP_NONCTL_CNT];


#define SOFT_DD_INC(pos) do { \
                            if (++(pos) == LUSB_DMA_EP_DESCR_CNT) \
                                pos = 0; \
                        } while(0)

#define SOFT_DD_EP_INFO(ep_addr) ((((ep_addr) & USB_EPDIR) == USB_EPDIR_OUT) ? \
                                   &f_rx_eps[(ep_addr & USB_EPNUM_MSK)-1] : \
                                   &f_tx_eps[(ep_addr & USB_EPNUM_MSK)-1])



static void f_start_rx(t_soft_dd_ep_info *info) {
    int out = 0;

    while (!out) {
        t_soft_dd_info *dd_info = &info->dd_info[info->dd_start_pos];
        if (dd_info->dd.status == LUSB_DDSTATUS_NOT_SERVICED)
            dd_info->dd.status = LUSB_DDSTATUS_IN_PROGRESS;
        if (dd_info->dd.status == LUSB_DDSTATUS_IN_PROGRESS) {
            unsigned rx_size = dd_info->dd.length - dd_info->started_cnt;


            while (!out && (rx_size!=0)) {
                int res = lusb_ll_ep_rd_packet_start(info->ep_descr, rx_size);
                if (res > 0) {
                    rx_size-=res;
                    dd_info->started_cnt += res;
                } else {
                    out = 1;
                }
            }

            if (rx_size == 0) {
                SOFT_DD_INC(info->dd_start_pos);
            }
        } else {
            out = 1;
        }
    }
}

static void f_start_tx(t_soft_dd_ep_info *info) {
    int out = 0;
    while (!out) {
        t_soft_dd_info *dd_info = &info->dd_info[info->dd_get_pos];
        if (dd_info->dd.status == LUSB_DDSTATUS_NOT_SERVICED)
            dd_info->dd.status = LUSB_DDSTATUS_IN_PROGRESS;
        if (dd_info->dd.status == LUSB_DDSTATUS_IN_PROGRESS) {
            unsigned tx_size = dd_info->dd.length - dd_info->dd.trans_cnt;
            unsigned proc_done = 0;

            while (!out) {
                int res = lusb_ll_ep_wr_packet(info->ep_descr, dd_info->buf == NULL ?
                                               NULL : &dd_info->buf[dd_info->dd.trans_cnt],
                                               tx_size);
                if (res >= 0) {

                    tx_size-=res;
                    dd_info->dd.trans_cnt += res;
                    if (tx_size == 0) {
                        if (!((res == info->ep_descr->wMaxPacketSize)
                              && (dd_info->dd.flags & LUSB_DD_FLAG_ADD_ZEROLEN_PACKET))) {
                            proc_done = 1;
                            out = 1;
                        }
                    }
                } else {
                    out = 1;
                }
            }

            if (proc_done && (tx_size == 0)) {
                dd_info->dd.status = LUSB_DDSTATUS_CPL_SUCCESS;
                SOFT_DD_INC(info->dd_get_pos);
                lusb_core_cb_dd_done(info->ep_descr->bEndpointAddress, LUSB_DD_EVENT_EOT, &dd_info->dd);
            }
        } else {
            out = 1;
        }
    }
}

int lusb_soft_dd_add(const t_usb_endpoint_descriptor *ep_descr, void *buf,
                     uint32_t length, uint32_t flags) {
    int err = 0;
    t_soft_dd_ep_info *info = SOFT_DD_EP_INFO(ep_descr->bEndpointAddress);

    info->ep_descr = ep_descr;

    t_soft_dd_info *dd_info = &info->dd_info[info->dd_set_pos];
    memset(dd_info, 0, sizeof(t_soft_dd_info));
    dd_info->dd.status = LUSB_DDSTATUS_NOT_SERVICED;
    dd_info->dd.last_addr = (uint32_t)buf;
    dd_info->dd.length = length;
    dd_info->dd.flags = flags;
    dd_info->buf = (uint8_t*)buf;


    SOFT_DD_INC(info->dd_set_pos);

    if ((ep_descr->bEndpointAddress & USB_EPDIR) == USB_EPDIR_OUT) {
        f_start_rx(info);
    } else {
        f_start_tx(info);
    }


    return err;
}

void lusb_soft_dd_ep_clr(const t_usb_endpoint_descriptor *ep_descr) {
    unsigned dd_idx;
    t_soft_dd_ep_info *info = SOFT_DD_EP_INFO(ep_descr->bEndpointAddress);
    info->dd_get_pos = info->dd_set_pos = info->dd_start_pos = 0;
    for (dd_idx = 0; dd_idx < LUSB_DMA_EP_DESCR_CNT; dd_idx++) {
        info->dd_info[dd_idx].dd.status = LUSB_DDSTATUS_NOT_VALID;
    }
    lusb_ll_ep_clr(ep_descr);
}





void lusb_soft_dd_packet_rx_cb(uint8_t ep_addr, int flags) {
    t_soft_dd_ep_info *info = SOFT_DD_EP_INFO(ep_addr);
    int done = 0;

    while (!done) {
        t_soft_dd_info *dd_info = &info->dd_info[info->dd_get_pos];
        if (dd_info->dd.status == LUSB_DDSTATUS_IN_PROGRESS) {
            unsigned rx_size = dd_info->dd.length - dd_info->dd.trans_cnt;

            int res = lusb_ll_ep_rd_packet(info->ep_descr, &dd_info->buf[dd_info->dd.trans_cnt],
                                            rx_size);

            if (res >= 0) {
                unsigned cpy_size = res;

                if (cpy_size > rx_size) {
                    cpy_size = rx_size;
                    dd_info->dd.status = LUSB_DDSTATUS_CPL_OVERRUN;
                } else if (cpy_size == rx_size) {
                    dd_info->dd.status = LUSB_DDSTATUS_CPL_SUCCESS;
                } else {
                    if ((res==0) || (res % info->ep_descr->wMaxPacketSize)) {
                        dd_info->dd.status = LUSB_DDSTATUS_CPL_UNDERRUN;
                    }
                }

                rx_size-=cpy_size;
                dd_info->dd.trans_cnt += cpy_size;
                dd_info->dd.last_addr += cpy_size;

                if (dd_info->dd.status != LUSB_DDSTATUS_IN_PROGRESS) {
                    lusb_core_cb_dd_done(ep_addr, LUSB_DD_EVENT_EOT, &dd_info->dd);
                    SOFT_DD_INC(info->dd_get_pos);
                }
            } else {
                done = 1;
            }
        } else {
            done = 1;
        }
    }

    f_start_rx(info);
}

void lusb_soft_dd_packet_tx_cb(uint8_t ep_addr, int flags) {
    f_start_tx(SOFT_DD_EP_INFO(ep_addr));
}

int lusb_soft_dd_ep_dd_status(const t_usb_endpoint_descriptor *ep_descr, t_lusb_dd_status *dd_st) {
    t_soft_dd_ep_info *info = SOFT_DD_EP_INFO(ep_descr->bEndpointAddress);
    *dd_st = info->dd_info[info->dd_get_pos].dd;
    return 0;
}

#endif
