/*
 * lusb_ctrlreq.h
 *
 *  Created on: 13.07.2010
 *      Author: borisov
 */

#ifndef __LUSB_CTRLREQ_H__
#define __LUSB_CTRLREQ_H__

#include "lusb_types.h"

int lusb_ctrlreq_set_config(int cfg_num);
int lusb_ctrlreq_features(const t_lusb_req* req);
int lusb_ctrlreq_set_interface(const t_lusb_req* req);
int lusb_ctrlreq_set_descr(const t_lusb_req* req, uint8_t* buf);
const uint8_t* lusb_ctrlreq_get_descr(const t_lusb_req* req, int *length, uint8_t *tx_buf);
const uint8_t* lusb_ctrlreq_get_status(const t_lusb_req* req, int *length, uint8_t *tx_buf);
const uint8_t* lusb_ctrlreq_get_interface(const t_lusb_req* req, int *length, uint8_t *tx_buf);
const uint8_t* lusb_ctrlreq_get_configuration(const t_lusb_req* req, int *length, uint8_t *tx_buf);
const uint8_t* lusb_ctrlreq_synch_frame(const t_lusb_req* req, int *length);



#endif /* LUSB_CTRLREQ_H_ */
