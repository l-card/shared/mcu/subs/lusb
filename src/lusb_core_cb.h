#ifndef LUSB_CORE_CB_H
#define LUSB_CORE_CB_H

#include "lusb_types.h"

//callback-функции из lusb_core
void lusb_core_cb_packet_rx(uint8_t ep_addr, int flags);
void lusb_core_cb_packet_tx(uint8_t ep_addr, int flags);
void lusb_core_cb_reset(void);
void lusb_core_cb_con_changed(uint8_t con);
void lusb_core_cb_susp_changed(uint8_t susp);
void lusb_core_cb_dd_done(uint8_t ep_addr, t_lusb_dd_event event, const t_lusb_dd_status *pDD);
void lusb_core_cb_activity(void);

#endif // LUSB_CORE_CB_H
