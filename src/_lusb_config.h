/*
 * lusb_config.h
 *
 *  Файл содержит пример настроек стека lusb.
 *  Каждый проект, который использует стек lusb должен включать
 *  свою копию данного файла с нужными настройками
 *
 *  Created on: 05.07.2010
 *      Author: borisov
 */

#ifndef __LUSB_CONFIG_INIT_H__
#define __LUSB_CONFIG_INIT_H__

#include "lusb_const.h"
#include "lusb_usbdefs.h"


/* Количество десерипторов на каждую конечную точку */
#define LUSB_DMA_EP_DESCR_CNT 16
/* Количество конечных точек без 0-ой. Точки в 2 стороны с одним адресом считаются за 1.
 * Определяет по сути максимальный адрес кт */
#define LUSB_EP_NONCTL_CNT    1

/* размер буфера на прием для 0 кт
 * должен быть не меньше максимального размера данных
 * передаваемых от PC к устройству за управляющий запрос
  */
#define LUSB_EP0_BUF_SIZE  512

/* Если определен данный макрос, то пользовательские управляющие
 * запросы host->device обрабатываются по пакетам, а не целиком
 */
//#define LUSB_EP0_RX_PARTIAL
/* Если определен данный макрос, то пользовательские управляющие
 * запросы device->host обрабатываются по пакетам, а не целиком
 */
//#define LUSB_EP0_TX_PARTIAL


/* флаг означает, что для обработки событий USB-модуля
 * (проверка флагов, вызов callback, считывание/запись данных)
 * используются прерывания, если не определен - эти действия
 * выполняются опросом в lusb_progress
 */
#define LUSB_INTERRUPT


/* спецификатор, использующийся при объявлении дескрипторов в H файле
 * и их определении в C файле соответственно (по-умолчанию - const) */
//#define LUSB_DESCR_SPEC_H const
//#define LUSB_DESCR_SPEC_C const


//************************ настройки USB-устройства, определяемые дескрипторами USB *****************/
//настройки устройства (из Device Descriptor)
#define LUSB_DEVICE_CLASS           0
#define LUSB_DEVICE_SUBCLASS        0
#define LUSB_DEVICE_PROTOCOL        0
#define LUSB_DEVICE_VENDOR_ID       0x0473
#define LUSB_DEVICE_PRODUCT_ID      0x0128
#define LUSB_DEVICE_RELEASE         0x0100

//размер пакета 0 конечной точки (8,16,32,64)
#define LUSB_EP0_PACKET_SIZE  64

//настройки конфигурации
#define LUSB_CONFIG_CNT          1  //кол-во конфигураций
#define LUSB_CONFIG_ATTR         (USB_CONFIG_ATTR_SELFPOW) //атрибуты конфигурации SELFPOW и WU
#define LUSB_DEV_POW             100  //подребляемый ток в мА


//********************** пользовательские callback'и **********************************************/
#define LUSB_APPL_CB_CUSTOM_CTRLREQ_RX    //обработка нестандартного
                                          //управляющего запроса PC->Device
#define LUSB_APPL_CB_CUSTOM_CTRLREQ_TX   //подготовка данных для нестандартного
                                        //управляющего запроса Device->PC
//#define LUSB_APPL_CB_BUS_RESET         //возникновение bus reset
//#define LUSB_APPL_CB_CONNECT_CHANGED   //изменение состояния подключение
//#define LUSB_APPL_CB_SUSPEND_CHANGED   //вход/выход из suspend
//#define LUSB_APPL_CB_ENTER_TEST_MODE   //перевод модуля в тестовый режим запросом SET_FEATURE
//#define LUSB_APPL_CB_DD_EVENT         //событие передачи данных
#define LUSB_APPL_CB_DEVSTATE_CHANGED  //изменение состояния устройства
                                         //(подключено/сконфигурировано)
//#define LUSB_APPL_CB_INTF_ALTSET_CHANGED /* изменение набора настроек для интерфейса */
#define LUSB_APPL_CB_ACTIVITY_IND      /* callback для отображения активности на шине.
                                          Требет таймера (или можно реализовать свои функции
                                          из lusb_activity_indication.c) */




/* Настройки поддержки Microsoft OS descriptors - которые можно использовать
   для передачи Compatibility ID для автоматической установки драйвера на группу
   устройств, либо для передачи расширенных свойств */

/* определение указывает, что будет включена поддержка Microsoft OS Descriptors */
#define LUSB_USE_MSFT_OS_DESCR
#ifdef LUSB_USE_MSFT_OS_DESCR
    /* Определяет пользовательский код запроса, который будет использоваться для
       получения этих декскрипторов (не занятый устройстовм код от 1 до 255). */
    #define LUSB_MSFT_OS_VENDOR_REQ_CODE 0xFF

    /* для стандартного compat-id декскриптора данное определение указывает,
       что нужно передать Compat-ID с указанием, что устройство совместимо
       с драйвером WinUSB */
    #define LUSB_MSFT_OS_USE_COMPATID_WINUSB

    /* Данное определение позволяет указать явно строку c Compat-ID, если не
       используется LUSB_MSFT_OS_USE_COMPATID_WINUSB */
    //#define LUSB_MSFT_OS_COMPATID

    /* Если определено, то пользователь должен сам объявить свой Comapat-ID
       декскриптор. Для этого нужно создать переменную lusb_compid_descr типа.
       const t_lusb_msft_comp_id_descr, проиницилизированную нужными значениями.
       Если не определено, то используется стандартный Compat-ID дескриптор,
       обявляющий совместимость с ID, указанным либо LUSB_MSFT_OS_USE_COMPATID_WINUSB,
       либо через LUSB_MSFT_OS_COMPATID */
    //#define LUSB_MSFT_OS_USE_CUSTOM_COMPATID_DESCR

    /* Количество расширенных свойств устройства. Если определено и отлично от нуля,
       то пользователь должен сам создать декспритор расширенных свойств - переменную
       lusb_exprop_descr типа const t_lusb_msft_os_exprop_descr, проиницилизированную
       нужными значениями */
    //#define LUSB_MSFT_EXPROP_BLOB_SIZE 0

#endif



/**********************************************************************
 *  настройки для стандартного интерфейса, который может
 *  использовать точки типа Bulk на прием и на передачу
 ***********************************************************************/
/* признак, что используется пользовательский интерфейс через bulk */
/* признак, что используется пользовательский интерфейс через bulk */
#define LUSB_USE_STDBULK_INTERFACE

#ifdef LUSB_USE_STDBULK_INTERFACE
    /* Переопределяет класс интерфейса в дескрипторе (по-умолчанию - vendor-specific) */
    #define LUSB_STDBULK_INTERFACE_CLASS     0xFF
    /* Переопределяет субкласс интерфейса в дескрипторе (по-умолчанию - 0) */
    #define LUSB_STDBULK_INTERFACE_SUBCLASS  0x10
    /* Переопределяет протокол интерфейса в дескрипторе (по-умолчанию - vendor-specific) */
    #define LUSB_STDBULK_INTERFACE_PROTOCOL  0x01


    /* количество конечных точек на прием и передачу */
    #define LUSB_STDBULK_TX_EP_CNT 1
    #define LUSB_STDBULK_RX_EP_CNT 1

    /*  адреса для конечный точек на прием и передачу */
    #define LUSB_STDBULK_TX_EP_ADDR(i)      2
    #define LUSB_STDBULK_RX_EP_ADDR(i)      2

    /* размер пакета кончной точки для обмена (bulk)
     * только для задания в LUSB_STDBULK_TX_EP_SIZE/LUSB_STDBULK_RX_EP_SIZE*/
    #define LUSB_STDBULK_EPIO_SIZE        64

    /* размеры конечных точек на прием и передачу */
    #if (LUSB_STDBULK_TX_EP_CNT > 0)
        #define LUSB_STDBULK_TX_EP_SIZE(i)     LUSB_STDBULK_EPIO_SIZE
    #endif

    #if (LUSB_STDBULK_RX_EP_CNT > 0)
        #define LUSB_STDBULK_RX_EP_SIZE(i)    LUSB_STDBULK_EPIO_SIZE
    #endif
#endif


//*************************************** настройки для mass storage ***************************
//признак, что нужно включить поддержку mass storage
//#define LUSB_USE_MSC_INTERFACE

#ifdef LUSB_USE_MSC_INTERFACE
    #include "classes/msc/lusb_msc_defs.h"

    #define LUSB_MSC_PROTOCOL LUSB_MSC_PROTOCOL_BBB
    #define LUSB_MSC_SUBCLASS LUSB_MSC_SUBCLASS_SCSI

    #define LUSB_MSC_PENDING_REQ_TIMEOUT  20000

    #define LUSB_EP_MSC_SIZE 64
    #define LUSB_EP_MSC_IN_ADDR  USB_EP_IN(5)
    #define LUSB_EP_MSC_OUT_ADDR USB_EP_OUT(5)

    #define LUSB_MSC_BUF_SIZE  1024

    #define LUSB_MSC_LUN_CNT 1

#endif

/*********************** настройки для IAPv2 EA Native Transport ***************/
//#define LUSB_USE_IAP_NATIVE_INTERFACE

#ifdef LUSB_USE_IAP_NATIVE_INTERFACE
    #define LUSB_EP_IAP_NATIVE_SIZE      64
    #define LUSB_EP_IAP_NATIVE_IN_ADDR   8
    #define LUSB_EP_IAP_NATIVE_OUT_ADDR  8
#endif


/* Если определено, указывает кол-во пользовательских интерфейсов
 * (которые не задавются настройками выше) */
//#define LUSB_USER_INTERFACES_CNT 0

/* Если определено, указывает кол-во конечных точек в пользовательских интерфейсах,
 * (которые не задаются настройками выше) */
//#define LUSB_USER_EP_CNT 0


/************************************** настройки DMA ********************************************/
 /*если определено - в стек включен код для использования DMA */
#define LUSB_USE_DMA

//*************************************** настройки для софтовой передачи ***************************
//#define LUSB_USE_SIO
#ifdef LUSB_USE_SIO
    //какие ep используют double buffering
    #define LUSB_EP_DB_MSK   (LUSB_EPFLAG(LUSB_EP1_ADDR(0)) | LUSB_EPFLAG(LUSB_EP2_ADDR(0)))
    #define LUSB_SIO_DD_CNT   16
#endif



//************************************  строки из строковых дескрипторов ****************************
/* если этот макрос определен, строки из дескрипторов
 * задаются просто константами LUSB_STR_XXX
 * (но такая инициализация может поддерживается не
 *  всеми компилятором - если это так, то данных макрос
 *  не нужно определять, а дескрипторы строк нужно определять
 * вручную в lusb_descriptors.c */
#define LUSB_DEF_UTF16_STRING

/* если данный макрос определен, то используются так же
 * строки для русского LANG ID */
#define LUSB_USE_RUS_STRINGS

/* если данный макрос определен, то строковый дескриптор
 * с серийным номером получается с помощью пользовательской
 * функции lusb_app_cb_get_serial()
 */
//#define LUSB_USE_MAN_GEN_SERIAL

/********************************************************
 * Определение строк для строковых дескрипторов
 ********************************************************/
#ifdef LUSB_DEF_UTF16_STRING
    /* при LUSB_DEF_UTF16_STRING определяем просто строки */
    #define LUSB_STR_MANUFACTURER     "L-Card"
    #define LUSB_STR_PRODUCT          "E-124 (английский)"
    #define LUSB_STR_CONFIG           "Main Configuration"
    #define LUSB_STR_BULK_INTERFACE   "E-124 Interface"
    #ifdef LUSB_USE_MSC_INTERFACE
        #define LUSB_STR_MSC_INTERFACE    "Mass storage device"
    #endif
    #ifdef LUSB_USE_IAP_NATIVE_INTERFACE
        #define LUSB_STR_IAP_EA_NATIVE_INTERFACE "ru.lcard.nativeTransport"
    #endif

/*
    #ifndef LUSB_USE_MAN_GEN_SERIAL
        #define LUSB_STR_SERIAL           "00R123456"
    #else
        #define LUSB_MAX_SERIAL_SIZE       32
    #endif
*/
    #ifdef LUSB_USE_RUS_STRINGS
        #define LUSB_STR_MANUFACTURER_RUS      "Л Кард"
        #define LUSB_STR_PRODUCT_RUS           "E-124 (русский)"
        #define LUSB_STR_CONFIG_RUS            "Основная конфигурация"
        #define LUSB_STR_BULK_INTERFACE_RUS    "Интерфейс E-124"
        #ifdef LUSB_USE_MSC_INTERFACE
            #define LUSB_STR_MSC_INTERFACE_RUS     "Интерфейс с SD-картой"
        #endif
    #endif
#else
  /* если такой возможности нет, то определяем строки посимвольно */
  #define LUSB_STR_MANUFACTURER_LEN   6
  #define LUSB_STR_MANUFACTURER       LUSB_CHAR_RUS('L'),\
                                      LUSB_CHAR_RUS('-'),\
                                      LUSB_CHAR_RUS('C'),\
                                      LUSB_CHAR_RUS('A'),\
                                      LUSB_CHAR_RUS('R'),\
                                      LUSB_CHAR_RUS('D')
                                     
  #define LUSB_STR_PRODUCT_LEN        5      
  #define LUSB_STR_PRODUCT            LUSB_CHAR_RUS('E'),\
                                      LUSB_CHAR_RUS('-'),\
                                      LUSB_CHAR_RUS('1'),\
                                      LUSB_CHAR_RUS('2'),\
                                      LUSB_CHAR_RUS('4')

                                        
  #define LUSB_STR_CONFIG_LEN        7      
  #define LUSB_STR_CONFIG             LUSB_CHAR_RUS('C'),\
                                      LUSB_CHAR_RUS('O'),\
                                      LUSB_CHAR_RUS('N'),\
                                      LUSB_CHAR_RUS('F'),\
                                      LUSB_CHAR_RUS('I'),\
                                      LUSB_CHAR_RUS('G'),\
                                      LUSB_CHAR_RUS('1')
                                        
  #define LUSB_STR_BULK_INTERFACE_LEN 10      
  #define LUSB_STR_BULK_INTERFACE     LUSB_CHAR_RUS('I'),\
                                      LUSB_CHAR_RUS('N'),\
                                      LUSB_CHAR_RUS('T'),\
                                      LUSB_CHAR_RUS('E'),\
                                      LUSB_CHAR_RUS('R'),\
                                      LUSB_CHAR_RUS('F'),\
                                      LUSB_CHAR_RUS('A'),\
                                      LUSB_CHAR_RUS('C'),\
                                      LUSB_CHAR_RUS('E'),\
                                      LUSB_CHAR_RUS('1') 
                                        
                                        
  
  
  
  #define LUSB_STR_MANUFACTURER_RUS_LEN   6
  #define LUSB_STR_MANUFACTURER_RUS   LUSB_CHAR_RUS('Л'),\
                                      LUSB_CHAR_RUS('-'),\
                                      LUSB_CHAR_RUS('К'),\
                                      LUSB_CHAR_RUS('а'),\
                                      LUSB_CHAR_RUS('р'),\
                                      LUSB_CHAR_RUS('д')
                                     
  #define LUSB_STR_PRODUCT_RUS_LEN    5
  #define LUSB_STR_PRODUCT_RUS        LUSB_CHAR_RUS('E'),\
                                      LUSB_CHAR_RUS('-'),\
                                      LUSB_CHAR_RUS('1'),\
                                      LUSB_CHAR_RUS('2'),\
                                      LUSB_CHAR_RUS('4')

                                        
  #define LUSB_STR_CONFIG_RUS_LEN     7      
  #define LUSB_STR_CONFIG_RUS         LUSB_CHAR_RUS('К'),\
                                      LUSB_CHAR_RUS('о'),\
                                      LUSB_CHAR_RUS('н'),\
                                      LUSB_CHAR_RUS('ф'),\
                                      LUSB_CHAR_RUS('и'),\
                                      LUSB_CHAR_RUS('г'),\
                                      LUSB_CHAR_RUS('1')
                                        
  #define LUSB_STR_BULK_INTERFACE_RUS_LEN 10      
  #define LUSB_STR_BULK_INTERFACE_RUS LUSB_CHAR_RUS('И'),\
                                      LUSB_CHAR_RUS('н'),\
                                      LUSB_CHAR_RUS('т'),\
                                      LUSB_CHAR_RUS('е'),\
                                      LUSB_CHAR_RUS('р'),\
                                      LUSB_CHAR_RUS('ф'),\
                                      LUSB_CHAR_RUS('е'),\
                                      LUSB_CHAR_RUS('й'),\
                                      LUSB_CHAR_RUS('с'),\
                                      LUSB_CHAR_RUS('1')                                             
                                        
#endif

#ifdef LUSB_USE_MSC_INTERFACE
    #define LUSB_MSC_VID  LUSB_STR_MANUFACTURER
    #define LUSB_MSC_PID  LUSB_STR_PRODUCT
    #define LUSB_MSC_PREV "0.1"
#endif






#endif /* LUSB_CONFIG_H_ */
