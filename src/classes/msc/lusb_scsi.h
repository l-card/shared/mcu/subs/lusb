/*
 * lusb_scsi.h
 *
 *  Created on: 17.01.2011
 *      Author: Melkor
 *
 *      Файл содержит определения из спецификации SCSI
 *      (для mass storage device используются команды SCSI)
 */

#ifndef LUSB_SCSI_H_
#define LUSB_SCSI_H_

#include "lcspec.h"
#include "lusb_typedefs.h"

//************************ коды статуса завершения команды *************************
#define SCSI_STATUS_GOOD                  0x00
#define SCSI_STATUS_CHECK_CONDITION       0x02 //доп. информация в Sence data
#define SCSI_STATUS_CONDITION_MET         0x04 //только для определенных команд (типа PRE FETCH)
#define SCSI_STATUS_BUSY                  0x08
#define SCSI_STATUS_RESERVATION_CONFLICT  0x18
#define SCSI_STATUS_TASK_SET_FULL         0x28
#define SCSI_STATUS_ACA_ACTIVE            0x30
#define SCSI_STATUS_TASK_ABORTED          0x40


#define SCSI_OPCODE_CMDFORMAT                   0xE0
#define SCSI_OPCODE_CMDFORMAT6                  0x00
#define SCSI_OPCODE_CMDFORMAT10                 0x20
#define SCSI_OPCODE_CMDFORMAT12                 0xA0
#define SCSI_OPCODE_CMDFORMAT16                 0x80
#define SCSI_OPCODE_CMDFORMAT32                 0x60
//****************************  коды команд  ****************************************/
#define SCSI_OPCODE_TEST_UNIT_READY             0x00  //SPC
#define SCSI_OPCODE_REQUEST_SENCSE              0x03  //SPC
#define SCSI_OPCODE_INQUIRY                     0x12  //SPC
#define SCSI_OPCODE_MODE_SENSE6                 0x1A  //SPC
#define SCSI_OPCODE_READ_FORMAT_CAPACITIES      0x23  //MMC
#define SCSI_OPCODE_READ_CAPACITY               0x25  //SBC
#define SCSI_OPCODE_READ6                       0x08  //SBC
#define SCSI_OPCODE_READ10                      0x28  //SBC
#define SCSI_OPCODE_WRITE6                      0x0A  //SBC
#define SCSI_OPCODE_WRITE10                     0x2A  //SBC
#define SCSI_OPCODE_PREVENT_ALLOW_MEDIUM_REMOVAL 0x1E //SBC
#define SCSI_OPCODE_START_STOP_UNIT              0x1B //SBC











//****************************  данные возвращаемые по команде INQUIRY **************/
#define SCSI_INQUIRY_VID_SIZE            8
#define SCSI_INQUIRY_PID_SIZE           16
#define SCSI_INQUIRY_REV_LVL_SIZE        4
#define SCSI_INQUIRY_VENDOR_SPEC_SIZE   20


//типы устрйоств (qualifier подразумевается всегда равным 0, кроме случая SCSI_DEVTYPE_UNSUP)
#define SCSI_DEVTYPE_DIRECT_ACCESS         0x00 //SBC
#define SCSI_DEVTYPE_SEQ_ACCESS            0x01 //SSC
#define SCSI_DEVTYPE_PRINTER               0x02 //SSC
#define SCSI_DEVTYPE_PROCESSOR             0x03 //SPC
#define SCSI_DEVTYPE_WRITE_ONCE            0x04 //SBC
#define SCSI_DEVTYPE_CD_DVD                0x05 //MMC
#define SCSI_DEVTYPE_SCANNER               0x06
#define SCSI_DEVTYPE_OPTICAL_MEMORY        0x07 //SBC
#define SCSI_DEVTYPE_MEDIA_CHANGER         0x08 //SCM
#define SCSI_DEVTYPE_COMMUNICATIONS        0x09
#define SCSI_DEVTYPE_STORAGE_ARRAY         0x0C //SCC
#define SCSI_DEVTYPE_ENCLOSURE_DERVICES    0x0D //SES
#define SCSI_DEVTYPE_SIML_DIRECT_ACCESS    0x0E //RBC
#define SCSI_DEVTYPE_OPTICAL_CARD_READER   0x0F //OCRW
#define SCSI_DEVTYPE_BRIDGE_CONTROLLER     0x10 //BCC
#define SCSI_DEVTYPE_OBJECT_BASED          0x11 //OSD
#define SCSI_DEVTYPE_AUTOMATIVE            0x12 //ADC
#define SCSI_DEVTYPE_SECURITY_MANAGER      0x13 //clause 9
#define SCSI_DEVTYPE_WLUN                  0x1E
#define SCSI_DEVTYPE_UNSUP                 0x7F

#define SCSI_INQUIRY_VERSION_NO_CLAIM      0x0
#define SCSI_INQUIRY_VERSION_SPC1          0x3
#define SCSI_INQUIRY_VERSION_SPC2          0x4
#define SCSI_INQUIRY_VERSION_SPC3          0x5
#define SCSI_INQUIRY_VERSION_SPC4          0x6


#define SCSI_INQUIRY_FLAGS1_RMB             0x80

#define SCSI_INQUIRY_FLAGS3_NORM_ACA        0x20
#define SCSI_INQUIRY_FLAGS3_HISUP           0x10
#define SCSI_INQUIRY_FLAGS3_RESP_FORMAT_Pos (0)
#define SCSI_INQUIRY_FLAGS3_RESP_FORMAT_Msk (0x0FUL << SCSI_INQUIRY_FLAGS3_RESP_FORMAT_Pos)
#define SCSI_INQUIRY_FLAGS3_RESP_FORMAT_SPC4  2


#define SCSI_INQUIRY_FLAGS5_SCCS            0x80 //storage controller component
#define SCSI_INQUIRY_FLAGS5_ACC             0x40  //access controls component
#define SCSI_INQUIRY_FLAGS5_TPGS_Pos        4  //assymetric logical access
#define SCSI_INQUIRY_FLAGS5_TPGS_Msk        (0x3UL << SCSI_INQUIRY_FLAGS5_TPGS_Pos)
#define SCSI_INQUIRY_FLAGS5_3PC             0x08 //3rd party cmd (extend copy)
#define SCSI_INQUIRY_FLAGS5_PROTECT         0x01

#define SCSI_INQUIRY_FLAGS6_ENC_SERV        0x40 //embedded enclosure service
#define SCSI_INQUIRY_FLAGS6_VS              0x20 //
#define SCSI_INQUIRY_FLAGS6_MULTIP          0x10 //multi-port
#define SCSI_INQUIRY_FLAGS6_ADDR16          0x01

#define SCSI_INQUIRY_FLAGS7_WBUS16          0x20
#define SCSI_INQUIRY_FLAGS7_SYNC            0x10
#define SCSI_INQUIRY_FLAGS7_CMD_QUE         0x02 //command managment model
#define SCSI_INQUIRY_FLAGS7_VS              0x01



#include "lcspec_pack_start.h"

struct st_scsi_inquiry_data
{
    uint8_t devtype;
    uint8_t flags1;
    uint8_t ver;
    uint8_t flags3;
    uint8_t addit_len;
    uint8_t flags5;
    uint8_t flags6;
    uint8_t flags7;
    char vid[SCSI_INQUIRY_VID_SIZE];
    char pid[SCSI_INQUIRY_PID_SIZE];
    char rev_lvl[SCSI_INQUIRY_REV_LVL_SIZE];
    uint8_t vendor_spec[SCSI_INQUIRY_VENDOR_SPEC_SIZE];
    uint8_t flags56;
    uint8_t res1;
    uint16_t ver_descr[8];
    uint8_t res2[22];
    uint8_t vendor_spec_par[0];
} LATTRIBUTE_PACKED;
typedef struct st_scsi_inquiry_data t_scsi_inquiry_data;


/***************************** команда SCSI_READ_FORMAT_CAPACITIES ***********************************/
//заголовок для дескрипторов
struct st_scsi_capacity_list_header
{
    uint8_t res[3];
    uint8_t list_len;
} LATTRIBUTE_PACKED;
typedef struct st_scsi_capacity_list_header t_scsi_capacity_list_header;


//тип дескриптора формата
#define SCSI_CAPACITY_DESCR_TYPE_BLANK     0x1
#define SCSI_CAPACITY_DESCR_TYPE_FORMATTED 0x2
#define SCSI_CAPACITY_DESCR_NOT_PRESENT    0x3

//дескриптор формата
struct st_scsi_capacity_descr
{
    uint8_t blocks_num[4];
    uint8_t type;
    uint8_t block_len[3];
} LATTRIBUTE_PACKED;
typedef struct st_scsi_capacity_descr t_scsi_capacity_descr;

/* макрос для удобного заполнения дескриптора
 *   _pdescr - t_scsi_capacity_descr* - указатель на заполняемый дескриптор
 *   _type      -  uint8_t           - тип из SCSI_CAPACITY_DESCR_TYPE_xxx
 *   _block_cnt -  uint32_t          - кол-во блоков
 *   _block_len -  uint32_t (24 valid) - размер блока
 */

#define SCSI_CAPACITY_FILL_DESCR(_pdescr, _type, _block_cnt, _block_len) \
    (_pdescr)->blocks_num[0] = ((_block_cnt)>>24) & 0xFF; \
    (_pdescr)->blocks_num[1] = ((_block_cnt)>>16) & 0xFF; \
    (_pdescr)->blocks_num[2] = ((_block_cnt)>>8) & 0xFF; \
    (_pdescr)->blocks_num[3] = ((_block_cnt)) & 0xFF; \
    (_pdescr)->type = (_type); \
    (_pdescr)->block_len[0] = ((_block_len)>>16) & 0xFF; \
    (_pdescr)->block_len[1] = ((_block_len)>>8) & 0xFF; \
    (_pdescr)->block_len[2] = (_block_len) & 0xFF;

#define SCSI_CAPACITY_DESCR_MAX_CNT  32


struct st_scsi_capacity_format_list
{
    t_scsi_capacity_list_header hdr;
    t_scsi_capacity_descr descr_list[SCSI_CAPACITY_DESCR_MAX_CNT];
} LATTRIBUTE_PACKED;
typedef struct st_scsi_capacity_format_list t_scsi_capacity_format_list;


#define SCSI_READ_CAPACITY_FILL(_buf, _block_cnt, _block_len) \
    (_buf)[0] = ((_block_cnt)>>24) & 0xFF; \
    (_buf)[1] = ((_block_cnt)>>16) & 0xFF; \
    (_buf)[2] = ((_block_cnt)>>8) & 0xFF; \
    (_buf)[3] = ((_block_cnt)) & 0xFF; \
    (_buf)[4] = ((_block_len)>>24) & 0xFF; \
    (_buf)[5] = ((_block_len)>>16) & 0xFF; \
    (_buf)[6] = ((_block_len)>>8) & 0xFF; \
    (_buf)[7] = (_block_len) & 0xFF;

//************************* VPD  data **************************************//
struct st_scsi_vpd_page
{
    uint8_t type;
    uint8_t page_code;
    uint8_t len;
    uint8_t data[0];
} LATTRIBUTE_PACKED;
typedef struct st_scsi_vpd_page t_scsi_vpd_page;



#define SCSI_VPD_PAGECODE_UNIT_SERIAL_NUMBER        0x80
#define SCSI_VPD_PAGECODE_ATA_INFORMATION           0x89
#define SCSI_VPD_PAGECODE_DEVICE_CONSTITUENTS       0x8B
#define SCSI_VPD_PAGECODE_DEVICE_ID                 0x83
#define SCSI_VPD_PAGECODE_EXTENDED_INQUIRY_DATA     0x86
#define SCSI_VPD_PAGECODE_MANAGEMENT_NETWORK_ADDR   0x85
#define SCSI_VPD_PAGECODE_MODE_PAGE_POLICY          0x87
#define SCSI_VPD_PAGECODE_POWER_CONDITION           0x8A
#define SCSI_VPD_PAGECODE_SCSI_PORTS                0x88
#define SCSI_VPD_PAGECODE_PROTOCOL_SPEC_LUN_INFO    0x90
#define SCSI_VPD_PAGECODE_PROTOCOL_SPEC_PORT_INFO   0x91
#define SCSI_VPD_PAGECODE_SOFT_INTERFACE_ID         0x84




/*****************************************************************************************
 *                                      MODE PAGES
 *****************************************************************************************/
struct st_scsi_mode_param_hdr6
{
    uint8_t data_len;
    uint8_t medium;
    uint8_t devspec;
    uint8_t block_descr_len;
} LATTRIBUTE_PACKED;
typedef struct st_scsi_mode_param_hdr6 t_scsi_mode_param_hdr6;

struct st_scsi_mode_param_hdr10
{
    uint16_t data_len;
    uint8_t medium;
    uint8_t devspec;
    uint8_t longlba;
    uint8_t reserv;
    uint16_t block_descr_len;
} LATTRIBUTE_PACKED;
typedef struct st_scsi_mode_param_hdr10 t_scsi_mode_param_hdr10;


#define SCSI_MODEPAGE_INFO_EXCEPT_CTL          0x1C00
#define SCSI_MODEPAGE_CACHING_MODE             0x0800
#define SCSI_MODEPAGE_ALL_PAGES                0x3F00


struct st_scsi_modepage_except_ctl
{
    uint8_t page_code;
    uint8_t page_len;
    uint8_t flags2;
    uint8_t MRIE;
    uint32_t interval_tmr;
    uint32_t report_cnt;
} LATTRIBUTE_PACKED;
typedef struct st_scsi_modepage_except_ctl t_scsi_modepage_except_ctl;

struct st_scsi_modepage_caching_mode
{
    uint8_t page_code;
    uint8_t page_len;
    uint8_t flags2;
    uint8_t priority;
    uint16_t disable_prefetch_len;
    uint16_t min_prefetch;
    uint16_t max_prefetch;
    uint16_t max_prefetch_ceiling;
    uint8_t flags12;
    uint8_t cache_segs;
    uint16_t cache_segs_size;
    uint8_t reserv;
    uint8_t obsolete[3];
} LATTRIBUTE_PACKED;
typedef struct st_scsi_modepage_caching_mode t_scsi_modepage_caching_mode;

//devspec for SBC mode page header
//write-protect (если в Control Page SWP - всегда 1, иначе - если физ. уст.
#define SCSI_MODEPAR_DEVSPEC_SBC_WP         0x80
#define SCSI_MODEPAR_DEVSPEC_SBC_DPOFUA     0x10




/*****************************************************************************************
 *                                      SENSE DATA
 *****************************************************************************************/
//************************ коды ответа на запрос REQUEST SENSE  *********************/
#define SCSI_SENSE_RESP_CURRENT_FIXED        0x70
#define SCSI_SENSE_RESP_DEFERRED_FIXED       0x71
#define SCSI_SENSE_RESP_CURRENT_DESCR        0x72
#define SCDI_SENSE_RESP_DEFERRED_DESCR       0x73



//фиксированный формат
struct st_scsi_sense_data
{
    uint8_t resp_code;
    uint8_t obsolete;
    uint8_t sense_key; //+3 флага
    uint32_t info;
    uint8_t addit_sense_len;
    uint32_t cmd_spec_info;
    uint8_t asc; //additional sense code
    uint8_t ascq; //additional sense code qualifier
    uint8_t field_replaceable_unit;
    uint8_t sense_key_spec[3];
    uint8_t addit_data[0];
} LATTRIBUTE_PACKED;
typedef struct st_scsi_sense_data t_scsi_sense_data;


//************************  Формат SENSE DATA в виде дескрипторов ********************/
struct st_scsi_sense_descr
{
    uint8_t type;
    uint8_t addit_len;
    uint8_t data[0];
} LATTRIBUTE_PACKED;
typedef struct st_scsi_sense_descr t_scsi_sense_descr;

#define SCSI_RESP_ADDIT_SENSE_LEN_MAX    244

struct st_scsi_sense_descr_data
{
    uint8_t resp_code;
    uint8_t sense_key;
    uint8_t asc; //additional sense code
    uint8_t ascq; //additional sense code qualifier
    uint8_t reserv[3];
    uint8_t addit_sense_len;
    struct st_scsi_sense_descr descr[0];
} LATTRIBUTE_PACKED;
typedef struct st_scsi_sense_descr_data t_scsi_sense_descr_data;

#include "lcspec_pack_restore.h"

//типы дескрипторов
#define SCSI_SENSE_DESCR_INFORMATION                0x00
#define SCSI_SENSE_DESCR_CMD_SPEC_INFO              0x01
#define SCSI_SENSE_DESCR_SENSE_KEY_SPEC             0x02
#define SCSI_SENSE_DESCR_FIELD_REPLACEABLE_UNIT     0x03
#define SCSI_SENSE_DESCR_STREAM_CMD                 0x04
#define SCSI_SENSE_DESCR_BLOCK_CMD                  0x05
#define SCSI_SENSE_DESCR_OSD_OBJECT_ID              0x06
#define SCSI_SENSE_DESCR_OSD_RESP_INTEGRITY_CHECK   0x07
#define SCSI_SENSE_DESCR_OSD_ATTR_ID                0x08
#define SCSI_SENSE_DESCR_ATA_STATUS_RETURN          0x09
#define SCSI_SENSE_DESCR_PROGRESS_INDICATION        0x0A
#define SCSI_SENSE_DESCR_USER_DATA_SEG_REFERRAL     0x0B
#define SCSI_SENSE_DESCR_FORWARDER_ADDIT_SENSE_DATA 0x0C

//биты в key specific sense information
#define SCSI_SENSE_KEYSPEC_FLAGS_SKSV                0x80 //признак, что информация действительна
#define SCSI_SENSE_KEYSPEC_FLAGS_CD                  0x40 //cmd или данные
#define SCSI_SENSE_KEYSPEC_FLAGS_BPV                 0x08 //признак, что битовое поле действительное
#define SCSI_SENSE_KEYSPEC_FLAGS_BITPTR_Mask         0x07

//коды sense_key
#define SCSI_SENSE_KEY_NO_SENSE             0x0
#define SCSI_SENSE_KEY_RECOVERED_ERROR      0x1
#define SCSI_SENSE_KEY_NOT_READY            0x2
#define SCSI_SENSE_KEY_MEDIUM_ERROR         0x3
#define SCSI_SENSE_KEY_HARDWARE_ERROR       0x4
#define SCSI_SENSE_KEY_ILLEGAL_REQUEST      0x5
#define SCSI_SENSE_KEY_UNIT_ATTENTION       0x6
#define SCSI_SENSE_KEY_DATA_PROTECT         0x7
#define SCSI_SENSE_KEY_BLANK_CHECK          0x8
#define SCSI_SENSE_KEY_VENDOR_SPEC          0x9
#define SCSI_SENSE_KEY_COPY_ABORTED         0xA
#define SCSI_SENSE_KEY_ABORTED_COMMAND      0xB
#define SCSI_SENSE_KEY_VOLUME_OVERFLOW      0xD
#define SCSI_SENSE_KEY_MISCOMPARE           0xE
#define SCSI_SENSE_KEY_COMPLETED            0xF

#define SCSI_ASC_INVALID_COMMAND_OPERATION_CODE       0x20
#define SCSI_ASC_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE   0x21
#define SCSI_ASC_INVALID_FIELD_IN_CDB                 0x24
#define SCSI_ASC_MEDIUM_NOT_PRESENT                   0x3A
//************ определение поляей из CDB (command description block) ***************
//биты поля CONTROL
#define SCSI_CDB_CONTROL_NACA  (0x1 << 2) //если установлен, то при завершении команды
                                          //со статусом CHECK CONDITION должен быть установлен
                                          //auto contingent allegiance (5.9 SAM-5)


#endif /* LUSB_SCSI_H_ */
