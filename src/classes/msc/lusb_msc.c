 /*
 * lusb_msc.c
 *
 *  Created on: 10.01.2011
 *      Author: Melkor
 */

#include "lusb.h"

#ifdef LUSB_USE_MSC_INTERFACE


#include "string.h"
#include "timer.h"




#define MSC_MAX_DISC_CNT 1
#define LUSB_MSC_MAX_BLOCK_CNT 32*2*1024*1024
#define LUSB_MSC_BLOCK_SIZE    512








#define LUSB_MSC_EPIND_TX 0
#define LUSB_MSC_EPIND_RX 1




static uint8_t f_msc_buf1[LUSB_MSC_BUF_SIZE];
static uint8_t f_msc_buf2[LUSB_MSC_BUF_SIZE];
static uint8_t* f_msc_buf[2] = {f_msc_buf1, f_msc_buf2};




#define LUSB_MSC_EVENT_NONE 0
#define LUSB_MSC_EVENT_RX   1
#define LUSB_MSC_EVENT_TX   2

static struct
{
    enum
    {
        LUSB_MSC_STATE_RESET,
        LUSB_MSC_STATE_NEED_SEND_CSW,
        LUSB_MSC_STATE_SEND_CSW,
       // LUSB_MSC_STATE_SEND_DATA,
        LUSB_MSC_STATE_SEND_WT_PENDING,
        LUSB_MSC_STATE_SEND_WT_USBBUF,
        LUSB_MSC_STATE_SEND_LAST_DATA,
        LUSB_MSC_STATE_SEND_WT_STALL,
        LUSB_MSC_STATE_RECV_WT_USBBUF,
        LUSB_MSC_STATE_RECV_WT_PENDING,
        //LUSB_MSC_STATE_WT_PENDING_TX,
       // LUSB_MSC_STATE_WT_PENDING_RX,
        //LUSB_MSC_STATE_SEND_FILL,
        LUSB_MSC_STATE_RESEND_DATA,
        LUSB_MSC_STATE_RECV_DATA,
        LUSB_MSC_STATE_RERECV_DATA,
        LUSB_MSC_STATE_STALL,
        LUSB_MSC_STATE_ERROR
    } state;

    uint32_t lba; //текущий адрес чтения/записи на диске
    uint32_t lb_cnt; //количество блоков, которые осталось прочитать/записать
    uint32_t max_block_trans; //макс. число блоков для передачи за один раз
    uint32_t lb_size;

    volatile uint8_t event; //события - пришли ли данные или данные переданы
    volatile uint32_t trans_cnt;
    volatile uint16_t trans_stat;
    uint8_t pending; //признак того, что запись обработка команды была отложена
    t_timer pending_tmr; //таймер на выполнение отложенной команды

    uint8_t cur_buf; //номер текущего буфера
    uint32_t offset; //смещение обрабатываемых данных
                     //текущей операции относительно начала
    t_lusb_msc_cbw cbw;
    t_lusb_msc_csw csw;

    volatile uint16_t recv_size[2]; //кол-во принятых данных по каждому буферу
    volatile uint32_t recv_resitude; //всего данных - кол-во принятых данных
    volatile uint8_t recv_buf;
    uint8_t req_buf;
    uint32_t proc_size;

    uint8_t* send_buf;
    int send_length;

    t_scsi_sense_data sense;
} f_msc_st;




static int f_msc_cmd_tx(void);
static int f_msc_cmd_rx(uint8_t* buf, int size);
static void f_msc_stall_in(void);
static void f_msc_stall_out(void);
static int f_msc_send_csw(void);
static int f_msc_proc_pending_rx(void);
static int f_msc_proc_pending_tx(void);

LINLINE static void f_msc_set_sense(uint8_t key, uint8_t asc, uint8_t ascq);
LINLINE static void f_msc_set_sense_invalid_cdb(uint16_t offset);
LINLINE static void f_msc_check_cmd_reserv(int start_offs, int end_offs);
static int f_msc_cmd_inquiry(void);
static int f_msc_cmd_read_format_capacity(void);
static int f_msc_cmd_read_capacity(void);
static int f_msc_cmd_req_sense(void);
static int f_msc_cmd_test_unit_ready(void);
static int f_msc_cmd_mode_sense(void);
static int f_msc_cmd_read(void);
static int f_msc_cmd_write(uint8_t* buf, int size);
static int f_msc_cmd_prevent_rem(void);
static int  f_msc_cmd_startstop(void);

#define LUSB_MSC_EP_IN_STALLED()  (g_lusb_state.eps_st.halted & LUSB_EPFLAG(LUSB_EP_MSC_IN_ADDR))
#define LUSB_MSC_EP_OUT_STALLED()  (g_lusb_state.eps_st.halted & LUSB_EPFLAG(LUSB_EP_MSC_OUT_ADDR))





//реакция на переход в сконфигурированное состояние
void lusb_msc_set_config(void)
{
    //устанавливаем переменные в начальное состояние
    f_msc_st.state = LUSB_MSC_STATE_RESET;
    f_msc_st.event = LUSB_MSC_EVENT_NONE;

    memset(&f_msc_st.sense, 0, sizeof(f_msc_st.sense));
    f_msc_st.sense.resp_code = SCSI_SENSE_RESP_CURRENT_FIXED;
    f_msc_st.sense.addit_sense_len = sizeof(f_msc_st.sense) - 8;

    f_msc_st.cur_buf = 0;
    f_msc_st.recv_buf = 0;

    //ставим запрос на прием пакета данных (первого cbw)
    lusb_ep_intf_add_dd(LUSB_MSC_INTERFACE_NUM, LUSB_MSC_EPIND_RX, f_msc_buf[0],
            LUSB_MSC_BUF_SIZE, 0);

    fs_disc_usb_connected();
}



int lusb_msc_req_rx(t_lusb_req* req, uint8_t* buf)
{
    int res = LUSB_ERR_UNSUPPORTED_REQ;

    if (req->request == LUSB_MSC_REQ_RESET)
    {
        //сбрасываем состояние автомата MSC
        f_msc_st.state = LUSB_MSC_STATE_RESET;
        res = LUSB_ERR_SUCCESS;

    }

    return res;
}

const void* lusb_msc_req_tx(t_lusb_req* req, int* length)
{
    const void* wr_buf = (uint8_t*) 0;
    if (req->request == LUSB_MSC_REQ_GET_MAX_LUN)
    {
        lusb_ctrl_tx_buf[0] = (LUSB_MSC_LUN_CNT-1);
        wr_buf = lusb_ctrl_tx_buf;
        *length = 1;
    }
    return wr_buf;
}



/***************************************************************
 * обработка события завершения передачи блока данных
 **************************************************************/
void lusb_msc_data_tx(uint8_t event, t_lusb_dd_status* pDD)
{
    if (event == LUSB_DMA_EVENT_ERR)
        f_msc_st.state = LUSB_MSC_STATE_ERROR;
    else if (event == LUSB_DMA_EVENT_EOT)
    {
        f_msc_st.event = LUSB_MSC_EVENT_TX;
        f_msc_st.trans_cnt = pDD->trans_cnt;
        f_msc_st.trans_stat = pDD->status;
    }
}


/************************************************************
 *   обработка события завершения приема данных
 ************************************************************/
void lusb_msc_data_rx(uint8_t event, t_lusb_dd_status* pDD)
{
    if (event == LUSB_DMA_EVENT_ERR)
        f_msc_st.state = LUSB_MSC_STATE_ERROR;
    else if (event == LUSB_DMA_EVENT_EOT)
    {
        f_msc_st.event = LUSB_MSC_EVENT_RX;
        f_msc_st.trans_cnt = pDD->trans_cnt;
        f_msc_st.trans_stat = pDD->status;
        f_msc_st.recv_size[f_msc_st.recv_buf] = pDD->trans_cnt;
        f_msc_st.recv_buf ^= 1;
    }
}



/******************************************************************************
 *   ставим блок полученных данных для команды на передачу
 *   переключаем буфер (двойная буферизация), изменяем data_residue
 *   проверяем условие конца передачи (высылаем статус)
 *   и проверяем признак досрочного завершения (некратный размеру кт пакет) -
 *   (stall условия)
 ******************************************************************************/
static void f_msc_send_cmd_data(void)
{

    f_msc_st.send_length = MIN(f_msc_st.send_length, f_msc_st.csw.data_residue);
    if (lusb_ep_intf_add_dd(LUSB_MSC_INTERFACE_NUM,
                    LUSB_MSC_EPIND_TX, f_msc_st.send_buf,
                    f_msc_st.send_length, 0)==0)
    {
        //переключаем буфер на передачу
        f_msc_st.cur_buf ^= 1;
        f_msc_st.csw.data_residue -= f_msc_st.send_length;

        //если передали все данные - передаем статус
        if (!f_msc_st.csw.data_residue)
        {
            f_msc_st.state = LUSB_MSC_STATE_SEND_LAST_DATA;
            f_msc_send_csw();
        }
        else if ((f_msc_st.send_length & (LUSB_EP_MSC_SIZE-1)) || (f_msc_st.send_length==0))
        {
            //было передано меньше чем нужно и это был завершающий пакет
            //сталлим кт и ждем, пока ее разблокируют, чтобы послать статус
            f_msc_st.state = LUSB_MSC_STATE_SEND_WT_STALL;

        }
        else
        {
            f_msc_st.state = LUSB_MSC_STATE_SEND_WT_USBBUF;
        }
    }
}



/************************************************************************
 *   Добавляем запрос на прием данных, если нужен при
 *   передачи данных команды от pc к контроллеру
 ************************************************************************/
static void f_msc_add_rx_req(void)
{
    int recv_len = MIN(LUSB_MSC_BUF_SIZE, f_msc_st.recv_resitude);
    if (recv_len)
    {
        if (!lusb_ep_intf_add_dd(LUSB_MSC_INTERFACE_NUM,
                LUSB_MSC_EPIND_RX, f_msc_buf[f_msc_st.req_buf], recv_len, 0))
        {
            ///xxx todo: обработать событие, что не смогли добавить дескр. на чтение
            f_msc_st.recv_resitude-=recv_len;
            f_msc_st.recv_size[f_msc_st.req_buf] = 0;
            f_msc_st.req_buf ^= 1;
        }
    }
}



/****************************************************************************
 *  Обработка события, что принятые данные были обработаны
 *  proc_size - кол-во обработанных данных
 *  уменьшаем data_residue
 *  отслеживаем событие, что не все данные обработаны (stall)
 *  добавляем запрос на прием если еще нужно
 *  и посылаем статус, если уже все обработали
 ****************************************************************************/
static void f_msc_progr_rcv_data(uint32_t proc_size)
{
    f_msc_st.csw.data_residue -= proc_size;
    if (proc_size!=f_msc_st.recv_size[f_msc_st.cur_buf])
    {
        //если обработали меньше, чем нужно - stall out и посылаем статус
        f_msc_stall_out();
        f_msc_st.state = f_msc_send_csw() ?
                LUSB_MSC_STATE_NEED_SEND_CSW : LUSB_MSC_STATE_SEND_CSW;
    }
    else if (f_msc_st.csw.data_residue > 0)
    {
        f_msc_st.cur_buf ^= 1;
        f_msc_add_rx_req();
        f_msc_st.state = LUSB_MSC_STATE_RECV_WT_USBBUF;
    }
    else
    {
       //приняли все данные - посылаем статус
       f_msc_st.state = f_msc_send_csw() ?
           LUSB_MSC_STATE_NEED_SEND_CSW : LUSB_MSC_STATE_SEND_CSW;
    }
}


/******************************************************************************
 *   разбор принятой команды
 ******************************************************************************/
static void f_msc_process_cbw(void)
{
    //в данном состоянии должны принять CBW
    t_lusb_msc_cbw* cbw = (t_lusb_msc_cbw*) f_msc_buf[0];
    if ((f_msc_st.trans_stat == LUSB_DDSTATUS_CPL_UNDERRUN)
            && (f_msc_st.trans_cnt == sizeof(t_lusb_msc_cbw))
            && (cbw->sign == LUSB_MSC_CBW_SIGN)
            && (cbw->LUN < LUSB_MSC_LUN_CNT))
    {
        //cbw valid - обработка
        f_msc_st.cbw = *cbw;
        f_msc_st.csw.data_residue = f_msc_st.recv_resitude = cbw->data_length;
        f_msc_st.csw.sign = LUSB_MSC_CSW_SIGN;
        f_msc_st.csw.tag = cbw->tag;
        f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_PASSED;
        f_msc_st.offset = 0;
        f_msc_st.pending = 0;
        f_msc_st.cur_buf = 0;
        f_msc_st.recv_size[0] = f_msc_st.recv_size[1] = 0;
        f_msc_st.recv_buf = 0;
        f_msc_st.req_buf = 0;

        //если длина 0 - обрабатываем запрос и шлем статус
        if (cbw->data_length == 0)
        {
            f_msc_cmd_rx(NULL, 0);
        }
        else if ((cbw->flags & LUSB_MSC_CBW_FLAGS_DIR)
                == LUSB_MSC_CBW_DIR_IN)
        {
            //если нужно передать данные - вызываем функцию для подготовки буфера и передаем
            f_msc_cmd_tx();
        }
        else
        {
            //если нужно принять данные...
            f_msc_add_rx_req();
            f_msc_add_rx_req();
            f_msc_st.state = LUSB_MSC_STATE_RECV_WT_USBBUF;
        }
    }
    else
    {
        lusb_printf("!!err not valid cbw\n");
        //cbw not valid - stall обеих ep

        f_msc_stall_in();
        f_msc_stall_out();
        f_msc_st.state = LUSB_MSC_STATE_ERROR;
    }
}




/**********************************************************************
 * функция продвижения msc стека
 * здесь обрабатываются принятые данные и произошедшие события
 * (все продвижение автомата)
 **********************************************************************/
void lusb_msc_progress(void)
{
    int dd_cnt;
    //завершена передача данных - обработываем событие

   /* if (f_msc_st.event == LUSB_MSC_EVENT_TX)
    {
        f_msc_st.event = LUSB_MSC_EVENT_NONE;
    }*/
    //приняли новые данные
    if (f_msc_st.event == LUSB_MSC_EVENT_RX)
    {
        f_msc_st.event = LUSB_MSC_EVENT_NONE;
        if (f_msc_st.state == LUSB_MSC_STATE_RESET)
            f_msc_process_cbw();
    }

    dd_cnt = lusb_ep_intf_get_dd_in_progress(LUSB_MSC_INTERFACE_NUM, LUSB_MSC_EPIND_TX);
    //фоновые задачи - обрабатываем отложенные запросы на чтение-запись
    // + попытки повторной передачи данных (на случай, если закончились свободные дескрипторы...
    switch (f_msc_st.state)
    {
        case LUSB_MSC_STATE_SEND_CSW:
        case LUSB_MSC_STATE_SEND_LAST_DATA:
            if (dd_cnt == 0)
            {
                //завершилась передача csw => готовы принимать новый cbw
                f_msc_st.state = LUSB_MSC_STATE_RESET;
                //ставим запрос на прием пакета данных (первого cbw)
                lusb_ep_intf_add_dd(LUSB_MSC_INTERFACE_NUM, LUSB_MSC_EPIND_RX, f_msc_buf[0],
                           LUSB_MSC_BUF_SIZE, 0);
            }
            break;
        case LUSB_MSC_STATE_NEED_SEND_CSW:
            f_msc_st.state = f_msc_send_csw() ?
                  LUSB_MSC_STATE_NEED_SEND_CSW : LUSB_MSC_STATE_SEND_CSW;
            break;
        case LUSB_MSC_STATE_SEND_WT_PENDING:
            {
                int err = f_msc_proc_pending_tx();
                if (err || timer_expired(&f_msc_st.pending_tmr))
                {
                    //закончился таймер на отложенную оперецию или ошибка чтения
                    //- отменяем команду
                    f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
                    f_msc_set_sense(SCSI_SENSE_KEY_HARDWARE_ERROR, 0, 0);
                    if (err)
                    {
                        lusb_printf("read error !!!\n");
                    }
                    else
                    {
                        lusb_printf("err timer snd tout\n");
                    }
                    f_msc_stall_in();
                    f_msc_st.state = LUSB_MSC_STATE_STALL;

                }
                else if (!f_msc_st.pending)
                {
                    //завершилась отложенная запись данных на передачу
                    //=> пытаемся передать данные
                    f_msc_send_cmd_data();
                }
            }
            break;
        case LUSB_MSC_STATE_SEND_WT_USBBUF:
            {
               // int dd_cnt = lusb_ep_intf_get_dd_in_progress(LUSB_MSC_INTERFACE_NUM, LUSB_MSC_EPIND_TX);
                if ((dd_cnt >= 0) && (dd_cnt < 2))
                    f_msc_cmd_tx();
            }
            break;
        case LUSB_MSC_STATE_SEND_WT_STALL:
            //if (lusb_ep_intf_get_dd_in_progress(LUSB_MSC_INTERFACE_NUM, LUSB_MSC_EPIND_TX)==0)
            if (dd_cnt == 0)
            {
                ///xxx когда можно сделать stall? (если dma закончился, пакет может идет еще...)
                //f_msc_stall_in();
               // LOG_PRINT_VAL("stall out    ", 16);
                f_msc_st.state = LUSB_MSC_STATE_STALL;
            }
            break;
        case LUSB_MSC_STATE_RECV_WT_USBBUF:
            {
                int len = f_msc_st.recv_size[f_msc_st.cur_buf];
                if (len)
                {
                    f_msc_cmd_rx(f_msc_buf[f_msc_st.cur_buf], len);
                }
            }
            break;
        case LUSB_MSC_STATE_RECV_WT_PENDING:
            {
                int res = f_msc_proc_pending_rx();
                if (f_msc_st.pending == 0)
                {
                    f_msc_progr_rcv_data(res == 0 ?
                         f_msc_st.recv_size[f_msc_st.cur_buf] : 0);
                }
                else if (timer_expired(&f_msc_st.pending_tmr))
                {
                    //закончился таймер на отложенную оперецию - отменяем команду
                    f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
                    f_msc_set_sense(SCSI_SENSE_KEY_HARDWARE_ERROR, 0, 0);
                    f_msc_stall_out();
                    f_msc_st.state = LUSB_MSC_STATE_NEED_SEND_CSW;
                }
            }
            break;
        case LUSB_MSC_STATE_ERROR:
            f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
            f_msc_stall_in();
            f_msc_stall_out();
            f_msc_st.state = LUSB_MSC_STATE_STALL;
            break;

        case LUSB_MSC_STATE_STALL:
            //если было STALL условие - по снятию stall - посылаем статус
            if (!LUSB_MSC_EP_IN_STALLED())
            {
                f_msc_st.state = f_msc_send_csw() ?
                     LUSB_MSC_STATE_NEED_SEND_CSW : LUSB_MSC_STATE_SEND_CSW;
            }
            break;
        default:
            break;
    }
}





//очищаем задание на передачу и переводим конечную точку in в stall
static void f_msc_stall_in(void)
{
    if (lusb_ep_intf_get_dd_in_progress(LUSB_MSC_INTERFACE_NUM, LUSB_MSC_EPIND_TX) > 0)
        lusb_ep_intf_clear_dma(LUSB_MSC_INTERFACE_NUM, LUSB_MSC_EPIND_TX);
    lusb_ll_ep_set_stall(LUSB_EP_MSC_IN_ADDR);
    g_lusb_state.eps_st.halted |= LUSB_EPFLAG(LUSB_EP_MSC_IN_ADDR);
}

//очищаем задание на прием и переводим конечную точку out в stall
static void f_msc_stall_out(void)
{
    if (lusb_ep_intf_get_dd_in_progress(LUSB_MSC_INTERFACE_NUM, LUSB_MSC_EPIND_RX) > 0)
        lusb_ep_intf_clear_dma(LUSB_MSC_INTERFACE_NUM, LUSB_MSC_EPIND_RX);
    lusb_ll_ep_set_stall(LUSB_EP_MSC_OUT_ADDR);
    g_lusb_state.eps_st.halted |= LUSB_EPFLAG(LUSB_EP_MSC_OUT_ADDR);
}


//посылка статусного слова csw (использует глоб. переменную f_msc_st.csw)
static int f_msc_send_csw(void)
{
    return lusb_ep_intf_add_dd(LUSB_MSC_INTERFACE_NUM, LUSB_MSC_EPIND_TX,
            &f_msc_st.csw, sizeof(f_msc_st.csw), 0);
}


LINLINE static void f_msc_set_sense_invalid_cdb(uint16_t offset)
{
    f_msc_st.sense.sense_key = SCSI_SENSE_KEY_ILLEGAL_REQUEST;
    f_msc_st.sense.asc = SCSI_ASC_INVALID_FIELD_IN_CDB;
    f_msc_st.sense.ascq = 0;
    f_msc_st.sense.sense_key_spec[0] = SCSI_SENSE_KEYSPEC_FLAGS_SKSV | SCSI_SENSE_KEYSPEC_FLAGS_CD;
    f_msc_st.sense.sense_key_spec[1] = (offset >> 8) & 0xFF;
    f_msc_st.sense.sense_key_spec[2] = offset & 0xFF;
}

LINLINE static void f_msc_set_sense(uint8_t key, uint8_t asc, uint8_t ascq)
{
    f_msc_st.sense.sense_key = key;
    f_msc_st.sense.asc = asc;
    f_msc_st.sense.ascq = 0;
    f_msc_st.sense.sense_key_spec[0] = 0;
}



/**************************************************************************
 *  проверка, что резервные байты (с start_offs до end_offs)
 *  в команде (f_msc_st.cbw.cmd) равны 0
 **************************************************************************/
LINLINE static void f_msc_check_cmd_reserv(int start_offs, int end_offs)
{
    for (int i = start_offs; (i <= end_offs) && !f_msc_st.csw.status; i++)
    {
        if (f_msc_st.cbw.cmd[i]!=0)
        {
            f_msc_set_sense_invalid_cdb(i);
            f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
        }
    }
}








/*****************************************************************************************
 * обработка команд без данных, или принимающих данные
 *    buf - принятый буфер с данными (NULL, если данных нет)
 *    size - размер принятых данных (0, если данных нет)
 * запрос возвращает количество реально обработанных данных
 * запрос использует статически значения:
 *     f_msc_st.cbw - чтобы узнать обрабатываемую команду
 *     f_msc_st.csw - чтобы узнать сколько данных уже было обработано и изменить статус
 * ***************************************************************************************/
static int f_msc_cmd_rx(uint8_t* buf, int size)
{
    int len = 0;

    f_msc_st.offset = f_msc_st.cbw.data_length - f_msc_st.csw.data_residue;

    switch (f_msc_st.cbw.cmd[0])
    {
        case SCSI_OPCODE_TEST_UNIT_READY:
            len = f_msc_cmd_test_unit_ready();
            break;
        case SCSI_OPCODE_WRITE6:
        case SCSI_OPCODE_WRITE10:
            len = f_msc_cmd_write(buf,size);
            break;
        case SCSI_OPCODE_PREVENT_ALLOW_MEDIUM_REMOVAL:
            len = f_msc_cmd_prevent_rem();
            break;
        case SCSI_OPCODE_START_STOP_UNIT:
            len = f_msc_cmd_startstop();
            break;
        default:
            lusb_printf("Unsup cmd: %#x", f_msc_st.cbw.cmd[0]);
            for (int i=1; i < f_msc_st.cbw.cmd_length; i++)
                lusb_printf(", %#x", f_msc_st.cbw.cmd[i]);
            lusb_printf("\n");

            f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
            f_msc_set_sense(SCSI_SENSE_KEY_ILLEGAL_REQUEST, SCSI_ASC_INVALID_COMMAND_OPERATION_CODE, 0);
            break;
    }


    if (!f_msc_st.pending)
    {
        f_msc_progr_rcv_data(len);
    }
    else
    {
        f_msc_st.state = LUSB_MSC_STATE_RECV_WT_PENDING;
        timer_set(&f_msc_st.pending_tmr, LUSB_MSC_PENDING_REQ_TIMEOUT*CLOCK_CONF_SECOND/1000);
    }
    return len;
}


/*****************************************************************************************
 * обработка команд, требующих передачи данных
 * запрос возвращает количество размер подготовленных данных (можно 0 - тогда передастся 0-ой пакет)
 *    если запрос может передать только часть данных (но остальные намеревается тоже передать),
 *    то должен возвращать размер кратный размеру конечной точки,
 *    если запрос больше не будет передавать данные - любой размер (включая 0)
 * запрос использует статически значения:
 *     f_msc_st.cbw - чтобы узнать обрабатываемую команду
 *     f_msc_st.csw - чтобы узнать сколько данных уже было обработано и изменить статус
 *     f_msc_st.send_buf - устанавливает указатель на буфер, который надо передать
 * ***************************************************************************************/
static int f_msc_cmd_tx(void)
{
    int len = 0;

    f_msc_st.send_buf = f_msc_buf[f_msc_st.cur_buf];
    f_msc_st.offset = f_msc_st.cbw.data_length - f_msc_st.csw.data_residue;

    switch (f_msc_st.cbw.cmd[0])
    {
        case SCSI_OPCODE_INQUIRY:
            len = f_msc_cmd_inquiry();
            break;
        case SCSI_OPCODE_READ_FORMAT_CAPACITIES:
            len = f_msc_cmd_read_format_capacity();
            break;
        case SCSI_OPCODE_REQUEST_SENCSE:
            len = f_msc_cmd_req_sense();
            break;
        case SCSI_OPCODE_READ_CAPACITY:
            len = f_msc_cmd_read_capacity();
            break;
        case SCSI_OPCODE_MODE_SENSE6:
            len = f_msc_cmd_mode_sense();
            break;
        case SCSI_OPCODE_READ6:
        case SCSI_OPCODE_READ10:
            len = f_msc_cmd_read();
            break;
        default:
            lusb_printf("Unsup cmd: %#x", f_msc_st.cbw.cmd[0]);
            for (int i=1; i < f_msc_st.cbw.cmd_length; i++)
                lusb_printf(", %#x", f_msc_st.cbw.cmd[i]);
            lusb_printf("\n");
            f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
            f_msc_set_sense(SCSI_SENSE_KEY_ILLEGAL_REQUEST, SCSI_ASC_INVALID_COMMAND_OPERATION_CODE, 0);
        break;
    }

    f_msc_st.send_length = len;

    if (!f_msc_st.pending)
    {
        f_msc_send_cmd_data();
    }
    else
    {
        f_msc_st.state = LUSB_MSC_STATE_SEND_WT_PENDING;
        timer_set(&f_msc_st.pending_tmr, LUSB_MSC_PENDING_REQ_TIMEOUT*CLOCK_CONF_SECOND/1000);
    }
    return len;
}

int f_msc_proc_pending_tx(void)
{
    int res = 0;
    if ((f_msc_st.cbw.cmd[0] == SCSI_OPCODE_READ6) ||
        (f_msc_st.cbw.cmd[0] == SCSI_OPCODE_READ10))
    {
        res = fs_disc_check_read_cpl(f_msc_st.cbw.LUN);
        if (res < 0)
        {
            f_msc_st.pending = 0;
        }
        else
        {
            f_msc_st.pending = res;
            res = 0;
        }
    }
    return res;
}

int f_msc_proc_pending_rx(void)
{
    int res =0;
    if ((f_msc_st.cbw.cmd[0] == SCSI_OPCODE_WRITE6) ||
            (f_msc_st.cbw.cmd[0] == SCSI_OPCODE_WRITE10))
    {
        res = fs_disc_check_write_cpl(f_msc_st.cbw.LUN);
        if (res < 0)
        {
            f_msc_st.pending = 0;
        }
        else
        {
            f_msc_st.pending = res;
            res = 0;
        }
    }
    return res;
}


static void f_set_scsi_ansii_str(char* dstr, const char* sstr, int str_size, int max_size)
{
    int tmp = MIN(str_size, max_size);
    memcpy(dstr, sstr, tmp);
    if (tmp < max_size)
    {
        memset(&dstr[tmp], ' ', max_size - tmp);
    }
}

/***********************************************************************************************
 *------------------------------  Обработка конкретных команд ----------------------------------
 **********************************************************************************************/

static int f_msc_cmd_inquiry(void)
{
    int len = 36;
    if (f_msc_st.offset)
        return 0;

    //получаем требуемые поля из команды
    uint8_t page_code = f_msc_st.cbw.cmd[2];
    uint16_t alloc = ((uint16_t)f_msc_st.cbw.cmd[3] << 8) | f_msc_st.cbw.cmd[4];
    uint8_t evpd = f_msc_st.cbw.cmd[1] & 1;


    if (evpd == 0)
    {
        t_scsi_inquiry_data *inq_ret = (t_scsi_inquiry_data*)f_msc_st.send_buf;
        inq_ret->flags1 = SCSI_INQUIRY_FLAGS1_RMB;
        inq_ret->devtype = SCSI_DEVTYPE_DIRECT_ACCESS;
        inq_ret->ver = 2; //SCSI_INQUIRY_VERSION_SPC4;
        inq_ret->flags3 = 0;// SCSI_INQUIRY_FLAGS3_RESP_FORMAT_SPC4; //нет ACA и Hierarchical addressing
        inq_ret->addit_len = len-4;
        inq_ret->flags5 = SCSI_INQUIRY_FLAGS5_SCCS;

        f_set_scsi_ansii_str(inq_ret->vid, LUSB_MSC_VID, sizeof(LUSB_MSC_VID), SCSI_INQUIRY_VID_SIZE);
        f_set_scsi_ansii_str(inq_ret->pid, LUSB_MSC_PID, sizeof(LUSB_MSC_PID), SCSI_INQUIRY_PID_SIZE);
        f_set_scsi_ansii_str(inq_ret->rev_lvl, LUSB_MSC_PREV, sizeof(LUSB_MSC_PREV), SCSI_INQUIRY_REV_LVL_SIZE);


        f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_PASSED;
    }
    else
    {

        lusb_printf("inq page %#x\n", page_code);
        if (page_code == SCSI_VPD_PAGECODE_UNIT_SERIAL_NUMBER)
        {
            t_scsi_vpd_page *page = (t_scsi_vpd_page *)f_msc_st.send_buf;
            page->type = SCSI_DEVTYPE_DIRECT_ACCESS;
            page->page_code = page_code;
#ifdef LUSB_USE_MAN_GEN_SERIAL
            page->len = strlen(lusb_app_cb_get_serial());
            memcpy(page->data, lusb_app_cb_get_serial(), page->len);
#else
            page->len = strlen(LUSB_STR_SERIAL);
            memcpy(page->data, LUSB_STR_SERIAL, page->len);
#endif

            len = page->len + 3;
        }
    }

    return MIN(len, alloc);
}

static int f_msc_cmd_read_format_capacity(void)
{
    int len = 0;
    if (f_msc_st.offset)
        return 0;

    f_msc_check_cmd_reserv(1,6);

    lusb_printf("read format capacity\n");

    if (!f_msc_st.csw.status)
    {
        uint16_t alloc = ((uint16_t)f_msc_st.cbw.cmd[7] << 8) | f_msc_st.cbw.cmd[8];
        t_scsi_capacity_format_list* list = (t_scsi_capacity_format_list*)f_msc_st.send_buf;
        list->hdr.res[0] = 0;
        list->hdr.res[1] = 0;
        list->hdr.res[2] = 0;
        list->hdr.list_len = MSC_MAX_DISC_CNT*sizeof(t_scsi_capacity_descr);
        len = sizeof(list->hdr) + list->hdr.list_len;
        if (!fs_disc_check_present(f_msc_st.cbw.LUN))
        {
            SCSI_CAPACITY_FILL_DESCR(&list->descr_list[0], SCSI_CAPACITY_DESCR_NOT_PRESENT,
                    LUSB_MSC_MAX_BLOCK_CNT, LUSB_MSC_BLOCK_SIZE);
        }
        else
        {
            uint32_t block_cnt, block_len;
            fs_disc_get_size(f_msc_st.cbw.LUN, &block_cnt, &block_len);
            SCSI_CAPACITY_FILL_DESCR(&list->descr_list[0], SCSI_CAPACITY_DESCR_TYPE_FORMATTED,
                    block_cnt-1, block_len);

        }
        len = MIN(len, alloc);
    }
    return len;
}

static int f_msc_cmd_read_capacity(void)
{
    int len = 0;
    if (f_msc_st.offset)
        return 0;

    f_msc_check_cmd_reserv(1,8);


    if (!f_msc_st.csw.status)
    {
        if (!fs_disc_check_present(f_msc_st.cbw.LUN))
        {
            SCSI_READ_CAPACITY_FILL(f_msc_st.send_buf, LUSB_MSC_MAX_BLOCK_CNT, LUSB_MSC_BLOCK_SIZE);
        }
        else
        {
            uint32_t block_cnt, block_len;
            fs_disc_get_size(f_msc_st.cbw.LUN, &block_cnt, &block_len);
            SCSI_READ_CAPACITY_FILL(f_msc_st.send_buf, block_cnt-1, block_len);
        }
        len = 8;
    }

    return len;
}

static int f_msc_cmd_test_unit_ready(void)
{
    f_msc_check_cmd_reserv(1,4);

    if (!f_msc_st.csw.status)
    {
        //если диска нет - сообщаем..
        if (!fs_disc_check_present(f_msc_st.cbw.LUN))
        {
            f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
            f_msc_set_sense(SCSI_SENSE_KEY_NOT_READY, SCSI_ASC_MEDIUM_NOT_PRESENT, 0);
        }
    }

    return 0;
}


static int f_msc_cmd_req_sense(void)
{
    int len = 0;
    if (f_msc_st.offset)
        return 0;

    //биты 1-3 должны быть 0 (включая DESC бит, так как дескрипторы не поддерживаем)
    f_msc_check_cmd_reserv(1,3);

    if (!f_msc_st.csw.status)
    {
        uint8_t alloc = f_msc_st.cbw.cmd[4];
        f_msc_st.send_buf = (uint8_t*)&f_msc_st.sense;

        len = MIN(sizeof(f_msc_st.sense), alloc);
    }
    return len;
}

static int f_msc_get_mode_page(uint16_t pagenum,
        uint8_t* buf, uint32_t size)
{
    int len = 0;

    switch (pagenum)
    {
        case SCSI_MODEPAGE_INFO_EXCEPT_CTL:
        {
            t_scsi_modepage_except_ctl* page = (t_scsi_modepage_except_ctl*)buf;
            len = sizeof(t_scsi_modepage_except_ctl);
            page->page_code = (pagenum>>8)&0xFF;
            page->page_len = len-2;
            page->flags2 = 0;
            page->MRIE = 0;
            page->interval_tmr = HTONS32(0);
            page->report_cnt = HTONS32(0);

        }
        case SCSI_MODEPAGE_CACHING_MODE:
        {
            t_scsi_modepage_caching_mode* page =
                    (t_scsi_modepage_caching_mode*)buf;
            len = sizeof(t_scsi_modepage_caching_mode);
            memset(page, 0, len);

            page->page_code = (pagenum>>8)&0xFF;
            page->page_len = len-2;
        }
    }

    return len;
}


static int f_msc_cmd_mode_sense(void)
{
    int len = 0;
    uint32_t alloc_len;
    uint8_t page;
    uint8_t page_ctl=0;
    uint16_t pagenum;
    uint8_t subpage;
    uint8_t hdr_size;
   // uint32_t pos;

    if (f_msc_st.cbw.cmd[0] == SCSI_OPCODE_MODE_SENSE6)
    {
        page = f_msc_st.cbw.cmd[2] & 0x3F;
        page_ctl = (f_msc_st.cbw.cmd[2] >> 6) & 0x3;
        subpage = f_msc_st.cbw.cmd[3];
        alloc_len = f_msc_st.cbw.cmd[4];
        hdr_size = sizeof(t_scsi_mode_param_hdr6);

        t_scsi_mode_param_hdr6* hdr = (t_scsi_mode_param_hdr6*)f_msc_st.send_buf;
        hdr->medium = 0;
        hdr->devspec = 0;
        hdr->block_descr_len = 0;
    }

    pagenum = ((uint16_t)page << 8) | subpage;
    len = hdr_size;
    lusb_printf("mode sense %#x\n", pagenum);



    //нужно проверить код страницы
    if (pagenum == SCSI_MODEPAGE_ALL_PAGES)
    {
        len+=f_msc_get_mode_page(SCSI_MODEPAGE_INFO_EXCEPT_CTL,
                &f_msc_st.send_buf[len], LUSB_MSC_BUF_SIZE-len);
        len+=f_msc_get_mode_page(SCSI_MODEPAGE_CACHING_MODE,
                &f_msc_st.send_buf[len], LUSB_MSC_BUF_SIZE-len);

    }
    else
    {
        len += f_msc_get_mode_page(pagenum,
                &f_msc_st.send_buf[len], LUSB_MSC_BUF_SIZE-hdr_size);
    }

    if (len <= hdr_size)
    {
         f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
         f_msc_set_sense(SCSI_SENSE_KEY_ILLEGAL_REQUEST, SCSI_ASC_INVALID_FIELD_IN_CDB, 0);
         len = 0;
    }
    else
    {
        len = MIN(len, alloc_len);
        f_msc_st.send_buf[0] = len-1;
    }

   // lmemset(f_msc_st.send_buf, 0, 8);
   // len = 8;

    return len;
}


static void f_check_rd_wr(void)
{
    //проверяем - подключен ли диск
    if (!fs_disc_check_present(f_msc_st.cbw.LUN))
    {
        f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
        f_msc_set_sense(SCSI_SENSE_KEY_NOT_READY, SCSI_ASC_MEDIUM_NOT_PRESENT, 0);
    }
    else if (f_msc_st.offset==0)
    {
        //если это первая команда - получаем параметры и проверяем параметры
        if ((f_msc_st.cbw.cmd[0] & SCSI_OPCODE_CMDFORMAT) == SCSI_OPCODE_CMDFORMAT6)
        {
            f_msc_st.lba = (((uint32_t)f_msc_st.cbw.cmd[1] &0x1F) << 16) |
                    ((uint32_t)f_msc_st.cbw.cmd[2] << 8) | f_msc_st.cbw.cmd[3];
            f_msc_st.lb_cnt = f_msc_st.cbw.cmd[4] ? f_msc_st.cbw.cmd[4] : 256;
        }
        else if ((f_msc_st.cbw.cmd[0] & SCSI_OPCODE_CMDFORMAT) == SCSI_OPCODE_CMDFORMAT10)
        {
            f_msc_st.lba = ((uint32_t)f_msc_st.cbw.cmd[2] << 24) |
                    ((uint32_t)f_msc_st.cbw.cmd[3] << 16) |
                    ((uint32_t)f_msc_st.cbw.cmd[4] << 8) |
                    f_msc_st.cbw.cmd[5];
            f_msc_st.lb_cnt = ((uint16_t)f_msc_st.cbw.cmd[7] << 8) |
                    f_msc_st.cbw.cmd[8];
        //    flags = f_msc_st.cbw.cmd[1];
        //    group = f_msc_st.cbw.cmd[6] & 0x1F;
        }

        if (!f_msc_st.csw.status)
        {
            uint32_t block_cnt;

            fs_disc_get_size(f_msc_st.cbw.LUN, &block_cnt, &f_msc_st.lb_size);
            if ((block_cnt - f_msc_st.lba) < f_msc_st.lb_cnt)
            {
                f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
                f_msc_set_sense(SCSI_SENSE_KEY_ILLEGAL_REQUEST,
                        SCSI_ASC_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE, 0);
            }
            else
            {
                f_msc_st.max_block_trans = LUSB_MSC_BUF_SIZE/f_msc_st.lb_size;
            }
        }
    }
}

static int f_msc_cmd_prevent_rem(void)
{
    if (!fs_disc_check_present(f_msc_st.cbw.LUN))
    {
        f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
        f_msc_set_sense(SCSI_SENSE_KEY_NOT_READY, SCSI_ASC_MEDIUM_NOT_PRESENT, 0);
    }
    else
    {
        f_msc_check_cmd_reserv(1,3);
    }

    if (!f_msc_st.csw.status)
    {

    }

    return 0;
}

static int  f_msc_cmd_startstop(void)
{
    uint8_t flags = f_msc_st.cbw.cmd[4];
    lusb_printf("start stop unit: %d\n", flags);
    fs_disc_start_stop(f_msc_st.cbw.LUN, flags&1);
    return 0;
}


static int f_msc_cmd_read(void)
{
    int len = 0;

    f_check_rd_wr();

    //если все ок - запускаем на чтение блок данных, который можем передать за раз
    if (!f_msc_st.csw.status)
    {
        uint32_t send_blocks = f_msc_st.lb_cnt - f_msc_st.offset/f_msc_st.lb_size;
        send_blocks = MIN(send_blocks, f_msc_st.max_block_trans);

        int32_t res = fs_disc_start_read(f_msc_st.cbw.LUN, f_msc_st.lba, f_msc_st.send_buf,
                send_blocks, f_msc_st.offset, f_msc_st.lb_cnt);
        if (res == 0)
        {
            f_msc_st.pending = fs_disc_check_read_cpl(f_msc_st.cbw.LUN);
            len = send_blocks*f_msc_st.lb_size;
            f_msc_st.lba += send_blocks;
        }
        else
        {
            f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
            f_msc_set_sense(SCSI_SENSE_KEY_MEDIUM_ERROR, 0, 0);
            lusb_printf("!!!! read error !!!!\n");
        }
    }
    return len;
}


static int f_msc_cmd_write(uint8_t* buf, int size)
{
    int len = 0;
    f_check_rd_wr();
    if (!f_msc_st.csw.status)
    {
        uint32_t send_blocks = size/f_msc_st.lb_size;
        int32_t res = fs_disc_start_write(f_msc_st.cbw.LUN, f_msc_st.lba, buf, send_blocks,
                f_msc_st.offset, f_msc_st.lb_cnt);
        if (res == 0)
        {
            f_msc_st.pending = fs_disc_check_write_cpl(f_msc_st.cbw.LUN);
            len = send_blocks*f_msc_st.lb_size;
            f_msc_st.lba += send_blocks;
        }
        else
        {
            lusb_printf("!!!! write error !!!!\n");
            f_msc_st.csw.status = LUSB_MSC_STATUS_CMD_FAILED;
            f_msc_set_sense(SCSI_SENSE_KEY_MEDIUM_ERROR, 0, 0);
        }
    }
    return len;
}




#endif

