/*
 * lusb_mass_storage.h
 *
 *  Created on: 09.01.2011
 *      Author: Melkor
 */

#ifndef LUSB_MASS_STORAGE_H_
#define LUSB_MASS_STORAGE_H_

#include "classes/msc/lusb_msc_defs.h"
#include "lusb_typedefs.h"


#define MSC_CMD_COMPLETE 0
#define MSC_CMD_PENDIG   1
#define MSC_CMD_ERROR    -1



//***************************  прототипы функций **********************************/
int lusb_msc_req_rx(t_lusb_req* req, uint8_t* buf);
const void* lusb_msc_req_tx(t_lusb_req* req, int* length);
void lusb_msc_data_tx(uint8_t event, t_lusb_dd_status* pDD);
void lusb_msc_data_rx(uint8_t event, t_lusb_dd_status* pDD);
void lusb_msc_set_config(void);
void lusb_msc_progress(void);

//********** функции, чтения-записи, которые должен реализовать порт **************/
int8_t fs_disc_check_present(uint8_t lun);
int32_t fs_disc_get_size(uint8_t lun, uint32_t* blocks, uint32_t* block_size);
int32_t fs_disc_start_read(uint8_t lun, uint32_t addr, uint8_t* buf, uint32_t size,
        uint32_t offset, uint32_t full_size);
int32_t fs_disc_check_read_cpl(uint8_t lun);
int32_t fs_disc_start_write(uint8_t lun, uint32_t lba, uint8_t* buf, uint32_t size,
        uint32_t offset, uint32_t full_size);
int32_t fs_disc_check_write_cpl(uint8_t lun);
int32_t fs_disc_start_stop(uint8_t lun, uint8_t state);
void fs_disc_usb_connected(void);



#endif /* LUSB_MASS_STORAGE_H_ */
