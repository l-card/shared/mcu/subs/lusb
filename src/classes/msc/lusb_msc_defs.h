/*
 * lusb_msc_defs.h
 *
 *  Created on: 17.01.2011
 *      Author: Melkor
 */

#ifndef LUSB_MSC_DEFS_H_
#define LUSB_MSC_DEFS_H_

#include "lcspec.h"
#include "lusb_scsi.h"


/* MSC Subclass Codes */
#define LUSB_MSC_SUBCLASS_RBC                0x01
#define LUSB_MSC_SUBCLASS_MMC5               0x02
#define LUSB_MSC_SUBCLASS_QIC157             0x03
#define LUSB_MSC_SUBCLASS_UFI                0x04
#define LUSB_MSC_SUBCLASS_SFF8070I           0x05
#define LUSB_MSC_SUBCLASS_SCSI               0x06
#define LUSB_MSC_SUBCLASS_LSDFS              0x07
#define LUSB_MSC_SUBCLASS_IEEE1667           0x08

/* MSC Protocol Codes */
#define LUSB_MSC_PROTOCOL_CBI_INT            0x00
#define LUSB_MSC_PROTOCOL_CBI_NOINT          0x01
#define LUSB_MSC_PROTOCOL_BBB                0x50
#define LUSB_MSC_PROTOCOL_UAS                0x62

/* MSC Request Codes */
#define LUSB_MSC_REQ_RESET                   0xFF
#define LUSB_MSC_REQ_GET_MAX_LUN             0xFE

/* CSW Status Definitions */
#define LUSB_MSC_STATUS_CMD_PASSED                  0x00
#define LUSB_MSC_STATUS_CMD_FAILED                  0x01
#define LUSB_MSC_STATUS_PHASE_ERROR                 0x02

#define LUSB_MSC_CMD_MAX_LEN   16


#include "lcspec_pack_start.h"

/* Bulk-only Command Block Wrapper */
 struct st_lusb_msc_cbw
{
    uint32_t sign;
    uint32_t tag;
    uint32_t data_length;
    uint8_t  flags;
    uint8_t  LUN;
    uint8_t  cmd_length;
    uint8_t  cmd[LUSB_MSC_CMD_MAX_LEN];
} LATTRIBUTE_PACKED;
typedef struct st_lusb_msc_cbw t_lusb_msc_cbw;

/* Bulk-only Command Status Wrapper */
struct st_lusb_msc_csw
{
    uint32_t sign;
    uint32_t tag;
    uint32_t data_residue;
    uint8_t  status;
} LATTRIBUTE_PACKED;
typedef struct st_lusb_msc_csw t_lusb_msc_csw;

#include "lcspec_pack_restore.h"


#ifndef HTONS16
    #ifdef BIG_ENDIAN
        #define HTONS16(n) (n)
    #else
        #define HTONS16(n) (uint16_t)((((uint16_t) (n) & 0xFF) << 8) | (((uint16_t) (n) &0xFF00) >> 8))
    #endif
#endif

#ifndef HTONS32
    #if BIG_ENDIAN
        #define HTONS32(n) (n)
    #else
        #define HTONS32(n) (uint32_t)((((uint32_t) (n) & 0xFFUL) << 24) | (((uint32_t) (n) &0xFF00UL) << 8) | \
                                (((uint32_t) (n) &0xFF0000UL) >> 8) | (((uint32_t) (n) &0xFF000000UL) >> 24))
    #endif
#endif


//************************ константы для протокола Bulk Only *********************************/

//константы для CBW (command block wrapper)
#define LUSB_MSC_CBW_SIGN  0x43425355 //код признака CBW
//флаги - направление
#define LUSB_MSC_CBW_FLAGS_DIR  0x80
#define LUSB_MSC_CBW_DIR_IN     0x80
#define LUSB_MSC_CBW_DIR_OUT    0x00

//константы для CSW (command status wrapper)
#define LUSB_MSC_CSW_SIGN 0x53425355



#endif /* LUSB_MSC_DEFS_H_ */
