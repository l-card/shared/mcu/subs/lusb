#ifndef LUSB_ACTIVITY_INDICATION_H
#define LUSB_ACTIVITY_INDICATION_H

#include "lusb_config_params.h"

#ifdef LUSB_ACTIVITY_INDICATION
    void lusb_activity_indication(void);
    void lusb_activity_pull(void);
#else
    #define lusb_activity_indication()
    #define lusb_activity_pull()
#endif

#endif // LUSB_ACTIVITY_INDICATION_H
