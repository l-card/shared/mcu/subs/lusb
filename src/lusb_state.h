#ifndef LUSB_STATE_H
#define LUSB_STATE_H

#include "lusb_types.h"

/** вся информация о состоянии всего устройства */
typedef struct {
    struct {
        uint16_t status; /**< Статус устройства, возвращаемый на GET_STATUS */
        uint8_t state;   /**< Состояние устройства (usb2 spec.) - набор флагов из LUSB_DEVSTATE_xxx */
    } dev;

    struct {
        const t_usb_interface_descriptor* descr;
        uint8_t alt_settings; /**< Установленный номер альтернативных настроек для интерфейса */
        uint8_t alt_settings_cnt; /**< Общее количество альтернативных настроек для интерфейса */
#if LUSB_EP_NONCTL_CNT
        t_lusb_ep_info ep[LUSB_MAX_EP_CNT]; /**< Конечные точки данного интерфейса */
#endif
    } intf[LUSB_INTERFACE_CNT];

    struct {
        const t_usb_configuration_descriptor* descr;
    } cfg;
} t_lusb_state;

extern t_lusb_state g_lusb_state;

#endif // LUSB_STATE_H
