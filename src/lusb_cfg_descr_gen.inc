
#if LUSB_CURCFG_SPEED == LUSB_SPEED_HIGH
    #define CURCFG_BULK_MAX_PACKET_SIZE   USB_HS_EP_BULK_MAX_PACKET_SIZE
#elif LUSB_CURCFG_SPEED == LUSB_SPEED_FULL
    #define CURCFG_BULK_MAX_PACKET_SIZE   USB_FS_EP_BULK_MAX_PACKET_SIZE
#endif



LUSB_DESCR_SPEC_C  struct {
    t_usb_configuration_descriptor cfg;
    t_usb_interface_descriptor bulk_intf;
    #if (LUSB_STDBULK_TX_EP_CNT > 0)
        t_usb_endpoint_descriptor ep_in[LUSB_STDBULK_TX_EP_CNT];
    #endif
    #if (LUSB_STDBULK_RX_EP_CNT > 0)
        t_usb_endpoint_descriptor ep_out[LUSB_STDBULK_RX_EP_CNT];
    #endif

    #ifdef LUSB_USE_MSC_INTERFACE
        t_usb_interface_descriptor msc_intf;
        t_usb_endpoint_descriptor msc_eps[LUSB_MSC_EP_CNT];
    #endif

    #ifdef LUSB_USE_IAP_NATIVE_INTERFACE
        t_usb_interface_descriptor iap_native_intf0;
        t_usb_interface_descriptor iap_native_intf1;
        t_usb_endpoint_descriptor iap_native_eps[2];
    #endif
    uint8_t end;
} LATTRIBUTE_PACKED LUSB_CURCFG_STRUCT_NAME  = {
    .cfg = {
        .bLength             = USB_CONFIGURATION_DESCRIPTOR_LENGTH,
        .bDescriptorType     = LUSB_CURCFG_DESCRTYPE,
        .wTotalLength        = sizeof(LUSB_CURCFG_STRUCT_NAME)-1,
        .bNumInterfaces      = LUSB_INTERFACE_CNT,
        .bConfigurationValue = 1,          //номер конфигурации (с 1)
        .iConfiguration      = LUSB_STRIND_CONFIG,   //индекс строки с описанием
        .bmAttributes        = USB_CONFIG_ATTR_DEFAULT|LUSB_CONFIG_ATTR, //аттрибуты (self powered, remote wakeup)
        .bMaxPower           = LUSB_DEV_POW/2,                   //потребляемый ток (mA)
    },
#ifdef LUSB_USE_STDBULK_INTERFACE
    .bulk_intf = {
        .bLength            = USB_INTERFACE_DESCRIPTOR_LENGTH,
        .bDescriptorType    = USB_DESCRTYPE_INTERFACE,
        .bInterfaceNumber   = 0,
        .bAlternateSetting  = 0,
        .bNumEndpoints      = LUSB_STDBULK_EP_CNT,
        .bInterfaceClass    = LUSB_STDBULK_INTERFACE_CLASS,
        .bInterfaceSubClass = LUSB_STDBULK_INTERFACE_SUBCLASS,
        .bInterfaceProtocol = LUSB_STDBULK_INTERFACE_PROTOCOL,
        .iInterface         = LUSB_STRIND_INTF_STDBULK,
    },
    #if (LUSB_STDBULK_TX_EP_CNT> 0)
    .ep_in[0] = {
        .bLength            = USB_ENDPOINT_DESCRIPTOR_LENGTH,
        .bDescriptorType    = USB_DESCRTYPE_ENDPOINT,
        .bEndpointAddress   = USB_EP_IN(LUSB_STDBULK_TX_EP_ADDR(0)),
        .bmAttributes       = USB_EPTYPE_BULK,
        .wMaxPacketSize     = MIN(CURCFG_BULK_MAX_PACKET_SIZE, LUSB_STDBULK_TX_EP_SIZE(0)),
        .bInterval          = 0,
        #if (LUSB_STDBULK_TX_EP_CNT > 1)
            #warning "You need manually specify descriptors for int eps with index > 0"
        #endif
    },
    #endif
    #if (LUSB_STDBULK_RX_EP_CNT > 0)
    .ep_out[0] = {
        .bLength            = USB_ENDPOINT_DESCRIPTOR_LENGTH,
        .bDescriptorType    = USB_DESCRTYPE_ENDPOINT,
        .bEndpointAddress   = USB_EP_OUT(LUSB_STDBULK_RX_EP_ADDR(0)),
        .bmAttributes       = USB_EPTYPE_BULK,
        .wMaxPacketSize     = MIN(CURCFG_BULK_MAX_PACKET_SIZE, LUSB_STDBULK_RX_EP_SIZE(0)),
        .bInterval          = 0,
        #if (LUSB_STDBULK_RX_EP_CNT > 1)
            #warning "You need manually specify descriptors for out eps with index > 0"
        #endif
    },
    #endif
#endif

#ifdef LUSB_USE_MSC_INTERFACE
    .msc_intf = {
        .bLength            = USB_INTERFACE_DESCRIPTOR_LENGTH,
        .bDescriptorType    = USB_DESCRTYPE_INTERFACE,
        .bInterfaceNumber   = LUSB_MSC_INTERFACE_NUM,
        .bAlternateSetting  = 0,
        .bNumEndpoints      = LUSB_MSC_EP_CNT,
        .bInterfaceClass    = USB_DEVICE_CLASS_MASS_STORAGE,
        .bInterfaceSubClass = LUSB_MSC_SUBCLASS,
        .bInterfaceProtocol = LUSB_MSC_PROTOCOL,
        .iInterface         = LUSB_STRIND_MSC,
    },
    #if (LUSB_MSC_PROTOCOL == LUSB_MSC_PROTOCOL_BBB)
    .msc_eps[0] = {
        .bLength            = USB_ENDPOINT_DESCRIPTOR_LENGTH,
        .bDescriptorType    = USB_DESCRTYPE_ENDPOINT,
        .bEndpointAddress   = USB_EP_IN(LUSB_EP_MSC_IN_ADDR),
        .bmAttributes       = USB_EPTYPE_BULK,
        .wMaxPacketSize     = MIN(CURCFG_BULK_MAX_PACKET_SIZE, LUSB_EP_MSC_SIZE),
        .bInterval          = 0,
    },
    .msc_eps[1] = {
        .bLength            = USB_ENDPOINT_DESCRIPTOR_LENGTH,
        .bDescriptorType    = USB_DESCRTYPE_ENDPOINT,
        .bEndpointAddress   = USB_EP_OUT(LUSB_EP_MSC_OUT_ADDR),
        .bmAttributes       = USB_EPTYPE_BULK,
        .wMaxPacketSize     = MIN(CURCFG_BULK_MAX_PACKET_SIZE, LUSB_EP_MSC_SIZE),
        .bInterval          = 0,
    },
    #endif
#endif
#ifdef LUSB_USE_IAP_NATIVE_INTERFACE
    .iap_native_intf0 = {
        .bLength            = USB_INTERFACE_DESCRIPTOR_LENGTH,
        .bDescriptorType    = USB_DESCRTYPE_INTERFACE,
        .bInterfaceNumber   = LUSB_IAP_NATIVE_INTERFACE_NUM,
        .bAlternateSetting  = 0,
        .bNumEndpoints      = 0,
        .bInterfaceClass    = USB_DEVICE_CLASS_VENDOR_SPECIFIC,
        .bInterfaceSubClass = 0xF0,
        .bInterfaceProtocol = 0x01,
        .iInterface         = LUSB_STRIND_IAP_NATIVE,
    },
    .iap_native_intf1 = {
        .bLength            = USB_INTERFACE_DESCRIPTOR_LENGTH,
        .bDescriptorType    = USB_DESCRTYPE_INTERFACE,
        .bInterfaceNumber   = LUSB_IAP_NATIVE_INTERFACE_NUM,
        .bAlternateSetting  = 1,
        .bNumEndpoints      = 2,
        .bInterfaceClass    = USB_DEVICE_CLASS_VENDOR_SPECIFIC,
        .bInterfaceSubClass = 0xF0,
        .bInterfaceProtocol = 0x01,
        .iInterface         = LUSB_STRIND_IAP_NATIVE,
    },
    .iap_native_eps[0] = {
        .bLength            = USB_ENDPOINT_DESCRIPTOR_LENGTH,
        .bDescriptorType    = USB_DESCRTYPE_ENDPOINT,
        .bEndpointAddress   = USB_EP_IN(LUSB_EP_IAP_NATIVE_IN_ADDR),
        .bmAttributes       = USB_EPTYPE_BULK,
        .wMaxPacketSize     = MIN(CURCFG_BULK_MAX_PACKET_SIZE, LUSB_EP_IAP_NATIVE_SIZE),
        .bInterval          = 0,
    },
    .iap_native_eps[1] = {
        .bLength            = USB_ENDPOINT_DESCRIPTOR_LENGTH,
        .bDescriptorType    = USB_DESCRTYPE_ENDPOINT,
        .bEndpointAddress   = USB_EP_OUT(LUSB_EP_IAP_NATIVE_OUT_ADDR),
        .bmAttributes       = USB_EPTYPE_BULK,
        .wMaxPacketSize     = MIN(CURCFG_BULK_MAX_PACKET_SIZE, LUSB_EP_IAP_NATIVE_SIZE),
        .bInterval          = 0,
    },
#endif
    .end = 0
};


LUSB_MAKE_CFG_PTR(LUSB_CURCFG_STRUCT_NAME);

#undef CURCFG_BULK_MAX_PACKET_SIZE
#undef LUSB_CURCFG_STRUCT_NAME
#undef LUSB_CURCFG_DESCRTYPE
#undef LUSB_CURCFG_SPEED
