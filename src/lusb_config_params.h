#ifndef LUSB_CONFIG_H_
#define LUSB_CONFIG_H_

#include "lusb_config.h"
#include "lusb_ll_port_params.h"


#ifndef MAX
#define MAX(a,b) (((a)>(b)) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a,b) (((a)<(b)) ? (a) : (b))
#endif

#ifndef lusb_printf
    #define lusb_printf(lvl, ...)
#endif


#ifndef LUSB_DESCR_SPEC_H
    #define LUSB_DESCR_SPEC_H const
#endif
#ifndef LUSB_DESCR_SPEC_C
    #define LUSB_DESCR_SPEC_C const
#endif


#ifndef LUSB_USER_INTERFACES_CNT
    #define LUSB_USER_INTERFACES_CNT 0
#endif

#ifndef LUSB_USER_EP_CNT
    #define LUSB_USER_EP_CNT 0
#endif

#ifdef LUSB_APPL_CB_ACTIVITY_IND
    #define LUSB_ACTIVITY_INDICATION

    #ifndef LUSB_ACTIVITY_IND_BLINK_TOUT
        #define LUSB_ACTIVITY_IND_BLINK_TOUT 75
    #endif

    #ifndef LUSB_ACTIVITY_IND_GUARD_TOUT
        #define LUSB_ACTIVITY_IND_GUARD_TOUT 75
    #endif
#endif

//генерация констант на основе lusb_config.h
//определение кол-ва конечных точек в стандартном интерфейсе
#ifdef LUSB_USE_STDBULK_INTERFACE
    #define LUSB_STDBULK_EP_CNT (LUSB_STDBULK_TX_EP_CNT + LUSB_STDBULK_RX_EP_CNT)
#else
    #define LUSB_STDBULK_EP_CNT 0
#endif

//индексы контрольных точке для передачи (начинаются с 0)
//определяются положением дескрипторов кт в конфигурации
//используются приложением при обращении к стеку
#ifdef LUSB_USE_STDBULK_INTERFACE
    #if (LUSB_STDBULK_TX_EP_CNT > 0)
        #define LUSB_STDBULK_TX_EP_NUM           0 //индекс точки на передачу
    #endif

    #if (LUSB_STDBULK_RX_EP_CNT > 0)
        #define LUSB_STDBULK_RX_EP_NUM           LUSB_STDBULK_TX_EP_CNT //индекс точки на прием
    #endif


    #define LUSB_TX_EP_IND LUSB_STDBULK_TX_EP_NUM
    #define LUSB_RX_EP_IND LUSB_STDBULK_RX_EP_NUM


    #ifndef LUSB_STDBULK_INTERFACE_CLASS
        #define LUSB_STDBULK_INTERFACE_CLASS USB_DEVICE_CLASS_VENDOR_SPECIFIC
    #endif

    #ifndef LUSB_STDBULK_INTERFACE_SUBCLASS
        #define LUSB_STDBULK_INTERFACE_SUBCLASS 0
    #endif

    #ifndef LUSB_STDBULK_INTERFACE_PROTOCOL
        #define LUSB_STDBULK_INTERFACE_PROTOCOL USB_INTERFACE_PROTOCOL_VENDOR_SPECIFIC
    #endif
#endif

#ifdef LUSB_USE_MSC_INTERFACE
    #if (LUSB_MSC_PROTOCOL == LUSB_MSC_PROTOCOL_BBB)
       #define LUSB_MSC_EP_CNT 2
    #else
        #error "unsupported msc protocol"
    #endif
#else
    #define LUSB_MSC_EP_CNT 0
#endif


#ifdef LUSB_USE_IAP_NATIVE_INTERFACE
    #define LUSB_IAP_NATIVE_EP_CNT 2
#else
    #define LUSB_IAP_NATIVE_EP_CNT 0
#endif


//определение номеров интерфейсов
#ifdef LUSB_USE_STDBULK_INTERFACE
    #define LUSB_STDBULK_INTERFACE_NUM 0
#else
   #define LUSB_STDBULK_INTERFACE_NUM -1
#endif

#ifdef LUSB_USE_MSC_INTERFACE
    #define LUSB_MSC_INTERFACE_NUM (LUSB_STDBULK_INTERFACE_NUM + 1) //номер интерфейса MSC (начиная с 0)
#else
    #define LUSB_MSC_INTERFACE_NUM LUSB_STDBULK_INTERFACE_NUM
#endif

#ifdef LUSB_USE_IAP_NATIVE_INTERFACE
    #define LUSB_IAP_NATIVE_INTERFACE_NUM (LUSB_MSC_INTERFACE_NUM + 1) //номер интерфейса MSC (начиная с 0)

    #define LUSB_IAP_NATIVE_TX_EP_NUM 0
    #define LUSB_IAP_NATIVE_RX_EP_NUM 1
#else
    #define LUSB_IAP_NATIVE_INTERFACE_NUM LUSB_MSC_INTERFACE_NUM
#endif

//полное кол-во интерфейсов
#define LUSB_INTERFACE_CNT (LUSB_IAP_NATIVE_INTERFACE_NUM + 1 + LUSB_USER_INTERFACES_CNT)

#if (LUSB_INTERFACE_CNT <= 0)
    #error "Usb interface not specifed";
#endif


//настройки стоковых дескрипторов
#ifdef LUSB_USE_RUS_STRINGS
    #define LUSB_LANGID_CNT   2 //кол-во языков
#else
    #define LUSB_LANGID_CNT   1 //кол-во языков
#endif

#ifdef LUSB_USE_MSC_INTERFACE
    #define LUSB_STRDESCR_CNT 6 //кол-во строк для каждого языка
#else
    #define LUSB_STRDESCR_CNT 5
#endif





/* максимальное кол-во конечных точек (без 0-ой)
    должно быть не меньше, чем в любом интерфейсе
    при объявлении дескрипторов */
#define LUSB_MAX_EP_CNT_LIM  MAX(MAX(LUSB_STDBULK_EP_CNT, LUSB_MSC_EP_CNT), \
                                 MAX(LUSB_IAP_NATIVE_EP_CNT, LUSB_USER_EP_CNT))
#ifndef LUSB_MAX_EP_CNT
    #define LUSB_MAX_EP_CNT    LUSB_MAX_EP_CNT_LIM
#else
    #if (LUSB_MAX_EP_CNT < LUSB_MAX_EP_CNT_LIM)
        #error "invalid LUSB_MAX_EP_CNT value"
    #endif
#endif


/* определение длины конфигурации */
#ifndef LUSB_USB_CONFIG_MAX_LENGTH
    #ifdef LUSB_USE_STDBULK_INTERFACE
        #define LUSB_STDBULK_INTF_SIZE   (USB_INTERFACE_DESCRIPTOR_LENGTH + LUSB_STDBULK_EP_CNT*USB_ENDPOINT_DESCRIPTOR_LENGTH)
    #else
        #define LUSB_STDBULK_INTF_SIZE 0
    #endif

    #ifdef LUSB_USE_MSC_INTERFACE
        #define LUSB_MSC_INTF_SIZE   (USB_INTERFACE_DESCRIPTOR_LENGTH + LUSB_MSC_EP_CNT*USB_ENDPOINT_DESCRIPTOR_LENGTH)
    #else
        #define LUSB_MSC_INTF_SIZE 0
    #endif

    #ifdef LUSB_USE_IAP_NATIVE_INTERFACE
        #define LUSB_IAP_NATIVE_INTF_SIZE    (2*USB_INTERFACE_DESCRIPTOR_LENGTH + LUSB_IAP_NATIVE_EP_CNT*USB_ENDPOINT_DESCRIPTOR_LENGTH)
    #else
        #define LUSB_IAP_NATIVE_INTF_SIZE    0
    #endif

    #define LUSB_USER_INTF_SIZE              (LUSB_USER_INTERFACES_CNT*USB_INTERFACE_DESCRIPTOR_LENGTH + LUSB_USER_EP_CNT*USB_ENDPOINT_DESCRIPTOR_LENGTH)

    #define LUSB_USB_CONFIG_MAX_LENGTH (USB_CONFIGURATION_DESCRIPTOR_LENGTH + \
                                        LUSB_STDBULK_INTF_SIZE + \
                                        LUSB_MSC_INTF_SIZE + \
                                        LUSB_IAP_NATIVE_INTF_SIZE + \
                                        LUSB_USER_INTF_SIZE)
#endif


//индексы строк
#define LUSB_STRIND_MANUFACTURER       1
#define LUSB_STRIND_PRODUCT            2
#ifdef LUSB_STR_CONFIG
    #define LUSB_STRIND_CONFIG             3
    #define LUSB_STRCNT_CONFIG             3
#else
    #define LUSB_STRIND_CONFIG             0
    #define LUSB_STRCNT_CONFIG             2
#endif


#if defined LUSB_USE_STDBULK_INTERFACE && defined LUSB_STR_BULK_INTERFACE
    #define LUSB_STRIND_INTF_STDBULK   (LUSB_STRCNT_CONFIG+1)
    #define LUSB_STRCNT_INTF_STDBULK   (LUSB_STRCNT_CONFIG+1)
#else
    #define LUSB_STRIND_INTF_STDBULK   0
    #define LUSB_STRCNT_INTF_STDBULK   (LUSB_STRCNT_CONFIG)
#endif

#if defined LUSB_USE_MSC_INTERFACE && defined LUSB_STR_MSC_INTERFACE
    #define LUSB_STRIND_MSC            (LUSB_STRCNT_INTF_STDBULK+1)
    #define LUSB_STRCNT_MSC            (LUSB_STRCNT_INTF_STDBULK+1)
#else
    #define LUSB_STRIND_MSC            0
    #define LUSB_STRCNT_MSC            (LUSB_STRCNT_INTF_STDBULK)
#endif

#if defined LUSB_USE_IAP_NATIVE_INTERFACE && defined LUSB_STR_IAP_EA_NATIVE_INTERFACE
    #define LUSB_STRIND_IAP_NATIVE     (LUSB_STRCNT_MSC+1)
    #define LUSB_STRCNT_IAP_NATIVE     (LUSB_STRCNT_MSC+1)
    #else
    #define LUSB_STRIND_IAP_NATIVE     0
    #define LUSB_STRCNT_IAP_NATIVE     (LUSB_STRCNT_MSC)
#endif



/* строковый индекс, с которого должны начинаться пользовательские строки
    (не задаваемые через настройки из lusb_config.h) */
#define LUSB_STRIND_USER             LUSB_STRCNT_IAP_NATIVE+1



#if defined LUSB_USE_MAN_GEN_SERIAL || defined LUSB_STR_SERIAL
    #define LUSB_STRIND_SERIAL             (LUSB_STRCNT_MSC+1)
#else
    #define LUSB_STRIND_SERIAL 0
#endif


#ifdef LUSB_USE_MAN_GEN_SERIAL
    const char *lusb_app_cb_get_serial(void);
#endif




#ifdef LUSB_USE_SUPERSPEED
   #define LUSB_USB_VER            0x300
#else
    #define LUSB_USB_VER           0x200
#endif

/* если есть не нулевые конечные точки, то используем передачу через
 * data descriptors (DD). Если порт поддерживает этот интерфейс, то используем
 * его, иначе используем реалицаю в софте */
#if (LUSB_EP_NONCTL_CNT > 0)
    #ifdef LUSB_PORT_SUPPORT_DD
        #define LUSB_USE_PORT_DD
    #else
        #define LUSB_USE_SOFT_DD
    #endif
#endif


#ifndef LUSB_EP_NONCTL_CNT
    #define LUSB_EP_NONCTL_CNT LUSB_EP_NONCTL_CNT_MAX
#endif

#if LUSB_EP_NONCTL_CNT > LUSB_EP_NONCTL_CNT_MAX
    #error "LUSB_EP_NONCTL_CNT exceed maximum hardware ep count"
#endif

#if LUSB_EP_NONCTL_CNT < 0
    #error "Invalid value of LUSB_EP_NONCTL_CNT"
#endif




#endif /* LUSB_CONFIG_H_ */
