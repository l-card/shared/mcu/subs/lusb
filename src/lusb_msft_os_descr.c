#include "lusb.h"
#include "lusb_msft_os_descr.h"


#ifdef LUSB_USE_MSFT_OS_DESCR
    #define USB_CFG_WORD(wrd)  wrd&0xFF, (wrd>>8)&0xFF

    const uint8_t lusb_msft_os_str_descr[LUSB_MSFT_OS_STRING_DESCR_LENGTH] = {
        LUSB_MSFT_OS_STRING_DESCR_LENGTH,
        USB_DESCRTYPE_STRING,
        USB_CFG_WORD(L'M'),
        USB_CFG_WORD(L'S'),
        USB_CFG_WORD(L'F'),
        USB_CFG_WORD(L'T'),
        USB_CFG_WORD(L'1'),
        USB_CFG_WORD(L'0'),
        USB_CFG_WORD(L'0'),
        LUSB_MSFT_OS_VENDOR_REQ_CODE,
        0x00
    };

#ifdef LUSB_MSFT_OS_USE_CUSTOM_COMPATID_DESCR
    extern const t_lusb_msft_comp_id_descr lusb_compid_descr;
#else
    #if LUSB_MSFT_COMPID_FUNC_CNT > 1
        #error You must use custom compat id descriptor for composite devices
    #endif
    #ifndef LUSB_MSFT_OS_COMPATID
        #define LUSB_MSFT_OS_COMPATID {0,0,0,0,0,0,0,0}
    #endif

    static const t_lusb_msft_comp_id_descr lusb_compid_descr = { {
            sizeof(t_lusb_msft_comp_id_descr),
            0x100,
            LUSB_MSFT_OS_FEATURE_EX_COMPAT_ID,
            LUSB_MSFT_COMPID_FUNC_CNT,
            {0,0,0,0,0,0,0},
        }, { {
                0, 0,
                LUSB_MSFT_OS_COMPATID,
                {0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0}
            }
        }
    };
#endif

#if LUSB_MSFT_EXPROP_BLOB_SIZE == 0
    static const t_lusb_msft_os_exprop_descr lusb_exprop_descr = { {
            sizeof(t_lusb_msft_os_exprop_descr),
            0x100,
            LUSB_MSFT_OS_FEATURE_EX_PROPERTIES,
            LUSB_MSFT_EXPROP_CNT
        }
    };
#else
    extern const t_lusb_msft_os_exprop_descr lusb_exprop_descr;
#endif




    const uint8_t *lusb_msft_os_get_feature_descr(const t_lusb_req* req, int* length, uint8_t *tx_buf) {
        const uint8_t *wr_buf = NULL;
        int feature = req->index;
        if (feature == LUSB_MSFT_OS_FEATURE_EX_COMPAT_ID) {
            *length = lusb_compid_descr.hdr.dwLength;
            wr_buf = (const uint8_t *)&lusb_compid_descr;
        } else if (feature == LUSB_MSFT_OS_FEATURE_EX_PROPERTIES) {
            *length = lusb_exprop_descr.hdr.dwLength;
            wr_buf = (const uint8_t *)&lusb_exprop_descr;
        }
        return wr_buf;
    }

#endif
