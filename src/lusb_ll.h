/*
 * lusb_ll.h
 *
 *  Created on: 05.07.2010
 *      Author: borisov
 *
 *      Файл содержит прототипы функций, которые должен реализовать порт
 */

#ifndef __LUSB_LL_H__
#define __LUSB_LL_H__

#include "lusb_config_params.h"
#include "lusb_usbdefs.h"
#include "lusb_types.h"

typedef enum {
    LUSB_LL_EP0_STATE_DATA_TX,
    LUSB_LL_EP0_STATE_DATA_RX,
    LUSB_LL_EP0_STATE_STATUS_TX,
    LUSB_LL_EP0_STATE_STATUS_RX
} t_lusb_ll_ep0_state;



bool lusb_ll_usbirq_en(bool en);


int lusb_ll_init(void);
int lusb_ll_close(void);
void lusb_ll_connect(bool con);
int lusb_ll_get_speed(void);
void lusb_ll_progress(void);
void lusb_ll_reset(void);
int lusb_ll_set_addr(uint8_t addr);
void lusb_ll_configure(int conf);

void lusb_ll_ep0_set_state(t_lusb_ll_ep0_state state, uint16_t total_size);


/** @brief Чтение принятого пакета данных для нулевой конечной точки.

    Функция вызывается в ответ на lusb_core_recv_packet_cb для нулевой конечной
    точки, когда верхний уровень готов принять данные для обработки.
    Функция должна вернуть не более одного пакета данных (размером до LUSB_EP0_PACKET_SIZE)

    @param[in] buf      Буфер для сохранения принятого пакета
                        (хотя бы на LUSB_EP0_PACKET_SIZE байт)
    @param[in] flags    Дополнительные флаги из LUSB_PKTFLAGS_xxx.
    @param[in] rem_size Информационное поле, означает сколько осталось данных
                        до конца пакета
    @return             Размер принятого пакета.
                        Может быть и больше rem_size (эту ошибочную ситуацию
                        обрабатывает верхний уровень).
   ************************************************************************/
int lusb_ll_ep0_rd_packet(uint8_t *buf, uint32_t flags, uint16_t rem_size);

/** @brief Запись пакета данных для нулевой конечной точки.

    Функция вызывается после завершения обработки запроса, либо для
    начала передачи данных  device->host, либо для передачи статуса в запросе
    host->device, и затем, в случае, если передача выполнена не вся,
    вызывается повторно после завершения передачи предыдущего пакета, о чем
    порт сообщает с вызовом lusb_core_cb_packet_tx()


    @param[in] buf      Буфер с передаваемыми данными
    @param[in] flags    Дополнительные флаги из LUSB_PKTFLAGS_xxx.
    @param[in] size     Количество данных в буфере
    @return             Размер данных, записанных для передачи. Не может быть
                        больше size.
   ************************************************************************/
int lusb_ll_ep0_wr_packet(const uint8_t *buf, uint32_t flags, uint16_t size);

/** @brief Перевод в режим STALL нулевой конечной точки.

    Вызывается при ошибках обработы запроса. Порт должен перевести нулевую
    конечную точку в режим, когда на все пакеты, проме SETUP она отвечает
    пакетом STALL. При приходе нового SETUP конечная точка должна автоматически
    выходить из этого состояния */
void lusb_ll_ep0_stall(void);




#if LUSB_EP_NONCTL_CNT
    int lusb_ll_ep_enable(const t_usb_endpoint_descriptor *ep_descr);
    int lusb_ll_ep_disable(const t_usb_endpoint_descriptor *ep_descr);
    void lusb_ll_ep_set_stall(uint8_t ep_addr);
    void lusb_ll_ep_clr_stall(uint8_t ep_addr);

    void lusb_ll_ep_clr(const t_usb_endpoint_descriptor *ep_descr);


    #ifdef LUSB_PORT_SUPPORT_DD
        int lusb_ll_ep_add_dd(const t_usb_endpoint_descriptor *ep_descr, void *buf,
                              uint32_t length, uint32_t flags);

        int lusb_ll_ep_dd_status(const t_usb_endpoint_descriptor *ep_descr, t_lusb_dd_status* dd_st);
    #else
        int lusb_ll_ep_wr_packet(const t_usb_endpoint_descriptor *ep_descr, const uint8_t *buf, unsigned size);
        int lusb_ll_ep_rd_packet_start(const t_usb_endpoint_descriptor *ep_descr, unsigned size);
        int lusb_ll_ep_rd_packet(const t_usb_endpoint_descriptor *ep_descr, uint8_t *buf, unsigned size);
    #endif
#endif



#endif /* LUSB_LL_H_ */
