/*
 * lusb.h
 *
 *  Created on: 12.07.2010
 *      Author: borisov
 */

#ifndef __LUSB_H__
#define __LUSB_H__

#include "lusb_config_params.h"
#include "lusb_types.h"
#include "lusb_state.h"
#include "lusb_descriptors.h"
#include "lusb_appl_cb.h"
#include "lusb_ll.h"
#include "lusb_log.h"


#ifdef LUSB_USE_MSC_INTERFACE
    #include "classes/msc/lusb_msc.h"
#endif




#define lusb_dev_get_state() g_lusb_state.dev.state
#define lusb_dev_is_configured() ((g_lusb_state.dev.state & LUSB_DEVSTATE_CONFIGURED) ? 1 : 0)
#define lusb_dev_is_suspended() ((g_lusb_state.dev.state & LUSB_DEVSTATE_SUSPENDED) ? 1 : 0)
#define lusb_dev_is_iordy()     (lusb_dev_is_configured() && !lusb_dev_is_suspended())






/** Инициализация стека. Функция должна вызываться один раз вначале перед
 *  вызовом остальных функций lusb */
int lusb_init(void);
int lusb_close(void);

void lusb_progress(void);
/***************************************************************************//**
 *  Проверка, есть ли задачи, которые должны выполняться в фоновом режиме.
 *  Данная функция позволяет определить, требуется ли вызывать lusb_progress()
 *    до возникновения прерываний. Может использоваться для ухода процессора
 *    в режим пониженного энергопотребления.
 *  @return возвращает 0, если нет фоновых задач, 1 --- иначе
 ******************************************************************************/
int lusb_has_background_work(void);

/** Функция выполняет подключение (или отключение) устройтва к шине
 *  (включение подтягивающего резистора, сигнализирующего хосту о наличие
 *   устройства). После lusb_init() устройство не подключено, только после
 *  вызова данной функции начнется инициализация устройства со стороны хоста.
 *  @param[in] con     1 - выполнить подключение к шине, 0 - отключение
 */
#define lusb_connect(con)   lusb_ll_connect(con)

/** Получить скорость подключения по USB. Одно из значений из #t_lusb_speed */
#define lusb_get_speed()    lusb_ll_get_speed()

#ifdef LUSB_EP0_RX_PARTIAL
    int lusb_ctrl_rx_packet_done(void);
#endif
#ifdef LUSB_EP0_TX_PARTIAL
    int lusb_ctrl_tx_packet(const uint8_t *buff, uint16_t size);
#endif

#if LUSB_EP_NONCTL_CNT

    #define lusb_ep_descr(intf_idx, ep_idx) g_lusb_state.intf[intf_idx].ep[ep_idx].descr

    /** Получить индексы интерфейса и конечной точки в интерфейсе по ее USB-адерсу.
     *
     * @return возвращает LUSB_ERR_SUCCESS, если точка найдена, иначе - код ошибки */
    int lusb_ep_idx(uint8_t ep_addr, uint8_t *ep_intf_idx, uint8_t *ep_idx);


    //функции работы с конечными точками с поддержкой нескольких интерфейсов

    /** Добавление задания на передачу данных из буфера buf длиной length для
     *  заданной конечной точки. Направление передачи определяется направлением
     *  самой конечной точки. При успешном выполнении задание только добавляется
     *  в очередь, сама передача выполняется в фоне (по DMA или прерываниям) и
     *  данные переданного буфера не должны изменяться до завершения передачи.
     *  О завершении можно узнать либо из вызываемой callback-функции, либо
     *  по количеству доступных заданий через lusb_ep_intf_get_dd_in_progress()
     *
     *  @param[in] intf     номер интерфейса, которому принадлежит конечная точка (с 0)
     *  @param[in] ep_idx   номер конечной точки в этом интерфейсе (с 0)
     *  @param[in] buf      Указатель на буфер для обмена данными
     *  @param[in] length   Размер буфера в байтах
     *  @param[in] flags    Флаги (резерв)
     *  @return             Код ошибки
     */
    int lusb_ep_intf_add_dd(uint8_t intf, uint8_t ep_idx, void *buf, uint32_t length, uint32_t flags);

    /** Получить количество заданий на передачу, которые находятся в процессе выполнения
     *  (были успешно добавлены, но еще не завершены) для заданной конечной точки.
     *  @param[in] intf     номер интерфейса, которому принадлежит конечная точка (с 0)
     *  @param[in] ep_idx   номер конечной точки в этом интерфейсе (с 0)
     *  @return             >= 0 - количество незавершенных заданий, < 0 - код ошибки */
    int lusb_ep_intf_get_dd_in_progress(uint8_t intf, uint8_t ep_idx);

    /** Очистить все задания на передачу для заданной конечной точки. Все текущие
     *  передачи будут остановлены. При этом, для корректного вовзобновления
     *  передачи требуется посылка хостом запроса ClearFeature(Halt) для сброса
     *  конечной точки, иначе может быть рассинхронизация данных с хостом.
     *  При этом при данном запросе очистка уже выполняется автоматически
     *  (также как при разрешении конечной точки после запрета).
     *  @param[in] intf     номер интерфейса, которому принадлежит конечная точка (с 0)
     *  @param[in] ep_idx   номер конечной точки в этом интерфейсе (с 0)
     *  @return             Код ошибки
     */
    int lusb_ep_intf_clear(uint8_t intf, uint8_t ep_idx);

    /** Получить информации о состоянии выполняемого в данный момент обмена по
     *  заданной конечной точке.
     *  @param[in] intf     номер интерфейса, которому принадлежит конечная точка (с 0)
     *  @param[in] ep_idx   номер конечной точки в этом интерфейсе (с 0)
     *  @param[out] dd_st   структура, в которой будет сохранена информация о выполняемом
     *                      запросе (если нет выполняемого, то будет установлен соответтсвующее
     *                      значение в поле статуса структуры)
     *  @return             Код ошибки
     */
    int lusb_ep_intf_get_dd_status(uint8_t intf, uint8_t ep_idx, t_lusb_dd_status *dd_st);

    int lusb_ep_intf_is_halted(uint8_t intf, uint8_t ep_idx, uint8_t *halted);

    //переопределенные функции для работы с нулевым интерфейсом (для удобаства)
    #define lusb_ep_get_info(ep_i) (lusb_dev_is_configured() ? &g_lusb_state.intf[0].ep[ep_i] : NULL) //состояние кт

    #define lusb_ep_add_dd(ep_ind, buf, length, flags) lusb_ep_intf_add_dd(0, ep_ind, buf, length, flags)
    #define lusb_ep_get_dd_status(ep_ind, dd_st)       lusb_ep_intf_get_dd_status(0, ep_ind, dd_st)
    #define lusb_ep_get_dd_in_progress(ep_ind)         lusb_ep_intf_get_dd_in_progress(0, ep_ind)
    #define lusb_ep_get_state(ep_ind)                  lusb_ep_intf_get_state(0, ep_ind)
    #define lusb_ep_clear(ep_ind)                      lusb_ep_intf_clear(0, ep_ind)



#endif


#endif /* LUSB_H_ */
