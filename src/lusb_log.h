#ifndef LUSB_LOG_H
#define LUSB_LOG_H


#define LUSB_LOG_LVL_ERROR    0
#define LUSB_LOG_LVL_WARN     1
#define LUSB_LOG_LVL_INFO     2
#define LUSB_LOG_LVL_DETAIL   3
#define LUSB_LOG_LVL_DEBUG    4

#endif // LUSB_LOG_H
