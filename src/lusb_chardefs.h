#ifndef LUSB_CHARDEFS_H__
#define LUSB_CHARDEFS_H__
                                  
#define LUSB_CHAR_RUS_FIRST(ch) (uint8_t)((ch >= '�') && (ch <= '�') ? ch - '�' + 0x10 : (ch >= '�') && (ch <= '�') ? ch - '�' + 0x30 : ch == '�' ? 0x51 : ch == '�' ? 0x01 : ch & 0xFF)
#define LUSB_CHAR_RUS_SEC(ch)   ((ch >= '�') && (ch <= '�')) || ((ch >= '�') && (ch <= '�')) || (ch== '�') || (ch=='�') ? 0x04 : 0
#define LUSB_CHAR_RUS(ch)       LUSB_CHAR_RUS_FIRST(ch), LUSB_CHAR_RUS_SEC(ch)                                                    

#endif /* LUSB_CHARDEFS_H__ */
