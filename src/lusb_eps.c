/*
 * lusb.c
 *
 *  Created on: 23.07.2010
 *      Author: borisov
 *
 *  Файл содержит функции ядра, которые оно предоставляет приложению
 *  (инициализация и управления конечными точками)
 */

#include "lusb_config_params.h"
#if LUSB_EP_NONCTL_CNT

#include "lusb.h"
#include "lusb_activity_indication.h"
#include "lusb_core_cb.h"


#ifdef LUSB_USE_SOFT_DD
#include "lusb_soft_dd.h"
#define lusb_ep_priv_add_dd     lusb_soft_dd_add
#define lusb_ep_priv_clear      lusb_soft_dd_ep_clr
#define lusb_ep_priv_dd_status  lusb_soft_dd_ep_dd_status
#else
#define lusb_ep_priv_add_dd    lusb_ll_ep_add_dd
#define lusb_ep_priv_clear     lusb_ll_ep_clr
#define lusb_ep_priv_dd_status lusb_ll_ep_dd_status
#endif


#define CHECK_EP_INDEX(intf, ep_idx) (!lusb_dev_is_configured() ? LUSB_ERR_DEV_UNCONFIGURED : \
    ep_idx >= g_lusb_state.intf[intf].descr->bNumEndpoints ?  LUSB_ERR_EP_INDEX : \
                                                             LUSB_ERR_SUCCESS)


/********************************************************************************
 * добавление дескриптора на чтение/запись данных для конечной точки.
 * Функция добавляет дескриптор  с заданными параметрами в цепочку дескрипторов для ep
 * если используется режим DMA - добавляется портом, иначе - софт обработка дескриптора
 *
 * ep_idx - индекс конечной точки
 * buf    - адрес буфера для приема/передачи
 * length - длина буфера в байтах
 * flags  - флаги
 *******************************************************************************/
int lusb_ep_intf_add_dd(uint8_t intf, uint8_t ep_idx, void *buf, uint32_t length,
                        uint32_t flags) {
    int res = LUSB_ERR_SUCCESS;
#ifdef LUSB_DD_DATA_SIZE_MAX
    if (length > LUSB_DD_DATA_SIZE_MAX)
       res = LUSB_ERR_TRANSF_SIZE;
#endif

    /* нелевой указатель на буфер возможен только при передаче посылки нулевой длины */
    if ((res == LUSB_ERR_SUCCESS) && (buf == NULL) && (length != 0))
        res = LUSB_ERR_PARAMETERS;

    if (res==LUSB_ERR_SUCCESS) {
        bool en = lusb_ll_usbirq_en(false);
        int dd_cnt = lusb_ep_intf_get_dd_in_progress(intf, ep_idx);
        if (dd_cnt < 0)
            res = dd_cnt;

#ifdef LUSB_DMA_EP_DESCR_CNT
        if ((res==LUSB_ERR_SUCCESS) && (dd_cnt >= LUSB_DMA_EP_DESCR_CNT)) {
            res = LUSB_ERR_NO_FREE_DESCR;
        }
#endif

        if (res==LUSB_ERR_SUCCESS) {
            /* если нет незвершенных дескрипторов - перезапись цепочки */
            if (dd_cnt == 0) {
                flags |= LUSB_ADDBUF_FLG_REINIT;
            }

            /* увеличиваем кол-во переданных дескрипторов. Следует учитывать,
             * что хоть прерывания в данный момент запрещены, но внетри
             * lusb_ep_priv_add_dd() может произойти завершение только что
             * добавленного (или предыдущего) задания на передачу */
            g_lusb_state.intf[intf].ep[ep_idx].dd_req_cnt++;
            res = lusb_ep_priv_add_dd(g_lusb_state.intf[intf].ep[ep_idx].descr,
                                      buf, length, flags);

            /* при неудаче - возвращаем кол-во дескрипторов */
            if (res != LUSB_ERR_SUCCESS)
                g_lusb_state.intf[intf].ep[ep_idx].dd_req_cnt--;
        }
        lusb_ll_usbirq_en(en);
    }
    return res;
}

/* Получить структуру LUSB_DD_STATUS, описывающую состояние
 * выполняющегося в данных момент дескриптора
 * (если дескриптор не обрабатывается в данных момент -
 *             возвращает LUSB_ERR_DESCR_NOT_FOUND)
 *
 * ep_idx - индекс конечной точки
 * dd_st - если возвращаемое значение LUSB_ERR_SUCCES -
 *             то по этому адресу возвращается состояние обрабатываемого дескриптора
 */

int lusb_ep_intf_get_dd_status(uint8_t intf, uint8_t ep_idx, t_lusb_dd_status *dd_st) {
    int res = CHECK_EP_INDEX(intf, ep_idx);
    if (res == LUSB_ERR_SUCCESS) {
        res = lusb_ep_priv_dd_status(g_lusb_state.intf[intf].ep[ep_idx].descr, dd_st);
    }
    return res;
}


/*************************************************************************
 *  Получить кол-во дескрипторов для ep которые находятся в обработке
 *  (поставлены в цепочку, но прием/передача всех данных не закончена)
 ************************************************************************/
int lusb_ep_intf_get_dd_in_progress(uint8_t intf, uint8_t ep_idx) {
    int res = CHECK_EP_INDEX(intf, ep_idx);
    if (res==LUSB_ERR_SUCCESS) {
        t_lusb_dd_cnt req_cnt = g_lusb_state.intf[intf].ep[ep_idx].dd_req_cnt;
        t_lusb_dd_cnt cpl_cnt = g_lusb_state.intf[intf].ep[ep_idx].dd_cpl_cnt;

        res = (int)((t_lusb_dd_cnt)(req_cnt - cpl_cnt));
    }
    return res;
}



int lusb_ep_intf_clear(uint8_t intf, uint8_t ep_idx) {
    int res = CHECK_EP_INDEX(intf, ep_idx);
    if (res == LUSB_ERR_SUCCESS) {
        bool int_en = lusb_ll_usbirq_en(false);
        lusb_ep_priv_clear(g_lusb_state.intf[intf].ep[ep_idx].descr);
        g_lusb_state.intf[intf].ep[ep_idx].dd_cpl_cnt = g_lusb_state.intf[intf].ep[ep_idx].dd_req_cnt;
        lusb_ll_usbirq_en(int_en);
    }
    return res;
}

int lusb_ep_intf_is_halted(uint8_t intf, uint8_t ep_idx, uint8_t *halted) {
    int res = CHECK_EP_INDEX(intf, ep_idx);
    if (res == LUSB_ERR_SUCCESS) {
        *halted = g_lusb_state.intf[intf].ep[ep_idx].halted;
    }
    return res;
}


int lusb_ep_idx(uint8_t ep_addr, uint8_t *ep_intf_idx, uint8_t *ep_idx) {
    int res = lusb_dev_is_configured() ? LUSB_ERR_SUCCESS : LUSB_ERR_DEV_UNCONFIGURED;
    if (res == LUSB_ERR_SUCCESS) {
        int fnd = 0;
        for (uint8_t intf = 0; (intf < LUSB_INTERFACE_CNT) && !fnd; intf++) {
            for (uint8_t ep=0; (ep <  g_lusb_state.intf[intf].descr->bNumEndpoints) && !fnd; ep++) {
                if (lusb_ep_descr(intf, ep)->bEndpointAddress == ep_addr) {
                    if (ep_intf_idx!=NULL)
                        *ep_intf_idx = intf;
                    if (ep_idx!=NULL)
                        *ep_idx = ep;
                    fnd = 1;
                }
            }
        }
        if (!fnd)
            res = LUSB_ERR_EP_DISABLED;
    }
    return res;
}


void lusb_core_cb_dd_done(uint8_t ep_addr, t_lusb_dd_event event, const t_lusb_dd_status *pDD) {
    uint8_t intf, ep_idx;
    if (lusb_ep_idx(ep_addr, &intf, &ep_idx)==LUSB_ERR_SUCCESS) {
        int event_processed = 0;
        /* передача данных блока DMA завершена -> сохраняем параметры */
        if (event == LUSB_DD_EVENT_EOT) {
            lusb_activity_indication();
            if (g_lusb_state.intf[intf].ep[ep_idx].dd_cpl_cnt != g_lusb_state.intf[intf].ep[ep_idx].dd_req_cnt)
                g_lusb_state.intf[intf].ep[ep_idx].dd_cpl_cnt++;
        }

#ifdef LUSB_USE_MSC_INTERFACE
        if (ep_addr == LUSB_EP_MSC_IN_ADDR) {
            lusb_msc_data_tx(event, pDD);
            event_processed = 1;
        } else if (ep_addr == LUSB_EP_MSC_OUT_ADDR) {
            lusb_msc_data_rx(event, pDD);
            event_processed = 1;
        }
#endif
        //вызываем пользовательский callback
#ifdef LUSB_APPL_CB_DD_EVENT
        if (!event_processed) {
            lusb_appl_cb_dd_event(intf, ep_idx, event, pDD);
            event_processed = 1;
        }
#endif
    }

}

#endif
