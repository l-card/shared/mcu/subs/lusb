#ifndef LUSB_LL_PORT_PARAMS_H
#define LUSB_LL_PORT_PARAMS_H

#include "lusb_config.h"

#ifndef LUSB_DMA_EP_DESCR_CNT
    #define LUSB_DMA_EP_DESCR_CNT 16
#endif


/* Должно быть определено, если порт поддерживает высокую скорость.
   Используется для реализации дополнительных дескрипторов */
#define LUSB_SUPPORT_HIGH_SPEED 1

/* В порту реально отключена возможность аппаратной посылки нулевого пакета, поэтому тут 0
   (Можно ли ее включить, не нарушив работу в частности частичного обмена - вопрос
   отдельного рассмотрения) */
#define LUSB_PORT_EP0_ZEROLEN_TX_AUTO 0

#define LUSB_PORT_SUPPORT_DD 1

#define LUSB_EP_NONCTL_CNT_MAX 5

/* максимальный размер данных в одном дескрипторе на передачу */
#define LUSB_DD_DATA_SIZE_MAX   (4096*4)


#endif // LUSB_LL_PORT_PARAMS_H
