/*
 * lusb_ll.c
 *
 *  Created on: 05.07.2010
 *      Author: borisov
 */

#include "lusb_ll.h"
#include "lusb_usbdefs.h"
#include "lusb_config_params.h"
#include "lpc43xx_usb_bits.h"
#include "lusb_core_cb.h"
#include "chip.h"

#include <string.h>


#define LUSB_EP_QHEAD(ep_addr)  ((((ep_addr) & USB_EPDIR)==USB_EPDIR_IN)? \
                                    &f_epQueues[(ep_addr)&USB_EPNUM_MSK].in : \
                                    &f_epQueues[(ep_addr)&USB_EPNUM_MSK].out)

#define LPC_EP_DIR_BIT_OFFS(ep_addr) ((((ep_addr) & USB_EPDIR)==USB_EPDIR_IN) ? 16 : 0)

#define LPC_EP_REG_MSK(ep_addr)  (0x1UL << (((ep_addr)&USB_EPNUM_MSK) + LPC_EP_DIR_BIT_OFFS(ep_addr)))

#define LUSB_EP_DTD_LIST(ep_addr) ((((ep_addr) & USB_EPDIR)==USB_EPDIR_IN)? \
    &f_ep_dtd[(ep_addr-1)&USB_EPNUM_MSK].in : \
    &f_ep_dtd[(ep_addr-1)&USB_EPNUM_MSK].out)



#define DTD_TERMINATE 0xDEAD001


#if LUSB_DMA_EP_DESCR_CNT % 8 != 0
    #error "ep descr count must be multiple of 8"
#endif


/*
 *  USB device endpoint queue head structure
 */
typedef struct  {
    uint32_t epCap;   //! Endpoint Capabilities/Characteristics
    uint32_t currentDtd;                //! Current dTD Pointer
    uint32_t nextDtd;                   //! Next dTD Pointer
    uint32_t dtdToken;                  //! Token
    uint32_t dtdBuffer[5];              //! Buffer Pointers
    uint32_t reserved;
    t_lusb_req setupBuffer;             //! Set-up Buffer
    uint32_t reserved1[3];
    uint16_t put_pos;
    uint16_t get_pos;
} t_queueHead;

// USB endpoint pair sturcture
typedef struct {
    t_queueHead out;
    t_queueHead in;
} t_EpPair;

/*
 *  USB device endpoint transfer descriptor
 */
typedef struct  {
    uint32_t nextDtd;       //! Next data transfer descriptor
    uint32_t dtdToken;      //! Token
    uint32_t dtdBuffer[5];  //! Buffers for data to be transferred
    uint32_t totalSize;    
} t_usbDtd;


typedef struct {
    t_usbDtd dtd[LUSB_DMA_EP_DESCR_CNT];
    uint32_t addr[LUSB_DMA_EP_DESCR_CNT];
} t_epDTDList;

typedef struct {
    t_epDTDList out;
    t_epDTDList in;    
} t_EpDtdListPair;

#define LSPEC_ALIGNMENT 2048
#include "lcspec_align.h"
volatile t_EpPair f_epQueues[LPC_USB_ENDPOINT_CNT];
#undef LSPEC_ALIGNMENT


#define LSPEC_ALIGNMENT 32
#include "lcspec_align.h"
static t_usbDtd f_ep0_tx_dtd;
#include "lcspec_align.h"
static t_usbDtd f_ep0_rx_dtd;
#if LPC_USB_ENDPOINT_CNT > 1
    #include "lcspec_align.h"
    static t_EpDtdListPair f_ep_dtd[LPC_USB_ENDPOINT_CNT-1];
#endif


static uint8_t f_ep0_rx_buf[LUSB_EP0_PACKET_SIZE];

static t_lpc_usb_devs_regs *usb_regs = (t_lpc_usb_devs_regs*)LPC_USB0_BASE;



static void f_ep_head_init(uint8_t ep_addr, int32_t nextDtd, uint32_t maxPacket) {
    volatile t_queueHead *head = LUSB_EP_QHEAD(ep_addr);
    uint32_t cap=0;
    if (!(ep_addr & USB_EPNUM_MSK)) {
        cap = LPC_USB_QH_EPCAP_IOS;
    }

    head->epCap = cap | LPC_USB_QH_EPCAP_ZLT | BF_SET(LPC_USB_QH_EPCAP_MAX_PACKET, maxPacket);

    head->currentDtd = 0;  // Will be updated by the controller with nextDtd on startup
    head->nextDtd = nextDtd;   // This will be the first dTD that the controller uses
    head->dtdToken = 0;    // Will be updated with the token from nextdTD on startup
}


static void f_init_dtd(t_usbDtd *dtd, uint32_t size, const void * buf, int irq) {
    unsigned tocken = irq ? LPC_USB_DTD_TOKEN_IOC : 0;
    dtd->nextDtd = 0xDEAD0001;   //! invalidate nextDtd (set T-bit)

    dtd->dtdToken = (tocken | BF_SET(LPC_USB_DTD_TOKEN_TOTAL_BYTES, size)
                        | LPC_USB_DTD_TOKEN_STATUS_ACTIVE);
    dtd->totalSize = size;

    //! Calculate & write the buffer pointers/
    // Split input buffer in 5 4K buffers
    int i;
    for (i = 0; i < 5; i++) {
        dtd->dtdBuffer[i] = (uint32_t) buf;

        // next buffer starts at 4K next 4K boundary
        buf = (uint32_t *) ((uint32_t) buf - (uint32_t) buf % 0x1000 + 0x1000);
    }
}

static void f_start_setup_rx_data(void) {
    f_init_dtd(&f_ep0_rx_dtd, sizeof(f_ep0_rx_buf), f_ep0_rx_buf, 1);

    //! - Put the OUT transfer descriptor on the queue head
    f_epQueues[0].out.nextDtd = (uint32_t) &f_ep0_rx_dtd;

    //! - Prime prime endpoints (OUT)
    usb_regs->ENDPTPRIME = LPC_USB_ENDPTPRIME_PERB(0);
}





/************************************************************************
  разрешение (en = 1) или запрещение (en = 0) прерываний по USB
  возвращает 1, если прерывания до этого были уже разрешены, иначе 0
  ***********************************************************************/
bool lusb_ll_usbirq_en(bool en) {
    const bool res = !!(NVIC->ISER[((uint32_t)(USB0_IRQn) >> 5)] & (1 << ((uint32_t)(USB0_IRQn) & 0x1F)));
    if (en)
        NVIC_EnableIRQ(USB0_IRQn);
    else
        NVIC_DisableIRQ(USB0_IRQn);
    return res;
}


/**************************************************************************************
    сброс регистров USB контроллера - выполняется при инициализации и по bus reset
 **************************************************************************************/
void lusb_ll_reset(void) {
    /* Clear all setup token semaphores */
    usb_regs->ENDPTSETUPSTAT = usb_regs->ENDPTSETUPSTAT;
    /* Clear all complete status bits */
    usb_regs->ENDPTCOMPLETE = usb_regs->ENDPTCOMPLETE;

    while (usb_regs->ENDPTPRIME!=0) {}
    /* Flush all endpoints */
    usb_regs->ENDPTFLUSH = 0xFFFFFFFF;
    //! - Wait for host to stop signaling reset
    /*while(usb_regs->PORTSC1 & LPC_USB_PORTSC1_PR) {
    }*/
}


/***************************************************************************************
 *  Инициализация usb-интерфейса (PCON и PLL1 уже должны быть настроены в startup!!!!!)
 *************************************************************************************/
int lusb_ll_init(void) {

    int res = LUSB_ERR_SUCCESS;

    LPC_CGU->BASE_CLK[CLK_BASE_USB0] =  LPC_BASE_USB0_CFG;
    LPC_CCU_CFG(CLK_MX_USB0, 1);
    LPC_CCU_CFG(CLK_USB0,    1);

    // разрешение USB0 Phy
    LPC_CREG->CREG0 &= ~(1 << 5);

    usb_regs->USBMODE = LPC_USB_USBMODE_CM_DEVICE | LPC_USB_USBMODE_SLOM;

    usb_regs->ENDPOINTLISTADDR = (uint32_t) &f_epQueues[0];

    f_ep_head_init(USB_EPDIR_IN, DTD_TERMINATE, LUSB_EP0_PACKET_SIZE);
    f_ep_head_init(USB_EPDIR_OUT, DTD_TERMINATE, LUSB_EP0_PACKET_SIZE);

#ifdef LUSB_INTERRUPT
    usb_regs->USBINTR = LPC_USB_USBINTR_UE | LPC_USB_USBINTR_UEE |
            LPC_USB_USBINTR_URE;


    lusb_ll_usbirq_en(true);
#endif

    return res;
}

int lusb_ll_close(void) {
    // запрет USB0 Phy
    LPC_CREG->CREG0 |= (1 << 5);

    LPC_CCU_CFG(CLK_MX_USB0, 0);
    LPC_CCU_CFG(CLK_USB0,    0);

    return 0;
}

/*****************************************************************************************************
 *  установка адреса устростройства (1-127, 0 - при сбросе до того как будет сконфигурировано)
 *  (вызывается при приеме дескриптора SET_ADDRESS)
 *****************************************************************************************************/
int lusb_ll_set_addr(uint8_t addr) {
    usb_regs->DEVICEADDR = BF_SET(LPC_USB_DEVICEADDR_USBADR, addr);
    return 0;
}

/***********************************************************************************************
 * установка признака, что устройство сконфигурировано
 * (вызывается на прием дескриптора SET_CONFIGURATION)
 *   conf - номер конфигурации (если 0 - не сконфигурировано, иначе - сконфигурировано)
 ***********************************************************************************************/
void lusb_ll_configure(int conf) {

}

//подключение к хосту (вкл. поддтяжки)
void lusb_ll_connect(bool con) {
    if (con) {
        usb_regs->USBCMD |= LPC_USB_USBCMD_RS;
    } else {
        usb_regs->USBCMD &= ~LPC_USB_USBCMD_RS;
    }
}


int lusb_ll_get_speed(void) {
    int speed = BF_GET(usb_regs->PORTSC1, LPC_USB_PORTSC1_PSPD);
    if (speed==LPC_USB_PORTSC1_PSPD_FULL)
        return LUSB_SPEED_FULL;
    if (speed==LPC_USB_PORTSC1_PSPD_LOW)
        return LUSB_SPEED_LOW;
    if (speed==LPC_USB_PORTSC1_PSPD_HIGH)
        return LUSB_SPEED_HIGH;
    return LUSB_SPEED_UNKNOWN;
}



int lusb_ll_ep0_rd_packet(uint8_t *buf, uint32_t flags, uint16_t rem_size) {
    int size = 0;
    uint8_t epnum = 0;
    volatile t_queueHead *head =LUSB_EP_QHEAD(USB_EP_OUT(epnum));
    if (flags & LUSB_PKTFLAGS_SETUP) {
        //t_lusb_req *set_req = (t_lusb_req*)(buff);
        /*! copy setup data from queue head to buffer and check semaphore\n
         * SUTW will be cleared when a new setup packet arrives.\n
         * To avoid data corruption, we check the SUTW flag after the copy\n
         * and when it is cleared, we repeat the copy to get the new setup data.
         */
        do {
            usb_regs->USBCMD |= LPC_USB_USBCMD_SUTW;
            memcpy(buf, (void*)&head->setupBuffer, sizeof(t_lusb_req)); //! - copy the setup data
        } while (!(usb_regs->USBCMD & LPC_USB_USBCMD_SUTW));   //! - repeat if SUTW got cleared

        //! - Clear setup identification
        usb_regs->ENDPTSETUPSTAT = LPC_USB_ENDPTSETUPSTAT(epnum);

        //! - Clear Setup tripwire bit
        usb_regs->USBCMD &= ~LPC_USB_USBCMD_SUTW;

        //! - Wait for ENDPSETUPSTAT to clear.\n
        //! It must be clear before the status phase/data phase can be primed
        while(usb_regs->ENDPTSETUPSTAT & LPC_USB_ENDPTSETUPSTAT(epnum));

        /* сброс прерываний от предыдущего управляющего запроса */
        usb_regs->ENDPTCOMPLETE = LPC_USB_ENDPTCOMPLETE_ERCE(epnum) | LPC_USB_ENDPTCOMPLETE_ETCE(epnum);

        size = sizeof(t_lusb_req);
        f_start_setup_rx_data();
    } else {
        if (!(f_ep0_rx_dtd.dtdToken &  LPC_USB_DTD_TOKEN_STATUS_ACTIVE)) {
            size = f_ep0_rx_dtd.totalSize - ((f_ep0_rx_dtd.dtdToken>>16)&0x7FFF);
            memcpy(buf, f_ep0_rx_buf, size);
            if (rem_size > (uint32_t)size)
                f_start_setup_rx_data();
        }
    }

    return size;
}

int lusb_ll_ep0_wr_packet(const uint8_t *buf, uint32_t flags, uint16_t size) {
    f_init_dtd(&f_ep0_tx_dtd, size, buf, 1);

    //! - Put the IN transfer descriptor on the queue head.
    f_epQueues[0].in.nextDtd = (uint32_t) &f_ep0_tx_dtd;

    //! - Prime prime endpoints (OUT)
    usb_regs->ENDPTPRIME = LPC_USB_ENDPTPRIME_PETB(0);

    while (usb_regs->ENDPTPRIME & (LPC_USB_ENDPTPRIME_PETB(0)))
    {}

    return size;
}



//stall для управляющей нулевой конечной точки (сразу и на in и на out)
void lusb_ll_ep0_stall(void) {
    usb_regs->ENDPTCTRL[0] |= LPC_USB_ENDPTCTRL_RXS | LPC_USB_ENDPTCTRL_TXS;
}




#if LUSB_EP_NONCTL_CNT

//перевод конечной точки в stall режим с адресом ep_addr
void lusb_ll_ep_set_stall(uint8_t ep_addr) {
    uint8_t offs = LPC_EP_DIR_BIT_OFFS(ep_addr);
    usb_regs->ENDPTCTRL[ep_addr & USB_EPNUM_MSK] |= LPC_USB_ENDPTCTRL_RXS << offs;
}

//сброс stall-режима для конечной точки с адресом ep_addr
void lusb_ll_ep_clr_stall(uint8_t ep_addr) {
    uint8_t offs = LPC_EP_DIR_BIT_OFFS(ep_addr);
    usb_regs->ENDPTCTRL[ep_addr & USB_EPNUM_MSK] &= ~(LPC_USB_ENDPTCTRL_RXS << offs);
}



static void f_ep_dd_list_clear(uint8_t ep_addr) {
    volatile t_queueHead *head = LUSB_EP_QHEAD(ep_addr);
    t_epDTDList *pDTDList = LUSB_EP_DTD_LIST(ep_addr);

    head->get_pos = 0;
    head->put_pos = 0;

    for (size_t i=0; i < LUSB_DMA_EP_DESCR_CNT; i++) {
        pDTDList->dtd[i].dtdToken = 0;
        pDTDList->addr[i] = 0;
    }
}


//разрешение конечной точки с адресом ep_addr
int lusb_ll_ep_enable(const t_usb_endpoint_descriptor *ep_descr) {
    uint8_t epnum = ep_descr->bEndpointAddress & USB_EPNUM_MSK;
    uint8_t offs = LPC_EP_DIR_BIT_OFFS(ep_descr->bEndpointAddress);


    f_ep_head_init(ep_descr->bEndpointAddress, DTD_TERMINATE, ep_descr->wMaxPacketSize);

    f_ep_dd_list_clear(ep_descr->bEndpointAddress);
    usb_regs->ENDPTCTRL[epnum] = (usb_regs->ENDPTCTRL[epnum] & ~(0xFFFFUL << offs)) |
              (BF_SET(LPC_USB_ENDPTCTRL_RXT, ep_descr->bmAttributes & 3) |
              LPC_USB_ENDPTCTRL_RXR | LPC_USB_ENDPTCTRL_RXE) << offs;
    return 0;
}

//запрет конечной точки с адресом ep_addr
int lusb_ll_ep_disable(const t_usb_endpoint_descriptor *ep_descr) {
    uint8_t epnum = ep_descr->bEndpointAddress & USB_EPNUM_MSK;
    uint8_t offs = LPC_EP_DIR_BIT_OFFS(ep_descr->bEndpointAddress);
    usb_regs->ENDPTCTRL[epnum] &= ~(LPC_USB_ENDPTCTRL_RXE << offs);
    return 0;
}



/***********************************************************************************
 *******************            Функции для работы с DMA      **********************
 ***********************************************************************************/

static void f_ep_empty_add_dd(uint8_t ep_addr, volatile t_queueHead *head, t_usbDtd *dtd) {
    uint32_t ep_mask = LPC_EP_REG_MSK(ep_addr);

    /* writ dQH next pointer and clear terminate */
    head->nextDtd = (uint32_t)dtd;
    /* clear active and halt bits */
    head->dtdToken = 0;
    /* prime ep */
    usb_regs->ENDPTPRIME = ep_mask;
}


static void f_get_dd_status(t_epDTDList *dtdList, unsigned pos, t_lusb_dd_status *dd_st) {
    t_usbDtd *dtd = &dtdList->dtd[pos];
    dd_st->length=dtd->totalSize ;
    dd_st->trans_cnt = dd_st->length - BF_GET(dtd->dtdToken, LPC_USB_DTD_TOKEN_TOTAL_BYTES);   
    dd_st->last_addr = dtdList->addr[pos] + dd_st->trans_cnt;
    dd_st->flags = 0;

    if (!(dtd->dtdToken & (LPC_USB_DTD_TOKEN_STATUS_HALTED |
                           LPC_USB_DTD_TOKEN_STATUS_BUF_ERR |
                           LPC_USB_DTD_TOKEN_STATUS_XACT_ERR |
                           LPC_USB_DTD_TOKEN_STATUS_ACTIVE))) {
        dd_st->status = LUSB_DDSTATUS_CPL_SUCCESS;
    } else if (dtd->dtdToken & LPC_USB_DTD_TOKEN_STATUS_BUF_ERR) {
        dd_st->status = LUSB_DDSTATUS_CPL_OVERRUN;
    } else if (dtd->dtdToken & LPC_USB_DTD_TOKEN_STATUS_ACTIVE){
        dd_st->status = LUSB_DDSTATUS_IN_PROGRESS;
    } else {
        dd_st->status = LUSB_DDSTATUS_SYSTEM_ERR;
    }
}



static void f_lusb_dma_cpl_check(uint8_t ep_addr) {
    volatile t_queueHead *head = LUSB_EP_QHEAD(ep_addr);
    t_epDTDList *pDTDList = LUSB_EP_DTD_LIST(ep_addr);
    uint16_t get_pos = head->get_pos;
    t_usbDtd *dtd = &pDTDList->dtd[get_pos];
    t_lusb_dd_status dd_st;

    while (!(dtd->dtdToken & LPC_USB_DTD_TOKEN_STATUS_ACTIVE) &&
           (pDTDList->addr[get_pos]!=0)) {
        f_get_dd_status(pDTDList, get_pos, &dd_st);
        pDTDList->addr[get_pos]=0;
        lusb_core_cb_dd_done(ep_addr, LUSB_DD_EVENT_EOT, &dd_st);

        if (++get_pos==LUSB_DMA_EP_DESCR_CNT)
            get_pos = 0;
        dtd = &pDTDList->dtd[get_pos];


        /* если передача остановлена, но еще есть дескрипторы - перезапускаем */
        if (head->dtdToken & LPC_USB_DTD_TOKEN_STATUS_HALTED)  {
            if (dtd->dtdToken & LPC_USB_DTD_TOKEN_STATUS_ACTIVE) {
                f_ep_empty_add_dd(ep_addr, head, dtd);
            }
        }
    }
    head->get_pos = get_pos;
}


/***************************************************************************************
 *   Добавление нового буфера (DMA-дескриптора) в список дескрипторов для конечной точки
 *      ep_descr  - (in) структура с дескриптором конечной точки, для которой добавляется дескриптор
 *      buff      - (in) буфер на передачу/прием
 *      length    - (in) резмер буфера в байтах
 *      flags     - (in) флаги:
 *                       LUSB_ADDBUF_FLG_AUTOSTART - для ep in - запуск dma,
 *                                    если до добавления не было дескрипторов
 *   Возвращаемое значение:
 *      код ошибки
 *****************************************************************************************/
int lusb_ll_ep_add_dd(const t_usb_endpoint_descriptor *ep_descr, void *buf,
                          uint32_t length, uint32_t flags) {
    volatile t_queueHead *head = LUSB_EP_QHEAD(ep_descr->bEndpointAddress);
    t_epDTDList *pDTDList = LUSB_EP_DTD_LIST(ep_descr->bEndpointAddress);
    uint32_t ep_mask = LPC_EP_REG_MSK(ep_descr->bEndpointAddress);

    t_usbDtd *dtd = &pDTDList->dtd[head->put_pos];

    /* нулевой указатель в адресе используется как признак недействительности
       в буфере, поэтому заменяем его на другой (на случай, если добавляем
       нулевой указатель с нулевым размером для посылки Zero-length packet) */
    pDTDList->addr[head->put_pos] = buf != NULL ? (uint32_t)buf : (uint32_t)(-1);
    f_init_dtd(dtd, length, buf, 1);
    if (flags & LUSB_ADDBUF_FLG_REINIT) {
        f_ep_empty_add_dd(ep_descr->bEndpointAddress, head, dtd);
    } else {
        uint16_t prev_pos = head->put_pos==0 ? LUSB_DMA_EP_DESCR_CNT-1 : head->put_pos-1;
        /* 1. add dd to list */
        pDTDList->dtd[prev_pos].nextDtd = (uint32_t)dtd;
        /* 2. if prime = 1 => done */
        if (!(usb_regs->ENDPTPRIME & ep_mask)) {
            uint32_t stat;
            do {
                /* 3. set ATDTW bit in USBCMD to 1*/
                usb_regs->USBCMD |= LPC_USB_USBCMD_ATDTW;
                /* 4. read status bit in ENDPTSTAT */
                stat = usb_regs->ENDPTSTAT & ep_mask;
                /* 5. if ATDTW 0 => to step 3 */
            } while (!(usb_regs->USBCMD & LPC_USB_USBCMD_ATDTW));
            /* 6. write ATDTW bit in USBCMD to 0 */
            usb_regs->USBCMD &= ~LPC_USB_USBCMD_ATDTW;
            /* 7/8 if stat = 0 => go to empty */
            if (!stat) {
                f_ep_empty_add_dd(ep_descr->bEndpointAddress, head, dtd);
            }
        }
    }

    if (++head->put_pos==LUSB_DMA_EP_DESCR_CNT)
        head->put_pos=0;


    return LUSB_ERR_SUCCESS;
}

/**************************************************************************************
 *  Получить состояние выполняющегося в настоящий момент дескриптора для
 *  конечной точки с адресом ep_addr
 *  Если не выполняется DMA-передача, то возвращает ошибку LUSB_ERR_DESCR_NOT_FOUND
 * ************************************************************************************/
int lusb_ll_ep_dd_status(const t_usb_endpoint_descriptor *ep_descr, t_lusb_dd_status *dd_st) {
    volatile t_queueHead *head = LUSB_EP_QHEAD(ep_descr->bEndpointAddress);
    t_epDTDList *pDTDList = LUSB_EP_DTD_LIST(ep_descr->bEndpointAddress);
    uint16_t get_pos = head->get_pos;
    t_usbDtd *dtd = &pDTDList->dtd[get_pos];
    int res = LUSB_ERR_SUCCESS;

    if (!(dtd->dtdToken & LPC_USB_DTD_TOKEN_STATUS_ACTIVE)) {
        res = LUSB_ERR_DESCR_NOT_FOUND;
    } else {
        f_get_dd_status(pDTDList, get_pos, dd_st);
    }

    return res;
}


void lusb_ll_ep_clr(const t_usb_endpoint_descriptor *ep_descr) {
    uint32_t ep_msk = LPC_EP_REG_MSK(ep_descr->bEndpointAddress);

    do {
        usb_regs->ENDPTFLUSH = ep_msk;

        while (usb_regs->ENDPTFLUSH & ep_msk) {}

    } while (usb_regs->ENDPTSTAT & ep_msk);

    f_ep_dd_list_clear(ep_descr->bEndpointAddress);
}

#endif






/* **********************************************************************************
 * опрос флагов прерывания USB-модуля
 * обработка и вызов требуемых callback-функций lusb_core
 * **********************************************************************************/
static void f_lusb_ll_isr(void) {
    uint32_t status = usb_regs->USBSTS;
    if (status) {
        usb_regs->USBSTS = status;

        if (status & LPC_USB_USBSTS_URI) {
            lusb_ll_reset();
            lusb_core_cb_reset();
        }

        if (status & LPC_USB_USBSTS_UI) {
            uint32_t ep_cpl, setup;

            setup = usb_regs->ENDPTSETUPSTAT & LPC_USB_ENDPTSETUPSTAT(0);
            if (setup) {  
                /* очищаем все незаконченные передачи по EP, чтобы не повлияли на
                 * новый запрос */
                usb_regs->ENDPTFLUSH = LPC_USB_ENDPTFLUSH_FERB(0) | LPC_USB_ENDPTFLUSH_FETB(0);

                while (usb_regs->ENDPTFLUSH & (LPC_USB_ENDPTFLUSH_FERB(0) | LPC_USB_ENDPTFLUSH_FETB(0))) {};

                f_epQueues[0].in.dtdToken = f_epQueues[0].out.dtdToken = 0;
                f_epQueues[0].in.nextDtd = f_epQueues[0].out.nextDtd = 0;
                lusb_core_cb_packet_rx(0, LUSB_PKTFLAGS_SETUP);
            }

            ep_cpl = usb_regs->ENDPTCOMPLETE;

            if (ep_cpl) {
                unsigned ep_num;
                usb_regs->ENDPTCOMPLETE = ep_cpl;

                for (ep_num=0; (ep_num < LPC_USB_ENDPOINT_CNT) && ep_cpl; ep_num++) {
                    if (ep_cpl & LPC_USB_ENDPTCOMPLETE_ETCE(ep_num)) {
                        if (ep_num==0) {
                            //! Check Active flag in OUT dTD and wait for it to clear.
                            //! This is the only true indication that the transfer did complete.
                            while ((f_ep0_tx_dtd.dtdToken & LPC_USB_DTD_TOKEN_STATUS_ACTIVE)
                                   && !(usb_regs->ENDPTSETUPSTAT &  LPC_USB_ENDPTSETUPSTAT(0)));
                            if (!(usb_regs->ENDPTSETUPSTAT &  LPC_USB_ENDPTSETUPSTAT(0))) {
                                lusb_core_cb_packet_tx(USB_EP_IN(0), 0);
                            }
#if LUSB_EP_NONCTL_CNT
                        } else {
                            f_lusb_dma_cpl_check(ep_num | USB_EPDIR_IN);
#endif
                        }
                        ep_cpl &= ~LPC_USB_ENDPTCOMPLETE_ETCE(ep_num);
                    }

                    if (ep_cpl & LPC_USB_ENDPTCOMPLETE_ERCE(ep_num)) {

                        if (ep_num==0) {
                            while ((f_ep0_rx_dtd.dtdToken & LPC_USB_DTD_TOKEN_STATUS_ACTIVE)
                                   && !(usb_regs->ENDPTSETUPSTAT &  LPC_USB_ENDPTSETUPSTAT(0)));

                            if (!(usb_regs->ENDPTSETUPSTAT &  LPC_USB_ENDPTSETUPSTAT(0))) {
                                lusb_core_cb_packet_rx(USB_EP_OUT(0), 0);
                            }
#if LUSB_EP_NONCTL_CNT
                        } else {
                            f_lusb_dma_cpl_check(ep_num | USB_EPDIR_OUT);
#endif
                        }
                        ep_cpl &= ~LPC_USB_ENDPTCOMPLETE_ERCE(ep_num);
                    }
                }
            }



        }
    }
}



#ifdef LUSB_INTERRUPT
#include "lcspec_interrupt.h"
void USB0_IRQHandler(void) {
    f_lusb_ll_isr();
}
#endif





/**************************************************************************
 *  Выполнение фоновых задач для порта
 *  очистка памяти для дескрипторов
 *  и отслеживание дисконнекта/конекта к usb-шине
 *  (так как аппаратно коннект не отслеживается...)
 * ************************************************************************ */
void lusb_ll_progress(void) {
    static uint8_t susp = 0;
    /* так как прерывание есть только на вхождение в SUSP, то проверяем
     * его в фоне */
    bool en = lusb_ll_usbirq_en(false);
    uint8_t susp1 = usb_regs->PORTSC1 & LPC_USB_PORTSC1_SUSP ? 1 : 0;
    if (susp1!=susp) {
        susp = susp1;
        lusb_core_cb_susp_changed(susp);
    }
    lusb_ll_usbirq_en(en);
#ifndef LUSB_INTERRUPT
    f_lusb_ll_isr();
#endif

}

void lusb_ll_ep0_set_state(t_lusb_ll_ep0_state state, uint16_t total_size) {

}
