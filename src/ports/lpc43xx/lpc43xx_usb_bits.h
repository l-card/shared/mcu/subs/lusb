#ifndef LPC43XX_USB_BITS_H
#define LPC43XX_USB_BITS_H

#include "lusb_ll_port_params.h"

#define LPC_USB_ENDPOINT_CNT  (LUSB_EP_NONCTL_CNT + 1)



typedef struct {                               /*!< USB Structure         */
    const volatile uint32_t  RESERVED0[64];
    const volatile uint32_t  CAPLENGTH;        /*!< Capability register length */
    const volatile uint32_t  HCSPARAMS;        /*!< Host controller structural parameters */
    const volatile uint32_t  HCCPARAMS;        /*!< Host controller capability parameters */
    const volatile uint32_t  RESERVED1[5];
    const volatile uint32_t  DCIVERSION;       /*!< Device interface version number */
    const volatile uint32_t  RESERVED2[7];
          volatile uint32_t  USBCMD;          /*!< USB command (device mode) */
          volatile uint32_t  USBSTS;          /*!< USB status (device mode) */
          volatile uint32_t  USBINTR;         /*!< USB interrupt enable (host mode) */
    const volatile uint32_t  FRINDEX;         /*!< USB frame index (device mode) */
    const volatile uint32_t  RESERVED3;
          volatile uint32_t  DEVICEADDR;      /*!< USB device address     */
          volatile uint32_t  ENDPOINTLISTADDR;    /*!< Address of endpoint list in memory (device mode) */
          volatile uint32_t  RESERVED3_1;
          volatile uint32_t  BURSTSIZE;                /*!< Programmable burst size */
          volatile uint32_t  TXFILLTUNING;            /*!< Host transmit pre-buffer packet tuning (host mode) */
    const volatile uint32_t  RESERVED4[2];
          volatile uint32_t  ULPIVIEWPORT;            /*!< ULPI viewport          */
          volatile uint32_t  BINTERVAL;                /*!< Length of virtual frame */
          volatile uint32_t  ENDPTNAK;                /*!< Endpoint NAK (device mode) */
          volatile uint32_t  ENDPTNAKEN;                /*!< Endpoint NAK Enable (device mode) */
    const volatile uint32_t  RESERVED5;
          volatile uint32_t  PORTSC1;            /*!< Port 1 status/control (device mode) */
    const volatile uint32_t  RESERVED6[7];
          volatile uint32_t  OTGSC;                    /*!< OTG status and control */
          volatile uint32_t  USBMODE;            /*!< USB mode (device mode) */
          volatile uint32_t  ENDPTSETUPSTAT;            /*!< Endpoint setup status  */
          volatile uint32_t  ENDPTPRIME;                /*!< Endpoint initialization */
          volatile uint32_t  ENDPTFLUSH;                /*!< Endpoint de-initialization */
          volatile uint32_t  ENDPTSTAT;                /*!< Endpoint status        */
          volatile uint32_t  ENDPTCOMPLETE;            /*!< Endpoint complete      */
          volatile uint32_t  ENDPTCTRL[LPC_USB_ENDPOINT_CNT];  /*!< Endpoint control 0     */
} t_lpc_usb_devs_regs;


/* USBCMD */
#define LPC_USB_USBCMD_RS              (0x1UL << 0)
#define LPC_USB_USBCMD_RST             (0x1UL << 1)
#define LPC_USB_USBCMD_SUTW            (0x1UL << 13)
#define LPC_USB_USBCMD_ATDTW           (0x1UL << 14)
#define LPC_USB_USBCMD_ITC             (0xFFUL << 16)

/* USBSTS */
#define LPC_USB_USBSTS_UI              (0x1UL << 0)  /* USB Interrupt */
#define LPC_USB_USBSTS_UEI             (0x1UL << 1)  /* USB Error interrupt */
#define LPC_USB_USBSTS_PCI             (0x1UL << 2)  /* Port change detect */
#define LPC_USB_USBSTS_URI             (0x1UL << 6)  /* USB Reset received */
#define LPC_USB_USBSTS_SRI             (0x1UL << 7)  /* SOF received */
#define LPC_USB_USBSTS_SLI             (0x1UL << 8)  /* DSSuspend */
#define LPC_USB_USBSTS_NAKI            (0x1UL << 16) /* NAK interrupt bit */

/* USBINTR */
#define LPC_USB_USBINTR_UE             (0x1UL << 0) /* USB interrupt enable */
#define LPC_USB_USBINTR_UEE            (0x1UL << 1) /* USB error interrupt enable */
#define LPC_USB_USBINTR_PCE            (0x1UL << 2) /* Port change detect enable */
#define LPC_USB_USBINTR_URE            (0x1UL << 6) /* USB reset enable */
#define LPC_USB_USBINTR_SRE            (0x1UL << 7) /* SOF received enable */
#define LPC_USB_USBINTR_SLE            (0x1UL << 8) /* Sleep enable */
#define LPC_USB_USBINTR_NAKE           (0x1UL << 16) /* NAK interrupt enable */


/* DEVICEADDR */
#define LPC_USB_DEVICEADDR_USBADRA     (0x1UL  << 24)
#define LPC_USB_DEVICEADDR_USBADR      (0x7FUL << 25)


/* PORTSC1 */
#define LPC_USB_PORTSC1_CCS            (0x1UL << 0)  /* Current connect status */
#define LPC_USB_PORTSC1_PE             (0x1UL << 2)  /* Port Enable (always 1) */
#define LPC_USB_PORTSC1_PEC            (0x1UL << 3)  /* Port Enable change (always 0) */
#define LPC_USB_PORTSC1_FPR            (0x1UL << 6)  /* Force port resume */
#define LPC_USB_PORTSC1_SUSP           (0x1UL << 7)  /* Suspended */
#define LPC_USB_PORTSC1_PR             (0x1UL << 8)  /* Port Reset */
#define LPC_USB_PORTSC1_HSP            (0x1UL << 9)  /* High speed status */
#define LPC_USB_PORTSC1_PIC            (0x3UL << 14) /* Port indicator control */
#define LPC_USB_PORTSC1_PTC            (0xFUL << 16) /* Port test control */
#define LPC_USB_PORTSC1_PHCD           (0x1UL << 23) /* PHY low power suspend */
#define LPC_USB_PORTSC1_PFSC           (0x1UL << 24) /* Port force full speed connect */
#define LPC_USB_PORTSC1_PSPD           (0x3UL << 26) /* Port speed */

#define LPC_USB_PORTSC1_PSPD_FULL      0x0UL
#define LPC_USB_PORTSC1_PSPD_LOW       0x1UL
#define LPC_USB_PORTSC1_PSPD_HIGH      0x2UL

/* USBMODE */
#define LPC_USB_USBMODE_CM             (0x3UL << 0)
#define LPC_USB_USBMODE_ES             (0x1UL << 2)
#define LPC_USB_USBMODE_SLOM           (0x1UL << 3)
#define LPC_USB_USBMODE_SDIS           (0x1UL << 4)

#define LPC_USB_USBMODE_CM_IDLE        0
#define LPC_USB_USBMODE_CM_DEVICE      2
#define LPC_USB_USBMODE_CM_HOST        3


/* ENDPTSETUPSTAT */
#define LPC_USB_ENDPTSETUPSTAT(ep)       (0x1UL << ep)

/* ENDPTPRIME */
#define LPC_USB_ENDPTPRIME_PERB(ep)      (0x1UL << ep)
#define LPC_USB_ENDPTPRIME_PETB(ep)      (0x1UL << (ep+16))

/* ENDPTFLUSH */
#define LPC_USB_ENDPTFLUSH_FERB(ep)      (0x1UL << ep)
#define LPC_USB_ENDPTFLUSH_FETB(ep)      (0x1UL << (ep+16))

/* ENDPTSTAT */
#define LPC_USB_ENDPTSTAT_ERBR(ep)       (0x1UL << ep)
#define LPC_USB_ENDPTSTAT_ETBR(ep)      (0x1UL << (ep+16))


/* ENDPTCOMPLETE */
#define LPC_USB_ENDPTCOMPLETE_ERCE(ep)   (0x1UL << ep) /* receive complete */
#define LPC_USB_ENDPTCOMPLETE_ETCE(ep)   (0x1UL << (ep+16)) /* transmit complete */

/* ENDPTCTRL */
#define LPC_USB_ENDPTCTRL_RXS            (0x1UL << 0)    /* endpoint rx stall */
#define LPC_USB_ENDPTCTRL_RXT            (0x3UL << 2)    /* endpoint rx type */
#define LPC_USB_ENDPTCTRL_RXI            (0x1UL << 5)    /* rx data toggle inhibit */
#define LPC_USB_ENDPTCTRL_RXR            (0x1UL << 6)    /* rx data toggle reset */
#define LPC_USB_ENDPTCTRL_RXE            (0x1UL << 7)    /* endpoint rx enable */
#define LPC_USB_ENDPTCTRL_TXS            (0x1UL << 16)   /* endpoint tx stall */
#define LPC_USB_ENDPTCTRL_TXT            (0x3UL << 18)   /* endpoint tx type */
#define LPC_USB_ENDPTCTRL_TXI            (0x1UL << 21)    /* tx data toggle inhibit */
#define LPC_USB_ENDPTCTRL_TXR            (0x1UL << 22)    /* tx data toggle reset */
#define LPC_USB_ENDPTCTRL_TXE            (0x1UL << 23)   /* endpoint tx enable */


#define LPC_USB_ENDPTCTRL_TYPE_CONTROL      0x0
#define LPC_USB_ENDPTCTRL_TYPE_ISOCHRONOUS  0x1
#define LPC_USB_ENDPTCTRL_TYPE_BULK         0x2
#define LPC_USB_ENDPTCTRL_TYPE_INTERRUPT    0x3


/* OTGSC */
#define LPC_USB_OTGSC_VD                (0x1UL << 0)
#define LPC_USB_OTGSC_VC                (0x1UL << 1)
#define LPC_USB_OTGSC_HAAR              (0x1UL << 2)
#define LPC_USB_OTGSC_OT                (0x1UL << 3)
#define LPC_USB_OTGSC_DP                (0x1UL << 4)
#define LPC_USB_OTGSC_IDPU              (0x1UL << 5)
#define LPC_USB_OTGSC_HADP              (0x1UL << 6)
#define LPC_USB_OTGSC_HABA              (0x1UL << 7)
#define LPC_USB_OTGSC_ID                (0x1UL << 8)
#define LPC_USB_OTGSC_AVV               (0x1UL << 9)
#define LPC_USB_OTGSC_ASV               (0x1UL << 10)
#define LPC_USB_OTGSC_BSV               (0x1UL << 11)
#define LPC_USB_OTGSC_BSE               (0x1UL << 12)
#define LPC_USB_OTGSC_MS1T              (0x1UL << 13)
#define LPC_USB_OTGSC_DPS               (0x1UL << 14)
#define LPC_USB_OTGSC_IDIS              (0x1UL << 16)
#define LPC_USB_OTGSC_AVVIS             (0x1UL << 17)
#define LPC_USB_OTGSC_ASVIS             (0x1UL << 18)
#define LPC_USB_OTGSC_BSVIS             (0x1UL << 19)
#define LPC_USB_OTGSC_BSEIS             (0x1UL << 20)
#define LPC_USB_OTGSC_MS1S              (0x1UL << 21)
#define LPC_USB_OTGSC_DPIS              (0x1UL << 22)
#define LPC_USB_OTGSC_IDIE              (0x1UL << 24)
#define LPC_USB_OTGSC_AVVIE             (0x1UL << 25)
#define LPC_USB_OTGSC_AVSIE             (0x1UL << 26)
#define LPC_USB_OTGSC_BSVIE             (0x1UL << 27)
#define LPC_USB_OTGSC_BSEIE             (0x1UL << 28)
#define LPC_USB_OTGSC_MS1E              (0x1UL << 29)
#define LPC_USB_OTGSC_DPIE              (0x1UL << 30)



/* queue head: endpoint capabilities */
#define LPC_USB_QH_EPCAP_IOS          (0x1UL << 15)
#define LPC_USB_QH_EPCAP_MAX_PACKET   (0x7FF << 16)
#define LPC_USB_QH_EPCAP_ZLT          (0x1UL << 29)
#define LPC_USB_QH_EPCAP_MULT         (0x3UL << 30)


/* DTD */
#define LPC_USB_DTD_NEXTDTD_T         (0x1UL <<  0)

#define LPC_USB_DTD_TOKEN_TOTAL_BYTES     (0x7FFFUL << 16)
#define LPC_USB_DTD_TOKEN_IOC             (0x1UL << 15)
#define LPC_USB_DTD_TOKEN_MULTO           (0x3UL << 10)
#define LPC_USB_DTD_TOKEN_STATUS          (0xFFUL << 0)
#define LPC_USB_DTD_TOKEN_STATUS_ACTIVE   (0x1UL << 7)
#define LPC_USB_DTD_TOKEN_STATUS_HALTED   (0x1UL << 6)
#define LPC_USB_DTD_TOKEN_STATUS_BUF_ERR  (0x1UL << 5)
#define LPC_USB_DTD_TOKEN_STATUS_XACT_ERR (0x1UL << 3)














#endif // LPC43XX_USB_BITS_H
