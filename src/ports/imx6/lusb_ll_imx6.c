/*
 * lusb_ll.c
 *
 *  Created on: 05.07.2010
 *      Author: borisov
 */

#include <string.h>

#include "lusb_ll.h"
#include "lusb_const.h"
#include "lusb_usbdefs.h"
#include "lusb_core_cb.h"
#include "lusb_config_params.h"


#include "registers/regsccm.h"
#include "registers/regsccmanalog.h"
#include "registers/regsusbphy.h"

#include "imx6_usb_registers.h"
#include "imx6_usb_defines.h"

#ifdef LUSB_INTERRUPT
    #include "core/interrupt.h"
#endif


#ifndef LUSB_SUPPORT_HIGH_SPEED
    #error Port always support high speed! define LUSB_SUPPORT_HIGH_SPEED in lusb_config.h
#endif

#include <stdio.h>

#define LUSB_LL_USB_CORE        USB_OTG
#define LUSB_LL_IRQ_ID          IMX_INT_USBOH3_UOTG
#define LUSB_LL_IRQ_PRIORITY    0
#define LUSB_LL_IRQ_CPU         CPU_0


#define IMX_EP_QHEAD(ep_addr)  ((((ep_addr) & USB_EPDIR)==USB_EPDIR_IN)? \
                                    &f_epQueues[(ep_addr)&USB_EPNUM_MSK].in : \
                                    &f_epQueues[(ep_addr)&USB_EPNUM_MSK].out)


#define DTD_TERMINATE 0xDEAD001

/*
 *  USB device endpoint queue head structure
 */

typedef struct  {
    uint32_t endpointCharacteristics;	//! Endpoint Capabilities/Characteristics
    uint32_t currentDtd;		//! Current dTD Pointer
    uint32_t nextDtd;			//! Next dTD Pointer
    uint32_t dtdToken;			//! Token
    uint32_t dtdBuffer[5];		//! Buffer Pointers
    uint32_t reserved;
    t_lusb_req setupBuffer;	//! Set-up Buffer
    uint32_t reserved1[4];
} t_queueHead;

// USB endpoint pair sturcture
typedef struct {
    t_queueHead out;
    t_queueHead in;
} t_EpPair;

/*
 *  USB device endpoint transfer descriptor
 */
typedef struct  {
    uint32_t nextDtd;		//! Next data transfer descriptor
    uint32_t dtdToken;		//! Token
    uint32_t dtdBuffer[5];	//! Buffers for data to be transferred
    uint32_t totalSize;
} t_usbDtd;




static void f_lusb_ll_isr(void);

#define LSPEC_ALIGNMENT 2048
#include "lcspec_align.h"
volatile t_EpPair f_epQueues[8];
#undef LSPEC_ALIGNMENT

#define LSPEC_ALIGNMENT 32
#include "lcspec_align.h"
static t_usbDtd f_ep0_tx_dtd;
#include "lcspec_align.h"
static t_usbDtd f_ep0_rx_dtd;

static uint8_t f_ep0_rx_buf[LUSB_EP0_PACKET_SIZE];


static volatile uint8_t f_setup = 0;
static volatile uint32_t f_setup_rx_size = 0;


static void f_set_usb_clock(void)
{
    HW_CCM_CCGR6.B.CG0 = 3;

    /*!
     * Enable the PLL\n
     * OTG, Host2 and Host3 controllers use USB_PLL0
     * Host1 controller uses USB_PLL1
     */

    HW_CCM_ANALOG_PLL_USB1_SET(BM_CCM_ANALOG_PLL_USB1_POWER);	//! - Turn PLL power on.
    HW_CCM_ANALOG_PLL_USB1_SET(BM_CCM_ANALOG_PLL_USB1_EN_USB_CLKS); //!Powers the 9-phase PLL outputs for USBPHY0
    while(!(HW_CCM_ANALOG_PLL_USB1_RD() & BM_CCM_ANALOG_PLL_USB1_LOCK));//! - Wait for PLL to lock

    HW_CCM_ANALOG_PLL_USB1_CLR(BM_CCM_ANALOG_PLL_USB1_BYPASS);	//! - Clear bypass
    HW_CCM_ANALOG_PLL_USB1_SET(BM_CCM_ANALOG_PLL_USB1_ENABLE); 	//! - Enable PLL clock output for the PHY
}

static int f_enable_transiver(unsigned instance)
{
    //! NOTE !! CLKGATE must be cleared before clearing power down
    HW_USBPHY_CTRL_CLR(instance, BM_USBPHY_CTRL_SFTRST);    //! - clear SFTRST
    HW_USBPHY_CTRL_CLR(instance, BM_USBPHY_CTRL_CLKGATE);   //! - clear CLKGATE
    HW_USBPHY_PWD_WR(instance, 0);	//! - clear all power down bits
    HW_USBPHY_CTRL_SET(instance, BM_USBPHY_CTRL_ENUTMILEVEL2 |
                       BM_USBPHY_CTRL_ENUTMILEVEL3 | BM_USBPHY_CTRL_ENHOSTDISCONDETECT);

    //! disable the charger detector. This must be off during normal operation
    {
        // this register is not documented. Will be updated in the next release
        uint32_t *ChargerDetectControl;
        ChargerDetectControl = (uint32_t *) 0x020c81b0;
        *ChargerDetectControl |= 1 << 20;   // disable detector
    }

    //! Check if all power down bits are clear
    if (HW_USBPHY_PWD_RD(instance) != 0)
        return -1;              // Phy still in power-down mode. Check if all clocks are running.
    else
        return 0;
}

static void f_ep_init(uint8_t ep_addr, int32_t nextDtd)
{
    volatile t_queueHead *head = IMX_EP_QHEAD(ep_addr);
    uint32_t endpointCapabilities=0;
    uint32_t maxPacket;
    if (!(ep_addr & USB_EPNUM_MSK))
    {
        maxPacket = LUSB_EP0_PACKET_SIZE;
        //if ((ep_addr & USB_EPDIR)==USB_EPDIR_IN)
        {
            endpointCapabilities = USB_EP_QH_EP_CHAR_IOS;
        }
    }
    else
    {
        /** @todo для остальных конечных точек */
    }


    head->endpointCharacteristics = (endpointCapabilities
                                   //  | USB_EP_QH_EP_CHAR_IOS
                                    | USB_EP_QH_EP_CHAR_ZLT   // Turn Zero Length Termination off
                                    | USB_EP_QH_EP_CHAR_MAX_PACKET(maxPacket));

    head->currentDtd = 0;  // Will be updated by the controller with nextDtd on startup
    head->nextDtd = nextDtd;   // This will be the first dTD that the controller uses
    head->dtdToken = 0;    // Will be updated with the token from nextdTD on startup
}


void f_init_dtd(t_usbDtd *dtd, uint32_t size, void * buf, int irq)
{
    unsigned tocken = irq ? USB_DTD_TOKEN_IOC : 0;
    dtd->nextDtd = 0xDEAD0001;   //! invalidate nextDtd (set T-bit)

    dtd->dtdToken = (tocken | USB_DTD_TOKEN_TOTAL_BYTES(size)
                        | USB_DTD_TOKEN_STAT_ACTIVE);
    dtd->totalSize = size;

    //! Calculate & write the buffer pointers/
    // Split input buffer in 5 4K buffers
    int i;
    for (i = 0; i < 5; i++) {
        dtd->dtdBuffer[i] = (uint32_t) buf;

        // next buffer starts at 4K next 4K boundary
        buf = (uint32_t *) ((uint32_t) buf - (uint32_t) buf % 0x1000 + 0x1000);
    }
}


/************************************************************************
  разрешение (en = 1) или запрещение (en = 0) прерываний по USB
  возвращает 1, если прерывания до этого были уже разрешены, иначе 0
  ***********************************************************************/
bool lusb_ll_usbirq_en(bool en)
{
#ifdef LUSB_INTERRUPT
    static bool f_irq_en = false;
    bool prev = f_irq_en;
    if (en)
    {
        enable_interrupt(LUSB_LL_IRQ_ID, LUSB_LL_IRQ_CPU, LUSB_LL_IRQ_PRIORITY);
        f_irq_en = 1;
    }
    else
    {
        disable_interrupt(LUSB_LL_IRQ_ID, LUSB_LL_IRQ_CPU);
        f_irq_en = 0;
    }
    return prev;
#else
    return 0;
#endif
}



/**************************************************************************************
    сброс регистров USB контроллера - выполняется при инициализации и по bus reset
 **************************************************************************************/
void lusb_ll_reset(void)
{
    //! - Clear all setup token semaphores
    HW_USBC_ENDPTSETUPSTAT_WR(LUSB_LL_USB_CORE, HW_USBC_ENDPTSETUPSTAT_RD(LUSB_LL_USB_CORE));

    //! - Clear all complete status bits
    HW_USBC_ENDPTCOMPLETE_WR(LUSB_LL_USB_CORE, HW_USBC_ENDPTCOMPLETE_RD(LUSB_LL_USB_CORE));

    //! - Wait for all primed status to clear
    while(HW_USBC_ENDPTPRIME_RD(LUSB_LL_USB_CORE));

    //! - Flush all endpoints
    HW_USBC_ENDPTFLUSH_WR(LUSB_LL_USB_CORE, 0xFFFFFFFF);

    //! - Wait for host to stop signaling reset
    while(HW_USBC_PORTSC1_RD(LUSB_LL_USB_CORE) & BM_USBC_UH1_PORTSC1_PR);
}


int lusb_ll_get_speed(void)
{
    int speed = BG_USBC_UH1_PORTSC1_PSPD(HW_USBC_PORTSC1_RD(LUSB_LL_USB_CORE));
    if (speed==0)
        return LUSB_SPEED_FULL;
    if (speed==1)
        return LUSB_SPEED_LOW;
    if (speed==2)
        return LUSB_SPEED_HIGH;
    return LUSB_SPEED_HIGH;
}


/***************************************************************************************
 *  Инициализация usb-интерфейса (PCON и PLL1 уже должны быть настроены в startup!!!!!)
 *************************************************************************************/
int lusb_ll_init(void)
{
    int err = 0;
    f_set_usb_clock();
    err = f_enable_transiver(HW_USBPHY1);
    if (!err)
    {
        //! - Select PHY type for this hardware
        HW_USBC_PORTSC1_WR(LUSB_LL_USB_CORE, HW_USBC_PORTSC1_RD(LUSB_LL_USB_CORE)
                           & (~(BF_USBC_UH1_PORTSC1_PTS_1(3) | BF_USBC_UH1_PORTSC1_PTS_2(1))));

        // Reset controller after switching PHY's
        HW_USBC_USBCMD_WR(LUSB_LL_USB_CORE, HW_USBC_USBCMD_RD(LUSB_LL_USB_CORE)
                          | BM_USBC_UH1_USBCMD_RST);

        // wait for reset to complete
        while (HW_USBC_USBCMD_RD(LUSB_LL_USB_CORE) & (BM_USBC_UH1_USBCMD_RST)) ;

        //! - Set controller to device Mode
        HW_USBC_USBMODE_WR(LUSB_LL_USB_CORE, USB_USBMODE_CM_DEVICE);

        //! - Set initial configuration for controller
        HW_USBC_USBCMD_WR(LUSB_LL_USB_CORE, HW_USBC_USBCMD_RD(LUSB_LL_USB_CORE)
                          & (~(BF_USBC_UH1_USBCMD_ITC(0xFF))));

        HW_USBC_USBMODE_WR(LUSB_LL_USB_CORE,  HW_USBC_USBMODE_RD(LUSB_LL_USB_CORE)
                           | BM_USBC_UH1_USBMODE_SLOM); // Setup Lockouts Off

        //! - Set the device endpoint list address
        HW_USBC_ASYNCLISTADDR_WR(LUSB_LL_USB_CORE, (uint32_t) &f_epQueues[0]);

        //! - Configure Endpoint 0.
        //  Only the required EP0 for control traffic is initialized at this time.
        HW_USBC_ENDPTCTRL0_WR(LUSB_LL_USB_CORE,  HW_USBC_ENDPTCTRL0_RD(LUSB_LL_USB_CORE)
                              | (USB_ENDPTCTRL_TXE | USB_ENDPTCTRL_RXE));

        f_ep_init(USB_EPDIR_IN, DTD_TERMINATE);
        f_ep_init(USB_EPDIR_OUT, DTD_TERMINATE);


#ifdef LUSB_INTERRUPT
        register_interrupt_routine(LUSB_LL_IRQ_ID, f_lusb_ll_isr);
        HW_USBC_USBINTR_WR(LUSB_LL_USB_CORE,  BM_USBC_UH1_USBINTR_URE
                           | BM_USBC_UH1_USBINTR_SEE
                           | BM_USBC_UH1_USBINTR_PCE
                           | BM_USBC_UH1_USBINTR_UEE
                           | BM_USBC_UH1_USBINTR_UE);
        lusb_ll_usbirq_en(true);
#endif
    }

    return err;
}






//подключение к хосту (вкл. поддтяжки)
void lusb_ll_connect(bool con)
{
    if (con)
    {
        HW_USBC_USBCMD_WR(LUSB_LL_USB_CORE, HW_USBC_USBCMD_RD(LUSB_LL_USB_CORE)
                          | BM_USBC_UH1_USBCMD_RS);
    }
    else
    {
        HW_USBC_USBCMD_WR(LUSB_LL_USB_CORE, HW_USBC_USBCMD_RD(LUSB_LL_USB_CORE)
                          & ~BM_USBC_UH1_USBCMD_RS);
    }
}




//назначение конечной точке с адресом ep_addr буфера нужного размера и ее инициализация
void lusb_ll_ep_config(uint8_t ep_addr, int ep_packet_size)
{

}

//перевод конечной точки в stall режим с адресом ep_addr
void lusb_ll_ep_set_stall(uint8_t ep_addr)
{

}

//сброс stall-режима для конечной точки с адресом ep_addr
void lusb_ll_ep_clr_stall(uint8_t ep_addr)
{

}

//запрет конечной точки с адресом ep_addr
void lusb_ll_ep_disable(uint8_t ep_addr)
{

}

//разрешение конечной точки с адресом ep_addr
void lusb_ll_ep_enable(uint8_t ep_addr)
{

}

//stall для управляющей нулевой конечной точки (сразу и на in и на out)
void lusb_ll_ep0_stall(void)
{
    HW_USBC_ENDPTCTRL0_WR(LUSB_LL_USB_CORE, HW_USBC_ENDPTCTRL0_RD(LUSB_LL_USB_CORE)
                          | (USB_ENDPTCTRL_TXS | USB_ENDPTCTRL_RXS));
}


static void f_start_stup_rx_data(void)
{
    f_init_dtd(&f_ep0_rx_dtd, sizeof(f_ep0_rx_buf), f_ep0_rx_buf, 1);

    //! - Put the OUT transfer descriptor on the queue head
    f_epQueues[0].out.nextDtd = (uint32_t) &f_ep0_rx_dtd;

    //! - Prime prime endpoints (OUT)
    HW_USBC_ENDPTPRIME_WR(LUSB_LL_USB_CORE, USB_ENDPTPRIME_PERB(1));
}

/* **************************************************************************************
 *  чтение пришедшего пакета из конечной точки OUT
 *         ep - логический номер конечной точки
 *         buff - адрес буфера, куда сохр. данные
 *  возвращаемое значение:
 *      размер пакета
 * ***************************************************************************************/
int lusb_ll_rd_packet(uint8_t ep, uint32_t* buff)
{
    int size = 0;
    int core = LUSB_LL_USB_CORE;
    volatile t_queueHead *head = IMX_EP_QHEAD(ep);
    t_lusb_req *set_req = (t_lusb_req*)(buff);
    if (head)
    {
        if (f_setup)
        {
            /*! copy setup data from queue head to buffer and check semaphore\n
             * SUTW will be cleared when a new setup packet arrives.\n
             * To avoid data corruption, we check the SUTW flag after the copy\n
             * and when it is cleared, we repeat the copy to get the new setup data.
             */
            do {
                HW_USBC_USBCMD_WR(core, HW_USBC_USBCMD_RD(core) | BM_USBC_UH1_USBCMD_SUTW);
                memcpy(buff, (void*)&head->setupBuffer, sizeof(t_lusb_req)); //! - copy the setup data
            } while (!(HW_USBC_USBCMD_RD(core) & BM_USBC_UH1_USBCMD_SUTW));   //! - repeat if SUTW got cleared

            //! - Clear setup identification
            HW_USBC_ENDPTSETUPSTAT_WR(core, HW_USBC_ENDPTSETUPSTAT_RD(core) | (USB_ENDPTSETUPSTAT_ENDPTSETUPSTAT(1)));

            //! - Clear Setup tripwire bit
            HW_USBC_USBCMD_WR(core, HW_USBC_USBCMD_RD(core) & (~BM_USBC_UH1_USBCMD_SUTW));

            //! - Flush Endpoint 0 IN and OUT in case some a previous transaction was left pending
            HW_USBC_ENDPTFLUSH_WR(core, HW_USBC_ENDPTFLUSH_RD(core) & (USB_ENDPTFLUSH_FERB(0) | USB_ENDPTFLUSH_FETB(0)));

            //! - Wait for ENDPSETUPSTAT to clear.\n
            //! It must be clear before the status phase/data phase can be primed
            while(HW_USBC_ENDPTSETUPSTAT_RD(core) & (USB_ENDPTSETUPSTAT_ENDPTSETUPSTAT(1)));

            lusb_printf("lusb: setup request  0x%x, 0x%x, val_l = 0x%x, val_h = 0x%x, index = 0x%x, len = %d\n",
                        set_req->request, set_req->req_type,
                        set_req->val_l, set_req->val_h,
                        set_req->index, set_req->length);

            size = sizeof(t_lusb_req);

            f_setup_rx_size = (set_req->req_type & USB_REQ_REQTYPE_DIR) == USB_REQ_REQTYPE_DIR_OUT ?
                        set_req->length : 0;

            if (f_setup_rx_size)
            {
                f_start_stup_rx_data();
            }

            f_setup = 0;
        }
        else// if (size > 0)
        {            
            if (!(f_ep0_rx_dtd.dtdToken &  USB_DTD_TOKEN_STAT_ACTIVE))
            {
                size = f_ep0_rx_dtd.totalSize - ((f_ep0_rx_dtd.dtdToken>>16)&0x7FFF);
                memcpy(buff, f_ep0_rx_buf, size);

                if (f_setup_rx_size > size)
                    f_setup_rx_size-=size;
                else
                    f_setup_rx_size = 0;

                if (f_setup_rx_size)
                    f_start_stup_rx_data();
            }
        }
    }

    return size;
}


/* *********************************************************************************************
 * запись пакета в буфер конечной точки IN
 *         ep - логический номер конечной точки
 *      size - размер данных (должен быть от 0 до размера пакета кт)
 *         buff - адрес буфера с данными на передачу
 *  возвращаемое значение:
 *      размер пакета
 * **********************************************************************************************/
int lusb_ll_wr_packet(uint8_t ep, int size, uint32_t* buff)
{

    //static uint8_t dump_rx_buf[0x40];     // Buffer to receive the data phase (i.e. zero length packet)
    //static uint8_t dump_tx_buf[0x40];



    //memcpy(dump_tx_buf, buff, size);

    //! - Initialize the device descriptor to send
    f_init_dtd(&f_ep0_tx_dtd, size, buff, 1);

    //! - Put the IN transfer descriptor on the queue head.
    f_epQueues[0].in.nextDtd = (uint32_t) &f_ep0_tx_dtd;


    /*!
     * Initialize a transfer descriptor for receive.\n
     * The host won't actually send anything,\n
     * but a dTD needs to be setup to correctly deal with the 0 byte OUT\n
     * packet the host sends after receiving the IN data.\n
     * The OUT transaction must be queued together with the IN transaction\n
     * for error recovery purposes.\n
     */
    f_init_dtd(&f_ep0_rx_dtd, sizeof(f_ep0_rx_buf), f_ep0_rx_buf, 1);

    //! - Put the OUT transfer descriptor on the queue head
    f_epQueues[0].out.nextDtd = (uint32_t) &f_ep0_rx_dtd;

    //! - Prime prime endpoints (OUT)
    HW_USBC_ENDPTPRIME_WR(LUSB_LL_USB_CORE, (USB_ENDPTPRIME_PETB(1) | USB_ENDPTPRIME_PERB(1)));

    while (HW_USBC_ENDPTPRIME_RD(LUSB_LL_USB_CORE) & (USB_ENDPTPRIME_PETB(1) | (USB_ENDPTPRIME_PERB(1))))
    {}



    while ((HW_USBC_ENDPTSTAT_RD(LUSB_LL_USB_CORE) &
            (USB_ENDPTSTATUS_ETBR(1) | (USB_ENDPTSTATUS_ERBR(1)))) !=
           (USB_ENDPTSTATUS_ETBR(1) | (USB_ENDPTSTATUS_ERBR(1))))
    {
    }


    return size;

}


/*****************************************************************************************************
 *  установка адреса устростройства (1-127, 0 - при сбросе до того как будет сконфигурировано)
 *  (вызывается при приеме дескриптора SET_ADDRESS)
 *****************************************************************************************************/
int lusb_ll_set_addr(uint8_t addr)
{
    uint32_t deviceAddress = addr << 25;
    //! - Set the new device address;
    HW_USBC_DEVICEADDR_WR(LUSB_LL_USB_CORE, deviceAddress);
    return 0;
}

/***********************************************************************************************
 * установка признака, что устройство сконфигурировано
 * (вызывается на прием дескриптора SET_CONFIGURATION)
 *   conf - номер конфигурации (если 0 - не сконфигурировано, иначе - сконфигурировано)
 ***********************************************************************************************/
void lusb_ll_configure(int conf)
{

}



/* **********************************************************************************
 * опрос флагов прерывания USB-модуля
 * обработка и вызов требуемых callback-функций lusb_core
 * **********************************************************************************/
static void f_lusb_ll_isr(void)
{
    uint32_t status = HW_USBC_USBSTS_RD(LUSB_LL_USB_CORE);

    if (status & BM_USBC_UH1_USBSTS_URI)
    {
        lusb_ll_reset();
        //! - Clear reset status bit
        HW_USBC_USBSTS_WR(LUSB_LL_USB_CORE, HW_USBC_USBSTS_RD(LUSB_LL_USB_CORE)
                          | (BM_USBC_UH1_USBSTS_URI | BM_USBC_UH1_USBSTS_UI));
        lusb_core_cb_reset();
    }
    else if (status & BM_USBC_UH1_USBSTS_UI)
    {
        uint32_t ep_cpl;


        /* прерывание по конечной точке */
        HW_USBC_USBSTS_WR(LUSB_LL_USB_CORE, HW_USBC_USBSTS_RD(LUSB_LL_USB_CORE)
                          | BM_USBC_UH1_USBSTS_UI);
        ep_cpl = HW_USBC_ENDPTCOMPLETE_RD(core);
        //   lusb_printf("ep cpl = 0x%08x\n", ep_cpl);

        if (ep_cpl)
        {
            HW_USBC_ENDPTCOMPLETE_WR(core, ep_cpl);

            //! - Check endpoint's complete flags and clear
            if (ep_cpl & USB_ENDPTCOMPLETE_ETCE0)
            {
                //! Check Active flag in OUT dTD and wait for it to clear.
                //! This is the only true indication that the transfer did complete.
                while (f_ep0_tx_dtd.dtdToken & USB_DTD_TOKEN_STAT_ACTIVE) ;
                lusb_core_cb_packet_tx(USB_EP_IN(0), 0);
            }

            if (ep_cpl & USB_ENDPTCOMPLETE_ERCE0)
            {
                while (f_ep0_rx_dtd.dtdToken & USB_DTD_TOKEN_STAT_ACTIVE) ;
                lusb_core_cb_packet_rx(USB_EP_OUT(0), 0);
            }
        }
    }    
}


/**************************************************************************
 *  Выполнение фоновых задач для порта
 *  очистка памяти для дескрипторов
 *  и отслеживание дисконнекта/конекта к usb-шине
 *  (так как аппаратно коннект не отслеживается...)
 * ************************************************************************ */
void lusb_ll_progress(void)
{
    uint32_t setup;
    setup = HW_USBC_ENDPTSETUPSTAT_RD(LUSB_LL_USB_CORE);
    if (setup)
    {
        f_setup = 1;
        lusb_core_cb_packet_rx(0, LUSB_PKTFLAGS_SETUP);
    }
#ifndef LUSB_INTERRUPT
    f_lusb_ll_isr();
#endif
}
