#ifndef LUSB_CONFIG_PORT_H
#define LUSB_CONFIG_PORT_H

/** Макрос для задания расположения буферов конечных точек USB */
#define LUSB_MEM(var) __attribute__ ((section (".usb_ram"))) var

/** Размер суммарного буфера на прием для первой конечной точки */
#define LUSB_PORT_EP1_RX_BUF_SIZE      512





#endif // LUSB_CONFIG_PORT_H
