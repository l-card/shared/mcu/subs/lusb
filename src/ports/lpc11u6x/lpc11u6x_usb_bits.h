#ifndef LPC11U6X_USB_BITS_H
#define LPC11U6X_USB_BITS_H

/* DEVCMDSTAT */
#define LPC_USB_DEVCMDSTAT_DEV_ADDR         (0x7FUL << 0)
#define LPC_USB_DEVCMDSTAT_DEV_EN           (0x1UL  << 7)
#define LPC_USB_DEVCMDSTAT_SETUP            (0x1UL  << 8)
#define LPC_USB_DEVCMDSTAT_PLL_ON           (0x1UL  << 9)
#define LPC_USB_DEVCMDSTAT_LPM_SUP          (0x1UL  << 11)
#define LPC_USB_DEVCMDSTAT_INTONNAK_AO      (0x1UL  << 12)
#define LPC_USB_DEVCMDSTAT_INTONNAK_AI      (0x1UL  << 13)
#define LPC_USB_DEVCMDSTAT_INTONNAK_CO      (0x1UL  << 14)
#define LPC_USB_DEVCMDSTAT_INTONNAK_CI      (0x1UL  << 15)
#define LPC_USB_DEVCMDSTAT_DCON             (0x1UL  << 16)
#define LPC_USB_DEVCMDSTAT_DSUS             (0x1UL  << 17)
#define LPC_USB_DEVCMDSTAT_LPM_SUS          (0x1UL  << 19)
#define LPC_USB_DEVCMDSTAT_LPM_REWP         (0x1UL  << 20)
#define LPC_USB_DEVCMDSTAT_DCON_C           (0x1UL  << 24)
#define LPC_USB_DEVCMDSTAT_DSUS_C           (0x1UL  << 25)
#define LPC_USB_DEVCMDSTAT_DRES_C           (0x1UL  << 26)
#define LPC_USB_DEVCMDSTAT_VBUSDEBOUNCED    (0x1UL  << 28)


/* USB Info register */
#define LPC_USB_INFO_FRAME_NR               (0x7FFUL  << 0)
#define LPC_USB_INFO_ERR_CODE               (0xFUL    << 11)

/* USB Link Power Management register */
#define LPC_USB_LPM_HIRD_HW                 (0xFUL << 0)
#define LPC_USB_LPM_HIRD_SW                 (0xFUL << 4)
#define LPC_USB_LPM_DATA_PENDING            (0x1UL << 8)

/* USB interrupt status register */
#define LPC_USB_INT_EP0OUT                  (0x1UL << 0)
#define LPC_USB_INT_EP0IN                   (0x1UL << 1)
#define LPC_USB_INT_EP1OUT                  (0x1UL << 2)
#define LPC_USB_INT_EP1IN                   (0x1UL << 3)
#define LPC_USB_INT_EP2OUT                  (0x1UL << 4)
#define LPC_USB_INT_EP2IN                   (0x1UL << 5)
#define LPC_USB_INT_EP3OUT                  (0x1UL << 6)
#define LPC_USB_INT_EP3IN                   (0x1UL << 7)
#define LPC_USB_INT_EP4OUT                  (0x1UL << 8)
#define LPC_USB_INT_EP4IN                   (0x1UL << 9)
#define LPC_USB_INT_FRAME_INT               (0x1UL << 30)
#define LPC_USB_INT_DEV_INT                 (0x1UL << 31)


#define LPC_USB_EP_MSK_ALL_NONCTL   (LPC_USB_INT_EP1OUT | LPC_USB_INT_EP1IN |\
                                     LPC_USB_INT_EP2OUT | LPC_USB_INT_EP2IN |\
                                     LPC_USB_INT_EP3OUT | LPC_USB_INT_EP3IN |\
                                     LPC_USB_INT_EP4OUT | LPC_USB_INT_EP4IN)

#define LPC_USB_EP_BIT(ep_addr)          (0x1UL << ((((ep_addr) & USB_EPDIR)==USB_EPDIR_OUT) ? \
                                                 2 * ((ep_addr)&USB_EPNUM_MSK) : 2*((ep_addr)&USB_EPNUM_MSK)+1))





#define LPC_USB_EPLIST_BUF_ADDR_OFFS       (0xFFFFUL << 0)
#define LPC_USB_EPLIST_BUF_NBYTES          (0x3FFUL << 16)
#define LPC_USB_EPLIST_TYPE_ISO            (0x1UL << 26)
#define LPC_USB_EPLIST_RF_TV               (0x1UL << 27)
#define LPC_USB_EPLIST_TOGGLE_RESET        (0x1UL << 28)
#define LPC_USB_EPLIST_STALL               (0x1UL << 29)
#define LPC_USB_EPLIST_DISABLE             (0x1UL << 30)
#define LPC_USB_EPLIST_ACTIVE              (0x1UL << 31)



#define LPC_USB_DEVCMDSTAT_WRVAL(val)  (((val) | \
                                        (LPC_USB_DEVCMDSTAT_DSUS | LPC_USB_DEVCMDSTAT_LPM_SUS)) & \
                                        ~(LPC_USB_DEVCMDSTAT_SETUP | LPC_USB_DEVCMDSTAT_DCON_C | \
                                         LPC_USB_DEVCMDSTAT_DSUS_C | LPC_USB_DEVCMDSTAT_DRES_C))






#endif // LPC11U6X_USB_BITS_H
