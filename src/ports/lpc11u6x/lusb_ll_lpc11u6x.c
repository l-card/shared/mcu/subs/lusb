/* Данный файл содержит порт стека lusb для контроллеров LPC11U6X.
 *
 *  Данный порт использует следующие зависимости и предположения:
 *   - PLL и другие настройки клока должны быть выполнены извне до вызова
 *      lusb_init(). Данный порт управляет только разрешением клока
 *      в SYSAHBCLKCTRL и подачей питания на трансивер USB.
 *   -  Должен быть файл "chip.h" с определениями для работы с чипом:
 *       - должны быть объявлены регистры CHIP_REGS_SYSCTL и LPC_USB (совместимо с lpcopen)
 *       - должны быть объявлены номер прерываний (совместимо с lpcopen)
 *       - должен быть объявлен макрос LPC_CLK_EN для разрешения клоков в SYSAHBCLKCTRL,
 *         либо они должны быть разрешены извне.
 *       - должны быть объявлены макросы для установки битовых полей BF_SET, BF_GET
 *
 *  @todo:
 *    1. Рассмотреть возможность ухода в режим низкого энергопотребления
 *    2. Рассмотреть возможность использования 2-х векторов прерываний
 **/

#include "lusb_ll.h"
#include "lusb_types.h"
#include "lusb_usbdefs.h"
#include "lusb_config.h"
#include "lusb_core_cb.h"
#include "lpc11u6x_usb_bits.h"

#include "chip.h"

#include <string.h>

#define LUSB_PORT_EP_BUF_MIN_SIZE  128
#define LUSB_PORT_EP_TX_BUF_SIZE   128



#define EP0_IDX_RX         0
#define EP0_IDX_SETUP      1
#define EP0_IDX_TX         2


#define LPC_EP_LIST_IDX(ep_addr) ((((ep_addr) & USB_EPDIR)==USB_EPDIR_OUT) ? \
                                    4 * ((ep_addr)&USB_EPNUM_MSK) : (4*((ep_addr)&USB_EPNUM_MSK)+2))

#define LPC_EP_LIST_IDX_FIRST_NONCTL  LPC_EP_LIST_IDX(USB_EPDIR_OUT | 1)
#define LPC_EP_LIST_ENTRY_CNT         (4*(LUSB_EP_NONCTL_CNT_MAX+1))


#define LPC_EP_LIST_SET_FLAGS(idx, flags)  (f_usb_ep_list[idx] |= (flags))
#define LPC_EP_LIST_CLR_FLAGS(idx, flags)  (f_usb_ep_list[idx] &= ~(flags))


#define LPC_EP_FILL_ENTRY(idx, offs, size, flags)  (f_usb_ep_list[(idx)] =  \
    BF_SET(LPC_USB_EPLIST_BUF_ADDR_OFFS, BUF_EP_ADDR(offs)>>6) | \
    BF_SET(LPC_USB_EPLIST_BUF_NBYTES, size) | flags)

#define LPC_EP_FILL_ENTRY_BUF(idx, offs, size) (f_usb_ep_list[(idx)] =  \
    (f_usb_ep_list[(idx)] & ~(LPC_USB_EPLIST_BUF_ADDR_OFFS | LPC_USB_EPLIST_BUF_NBYTES)) | \
    BF_SET(LPC_USB_EPLIST_BUF_ADDR_OFFS, BUF_EP_ADDR(offs)>>6) | \
    BF_SET(LPC_USB_EPLIST_BUF_NBYTES, size))




#if LUSB_EP_NONCTL_CNT < 4
    #undef LUSB_PORT_EP4_RX_BUF_SIZE

    #define LUSB_PORT_EP4_RX_BUF_SIZE 0
#else
    #ifndef LUSB_PORT_EP4_RX_BUF_SIZE
        #define LUSB_PORT_EP4_RX_BUF_SIZE LUSB_PORT_EP_BUF_MIN_SIZE
    #elif (LUSB_PORT_EP4_RX_BUF_SIZE % LUSB_PORT_EP_BUF_MIN_SIZE)
        #error "eps buffer size must be multiply of LUSB_PORT_EP_BUF_MIN_SIZE"
    #endif
#endif
#if LUSB_EP_NONCTL_CNT < 3
    #undef LUSB_PORT_EP3_RX_BUF_SIZE

    #define LUSB_PORT_EP3_RX_BUF_SIZE 0
#else
    #ifndef LUSB_PORT_EP3_RX_BUF_SIZE
        #define LUSB_PORT_EP3_RX_BUF_SIZE LUSB_PORT_EP_BUF_MIN_SIZE
    #elif (LUSB_PORT_EP3_RX_BUF_SIZE % LUSB_PORT_EP_BUF_MIN_SIZE)
        #error "eps buffer size must be multiply of LUSB_PORT_EP_BUF_MIN_SIZE"
    #endif
#endif
#if LUSB_EP_NONCTL_CNT < 2
    #undef LUSB_PORT_EP2_RX_BUF_SIZE

    #define LUSB_PORT_EP2_RX_BUF_SIZE 0
#else
    #ifndef LUSB_PORT_EP2_RX_BUF_SIZE
        #define LUSB_PORT_EP2_RX_BUF_SIZE LUSB_PORT_EP_BUF_MIN_SIZE
    #elif (LUSB_PORT_EP2_RX_BUF_SIZE % LUSB_PORT_EP_BUF_MIN_SIZE)
        #error "eps buffer size must be multiply of LUSB_PORT_EP_BUF_MIN_SIZE"
    #endif
#endif
#if LUSB_EP_NONCTL_CNT < 1
    #undef LUSB_PORT_EP1_RX_BUF_SIZE

    #define LUSB_PORT_EP1_RX_BUF_SIZE 0
#else
    #ifndef LUSB_PORT_EP1_RX_BUF_SIZE
        #define LUSB_PORT_EP1_RX_BUF_SIZE LUSB_PORT_EP_BUF_MIN_SIZE
    #elif (LUSB_PORT_EP1_RX_BUF_SIZE % LUSB_PORT_EP_BUF_MIN_SIZE)
        #error "eps buffer size must be multiply of LUSB_PORT_EP_BUF_MIN_SIZE"
    #endif
#endif


#define LUSB_EP_NONCTL_BUF_SIZE (LUSB_PORT_EP1_RX_BUF_SIZE + LUSB_PORT_EP_TX_BUF_SIZE + \
                                 LUSB_PORT_EP2_RX_BUF_SIZE + LUSB_PORT_EP_TX_BUF_SIZE + \
                                 LUSB_PORT_EP3_RX_BUF_SIZE + LUSB_PORT_EP_TX_BUF_SIZE + \
                                 LUSB_PORT_EP4_RX_BUF_SIZE + LUSB_PORT_EP_TX_BUF_SIZE)

#if (LUSB_EP_NONCTL_CNT > 0) && (LUSB_EP_NONCTL_BUF_SIZE==0)
    #warning "noncontrol ep enabled, but all eps buffer sizes defined as 0"
#endif

#ifndef LUSB_MEM
    #error "LUSB_MEM macros must be defined to specify endpoints buffers and status location in memory"
#endif



#define LSPEC_ALIGNMENT 256
#include "lcspec_align.h"
LUSB_MEM(static volatile uint32_t f_usb_ep_list[LPC_EP_LIST_ENTRY_CNT]);
#undef LSPEC_ALIGNMENT

#define LSPEC_ALIGNMENT LUSB_PORT_EP_BUF_MIN_SIZE
#include "lcspec_align.h"
LUSB_MEM(static volatile uint8_t f_usb_buf[LUSB_EP_NONCTL_BUF_SIZE + 2*LUSB_EP0_PACKET_SIZE + USB_SETUP_PACKET_SIZE]);
#undef LSPEC_ALIGNMENT





#define BUF_EP0_RX_OFFSET       (LUSB_EP_NONCTL_BUF_SIZE)
#define BUF_EP0_TX_OFFSET       (BUF_EP0_RX_OFFSET + LUSB_EP0_PACKET_SIZE)
#define BUF_EP0_SETUP_OFFSET    (BUF_EP0_TX_OFFSET + LUSB_EP0_PACKET_SIZE)

#define BUF_EP(offs) ((uint8_t*)&f_usb_buf[(offs)])

#define BUF_EP_ADDR(offs) ((uint32_t)BUF_EP(offs))


#define LPC_USB_EP_BUF_CNT 2
#define LPC_USB_EP_BUF_IDX(buf) ((buf)&1)
#define LPC_USB_EP_BUF_BUSY_CNT(param) ((uint8_t)((param)->buf_num_set - (param)->buf_num_get))
#define LPC_USB_EP_BUF_IN_USE_IDX(ep_addr) (CHIP_REGS_USB->EPINUSE & LPC_USB_EP_BIT(ep_addr) ? 1 : 0)


#if (LUSB_EP_NONCTL_CNT > 0)
    typedef struct {
        struct {
            uint16_t offs;
            uint16_t len;
        } buf[LPC_USB_EP_BUF_CNT];
        uint8_t buf_num_set;
        uint8_t buf_num_get;
        uint16_t max_len;
    } t_ep_buf_param;


    static struct {
        t_ep_buf_param out;
        t_ep_buf_param in;
    } f_eps_bufs[LUSB_EP_NONCTL_CNT];

    #define EP_BUF_PARAM(ep_addr) ((((ep_addr) & USB_EPDIR)==USB_EPDIR_OUT) ? \
                                     &f_eps_bufs[(ep_addr & USB_EPNUM_MSK)-1].out : &f_eps_bufs[(ep_addr & USB_EPNUM_MSK)-1].in)
#endif


/************************************************************************
  разрешение (en = 1) или запрещение (en = 0) прерываний по USB
  возвращает 1, если прерывания до этого были уже разрешены, иначе 0
  ***********************************************************************/
bool lusb_ll_usbirq_en(bool en) {
    const bool res = !!(NVIC->ISER[((uint32_t)(USB0_IRQn) >> 5)] & (1 << ((uint32_t)(USB0_IRQn) & 0x1F)));
    if (en)
        NVIC_EnableIRQ(USB0_IRQn);
    else
        NVIC_DisableIRQ(USB0_IRQn);
    return res;
}


/**************************************************************************************
    сброс регистров USB контроллера - выполняется при инициализации и по bus reset
 **************************************************************************************/
void lusb_ll_reset(void) {
    for (unsigned i=LPC_EP_LIST_IDX_FIRST_NONCTL; i < LPC_EP_LIST_ENTRY_CNT; i++) {
        f_usb_ep_list[i] = LPC_USB_EPLIST_DISABLE;
    }
}



#if LUSB_EP_NONCTL_CNT
static void f_fill_ep_buf_params(int ep_idx, uint16_t *poffs, uint16_t rx_size, uint16_t tx_size) {
    uint16_t offs = *poffs;
    f_eps_bufs[ep_idx].out.max_len  = rx_size/2;
    f_eps_bufs[ep_idx].out.buf[0].offs = offs;
    f_eps_bufs[ep_idx].out.buf[0].len  = 0;
    offs+=f_eps_bufs[ep_idx].out.max_len;
    f_eps_bufs[ep_idx].out.buf[1].offs = offs;
    f_eps_bufs[ep_idx].out.buf[1].len  = 0;
    offs+=f_eps_bufs[ep_idx].out.max_len;

    f_eps_bufs[ep_idx].in.max_len = tx_size/2;
    f_eps_bufs[ep_idx].in.buf[0].offs = offs;
    f_eps_bufs[ep_idx].in.buf[0].len  = 0;
    offs+=f_eps_bufs[ep_idx].in.max_len;

    f_eps_bufs[ep_idx].in.buf[1].offs = offs;
    f_eps_bufs[ep_idx].in.buf[1].len  = 0;
    offs+=f_eps_bufs[ep_idx].in.max_len;

    *poffs = offs;
}
#endif

/***************************************************************************************
 *  Инициализация usb-интерфейса
 *************************************************************************************/
int lusb_ll_init(void) {
    int res = LUSB_ERR_SUCCESS;
#ifdef CHIP_PER_ID_USB
    chip_per_clk_en(CHIP_PER_ID_USB);
    chip_per_clk_en(CHIP_PER_ID_USBRAM);
#endif

    CHIP_REGS_SYSCON->PDRUNCFG &= ~CHIP_REGFLD_SYSCON_PDRUNCFG_USBPAD_PD;

    CHIP_REGS_USB->EPLISTSTART  = (uint32_t)f_usb_ep_list;
    CHIP_REGS_USB->DATABUFSTART = (uint32_t)&f_usb_buf[0];

    memset((uint8_t*)f_usb_ep_list, 0, sizeof(f_usb_ep_list));


    /* настройка буферов нулевой конечной точки */     
    LPC_EP_FILL_ENTRY(EP0_IDX_SETUP, BUF_EP0_SETUP_OFFSET, 0, 0);
    LPC_EP_FILL_ENTRY(EP0_IDX_RX, BUF_EP0_RX_OFFSET, 0, 0);
    LPC_EP_FILL_ENTRY(EP0_IDX_TX, BUF_EP0_TX_OFFSET, 0, 0);

#if LUSB_EP_NONCTL_CNT
    uint16_t offs = 0;
    f_fill_ep_buf_params(0, &offs, LUSB_PORT_EP1_RX_BUF_SIZE, LUSB_PORT_EP_TX_BUF_SIZE);
#if LUSB_EP_NONCTL_CNT > 1
    f_fill_ep_buf_params(1, &offs, LUSB_PORT_EP2_RX_BUF_SIZE, LUSB_PORT_EP_TX_BUF_SIZE);
#endif
#if LUSB_EP_NONCTL_CNT > 2
    f_fill_ep_buf_params(2, &offs, LUSB_PORT_EP3_RX_BUF_SIZE, LUSB_PORT_EP_TX_BUF_SIZE);
#endif
#if LUSB_EP_NONCTL_CNT > 3
    f_fill_ep_buf_params(3, &offs, LUSB_PORT_EP3_RX_BUF_SIZE, LUSB_PORT_EP_TX_BUF_SIZE);
#endif

    CHIP_REGS_USB->EPBUFCFG = LPC_USB_EP_MSK_ALL_NONCTL;
#endif

    lusb_ll_reset();

#ifdef LUSB_INTERRUPT
    CHIP_REGS_USB->INTEN = LPC_USB_INT_DEV_INT | LPC_USB_INT_EP0OUT | LPC_USB_INT_EP0IN;

    lusb_ll_usbirq_en(true);
#endif
    return res;
}

int lusb_ll_close(void) {
#ifdef LUSB_INTERRUPT
    CHIP_REGS_USB->INTEN = 0;
#endif

    CHIP_REGS_SYSCON->PDRUNCFG &= ~CHIP_REGFLD_SYSCON_PDRUNCFG_USBPAD_PD;

#ifdef CHIP_PER_ID_USB
    chip_per_clk_dis(CHIP_PER_ID_USB);
    chip_per_clk_dis(CHIP_PER_ID_USBRAM);
#endif

    return 0;
}


//подключение к хосту (вкл. поддтяжки)
void lusb_ll_connect(bool con) {
    const bool en = lusb_ll_usbirq_en(false);
    CHIP_REGS_USB->DEVCMDSTAT = LPC_USB_DEVCMDSTAT_WRVAL(con ? (CHIP_REGS_USB->DEVCMDSTAT | (LPC_USB_DEVCMDSTAT_DCON | LPC_USB_DEVCMDSTAT_DEV_EN)) :
                                                  (CHIP_REGS_USB->DEVCMDSTAT & ~LPC_USB_DEVCMDSTAT_DCON));
    lusb_ll_usbirq_en(en);
}


int lusb_ll_get_speed(void) {
    return LUSB_SPEED_FULL;
}




//stall для управляющей нулевой конечной точки (сразу и на in и на out)
void lusb_ll_ep0_stall(void) {
    LPC_EP_LIST_SET_FLAGS(EP0_IDX_RX, LPC_USB_EPLIST_STALL);
    LPC_EP_LIST_SET_FLAGS(EP0_IDX_TX, LPC_USB_EPLIST_STALL);
}


int lusb_ll_ep0_rd_packet(uint8_t* buf, uint32_t flags, uint16_t rem_size) {
    int size = 0;
    if (flags & LUSB_PKTFLAGS_SETUP) {
        size = USB_SETUP_PACKET_SIZE;
        memcpy(buf, BUF_EP(BUF_EP0_SETUP_OFFSET), USB_SETUP_PACKET_SIZE);
        LPC_EP_LIST_CLR_FLAGS(EP0_IDX_RX, (LPC_USB_EPLIST_STALL | LPC_USB_EPLIST_ACTIVE));
        LPC_EP_LIST_CLR_FLAGS(EP0_IDX_TX, (LPC_USB_EPLIST_STALL | LPC_USB_EPLIST_ACTIVE));
        CHIP_REGS_USB->INTSTAT = LPC_USB_INT_EP0IN;
        CHIP_REGS_USB->DEVCMDSTAT = LPC_USB_DEVCMDSTAT_WRVAL(CHIP_REGS_USB->DEVCMDSTAT) | LPC_USB_DEVCMDSTAT_SETUP;

        LPC_EP_FILL_ENTRY(EP0_IDX_RX, BUF_EP0_RX_OFFSET, LUSB_EP0_PACKET_SIZE, LPC_USB_EPLIST_ACTIVE);
    } else {
        size = LUSB_EP0_PACKET_SIZE - BF_GET(f_usb_ep_list[EP0_IDX_RX], LPC_USB_EPLIST_BUF_NBYTES);
        memcpy(buf, BUF_EP(BUF_EP0_RX_OFFSET), size);
        LPC_EP_FILL_ENTRY(EP0_IDX_RX, BUF_EP0_RX_OFFSET, LUSB_EP0_PACKET_SIZE, LPC_USB_EPLIST_ACTIVE);
    }
    return size;
}


int lusb_ll_ep0_wr_packet(const uint8_t *buf, uint32_t flags, uint16_t size) {
    if (size > LUSB_EP0_PACKET_SIZE)
        size = LUSB_EP0_PACKET_SIZE;
    memcpy(BUF_EP(BUF_EP0_TX_OFFSET), buf, size);
    LPC_EP_FILL_ENTRY(EP0_IDX_TX, BUF_EP0_TX_OFFSET, size, LPC_USB_EPLIST_ACTIVE);
    return size;
}



int lusb_ll_set_addr(uint8_t addr) {
    const bool en = lusb_ll_usbirq_en(false);
    CHIP_REGS_USB->DEVCMDSTAT = (LPC_USB_DEVCMDSTAT_WRVAL(CHIP_REGS_USB->DEVCMDSTAT) & ~LPC_USB_DEVCMDSTAT_DEV_ADDR)
            | BF_SET(LPC_USB_DEVCMDSTAT_DEV_ADDR, addr);
    lusb_ll_usbirq_en(en);
    return 0;
}

void lusb_ll_ep0_set_state(t_lusb_ll_ep0_state state, uint16_t total_size) {

}

void lusb_ll_configure(int conf) {

}



#if LUSB_EP_NONCTL_CNT
//перевод конечной точки в stall режим с адресом ep_addr
void lusb_ll_ep_set_stall(uint8_t ep_addr) {
    unsigned idx = LPC_EP_LIST_IDX(ep_addr);
    /* устанавливаем STALL для обоих буферов */
    f_usb_ep_list[idx]   |= LPC_USB_EPLIST_STALL;
    f_usb_ep_list[idx+1] |= LPC_USB_EPLIST_STALL;
}

//сброс stall-режима для конечной точки с адресом ep_addr
void lusb_ll_ep_clr_stall(uint8_t ep_addr) {
    unsigned idx = LPC_EP_LIST_IDX(ep_addr);
    f_usb_ep_list[idx]   &= ~LPC_USB_EPLIST_STALL;
    f_usb_ep_list[idx+1] &= ~LPC_USB_EPLIST_STALL;

    f_usb_ep_list[idx + LPC_USB_EP_BUF_IN_USE_IDX(ep_addr)] |= LPC_USB_EPLIST_TOGGLE_RESET;
}




int lusb_ll_ep_enable(const t_usb_endpoint_descriptor *ep_descr) {
    unsigned idx = LPC_EP_LIST_IDX(ep_descr->bEndpointAddress);
    t_ep_buf_param *params = EP_BUF_PARAM(ep_descr->bEndpointAddress);

    params->buf_num_set = params->buf_num_get = LPC_USB_EP_BUF_IN_USE_IDX(ep_descr->bEndpointAddress);
    /* сбрасываем все флаги, включая DISABLED. Сброс toggle для первого пакета*/
    f_usb_ep_list[idx] = 0;
    f_usb_ep_list[idx+1] = 0;
    f_usb_ep_list[idx+params->buf_num_set] = LPC_USB_EPLIST_TOGGLE_RESET;

    CHIP_REGS_USB->INTSTAT = LPC_USB_EP_BIT(ep_descr->bEndpointAddress);
#ifdef LUSB_INTERRUPT
    CHIP_REGS_USB->INTEN |= LPC_USB_EP_BIT(ep_descr->bEndpointAddress);
#endif
    return 0;
}


int lusb_ll_ep_disable(const t_usb_endpoint_descriptor *ep_descr) {
    unsigned idx = LPC_EP_LIST_IDX(ep_descr->bEndpointAddress);
    /* верхний уровень обеспечивает, что нет обмена по КТ */
    f_usb_ep_list[idx] = LPC_USB_EPLIST_DISABLE;
    f_usb_ep_list[idx+1] = LPC_USB_EPLIST_DISABLE;
    return 0;
}

void lusb_ll_ep_clr(const t_usb_endpoint_descriptor *ep_descr) {
    t_ep_buf_param *params = EP_BUF_PARAM(ep_descr->bEndpointAddress);
    uint32_t ep_flag = LPC_USB_EP_BIT(ep_descr->bEndpointAddress);

    CHIP_REGS_USB->INTEN &= ~ep_flag;

    for (unsigned i=0; i < LPC_USB_EP_BUF_CNT; i++) {
        uint8_t buf_num = LPC_USB_EP_BUF_IDX(params->buf_num_set+i);
        unsigned idx = LPC_EP_LIST_IDX(ep_descr->bEndpointAddress) + buf_num;       
        if (f_usb_ep_list[idx] & LPC_USB_EPLIST_ACTIVE) {
            if (LPC_USB_EP_BUF_IN_USE_IDX(ep_descr->bEndpointAddress) == buf_num) {
                CHIP_REGS_USB->EPSKIP = ep_flag;
                while (f_usb_ep_list[idx] & LPC_USB_EPLIST_ACTIVE) {}
            }
            f_usb_ep_list[idx] = 0;
        }
    }

    params->buf_num_set = params->buf_num_get = LPC_USB_EP_BUF_IN_USE_IDX(ep_descr->bEndpointAddress);
    CHIP_REGS_USB->INTSTAT = ep_flag;
    CHIP_REGS_USB->INTEN |= ep_flag;
}

int lusb_ll_ep_rd_packet_start(const t_usb_endpoint_descriptor *ep_descr, unsigned size) {

    t_ep_buf_param *params = EP_BUF_PARAM(ep_descr->bEndpointAddress);
    uint8_t buf_num = LPC_USB_EP_BUF_IDX(params->buf_num_set);
    unsigned idx = LPC_EP_LIST_IDX(ep_descr->bEndpointAddress) + buf_num;
    int ret = LUSB_ERR_EP_BUSY;

    if (LPC_USB_EP_BUF_BUSY_CNT(params) < LPC_USB_EP_BUF_CNT) {
        if (size > params->max_len)
            size = params->max_len;

        ret = size;

        if (size < ep_descr->wMaxPacketSize)
            size = ep_descr->wMaxPacketSize;
        params->buf[buf_num].len = size;
        LPC_EP_FILL_ENTRY_BUF(idx, params->buf[buf_num].offs, size);
        LPC_EP_LIST_SET_FLAGS(idx, LPC_USB_EPLIST_ACTIVE);
        params->buf_num_set++;
    }
    return ret;
}


int lusb_ll_ep_rd_packet(const t_usb_endpoint_descriptor *ep_descr, uint8_t *buf, unsigned size) {
    t_ep_buf_param *params = EP_BUF_PARAM(ep_descr->bEndpointAddress);
    uint8_t buf_num = LPC_USB_EP_BUF_IDX(params->buf_num_get);
    int ret = LUSB_ERR_EP_BUSY;

    if (LPC_USB_EP_BUF_BUSY_CNT(params) != 0) {
        unsigned idx = LPC_EP_LIST_IDX(ep_descr->bEndpointAddress) + buf_num;
        if (!(f_usb_ep_list[idx] & LPC_USB_EPLIST_ACTIVE)) {
            ret = params->buf[buf_num].len - BF_GET(f_usb_ep_list[idx], LPC_USB_EPLIST_BUF_NBYTES);
            if (ret > 0) {
                if ((int)size < ret)
                    size = ret;
                memcpy(buf, BUF_EP(params->buf[buf_num].offs), size);
            }
            params->buf_num_get++;
        }
    }
    return ret;
}


int lusb_ll_ep_wr_packet(const t_usb_endpoint_descriptor *ep_descr, const uint8_t *buf, unsigned size) {
    t_ep_buf_param *params = EP_BUF_PARAM(ep_descr->bEndpointAddress);
    uint8_t buf_num = LPC_USB_EP_BUF_IDX(params->buf_num_set);
    unsigned idx = LPC_EP_LIST_IDX(ep_descr->bEndpointAddress) + buf_num;
    int ret = LUSB_ERR_EP_BUSY;

    if (!(f_usb_ep_list[idx] & LPC_USB_EPLIST_ACTIVE)) {
        if (size > ep_descr->wMaxPacketSize)
            size = ep_descr->wMaxPacketSize;

        params->buf[buf_num].len = size;
        if (size != 0) {
            memcpy(BUF_EP(params->buf[buf_num].offs), buf, size);
        }

        LPC_EP_FILL_ENTRY_BUF(idx, params->buf[buf_num].offs, size);
        LPC_EP_LIST_SET_FLAGS(idx, LPC_USB_EPLIST_ACTIVE);
        ret = size;
        params->buf_num_set++;
    }
    return ret;
}




#endif


/* **********************************************************************************
 * опрос флагов прерывания USB-модуля
 * обработка и вызов требуемых callback-функций lusb_core
 * **********************************************************************************/
static void f_lusb_ll_isr(void) {
    uint32_t status = CHIP_REGS_USB->INTSTAT;
    if (status) {
        CHIP_REGS_USB->INTSTAT = status;

        if (status & LPC_USB_INT_DEV_INT) {
            uint32_t devst = CHIP_REGS_USB->DEVCMDSTAT;
            /* сбрасываем биты статуса */
            CHIP_REGS_USB->DEVCMDSTAT = LPC_USB_DEVCMDSTAT_WRVAL(devst) |
                    (devst & (LPC_USB_DEVCMDSTAT_SETUP | LPC_USB_DEVCMDSTAT_DCON_C | \
                    LPC_USB_DEVCMDSTAT_DSUS_C | LPC_USB_DEVCMDSTAT_DRES_C));
            if (devst & LPC_USB_DEVCMDSTAT_DRES_C) {
                lusb_ll_reset();
                lusb_core_cb_reset();
            } else if (devst & LPC_USB_DEVCMDSTAT_DCON_C) {
                lusb_core_cb_con_changed(0);
            } else if (devst & LPC_USB_DEVCMDSTAT_DSUS_C) {
                lusb_core_cb_susp_changed((devst & LPC_USB_DEVCMDSTAT_DSUS) != 0);
            }
        }

        if (status & LPC_USB_INT_EP0OUT) {
            lusb_core_cb_packet_rx(0, CHIP_REGS_USB->DEVCMDSTAT & LPC_USB_DEVCMDSTAT_SETUP
                                   ? LUSB_PKTFLAGS_SETUP : 0);
        }

        if(status & LPC_USB_INT_EP0IN) {
            if (!(f_usb_ep_list[EP0_IDX_TX] & LPC_USB_EPLIST_ACTIVE)) {
                lusb_core_cb_packet_tx(USB_EP_IN(0), 0);
            }
        }



#if LUSB_EP_NONCTL_CNT
        status &= LPC_USB_EP_MSK_ALL_NONCTL;
        for (unsigned ep=1; (ep < (LUSB_EP_NONCTL_CNT+1)) && (status!=0); ep++) {
            uint8_t ep_addr = USB_EP_OUT(ep);
            if (status & LPC_USB_EP_BIT(ep_addr)) {
                status &=~LPC_USB_EP_BIT(ep_addr);
                lusb_core_cb_packet_rx(ep_addr, 0);
            }
            ep_addr = USB_EP_IN(ep);
            if (status & LPC_USB_EP_BIT(ep_addr)) {
                status &=~LPC_USB_EP_BIT(ep_addr);
                lusb_core_cb_packet_tx(ep_addr, 0);
            }
        }
#endif
    }

}


#ifdef LUSB_INTERRUPT
#include "lcspec_interrupt.h"
void USB_IRQHandler(void) {
    f_lusb_ll_isr();
}
#endif


/**************************************************************************
 *  Выполнение фоновых задач для порта
 *  очистка памяти для дескрипторов
 *  и отслеживание дисконнекта/конекта к usb-шине
 *  (так как аппаратно коннект не отслеживается...)
 * ************************************************************************ */
void lusb_ll_progress(void) {
#ifndef LUSB_INTERRUPT
    f_lusb_ll_isr();
#endif
}
