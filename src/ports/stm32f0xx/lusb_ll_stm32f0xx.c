/* Данный файл содержит порт стека lusb для контроллеров LPC11U6X.
 *
 *  Данный порт использует следующие зависимости и предположения:
 *   - PLL и другие настройки клока должны быть выполнены извне до вызова
 *      lusb_init(). Данный порт управляет только разрешением клока
 *      в SYSAHBCLKCTRL и подачей питания на трансивер USB.
 *   -  Должен быть файл "chip.h" с определениями для работы с чипом:
 *       - должны быть объявлены регистры LPC_SYSCTL и LPC_USB (совместимо с lpcopen)
 *       - должны быть объявлены номер прерываний (совместимо с lpcopen)
 *       - должен быть объявлен макрос LPC_CLK_EN для разрешения клоков в SYSAHBCLKCTRL,
 *         либо они должны быть разрешены извне.
 *       - должны быть объявлены макросы для установки битовых полей BF_SET, BF_GET
 *
 *  @todo:
 *    1. Рассмотреть возможность ухода в режим низкого энергопотребления
 *    2. Рассмотреть возможность использования 2-х векторов прерываний
 **/

#include "lusb_ll.h"
#include "lusb_types.h"
#include "lusb_usbdefs.h"
#include "lusb_config_params.h"
#include "lusb_core_cb.h"

#include "chip.h"

#include <string.h>

/* Частота APB должна быть не менее 10 МГц, чтобы избежать ошибок Overrun,Underrun
 * при обмене по USB */
#if CHIP_CLK_APB_FREQ < CHIP_MHZ(10)
    #error APB clock is to low for usb operation
#endif
/* Время, необходимое выждать после включения аналоговой части, в мкс */
#define USB_STARTUP_TIME_US     1

#define USB_MEM_ADDR_OFFS(buf) (((intptr_t)(buf) - CHIP_BASE_ADDR_PERIPH_USBCAN_SRAM) & 0xFFFF)


#define EPLIST_ENTRY_CNT        (LUSB_EP_NONCTL_CNT_MAX + 1)

#define LSPEC_ALIGNMENT 8
#include "lcspec_align.h"
LUSB_MEM(static volatile CHIP_REGS_USB_BTABLE_REC_T f_btable[CHIP_USB_EPREGS_CNT]);
#undef LSPEC_ALIGNMENT

#define LSPEC_ALIGNMENT 2
#include "lcspec_align.h"
LUSB_MEM(static volatile uint16_t f_ep0_tx_buf[LUSB_EP0_PACKET_SIZE/2]);
#include "lcspec_align.h"
LUSB_MEM(static volatile uint16_t f_ep0_rx_buf[LUSB_EP0_PACKET_SIZE/2]);
#undef LSPEC_ALIGNMENT


#define USB_EP0_EPR_CFG (LBITFIELD_SET(CHIP_REGFLD_USB_EPR_EA, 0) | LBITFIELD_SET(CHIP_REGFLD_USB_EPR_EP_TYPE, CHIP_REGFLDVAL_USB_EPR_EP_TYPE_CONTROL))

/* Установка состояния control-pipe на прием, передачу, а также флага
 * OUT_STATUS.
 * Состояние меняется с мопощью toggle при записи 1, поэтому для определения
 * значения сперва читаем текщуее состояние, затем определяем, какие биты нужно
 * поменять в соответствии с разницей и только при отличии записываем нужное значение.
 * Флаги прерываний записываем 1, чтобы не сбросить */
static inline void f_ep0_set_stat(uint8_t rx_stat, uint8_t tx_stat, bool out_status) {
    const uint16_t epr = CHIP_REGS_USB->EP[0].EPR;

    const uint8_t rx_upd = (LBITFIELD_GET(epr, CHIP_REGFLD_USB_EPR_STAT_RX) ^ rx_stat);
    const uint8_t tx_upd = (LBITFIELD_GET(epr, CHIP_REGFLD_USB_EPR_STAT_TX) ^ tx_stat);
    const uint8_t out_stat_up = (LBITFIELD_GET(epr, CHIP_REGFLD_USB_EPR_EP_KIND) != out_status);

    if ((rx_upd != 0) || (tx_upd != 0) || (out_stat_up != 0)) {
        CHIP_REGS_USB->EP[0].EPR = (USB_EP0_EPR_CFG | CHIP_REGFLD_USB_EPR_CTR_RX | CHIP_REGFLD_USB_EPR_CTR_TX)
                | LBITFIELD_SET(CHIP_REGFLD_USB_EPR_EP_KIND, out_status)
                | LBITFIELD_SET(CHIP_REGFLD_USB_EPR_STAT_TX, tx_upd)
                | LBITFIELD_SET(CHIP_REGFLD_USB_EPR_STAT_RX, rx_upd);
    }
}

/* определение требуемого состояния для направления обмена control pipe,
 * соответствующего завершающей стадии передачи статуса во время приема данных по
 * control-pipe.
 * rem_size - кол-во остающихся данных. Необходимо для всех обменов, кроме последнего
 * устанавливать для противоположного направления состояние STALL, а на последнем - NACK,
 * чтобы корректно можно было принять статус.
 * Если данных хватает на полный пакет, то он последний (нулевой пакет на прием не используется). */
static inline uint8_t f_ep0_rxd_status_dir_stat(uint16_t rem_size) {
    return rem_size <= LUSB_EP0_PACKET_SIZE ? CHIP_REGFLDVAL_USB_EPR_STAT_NACK : CHIP_REGFLDVAL_USB_EPR_STAT_STALL;
}

/* Функции для копирования пользовательских данных в буфер конечной точки или
 * из буфера конечной точки. Весь доступ должен идти обязательно обращениями по
 * 16-битным словам, поэтому не можем использовать просто memcpy.
 * Если пользовательские данные имеют нечетный размер, то последнее слово обрабатываем
 * отдельно, чтобы не вылезти за границу пользовательского буфера */
static void f_ep_buf_wr(volatile uint16_t *ep_buf, const uint8_t *user_buf, uint16_t len) {
    for (uint16_t i = 0; i < len/2; ++i) {
        uint16_t wrd = *user_buf++;
        *ep_buf++ = wrd | ((uint16_t)(*user_buf++) << 8);
    }
    if (len & 1) {
        *ep_buf = *user_buf;
    }
}

static void f_ep_buf_rd(volatile const uint16_t *ep_buf, uint8_t *user_buf, uint16_t len) {
    for (uint16_t i = 0; i < len/2; ++i) {
        uint16_t wrd = *ep_buf++;
        *user_buf++ = wrd & 0xFF;
        *user_buf++ = (wrd >> 8) & 0xFF;
    }
    if (len & 1) {
        *user_buf++ = *ep_buf & 0xFF;
    }
}

/************************************************************************
  разрешение (en = 1) или запрещение (en = 0) прерываний по USB
  возвращает 1, если прерывания до этого были уже разрешены, иначе 0
  ***********************************************************************/
bool lusb_ll_usbirq_en(bool en) {
    const bool res = !!(NVIC->ISER[((uint32_t)(USB_IRQn) >> 5)] & (1 << ((uint32_t)(USB_IRQn) & 0x1F)));
    if (en) {
        NVIC_EnableIRQ(USB_IRQn);
    } else {
        NVIC_DisableIRQ(USB_IRQn);
    }
    return res;
}


/**************************************************************************************
    сброс регистров USB контроллера - выполняется при инициализации и по bus reset
 **************************************************************************************/
void lusb_ll_reset(void) {

    f_btable[0].SB.TX.ADDR = USB_MEM_ADDR_OFFS(&f_ep0_tx_buf[0]);
    f_btable[0].SB.RX.ADDR = USB_MEM_ADDR_OFFS(&f_ep0_rx_buf[0]);
    f_btable[0].SB.RX.COUNT = LBITFIELD_SET(CHIP_REGFLD_USB_COUNT_RX_BLSIZE, CHIP_REGFLDVAL_USB_COUNT_RX_BLSIZE_32B) |
            LBITFIELD_SET(CHIP_REGFLD_USB_COUNT_RX_NUM_BLOCK, 1);
    f_ep0_set_stat(CHIP_REGFLDVAL_USB_EPR_STAT_VALID, CHIP_REGFLDVAL_USB_EPR_STAT_NACK, false);
    CHIP_REGS_USB->BTABLE = USB_MEM_ADDR_OFFS(&f_btable[0]);

    lusb_ll_set_addr(0);
}


/***************************************************************************************
 *  Инициализация usb-интерфейса
 *************************************************************************************/
int lusb_ll_init(void) {
    int res = LUSB_ERR_SUCCESS;

    /* разрешаем клоки блока USB и выполняем общий сброс */
    chip_per_clk_en(CHIP_PER_ID_USB);
    chip_per_rst(CHIP_PER_ID_USB);

    /* Снимаем Power-Down бит для включения питания аналоговой части */
    CHIP_REGS_USB->CNTR &= ~CHIP_REGFLD_USB_CNTR_PDWN;
    /* Требуется выдержать время на включение перед съемом FRES */
    chip_delay_clk((CHIP_CLK_CPU_FREQ * USB_STARTUP_TIME_US + 999999)/ 1000000);
    /* Снимаем сброс USB */
    CHIP_REGS_USB->CNTR &= ~CHIP_REGFLD_USB_CNTR_FRES;
    /* Сброс битов прерываний */
    CHIP_REGS_USB->ISTR = 0;

    memset((void*)&f_btable[0], 0, sizeof(f_btable));

    lusb_ll_reset();
#ifdef LUSB_INTERRUPT
    CHIP_REGS_USB->CNTR |= CHIP_REGFLD_USB_CNTR_RESET_M
            //| CHIP_REGFLD_USB_CNTR_SUSP_M
            | CHIP_REGFLD_USB_CNTR_CTR_M
            | CHIP_REGFLD_USB_CNTR_ERR_M;
    lusb_ll_usbirq_en(true);
#endif
    return res;
}

int lusb_ll_close(void) {
#ifdef LUSB_INTERRUPT
    NVIC_DisableIRQ(USB_IRQn);
#endif
    CHIP_REGS_USB->CNTR |= CHIP_REGFLD_USB_CNTR_FRES | CHIP_REGFLD_USB_CNTR_PDWN;

    chip_per_clk_dis(CHIP_PER_ID_USB);

    return 0;
}


//подключение к хосту (вкл. поддтяжки)
void lusb_ll_connect(bool con) {
    if (con) {
        CHIP_REGS_USB->BCDR |= CHIP_REGFLD_USB_BCDR_DPPU;
    } else {
        CHIP_REGS_USB->BCDR &= ~CHIP_REGFLD_USB_BCDR_DPPU;
    }
}


int lusb_ll_get_speed(void) {
    return LUSB_SPEED_FULL;
}

void lusb_ll_ep0_set_state(t_lusb_ll_ep0_state state, uint16_t total_size) {
    if (state == LUSB_LL_EP0_STATE_STATUS_RX) {
        f_ep0_set_stat(CHIP_REGFLDVAL_USB_EPR_STAT_VALID,
                       CHIP_REGFLDVAL_USB_EPR_STAT_STALL, true);
    } else if (state == LUSB_LL_EP0_STATE_STATUS_TX) {
        f_ep0_set_stat(CHIP_REGFLDVAL_USB_EPR_STAT_STALL,
                       CHIP_REGFLDVAL_USB_EPR_STAT_NACK, false);
    } else if (state == LUSB_LL_EP0_STATE_DATA_RX) {
        f_ep0_set_stat(CHIP_REGFLDVAL_USB_EPR_STAT_VALID, f_ep0_rxd_status_dir_stat(total_size), false);
    } else if (state == LUSB_LL_EP0_STATE_DATA_TX) {
        f_ep0_set_stat(CHIP_REGFLDVAL_USB_EPR_STAT_STALL,
                       CHIP_REGFLDVAL_USB_EPR_STAT_NACK, false);
    }
}


//stall для управляющей нулевой конечной точки (сразу и на in и на out)
void lusb_ll_ep0_stall(void) {
    f_ep0_set_stat(CHIP_REGFLDVAL_USB_EPR_STAT_STALL, CHIP_REGFLDVAL_USB_EPR_STAT_STALL, false);
}


int lusb_ll_ep0_rd_packet(uint8_t *buf, uint32_t flags, uint16_t rem_size) {
    uint16_t rx_size = LBITFIELD_GET(f_btable[0].SB.RX.COUNT, CHIP_REGFLD_USB_COUNT_RX_COUNT);
    if (rx_size > rem_size) {
        rx_size = rem_size;
    }
    f_ep_buf_rd(f_ep0_rx_buf, buf, rx_size);

    /* Для приема SETUP состояние ep изменяется после анализа управляющего пакета
     * при вызове lusb_ll_ep0_set_state(). */
    if (!(flags & LUSB_PKTFLAGS_SETUP)) {
        /* если прочитали статус, то готовы принимать следующий SETUP пакет
           (при этом NACK для того, чтобы отложить следующую стадию за SETUP) */
        if (flags & LUSB_PKTFLAGS_STATUS) {
            f_ep0_set_stat(CHIP_REGFLDVAL_USB_EPR_STAT_VALID, CHIP_REGFLDVAL_USB_EPR_STAT_NACK, false);
        }   else {
            f_ep0_set_stat(rx_size < LUSB_EP0_PACKET_SIZE ? CHIP_REGFLDVAL_USB_EPR_STAT_STALL :
                                                            CHIP_REGFLDVAL_USB_EPR_STAT_VALID,
                           f_ep0_rxd_status_dir_stat(rem_size - rx_size), false);
        }
    }

    return rx_size;
}


int lusb_ll_ep0_wr_packet(const uint8_t *buf, uint32_t flags, uint16_t size) {
    if (size > sizeof(f_ep0_rx_buf)) {
        size = sizeof(f_ep0_tx_buf);
    }
    f_ep_buf_wr(f_ep0_tx_buf, buf, size);

    f_btable->SB.TX.COUNT = size;
    chip_dmb();

    /* если завершили запись стутуса, то готовы принимать следующий SETUP.
     * состояние на передачу само сменится с VALID на NACK по завершению передачи статуса */
    if (flags & LUSB_PKTFLAGS_STATUS) {
        f_ep0_set_stat(CHIP_REGFLDVAL_USB_EPR_STAT_VALID, CHIP_REGFLDVAL_USB_EPR_STAT_VALID, false);
    } else {
        if (flags & LUSB_PKTFLAGS_LAST_DATA) {
            f_ep0_set_stat(CHIP_REGFLDVAL_USB_EPR_STAT_NACK, CHIP_REGFLDVAL_USB_EPR_STAT_VALID, true);
        } else {
            f_ep0_set_stat(CHIP_REGFLDVAL_USB_EPR_STAT_STALL, CHIP_REGFLDVAL_USB_EPR_STAT_VALID, true);
        }
    }
    return size;
}



int lusb_ll_set_addr(uint8_t addr) {    
    CHIP_REGS_USB->DADDR = LBITFIELD_SET(CHIP_REGFLD_USB_DADDR_ADD, addr) |
            LBITFIELD_SET(CHIP_REGFLD_USB_DADDR_EF, 1);
    return LUSB_ERR_SUCCESS;
}


void lusb_ll_configure(int conf) {

}



#if LUSB_EP_NONCTL_CNT
//перевод конечной точки в stall режим с адресом ep_addr
void lusb_ll_ep_set_stall(uint8_t ep_addr) {

}

//сброс stall-режима для конечной точки с адресом ep_addr
void lusb_ll_ep_clr_stall(uint8_t ep_addr) {

}




int lusb_ll_ep_enable(const t_usb_endpoint_descriptor *ep_descr) {

    return 0;
}


int lusb_ll_ep_disable(const t_usb_endpoint_descriptor *ep_descr) {

    return 0;
}

void lusb_ll_ep_clr(const t_usb_endpoint_descriptor *ep_descr) {

}

int lusb_ll_ep_rd_packet_start(const t_usb_endpoint_descriptor *ep_descr, unsigned size) {
    int ret = LUSB_ERR_EP_BUSY;

    return ret;
}


int lusb_ll_ep_rd_packet(const t_usb_endpoint_descriptor *ep_descr, uint8_t *buf, unsigned size) {
    int ret = LUSB_ERR_EP_BUSY;


    return ret;
}


int lusb_ll_ep_wr_packet(const t_usb_endpoint_descriptor *ep_descr, const uint8_t *buf, unsigned size) {
    int ret = LUSB_ERR_EP_BUSY;

    return ret;
}


#endif


/* **********************************************************************************
 * опрос флагов прерывания USB-модуля
 * обработка и вызов требуемых callback-функций lusb_core
 * **********************************************************************************/
static void f_lusb_ll_isr(void) {
    uint16_t irq_stat = CHIP_REGS_USB->ISTR;

    if (irq_stat & CHIP_REGFLD_USB_ISTR_RESET) {
        lusb_ll_reset();
        lusb_core_cb_reset();
    } else if (irq_stat & CHIP_REGFLD_USB_ISTR_SUSP) {
        lusb_core_cb_susp_changed(true);
    }


    if (irq_stat & CHIP_REGFLD_USB_ISTR_CTR) {

        const uint8_t ep_id = LBITFIELD_GET(irq_stat, CHIP_REGFLD_USB_ISTR_EP_ID);
        const bool is_rx = (LBITFIELD_GET(irq_stat, CHIP_REGFLD_USB_ISTR_DIR) == CHIP_REGFLDVAL_USB_ISTR_DIR_RX);

        if (ep_id == 0) {
            if (is_rx) {
                const bool is_setup = CHIP_REGS_USB->EP[0].EPR & CHIP_REGFLD_USB_EPR_SETUP;
                CHIP_REGS_USB->EP[0].EPR = USB_EP0_EPR_CFG | (CHIP_REGS_USB->EP[0].EPR & CHIP_REGFLD_USB_EPR_EP_KIND) | CHIP_REGFLD_USB_EPR_CTR_TX;
                lusb_core_cb_packet_rx(0, is_setup ? LUSB_PKTFLAGS_SETUP : 0);
            } else {
                CHIP_REGS_USB->EP[0].EPR = USB_EP0_EPR_CFG | (CHIP_REGS_USB->EP[0].EPR & CHIP_REGFLD_USB_EPR_EP_KIND) | CHIP_REGFLD_USB_EPR_CTR_RX;
                lusb_core_cb_packet_tx(USB_EP_IN(0), 0);
            }
        }
    }

    CHIP_REGS_USB->ISTR = ~irq_stat;


}


#ifdef LUSB_INTERRUPT
#include "lcspec_interrupt.h"
void USB_IRQHandler(void) {
    f_lusb_ll_isr();
}
#endif


/**************************************************************************
 *  Выполнение фоновых задач для порта
 *  очистка памяти для дескрипторов
 *  и отслеживание дисконнекта/конекта к usb-шине
 *  (так как аппаратно коннект не отслеживается...)
 * ************************************************************************ */
void lusb_ll_progress(void) {
#ifndef LUSB_INTERRUPT
    f_lusb_ll_isr();
#endif
}
