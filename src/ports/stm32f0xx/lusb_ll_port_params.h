#ifndef LUSB_LL_PORT_PARAMS_H
#define LUSB_LL_PORT_PARAMS_H

#include "lusb_config.h"

#define LUSB_EP_NONCTL_CNT_MAX          4
#define LUSB_PORT_EP0_ZEROLEN_TX_AUTO   0

#endif // LUSB_LL_PORT_PARAMS_H
