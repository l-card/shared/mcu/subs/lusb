#ifndef LUSB_LL_PORT_PARAMS_H
#define LUSB_LL_PORT_PARAMS_H

#include "lusb_port_config.h"

#define LUSB_PORT_SUPPORT_DD 1

#define LUSB_EP_NONCTL_CNT_MAX 15

#endif // LUSB_LL_PORT_PARAMS_H
