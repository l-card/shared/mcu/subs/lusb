/*
 * lusb_port_config.h
 *
 *  Created on: 05.07.2010
 *      Author: borisov
 *  Файл содержит пример настроек порта стека lusb под LPC17xx
 *  Каждый проект должен содержать копию этого файла с требуемыми настройками
 */

#ifndef __LUSB_PORT_CONFIG_H__
#define __LUSB_PORT_CONFIG_H__

#include "lusb_config_params.h"

#define LUSB_LPC17XX_PORT


#define LUSB_PORT_DESCR_MEM(var)  var __attribute__ ((section (".usb_ram")))


/* Передача нулевого пакета при зачистке DMA. Софт должен быть
 * готов к приему нулевого пакета, но без этого возможна редкая
 * потеря первого пакета после зачистки */
#define LUSB_PORT_ZERO_PACKET_ON_CLEAR


//настройки для порта
#define LUSB_INTERRUPT_PRIORITY     0x10   //приоритет прерываний USB
#define LUSB_DMA_DESCR_NONISO_CNT     10   //максимальное кол-во дескрипторов DMA
                                           //для всех неизохронных точек
//LUSB_DISABLE_LED_PIN
//LUSB_DISABLE_CONNECT_PIN
//LUSB_DISABLE_SENSE_PIN

//признак ручного управления состоянием 2-х цветного светодиода
#define LUSB_LINK_STATUS_CONTROL

#ifdef LUSB_LINK_STATUS_CONTROL
    //если включен, то без соединения - оранжевый свет (иначе - зеленый)
    #define LUSB_LED_UNCON_ORANGE
    //интервал "мигания" при приходе пакета данных
    #define LUSB_LED_BLINK_TOUT  75
    //интервал после "мигания" в течении которого запросы на новое игнорируются
    #define LUSB_LED_GUARD_TOUT  75

    //макросы для управления зеленым светодиодом
    #define LUSB_LED_GREEN_SET_DIR() LPC_GPIO1->FIODIR |= (1<<19)
    #define LUSB_LED_GREEN_SET() LPC_GPIO1->FIOSET = (1<<19)
    #define LUSB_LED_GREEN_CLR() LPC_GPIO1->FIOCLR = (1<<19)
    //макросы для управления красным светодиодом
    #define LUSB_LED_RED_SET_DIR()  LPC_GPIO1->FIODIR |= (1<<18)
    #define LUSB_LED_RED_SET()      LPC_GPIO1->FIOCLR  = (1<<18)
    #define LUSB_LED_RED_CLR()      LPC_GPIO1->FIOSET  = (1<<18)
#endif

#endif /* LUSB_CONFIG_H_ */
