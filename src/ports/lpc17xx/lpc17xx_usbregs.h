/*
 * lpc17xx_usbregs.h
 *
 *  Created on: 12.07.2010
 *      Author: borisov
 */

#ifndef __LPC17XX_USBREGS_H__
#define __LPC17XX_USBREGS_H__

/*******************************************************************************************************
 *                                    USB                                                              *
 *******************************************************************************************************/
//USB Clock Control register
#define LPC_USB_ClkCtrl_DEV_CLK_EN_Pos           (1)
#define LPC_USB_ClkCtrl_DEV_CLK_EN_Msk           (0x1UL << LPC_USB_ClkCtrl_DEV_CLK_EN_Pos)

#define LPC_USB_ClkCtrl_PORTSEL_CLK_EN_Pos       (3)
#define LPC_USB_ClkCtrl_PORTSEL_CLK_EN_Msk       (0x1UL << LPC_USB_ClkCtrl_PORTSEL_CLK_EN_Pos)

#define LPC_USB_ClkCtrl_AHB_CLK_EN_Pos           (4)
#define LPC_USB_ClkCtrl_AHB_CLK_EN_Msk           (0x1UL << LPC_USB_ClkCtrl_AHB_CLK_EN_Pos)


//USB Clock Status register
#define LPC_USB_ClkSt_DEV_CLK_ON_Pos             (1)
#define LPC_USB_ClkSt_DEV_CLK_ON_Msk             (0x1UL << LPC_USB_ClkSt_DEV_CLK_ON_Pos)

#define LPC_USB_ClkSt_PORTSEL_CLK_ON_Pos         (3)
#define LPC_USB_ClkSt_PORTSEL_CLK_ON_Msk         (0x1UL << LPC_USB_ClkSt_PORTSEL_CLK_ON_Pos)

#define LPC_USB_ClkSt_AHB_CLK_ON_Pos             (4)
#define LPC_USB_ClkSt_AHB_CLK_ON_Msk             (0x1UL << LPC_USB_ClkSt_AHB_CLK_ON_Pos)

//USB Interrupt Status register
#define LPC_USB_IntSt_INT_REQ_LP_Pos             (0)
#define LPC_USB_IntSt_INT_REQ_LP_Msk             (0x1UL << LPC_USB_IntSt_INT_REQ_LP_Pos)

#define LPC_USB_IntSt_INT_REQ_HP_Pos             (1)
#define LPC_USB_IntSt_INT_REQ_HP_Msk             (0x1UL << LPC_USB_IntSt_INT_REQ_HP_Pos)

#define LPC_USB_IntSt_INT_REQ_DMA_Pos            (2)
#define LPC_USB_IntSt_INT_REQ_DMA_Msk            (0x1UL << LPC_USB_IntSt_INT_REQ_DMA_Pos)

#define LPC_USB_IntSt_USB_NEED_CLK_Pos           (8)
#define LPC_USB_IntSt_USB_NEED_CLK_Msk           (0x1UL << LPC_USB_IntSt_USB_NEED_CLK_Pos)

#define LPC_USB_IntSt_EN_USB_INTS_Pos            (31)
#define LPC_USB_IntSt_EN_USB_INTS_Msk            (0x1UL << LPC_USB_IntSt_EN_USB_INTS_Pos)


//USB Device Interrupt Status register
#define LPC_USB_DevIntSt_FRAME_Pos               (0)
#define LPC_USB_DevIntSt_FRAME_Msk               (0x1UL << LPC_USB_DevIntSt_FRAME_Pos)

#define LPC_USB_DevIntSt_EP_FAST_Pos             (1)
#define LPC_USB_DevIntSt_EP_FAST_Msk             (0x1UL << LPC_USB_DevIntSt_EP_FAST_Pos)

#define LPC_USB_DevIntSt_EP_SLOW_Pos             (2)
#define LPC_USB_DevIntSt_EP_SLOW_Msk             (0x1UL << LPC_USB_DevIntSt_EP_SLOW_Pos)

#define LPC_USB_DevIntSt_DEV_STAT_Pos            (3)
#define LPC_USB_DevIntSt_DEV_STAT_Msk            (0x1UL << LPC_USB_DevIntSt_DEV_STAT_Pos)

#define LPC_USB_DevIntSt_CCEMPTY_Pos             (4)
#define LPC_USB_DevIntSt_CCEMPTY_Msk             (0x1UL << LPC_USB_DevIntSt_CCEMPTY_Pos)

#define LPC_USB_DevIntSt_CDFULL_Pos              (5)
#define LPC_USB_DevIntSt_CDFULL_Msk              (0x1UL << LPC_USB_DevIntSt_CDFULL_Pos)

#define LPC_USB_DevIntSt_RxENDPKT_Pos            (6)
#define LPC_USB_DevIntSt_RxENDPKT_Msk            (0x1UL << LPC_USB_DevIntSt_RxENDPKT_Pos)

#define LPC_USB_DevIntSt_TxENDPKT_Pos            (7)
#define LPC_USB_DevIntSt_TxENDPKT_Msk            (0x1UL << LPC_USB_DevIntSt_TxENDPKT_Pos)

#define LPC_USB_DevIntSt_EP_RLZED_Pos            (8)
#define LPC_USB_DevIntSt_EP_RLZED_Msk            (0x1UL << LPC_USB_DevIntSt_EP_RLZED_Pos)

#define LPC_USB_DevIntSt_ERR_INT_Pos             (9)
#define LPC_USB_DevIntSt_ERR_INT_Msk             (0x1UL << LPC_USB_DevIntSt_ERR_INT_Pos)


//USB Device Interrupt Enable register
#define LPC_USB_DevIntEn_FRAME_Pos               (0)
#define LPC_USB_DevIntEn_FRAME_Msk               (0x1UL << LPC_USB_DevIntEn_FRAME_Pos)

#define LPC_USB_DevIntEn_EP_FAST_Pos             (1)
#define LPC_USB_DevIntEn_EP_FAST_Msk             (0x1UL << LPC_USB_DevIntEn_EP_FAST_Pos)

#define LPC_USB_DevIntEn_EP_SLOW_Pos             (2)
#define LPC_USB_DevIntEn_EP_SLOW_Msk             (0x1UL << LPC_USB_DevIntEn_EP_SLOW_Pos)

#define LPC_USB_DevIntEn_DEV_STAT_Pos            (3)
#define LPC_USB_DevIntEn_DEV_STAT_Msk            (0x1UL << LPC_USB_DevIntEn_DEV_STAT_Pos)

#define LPC_USB_DevIntEn_CCEMPTY_Pos             (4)
#define LPC_USB_DevIntEn_CCEMPTY_Msk             (0x1UL << LPC_USB_DevIntEn_CCEMPTY_Pos)

#define LPC_USB_DevIntEn_CDFULL_Pos              (5)
#define LPC_USB_DevIntEn_CDFULL_Msk              (0x1UL << LPC_USB_DevIntEn_CDFULL_Pos)

#define LPC_USB_DevIntEn_RxENDPKT_Pos            (6)
#define LPC_USB_DevIntEn_RxENDPKT_Msk            (0x1UL << LPC_USB_DevIntEn_RxENDPKT_Pos)

#define LPC_USB_DevIntEn_TxENDPKT_Pos            (7)
#define LPC_USB_DevIntEn_TxENDPKT_Msk            (0x1UL << LPC_USB_DevIntEn_TxENDPKT_Pos)

#define LPC_USB_DevIntEn_EP_RLZED_Pos            (8)
#define LPC_USB_DevIntEn_EP_RLZED_Msk            (0x1UL << LPC_USB_DevIntEn_EP_RLZED_Pos)

#define LPC_USB_DevIntEn_ERR_INT_Pos             (9)
#define LPC_USB_DevIntEn_ERR_INT_Msk             (0x1UL << LPC_USB_DevIntEn_ERR_INT_Pos)


//USB Device Interrupt Clear register
#define LPC_USB_DevIntClr_FRAME_Pos               (0)
#define LPC_USB_DevIntClr_FRAME_Msk               (0x1UL << LPC_USB_DevIntClr_FRAME_Pos)

#define LPC_USB_DevIntClr_EP_FAST_Pos             (1)
#define LPC_USB_DevIntClr_EP_FAST_Msk             (0x1UL << LPC_USB_DevIntClr_EP_FAST_Pos)

#define LPC_USB_DevIntClr_EP_SLOW_Pos             (2)
#define LPC_USB_DevIntClr_EP_SLOW_Msk             (0x1UL << LPC_USB_DevIntClr_EP_SLOW_Pos)

#define LPC_USB_DevIntClr_DEV_STAT_Pos            (3)
#define LPC_USB_DevIntClr_DEV_STAT_Msk            (0x1UL << LPC_USB_DevIntClr_DEV_STAT_Pos)

#define LPC_USB_DevIntClr_CCEMPTY_Pos             (4)
#define LPC_USB_DevIntClr_CCEMPTY_Msk             (0x1UL << LPC_USB_DevIntClr_CCEMPTY_Pos)

#define LPC_USB_DevIntClr_CDFULL_Pos              (5)
#define LPC_USB_DevIntClr_CDFULL_Msk              (0x1UL << LPC_USB_DevIntClr_CDFULL_Pos)

#define LPC_USB_DevIntClr_RxENDPKT_Pos            (6)
#define LPC_USB_DevIntClr_RxENDPKT_Msk            (0x1UL << LPC_USB_DevIntClr_RxENDPKT_Pos)

#define LPC_USB_DevIntClr_TxENDPKT_Pos            (7)
#define LPC_USB_DevIntClr_TxENDPKT_Msk            (0x1UL << LPC_USB_DevIntClr_TxENDPKT_Pos)

#define LPC_USB_DevIntClr_EP_RLZED_Pos            (8)
#define LPC_USB_DevIntClr_EP_RLZED_Msk            (0x1UL << LPC_USB_DevIntClr_EP_RLZED_Pos)

#define LPC_USB_DevIntClr_ERR_INT_Pos             (9)
#define LPC_USB_DevIntClr_ERR_INT_Msk             (0x1UL << LPC_USB_DevIntClr_ERR_INT_Pos)


//USB Device Interrupt Set register
#define LPC_USB_DevIntSet_FRAME_Pos               (0)
#define LPC_USB_DevIntSet_FRAME_Msk               (0x1UL << LPC_USB_DevIntSet_FRAME_Pos)

#define LPC_USB_DevIntSet_EP_FAST_Pos             (1)
#define LPC_USB_DevIntSet_EP_FAST_Msk             (0x1UL << LPC_USB_DevIntSet_EP_FAST_Pos)

#define LPC_USB_DevIntSet_EP_SLOW_Pos             (2)
#define LPC_USB_DevIntSet_EP_SLOW_Msk             (0x1UL << LPC_USB_DevIntSet_EP_SLOW_Pos)

#define LPC_USB_DevIntSet_DEV_STAT_Pos            (3)
#define LPC_USB_DevIntSet_DEV_STAT_Msk            (0x1UL << LPC_USB_DevIntSet_DEV_STAT_Pos)

#define LPC_USB_DevIntSet_CCEMPTY_Pos             (4)
#define LPC_USB_DevIntSet_CCEMPTY_Msk             (0x1UL << LPC_USB_DevIntSet_CCEMPTY_Pos)

#define LPC_USB_DevIntSet_CDFULL_Pos              (5)
#define LPC_USB_DevIntSet_CDFULL_Msk              (0x1UL << LPC_USB_DevIntSet_CDFULL_Pos)

#define LPC_USB_DevIntSet_RxENDPKT_Pos            (6)
#define LPC_USB_DevIntSet_RxENDPKT_Msk            (0x1UL << LPC_USB_DevIntSet_RxENDPKT_Pos)

#define LPC_USB_DevIntSet_TxENDPKT_Pos            (7)
#define LPC_USB_DevIntSet_TxENDPKT_Msk            (0x1UL << LPC_USB_DevIntSet_TxENDPKT_Pos)

#define LPC_USB_DevIntSet_EP_RLZED_Pos            (8)
#define LPC_USB_DevIntSet_EP_RLZED_Msk            (0x1UL << LPC_USB_DevIntSet_EP_RLZED_Pos)

#define LPC_USB_DevIntSet_ERR_INT_Pos             (9)
#define LPC_USB_DevIntSet_ERR_INT_Msk             (0x1UL << LPC_USB_DevIntSet_ERR_INT_Pos)


//USB Device Interrupt Priority register
#define LPC_USB_DevIntPri_FRAME_Pos               (0)
#define LPC_USB_DevIntPri_FRAME_Msk               (0x1UL << LPC_USB_DevIntPri_FRAME_Pos)

#define LPC_USB_DevIntPri_EP_FAST_Pos             (1)
#define LPC_USB_DevIntPri_EP_FAST_Msk             (0x1UL << LPC_USB_DevIntPri_EP_FAST_Pos)


//USB Endpoint Interrupt Status register

//USB Endpoint Interrupt Enable register

//USB Endpoint Interrupt Clear register

//USB Endpoint Interrupt Set register

//USB Endpoint Interrupt Priority register


//USB Receive Packet Length register (USBRxPlen - address 0x5000 C220)
#define LPC_USB_RxPlen_PKT_LNGTH_Pos               (0)
#define LPC_USB_RxPlen_PKT_LNGTH_Msk               (0x3FFUL << LPC_USB_RxPlen_PKT_LNGTH_Pos)

#define LPC_USB_RxPlen_DV_Pos                      (10)
#define LPC_USB_RxPlen_DV_Msk                      (0x1UL << LPC_USB_RxPlen_DV_Pos)

#define LPC_USB_RxPlen_PKT_RDY_Pos                 (11)
#define LPC_USB_RxPlen_PKT_RDY_Msk                 (0x1UL << LPC_USB_RxPlen_PKT_RDY_Pos)


//USB Control register (USBCtrl - 0x5000 C228)
#define LPC_USB_Ctrl_RD_EN_Pos                     (0)
#define LPC_USB_Ctrl_RD_EN_Msk                     (0x1UL << LPC_USB_Ctrl_RD_EN_Pos)

#define LPC_USB_Ctrl_WR_EN_Pos                     (1)
#define LPC_USB_Ctrl_WR_EN_Msk                     (0x1UL << LPC_USB_Ctrl_WR_EN_Pos)

#define LPC_USB_Ctrl_LOG_ENDPOINT_Pos              (2)
#define LPC_USB_Ctrl_LOG_ENDPOINT_Msk              (0xFUL << LPC_USB_Ctrl_LOG_ENDPOINT_Pos)

//USB Command Code register (USBCmdCode - 0x5000 C210)
#define LPC_USB_CmdCode_CMD_PHASE_Pos              (8)
#define LPC_USB_CmdCode_CMD_PHASE_Msk              (0xFFUL << LPC_USB_CmdCode_CMD_PHASE_Pos)

#define LPC_USB_CmdCode_CMD_CODE_Pos               (16)
#define LPC_USB_CmdCode_CMD_CODE_Msk               (0xFFUL << LPC_USB_CmdCode_CMD_CODE_Pos)

#define LPC_USB_CmdCode_CMD_WDATA_Pos              (16)
#define LPC_USB_CmdCode_CMD_WDATA_Msk              (0xFFUL << LPC_USB_CmdCode_CMD_WDATA_Pos)

//фазы команд к SIE
#define LPC_USB_CMDPHASE_Command                   (0x05)
#define LPC_USB_CMDPHASE_Read                      (0x02)
#define LPC_USB_CMDPHASE_Write                     (0x01)


//--------------------------------   биты регистров SIE для различных команд --------------------------
//Set Address Command
#define LPC_USB_CMD_SETADDR_DEV_ADDR_Pos           (0)
#define LPC_USB_CMD_SETADDR_DEV_ADDR_Msk           (0x3FUL << LPC_USB_CMD_SETADDR_DEV_ADDR_Pos)

#define LPC_USB_CMD_SETADDR_DEV_EN_Pos             (7)
#define LPC_USB_CMD_SETADDR_DEV_EN_Msk             (0x1UL << LPC_USB_CMD_SETADDR_DEV_EN_Pos)

//Configure Device Command
#define LPC_USB_CMD_CONFDEV_CONF_Pos               (0)
#define LPC_USB_CMD_CONFDEV_CONF_Msk               (0x1UL << LPC_USB_CMD_CONFDEV_CONF_Pos)


//Set Device Status Command
#define LPC_USB_CMD_DEVSTATUS_CON_Pos              (0)
#define LPC_USB_CMD_DEVSTATUS_CON_Msk              (0x1UL << LPC_USB_CMD_DEVSTATUS_CON_Pos)

#define LPC_USB_CMD_DEVSTATUS_CON_CH_Pos           (1)
#define LPC_USB_CMD_DEVSTATUS_CON_CH_Msk           (0x1UL << LPC_USB_CMD_DEVSTATUS_CON_CH_Pos)

#define LPC_USB_CMD_DEVSTATUS_SUS_Pos              (2)
#define LPC_USB_CMD_DEVSTATUS_SUS_Msk              (0x1UL << LPC_USB_CMD_DEVSTATUS_SUS_Pos)

#define LPC_USB_CMD_DEVSTATUS_SUS_CH_Pos           (3)
#define LPC_USB_CMD_DEVSTATUS_SUS_CH_Msk           (0x1UL << LPC_USB_CMD_DEVSTATUS_SUS_CH_Pos)

#define LPC_USB_CMD_DEVSTATUS_RST_Pos              (4)
#define LPC_USB_CMD_DEVSTATUS_RST_Msk              (0x1UL << LPC_USB_CMD_DEVSTATUS_RST_Pos)

//Set Endpoint Status Command
#define LPC_USB_CMD_EPSTATUS_ST_Pos                (0)
#define LPC_USB_CMD_EPSTATUS_ST_Msk                (0x1UL << LPC_USB_CMD_EPSTATUS_ST_Pos)

#define LPC_USB_CMD_EPSTATUS_DA_Pos                (5)
#define LPC_USB_CMD_EPSTATUS_DA_Msk                (0x1UL << LPC_USB_CMD_EPSTATUS_DA_Pos)

#define LPC_USB_CMD_EPSTATUS_RF_MO_Pos             (6)
#define LPC_USB_CMD_EPSTATUS_RF_MO_Msk             (0x1UL << LPC_USB_CMD_EPSTATUS_RF_MO_Pos)

#define LPC_USB_CMD_EPSTATUS_CND_ST_Pos            (7)
#define LPC_USB_CMD_EPSTATUS_CND_ST_Msk            (0x1UL << LPC_USB_CMD_EPSTATUS_CND_ST_Pos)

//Clear Buffer Command
#define LPC_USB_CMD_CLRBUFFER_PO_Pos               (0)
#define LPC_USB_CMD_CLRBUFFER_PO_Msk               (0x1UL << LPC_USB_CMD_CLRBUFFER_PO_Pos)

//Set Mode Command
#define LPC_USB_CMD_SETMODE_AP_CLK_Pos             (0)
#define LPC_USB_CMD_SETMODE_AP_CLK_Msk             (0x1UL << LPC_USB_CMD_SETMODE_AP_CLK_Pos)

#define LPC_USB_CMD_SETMODE_INAK_CI_Pos            (1)
#define LPC_USB_CMD_SETMODE_INAK_CI_Msk            (0x1UL << LPC_USB_CMD_SETMODE_INAK_CI_Pos)

#define LPC_USB_CMD_SETMODE_INAK_CO_Pos            (2)
#define LPC_USB_CMD_SETMODE_INAK_CO_Msk            (0x1UL << LPC_USB_CMD_SETMODE_INAK_CO_Pos)

#define LPC_USB_CMD_SETMODE_INAK_II_Pos            (3)
#define LPC_USB_CMD_SETMODE_INAK_II_Msk            (0x1UL << LPC_USB_CMD_SETMODE_INAK_II_Pos)

#define LPC_USB_CMD_SETMODE_INAK_IO_Pos            (4)
#define LPC_USB_CMD_SETMODE_INAK_IO_Msk            (0x1UL << LPC_USB_CMD_SETMODE_INAK_IO_Pos)

#define LPC_USB_CMD_SETMODE_INAK_BI_Pos            (5)
#define LPC_USB_CMD_SETMODE_INAK_BI_Msk            (0x1UL << LPC_USB_CMD_SETMODE_INAK_BI_Pos)

#define LPC_USB_CMD_SETMODE_INAK_BO_Pos            (6)
#define LPC_USB_CMD_SETMODE_INAK_BO_Msk            (0x1UL << LPC_USB_CMD_SETMODE_INAK_BO_Pos)

// -------------------  коды команд SIE ---------------------------
#define LPC_USB_CMDCODE_SET_ADDRESS           (0xD0)
#define LPC_USB_CMDCODE_CONFIGURE_DEVICE      (0xD8)
#define LPC_USB_CMDCODE_SET_MODE              (0xF3)
#define LPC_USB_CMDCODE_READ_CUR_FRAME_NUM    (0xF5)
#define LPC_USB_CMDCODE_READ_TEST_REG         (0xFD)
#define LPC_USB_CMDCODE_SET_DEVICE_STATUS     (0xFE)
#define LPC_USB_CMDCODE_GET_DEVICE_STATUS     (0xFE)
#define LPC_USB_CMDCODE_GET_ERROR_CODE        (0xFF)
#define LPC_USB_CMDCODE_READ_ERROR_STATUS     (0xFB)
#define LPC_USB_CMDCODE_EP_SELECT(ep)         (ep)
#define LPC_USB_CMDCODE_EP_SELECT_CLRINT(ep)  (ep+0x40)
#define LPC_USB_CMDCODE_EP_SET_STATUS(ep)     (ep+0x40)
#define LPC_USB_CMDCODE_EP_CLEAR_BUFFER       (0xF2)
#define LPC_USB_CMDCODE_EP_VALIDATE_BUFFER    (0xFA)



//USB Command Data register (USBCmdData - 0x5000 C214)
#define LPC_USB_CmdData_CMD_RDATA_Pos              (0)
#define LPC_USB_CmdData_CMD_RDATA_Msk              (0xFFUL << LPC_USB_CmdData_CMD_RDATA_Pos)


//USB DMA Interrupt Status register (USBDMAIntSt - 0x5000 C290)
#define LPC_USB_DMAIntSt_EOT_Pos                   (0)
#define LPC_USB_DMAIntSt_EOT_Msk                   (0x01UL << LPC_USB_DMAIntSt_EOT_Pos)

#define LPC_USB_DMAIntSt_NDDR_Pos                  (1)
#define LPC_USB_DMAIntSt_NDDR_Msk                  (0x01UL << LPC_USB_DMAIntSt_NDDR_Pos)

#define LPC_USB_DMAIntSt_ERR_Pos                   (2)
#define LPC_USB_DMAIntSt_ERR_Msk                   (0x01UL << LPC_USB_DMAIntSt_ERR_Pos)

//USB DMA Interrupt Enable register (USBDMAIntEn - 0x5000 C294)
#define LPC_USB_DMAIntEn_EOT_Pos                   (0)
#define LPC_USB_DMAIntEn_EOT_Msk                   (0x01UL << LPC_USB_DMAIntEn_EOT_Pos)

#define LPC_USB_DMAIntEn_NDDR_Pos                  (1)
#define LPC_USB_DMAIntEn_NDDR_Msk                  (0x01UL << LPC_USB_DMAIntEn_NDDR_Pos)

#define LPC_USB_DMAIntEn_ERR_Pos                   (2)
#define LPC_USB_DMAIntEn_ERR_Msk                   (0x01UL << LPC_USB_DMAIntEn_ERR_Pos)



//поля дескриптора DMA для USB
//поле 1 - Control
#define LPC_USB_DDCTL_DMA_MODE_Pos                  (0)
#define LPC_USB_DDCTL_DMA_MODE_Msk                  (0x03UL << LPC_USB_DDCTL_DMA_MODE_Pos)

#define LPC_USB_DDCTL_NEXT_DD_VALID_Pos             (2)
#define LPC_USB_DDCTL_NEXT_DD_VALID_Msk             (0x01UL << LPC_USB_DDCTL_NEXT_DD_VALID_Pos)

#define LPC_USB_DDCTL_ISO_EP_Pos                    (4)
#define LPC_USB_DDCTL_ISO_EP_Msk                    (0x01UL << LPC_USB_DDCTL_ISO_EP_Pos)

#define LPC_USB_DDCTL_MAX_PACKET_SIZE_Pos           (5)
#define LPC_USB_DDCTL_MAX_PACKET_SIZE_Msk           (0x7FFUL << LPC_USB_DDCTL_MAX_PACKET_SIZE_Pos)

#define LPC_USB_DDCTL_DMA_BUFFER_LENGTH_Pos         (16)
#define LPC_USB_DDCTL_DMA_BUFFER_LENGTH_Msk         (0xFFFFUL << LPC_USB_DDCTL_DMA_BUFFER_LENGTH_Pos)

//режимы DMA
#define LPC_USB_DDCTL_DMA_MODE_NORMAL               (0 << LPC_USB_DDCTL_DMA_MODE_Pos)
#define LPC_USB_DDCTL_DMA_MODE_ATLE                 (1 << LPC_USB_DDCTL_DMA_MODE_Pos)

//поле 3 - Status
#define LPC_USB_DDSTAT_DD_RETIRED_Pos               (0)
#define LPC_USB_DDSTAT_DD_RETIRED_Msk               (0x1UL << LPC_USB_DDSTAT_DD_RETIRED_Pos)

#define LPC_USB_DDSTAT_DD_STATUS_Pos                (1)
#define LPC_USB_DDSTAT_DD_STATUS_Msk                (0xFUL << LPC_USB_DDSTAT_DD_STATUS_Pos)

#define LPC_USB_DDSTAT_PACKET_VALID_Pos             (5)
#define LPC_USB_DDSTAT_PACKET_VALID_Msk             (0x1UL << LPC_USB_DDSTAT_PACKET_VALID_Pos)

#define LPC_USB_DDSTAT_LS_BYTE_EXTRACTED_Pos        (6)
#define LPC_USB_DDSTAT_LS_BYTE_EXTRACTED_Msk        (0x1UL << LPC_USB_DDSTAT_LS_BYTE_EXTRACTED_Pos)

#define LPC_USB_DDSTAT_MS_BYTE_EXTRACTED_Pos        (7)
#define LPC_USB_DDSTAT_MS_BYTE_EXTRACTED_Msk        (0x1UL << LPC_USB_DDSTAT_MS_BYTE_EXTRACTED_Pos)

#define LPC_USB_DDSTAT_MESSAGE_LENGTH_POSITION_Pos  (8)
#define LPC_USB_DDSTAT_MESSAGE_LENGTH_POSITION_Msk  (0x3FUL << LPC_USB_DDSTAT_MESSAGE_LENGTH_POSITION_Pos)

#define LPC_USB_DDSTAT_PRESENT_DMA_COUNT_Pos        (16)
#define LPC_USB_DDSTAT_PRESENT_DMA_COUNT_Msk        (0xFFFFUL << LPC_USB_DDSTAT_PRESENT_DMA_COUNT_Pos)

//различные статусы завершения DMA
#define LPC_USB_DD_STATUS_NOT_SERVICED             (0 << LPC_USB_DDSTAT_DD_STATUS_Pos)
#define LPC_USB_DD_STATUS_BEING_SERVICED           (1 << LPC_USB_DDSTAT_DD_STATUS_Pos)
#define LPC_USB_DD_STATUS_NORMAL_COMPLITION        (2 << LPC_USB_DDSTAT_DD_STATUS_Pos)
#define LPC_USB_DD_STATUS_DATA_UNDERRUN            (3 << LPC_USB_DDSTAT_DD_STATUS_Pos)
#define LPC_USB_DD_STATUS_DATA_OVERRUN             (8 << LPC_USB_DDSTAT_DD_STATUS_Pos)
#define LPC_USB_DD_STATUS_SYSTEM_ERROR             (9 << LPC_USB_DDSTAT_DD_STATUS_Pos)


//получение физического адреса EP из логического
#define LPC_USB_EPRX_NUM(ep)                           ((ep)<<1)
#define LPC_USB_EPTX_NUM(ep)                           (((ep)<<1)+1)

#define LPC_USB_EPRX_FLG(ep)                            (1 << LPC_USB_EPRX_NUM(ep))
#define LPC_USB_EPTX_FLG(ep)                            (1 << LPC_USB_EPTX_NUM(ep))


#endif /* LPC17XX_USBREGS_H_ */
