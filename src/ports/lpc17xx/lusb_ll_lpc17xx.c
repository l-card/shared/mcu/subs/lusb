/*
 * lusb_ll.c
 *
 *  Created on: 05.07.2010
 *      Author: borisov
 */

#include "lusb_ll.h"
#include "lusb_usbdefs.h"
#include "lusb_core_cb.h"
#include "ports/lpc17xx/lpc17xx_usbregs.h"

#include "iolpc17XX.h"
#include "lpc17xx_pinfunc.h"
#include "LPC17xx.h"

#include "lusb_config_params.h"
#include "lusb_port_config.h"

#include <stdlib.h>



#ifdef LUSB_LINK_STATUS_CONTROL
    #include "ltimer.h"
#endif

#ifndef LUSB_LPC17XX_PORT
    #error "Неправильный файл настроек порта lusb_port_config.h"
#endif

#ifdef LUSB_SUPPORT_HIGH_SPEED
    #error high speed is not supported by port! undefLUSB_SUPPORT_HIGH_SPEED in lusb_config.h
#endif


#ifndef LUSB_PORT_DESCR_MEM
    #ifdef LUSB_USBMEM
        #define LUSB_PORT_DESCR_MEM(var)  LUSB_USBMEM var
    #endif
#endif


#define LUSB_PHY_EP_NUM 32


#define LPC_EP_PHY_ADDR(ep_addr)  (((ep_addr) & USB_EPDIR)? \
                                    LPC_USB_EPTX_NUM((ep_addr)&USB_EPNUM_MSK) : \
                                    LPC_USB_EPRX_NUM((ep_addr)&USB_EPNUM_MSK))

#define LPC_EP_FLAG(ep_addr)  (1<<LPC_EP_PHY_ADDR(ep_addr))


#define LPC_GET_USBV() ((LPC_GPIO1->FIOPIN3 >> 6) & 0x1)





#ifdef LUSB_LINK_STATUS_CONTROL
    //текущее состояния светодиода
    typedef enum {
        e_LEDSTATE_INIT, //начальное (usb не сконфигурирован)
        e_LEDSTATE_CONFIGURED, //usb сконфигурирован
        e_LEDSTATE_TXRX_IND, //сигнал прихода данных
        e_LEDSTATE_WAIT_AFTER_TXRX //задержка после индикации прихода
    } t_led_status;
    static t_led_status f_led_status;
    static t_ltimer f_led_timer;
#ifdef LUSB_LED_UNCON_ORANGE
    static uint8_t f_color;
#endif
    //установка состояния светодиода
    static void f_set_led_status(t_led_status status)
    {
        switch (status)
        {
        case e_LEDSTATE_INIT:

            LUSB_LED_GREEN_SET();
            LUSB_LED_RED_CLR();
#ifdef LUSB_LED_UNCON_ORANGE
            ltimer_set(&f_led_timer, 10);
            f_color = 0;
#endif
            f_led_status = status;
            break;
        case e_LEDSTATE_CONFIGURED:

            LUSB_LED_GREEN_CLR();
            LUSB_LED_RED_SET();
            f_led_status = status;
            break;
        case e_LEDSTATE_TXRX_IND:
            if (f_led_status == e_LEDSTATE_CONFIGURED)
            {
                LUSB_LED_GREEN_CLR();
                LUSB_LED_RED_CLR();
                ltimer_set(&f_led_timer, LTIMER_MS_TO_CLOCK_TICKS(LUSB_LED_BLINK_TOUT));
                f_led_status = status;
            }
            break;
        case e_LEDSTATE_WAIT_AFTER_TXRX:
            if (f_led_status == e_LEDSTATE_TXRX_IND)
            {
                LUSB_LED_GREEN_CLR();
                LUSB_LED_RED_SET();
                ltimer_set(&f_led_timer, LTIMER_MS_TO_CLOCK_TICKS(LUSB_LED_BLINK_TOUT));
                f_led_status = status;
            }
            break;
        }

    }

    //управление светодиодом в фоне
    static void f_led_process(void)
    {
#ifdef LUSB_LED_UNCON_ORANGE
        if ((f_led_status == e_LEDSTATE_INIT)
                && ltimer_expired(&f_led_timer))
        {
            if (f_color)
            {
                LUSB_LED_GREEN_SET();
                LUSB_LED_RED_CLR();
            }
            else
            {
                LUSB_LED_GREEN_CLR();
                LUSB_LED_RED_SET();
            }
            ltimer_set(&f_led_timer, f_color ? 12 : 7);
            f_color = !f_color;
        }
        else
#endif
        if ((f_led_status == e_LEDSTATE_TXRX_IND)
                    && ltimer_expired(&f_led_timer))
        {
            f_set_led_status(e_LEDSTATE_WAIT_AFTER_TXRX);
        }
        else if ((f_led_status == e_LEDSTATE_WAIT_AFTER_TXRX)
                && ltimer_expired(&f_led_timer))
        {
            f_set_led_status(e_LEDSTATE_CONFIGURED);
        }
    }
#endif



#if LUSB_EP_NONCTL_CNT
    //тип дескриптора DMA для USB (из LPC17xx User Manual)
    typedef struct {
        uint32_t NextDDPointer;
        uint16_t Control;
        uint16_t BuffLength;
        uint32_t StartAddr;
        uint16_t Status;
        uint16_t PresentDMACount;
    } t_usb_dd;


    #define LSPEC_ALIGNMENT 128
    #include "lcspec_align.h"  
    LUSB_PORT_DESCR_MEM(static volatile uint32_t f_udca[32]);  //массив адресов дескрипторов по ep
    LUSB_PORT_DESCR_MEM(static volatile uint32_t f_udca_init[32]); //массив адресов на последний необработанный дескриптор
    LUSB_PORT_DESCR_MEM(static volatile t_usb_dd f_usb_noiso_dd[LUSB_DMA_DESCR_NONISO_CNT]); //дескприпторы DMA (кроме изохронных)
    LUSB_PORT_DESCR_MEM(static volatile uint32_t f_usb_dd_mmap); //переменная показывает, какие дескрипторы заняты (1 - занят, 0 свободен)
    LUSB_PORT_DESCR_MEM(static volatile uint32_t f_usb_dd_need_free); //переменная показывает, какие дескрипторы можно освободить

    #if (LUSB_DMA_DESCR_NONISO_CNT > 32)
        #error "MAX USB DESCR CNT = 32"
    #endif

#ifdef LUSB_PORT_ZERO_PACKET_ON_CLEAR
    static uint8_t f_dummy_buf[64];
    static volatile uint32_t f_ep_req_dummy_pack;
#endif

    static volatile uint32_t f_ep_dd_processed;
    static int f_core_dd(volatile t_usb_dd* dd) {
#ifdef LUSB_PORT_ZERO_PACKET_ON_CLEAR
        return ((dd->StartAddr-dd->PresentDMACount)!=(uint32_t)f_dummy_buf);
#else
        return 1;
#endif
    }
#endif

/************************************************************************
  разрешение (en = 1) или запрещение (en = 0) прерываний по USB
  возвращает 1, если прерывания до этого были уже разрешены, иначе 0
  ***********************************************************************/
bool lusb_ll_usbirq_en(bool en) {
    const bool res = !!(NVIC->ISER[((uint32_t)(USB_IRQn) >> 5)] & (1 << ((uint32_t)(USB_IRQn) & 0x1F)));
    if (en) {
        NVIC_EnableIRQ(USB_IRQn);
    } else {
        NVIC_DisableIRQ(USB_IRQn);
    }
    return res;
}
/*************************************************************************
  разрешение новой конечной точки с физическим номером ep
  и размером буфера ep_size байт
  ************************************************************************/
static int f_usb_ep_realize(int ep, int ep_size) {
    LPC_USB->USBDevIntClr = LPC_USB_DevIntClr_EP_RLZED_Msk;
    LPC_USB->USBReEp |= (uint32_t) ((0x1 << ep));

    /* Load Endpoint index Reg with physical endpoint no.*/
    LPC_USB->USBEpInd = (uint32_t) ep;
    /* load the max packet size Register */
    LPC_USB->USBMaxPSize = ep_size;

    /* wait until endpoint realization is complete */
    while (!(LPC_USB->USBDevIntSt & LPC_USB_DevIntSt_EP_RLZED_Msk)) {}
    /* Clear the EP_RLZED bit */
    LPC_USB->USBDevIntClr = LPC_USB_DevIntClr_EP_RLZED_Msk;
    return 0;
}

#if LUSB_EP_NONCTL_CNT
static void f_usb_ep_unrealize(int ep) {
    LPC_USB->USBDevIntClr = LPC_USB_DevIntClr_EP_RLZED_Msk;
    LPC_USB->USBReEp &= ~(uint32_t) ((0x1 << ep));
    /* wait until endpoint realization is complete */
    while (!(LPC_USB->USBDevIntSt & LPC_USB_DevIntSt_EP_RLZED_Msk)) {}
    /* Clear the EP_RLZED bit */
    LPC_USB->USBDevIntClr = LPC_USB_DevIntClr_EP_RLZED_Msk;
}
#endif

//выполнение команды SIE без данных
static void usb_cmd(uint8_t cmd_code) {
    const bool int_en = lusb_ll_usbirq_en(false);
    //сброс флагов прерывания
    LPC_USB->USBDevIntClr = LPC_USB_DevIntClr_CCEMPTY_Msk |
            LPC_USB_DevIntClr_CDFULL_Msk;
    //запись кода команыды
    LPC_USB->USBCmdCode = (cmd_code << LPC_USB_CmdCode_CMD_CODE_Pos) |
            (LPC_USB_CMDPHASE_Command << LPC_USB_CmdCode_CMD_PHASE_Pos);
    //ожидание завершения записи
    while (!(LPC_USB->USBDevIntSt & LPC_USB_DevIntSt_CCEMPTY_Msk));
    //сброс флага завершения записи
    LPC_USB->USBDevIntClr = LPC_USB_DevIntSt_CCEMPTY_Msk;
    lusb_ll_usbirq_en(int_en);
}


//выполнение команды SIE с записью 1-го слова данных
static void usb_cmd_wr(uint8_t cmd_code, uint32_t wr_data) {
    const bool int_en = lusb_ll_usbirq_en(false);
    //запись кода команды
    usb_cmd(cmd_code);

    //сброс флага завершения записи
    LPC_USB->USBDevIntClr = LPC_USB_DevIntSt_CCEMPTY_Msk;
    //запись байта данных
    LPC_USB->USBCmdCode =     (wr_data << LPC_USB_CmdCode_CMD_CODE_Pos) |
            (LPC_USB_CMDPHASE_Write << LPC_USB_CmdCode_CMD_PHASE_Pos); //write data
    //ожидание завершения записи
    while (!(LPC_USB->USBDevIntSt & LPC_USB_DevIntSt_CCEMPTY_Msk));
    //сброс флага завершения записи
    LPC_USB->USBDevIntClr = LPC_USB_DevIntSt_CCEMPTY_Msk;

    lusb_ll_usbirq_en(int_en);
}

//выполнение команды SIE с чтением байта данных
static uint8_t usb_cmd_rd(uint8_t cmd_code) {
    uint8_t tmp;
    const bool int_en = lusb_ll_usbirq_en(false);
    //запись кода команды
    usb_cmd(cmd_code);

    //запрос на чтение байта
    LPC_USB->USBCmdCode =     (cmd_code << LPC_USB_CmdCode_CMD_CODE_Pos) |
            (LPC_USB_CMDPHASE_Read << LPC_USB_CmdCode_CMD_PHASE_Pos);
    //ожидание завершения чтения
    while (!(LPC_USB->USBDevIntSt & LPC_USB_DevIntSt_CDFULL_Msk));
    //чтение байта
    tmp = LPC_USB->USBCmdData;
    //сброс флага завершения чтения
    LPC_USB->USBDevIntClr = LPC_USB_DevIntSt_CDFULL_Msk;

    lusb_ll_usbirq_en(int_en);
    return tmp;
}

//выполнение команды SIE с чтением двух байт данных
static uint16_t usb_cmd_rd2(uint8_t cmd_code) {
    uint16_t tmp;
    const bool int_en = lusb_ll_usbirq_en(false);
    tmp = usb_cmd_rd(cmd_code);
    //запрос на чтение байта
    LPC_USB->USBCmdCode =     (cmd_code << LPC_USB_CmdCode_CMD_CODE_Pos) |
            (LPC_USB_CMDPHASE_Read << LPC_USB_CmdCode_CMD_PHASE_Pos);
        //ожидание завершения чтения
    while (!(LPC_USB->USBDevIntSt & LPC_USB_DevIntSt_CDFULL_Msk));
    //чтение байта
    tmp = ((LPC_USB->USBCmdData) << 8) + tmp;
    LPC_USB->USBDevIntClr = LPC_USB_DevIntSt_CDFULL_Msk;
    lusb_ll_usbirq_en(int_en);
    return tmp;
}



/************************************************************************
 * Сброс и установка начальных значений регистров USB-модуля,
 * которые управляют DMA
 ************************************************************************/
static void f_dma_reset(void) {
    //инициализация DMA
    //запрещаем все DMA и сбрасываем флаги прерывания
    LPC_USB->USBEpDMADis     = 0xFFFFFFFF;
    LPC_USB->USBDMARClr      = 0xFFFFFFFF;
    LPC_USB->USBEoTIntClr    = 0xFFFFFFFF;
    LPC_USB->USBNDDRIntClr   = 0xFFFFFFFF;
    LPC_USB->USBSysErrIntClr = 0xFFFFFFFF;

#if LUSB_EP_NONCTL_CNT
    LPC_USB->USBUDCAH = (uint32_t)f_udca; //указатель на область с текущими адресами дескрипторов для ep
    //разрешение прерываний DMA
    LPC_USB->USBDMAIntEn = LPC_USB_DMAIntEn_EOT_Msk |
            LPC_USB_DMAIntEn_NDDR_Msk | LPC_USB_DMAIntEn_ERR_Msk;

    //устанавливаем текущие указатели на дескрипторы в 0 (нет пока дескрипторов)
    for (uint8_t i=0; i < LUSB_PHY_EP_NUM; i++) {
        f_udca[i] = 0;
        f_udca_init[i] = 0;
    }

    //вся память под дескрипторы DMA свободна
    f_usb_dd_mmap = 0;
    f_usb_dd_need_free = 0;
    f_ep_dd_processed = 0;

#ifdef LUSB_PORT_ZERO_PACKET_ON_CLEAR
    f_ep_req_dummy_pack = 0;
#endif
#endif
}

/**************************************************************************************
    сброс регистров USB контроллера - выполняется при инициализации и по bus reset
 **************************************************************************************/
void lusb_ll_reset(void) {
#ifdef LUSB_LINK_STATUS_CONTROL
    f_set_led_status(e_LEDSTATE_INIT);
#endif
    LPC_USB->USBReEp = 0;
    LPC_USB->USBEpIntClr = 0xFFFFFFFF;
    LPC_USB->USBDevIntClr = 0xFFFFFFFF;


    //разрешение прерываний от конечных точек без DMA (для 0-ых - всегда)
    LPC_USB->USBEpIntEn = 0x3;

#ifdef LUSB_INTERRUPT
    //разрешение прерываний
    LPC_USB->USBDevIntEn = LPC_USB_DevIntEn_DEV_STAT_Msk |
            LPC_USB_DevIntEn_EP_SLOW_Msk | LPC_USB_DevIntEn_EP_FAST_Msk;
#endif

    lusb_ll_set_addr(0); //устанавливаем нулевой адрес (неинициализированный режим)
    lusb_ll_configure(0); //устанавливаем нулевую конфигурацю - не сконфигурированы
}


#define LPC_USB_TEST_CODE (0xA50F)

/***************************************************************************************
 *  Инициализация usb-интерфейса (PCON и PLL1 уже должны быть настроены в startup!!!!!)
 *************************************************************************************/
int lusb_ll_init(void) {
    //PCON и PLL1 настраиваются в startup....
    int res = LUSB_ERR_SUCCESS;

    LPC_SC->PCONP |= LPC_SC_PCONP_PCUSB_Msk;

    //разрешение клока
    LPC_USB->USBClkCtrl = LPC_USB_ClkCtrl_DEV_CLK_EN_Msk | LPC_USB_ClkCtrl_AHB_CLK_EN_Msk;


    //*****************  настраиваем пины для USB **************************
    //D+/D-
    LPC_PINCON->PINMODE1 = (LPC_PINCON->PINMODE1 &
            (~(LPC_PINCON_PINMODE_PX_Msk(29) | LPC_PINCON_PINMODE_PX_Msk(30))))
            | (LPC_PINMODE_NEITHER << LPC_PINCON_PINMODE_PX_Pos(29))
            | (LPC_PINMODE_NEITHER << LPC_PINCON_PINMODE_PX_Pos(30));

    LPC_PINCON->PINSEL1 = (LPC_PINCON->PINSEL1 &
            (~(LPC_PINCON_PINSEL_PX29_Msk | LPC_PINCON_PINSEL_PX30_Msk)))
            | (LPC_PINFUNC_P0_29_USBD | LPC_PINFUNC_P0_30_USBD);

#ifndef LUSB_DISABLE_SENSE_PIN
    //Usb Sense
    LPC_PINCON->PINMODE3 = (LPC_PINCON->PINMODE3 &
                ~LPC_PINCON_PINMODE_PX_Msk(30))
                | (LPC_PINMODE_NEITHER << LPC_PINCON_PINMODE_PX_Pos(30));

    LPC_PINCON->PINSEL3 = (LPC_PINCON->PINSEL3 &
            ~LPC_PINCON_PINSEL_PX30_Msk) | LPC_PINFUNC_P1_30_VBUS;
#endif


#ifndef LUSB_DISABLE_CONNECT_PIN
    //Usb Connect
    LPC_PINCON->PINMODE4 = (LPC_PINCON->PINMODE4 &
                ~LPC_PINCON_PINMODE_PX_Msk(9))
                | (LPC_PINMODE_NEITHER << LPC_PINCON_PINMODE_PX_Pos(9));
    LPC_PINCON->PINSEL4 = (LPC_PINCON->PINSEL4 &
            ~LPC_PINCON_PINSEL_PX9_Msk) | LPC_PINFUNC_P2_9_USB_CONNECT;
#endif

    //Usb Led
#ifdef LUSB_LINK_STATUS_CONTROL
    LPC_SC->PCONP |= LPC_SC_PCONP_PCGPIO_Msk;
    LUSB_LED_GREEN_SET_DIR();
    LUSB_LED_RED_SET_DIR();
    f_set_led_status(e_LEDSTATE_INIT);

#elif !defined LUSB_DISABLE_LED_PIN && !defined LUSB_ACTIVITY_INDICATION
    LPC_PINCON->PINMODE3 = (LPC_PINCON->PINMODE3 &
                    ~LPC_PINCON_PINMODE_PX_Msk(18))
                    | (LPC_PINMODE_NEITHER << LPC_PINCON_PINMODE_PX_Pos(18));

    LPC_PINCON->PINSEL3 = (LPC_PINCON->PINSEL3 &
            ~LPC_PINCON_PINSEL_PX18_Msk) | LPC_PINFUNC_P1_18_USB_UP_LED;
#endif


    //ждем пока установится клок
    while (!(LPC_USB->USBClkSt & LPC_USB_ClkSt_DEV_CLK_ON_Msk)
            || !(LPC_USB->USBClkSt & LPC_USB_ClkSt_AHB_CLK_ON_Msk));


    lusb_ll_connect(false);


    //сбрасываем значения регистров
    lusb_ll_reset();

    //чтение тестового слова модуля USB
    if (usb_cmd_rd2(LPC_USB_CMDCODE_READ_TEST_REG)!=LPC_USB_TEST_CODE) {
        res = LUSB_ERR_MODULE_TST;
    }

    //tmp
    usb_cmd_wr(LPC_USB_CMDCODE_SET_MODE, LPC_USB_CMD_SETMODE_INAK_BI_Msk | LPC_USB_CMD_SETMODE_INAK_II_Msk);


#ifdef LUSB_INTERRUPT
#ifdef LUSB_INTERRUPT_PRIORITY
    //устанавливаем приоритет для usb прерываний
    NVIC_SetPriority(USB_IRQn, LUSB_INTERRUPT_PRIORITY);
#endif
    //общее резрешение прерываний от USB
    NVIC_EnableIRQ(USB_IRQn);
    LPC_SC->USBIntSt |= LPC_USB_IntSt_EN_USB_INTS_Msk;
#endif
    return res;
}


int lusb_ll_close(void) {
    LPC_USB->USBClkCtrl = 0;
    LPC_SC->PCONP &= ~LPC_SC_PCONP_PCUSB_Msk;
    return 0;
}


//подключение к хосту (вкл. поддтяжки)
void lusb_ll_connect(bool con) {
    usb_cmd_wr(LPC_USB_CMDCODE_SET_DEVICE_STATUS,
            con ? LPC_USB_CMD_DEVSTATUS_CON_Msk : 0);
#ifdef LUSB_LINK_STATUS_CONTROL
    if (!con)
        f_set_led_status(e_LEDSTATE_INIT);
#endif
}

int lusb_ll_set_addr(uint8_t addr) {
    uint32_t tmp = LPC_USB_CMD_SETADDR_DEV_EN_Msk | addr;
    usb_cmd_wr(LPC_USB_CMDCODE_SET_ADDRESS, tmp); //2 раза взято из application note......
    usb_cmd_wr(LPC_USB_CMDCODE_SET_ADDRESS, tmp); //без этого не работает.................
    return 0;
}


void lusb_ll_configure(int conf) {
    usb_cmd_wr(LPC_USB_CMDCODE_CONFIGURE_DEVICE,
            conf ? LPC_USB_CMD_CONFDEV_CONF_Msk : 0);

    if (conf == 0) {
#ifdef LUSB_LINK_STATUS_CONTROL
        f_set_led_status(e_LEDSTATE_INIT);
#endif
        /* разрешаем только Control EP */
        LPC_USB->USBReEp = 0;
        f_usb_ep_realize(LPC_USB_EPRX_NUM(0), LUSB_EP0_PACKET_SIZE);
        f_usb_ep_realize(LPC_USB_EPTX_NUM(0), LUSB_EP0_PACKET_SIZE);

        /* сбрасываем все флаги по DMA */
        f_dma_reset();


#ifdef LUSB_LINK_STATUS_CONTROL
    } else {
        f_set_led_status(e_LEDSTATE_CONFIGURED);
#endif
    }
}


int lusb_ll_get_speed(void) {
    return LUSB_SPEED_FULL;
}


int lusb_ll_ep0_rd_packet(uint8_t *buf, uint32_t flags, uint32_t rem_size) {
    unsigned size = 0, i;
    unsigned ep = 0;
    uint8_t phy_num = 0;


    //разрешаем чтение по ep
    LPC_USB->USBCtrl = LPC_USB_Ctrl_RD_EN_Msk |
            ((ep << LPC_USB_Ctrl_LOG_ENDPOINT_Pos)&LPC_USB_Ctrl_LOG_ENDPOINT_Msk);
    //ожидание пока пакет захвачен
    while (!(LPC_USB->USBRxPLen & LPC_USB_RxPlen_PKT_RDY_Msk));
    //получаем размер пакета
    size = LPC_USB->USBRxPLen & LPC_USB_RxPlen_PKT_LNGTH_Msk;

    if (size > rem_size)
        size = rem_size;
    //чтение данных в буфер
    for (i = 0; i < (size + 3) / 4; i++) {
        uint32_t wrd = LPC_USB->USBRxData;
        uint8_t cpy_size = (size - 4*i);
        *buf++ = wrd & 0xFF;
        if (cpy_size > 1)
            *buf++ = (wrd >> 8) & 0xFF;
        if (cpy_size > 2)
            *buf++ = (wrd >> 16) & 0xFF;
        if (cpy_size > 3)
            *buf++ = (wrd >> 24) & 0xFF;
    }
    LPC_USB->USBCtrl = 0;

    //выбор конечной точки для очистки буфера
    usb_cmd(LPC_USB_CMDCODE_EP_SELECT(phy_num));
    //передача команды ClearBuffer
    usb_cmd_rd(LPC_USB_CMDCODE_EP_CLEAR_BUFFER);

    return size;
}

int lusb_ll_ep0_wr_packet(const uint8_t *buf, unsigned size) {
    uint32_t phy_num,i;
    unsigned ep = 0;
    phy_num = 2*ep+1;

    if (size > LUSB_EP0_PACKET_SIZE)
        size = LUSB_EP0_PACKET_SIZE;

    //разрешаем запись по ep
    LPC_USB->USBCtrl = LPC_USB_Ctrl_WR_EN_Msk |
            ((ep << LPC_USB_Ctrl_LOG_ENDPOINT_Pos)&LPC_USB_Ctrl_LOG_ENDPOINT_Msk);
    //записываем размер пакета
    LPC_USB->USBTxPLen = size;
    //запись данных в буфер
    for (i = 0; i < (size + 3) / 4; i++) {
        uint32_t wrd = 0;
        uint8_t cpy_size = (size - 4*i);
        wrd |= *buf++;
        if (cpy_size > 1)
            wrd |= *buf++ << 8;
        if (cpy_size > 2)
            wrd |= *buf++ << 16;
        if (cpy_size > 3)
            wrd |= *buf++ << 24;
        LPC_USB->USBTxData = wrd;
    }
    LPC_USB->USBCtrl = 0;

    //выбор кт
    usb_cmd(LPC_USB_CMDCODE_EP_SELECT(phy_num));
    //признак что данные готовы к передаче
    usb_cmd(LPC_USB_CMDCODE_EP_VALIDATE_BUFFER);
    return size;
}

//stall для управляющей нулевой конечной точки (сразу и на in и на out)
void lusb_ll_ep0_stall(void) {
    usb_cmd_wr(LPC_USB_CMDCODE_EP_SET_STATUS(LPC_USB_EPRX_NUM(0)),
            LPC_USB_CMD_EPSTATUS_CND_ST_Msk);
}


#if LUSB_EP_NONCTL_CNT

//перевод конечной точки в stall режим с адресом ep_addr
void lusb_ll_ep_set_stall(uint8_t ep_addr) {
    lusb_printf("set stall for ep = 0x%02X\n", ep_addr);
    usb_cmd_wr(LPC_USB_CMDCODE_EP_SET_STATUS(LPC_EP_PHY_ADDR(ep_addr)),
            LPC_USB_CMD_EPSTATUS_ST_Msk);
}

//сброс stall-режима для конечной точки с адресом ep_addr
void lusb_ll_ep_clr_stall(uint8_t ep_addr) {
    usb_cmd_wr(LPC_USB_CMDCODE_EP_SET_STATUS(LPC_EP_PHY_ADDR(ep_addr)), 0);
}



void f_ep_disable(uint8_t ep_addr) {
    usb_cmd_wr(LPC_USB_CMDCODE_EP_SET_STATUS(LPC_EP_PHY_ADDR(ep_addr)),
               LPC_USB_CMD_EPSTATUS_DA_Msk);
}

void f_ep_enable(uint8_t ep_addr) {
    usb_cmd_wr(LPC_USB_CMDCODE_EP_SET_STATUS(LPC_EP_PHY_ADDR(ep_addr)), 0);
}

/* разрешение конечной точки с адресом ep_addr */
int lusb_ll_ep_enable(const t_usb_endpoint_descriptor *ep_descr) {
    LPC_USB->USBEpDMAEn = LPC_EP_FLAG(ep_descr->bEndpointAddress);
    f_usb_ep_realize(LPC_EP_PHY_ADDR(ep_descr->bEndpointAddress), ep_descr->wMaxPacketSize);
    f_ep_enable(ep_descr->bEndpointAddress);
    return 0;
}

/* запрет конечной точки с адресом ep_addr */
int lusb_ll_ep_disable(const t_usb_endpoint_descriptor *ep_descr) {
    f_ep_disable(ep_descr->bEndpointAddress);
    f_usb_ep_unrealize(LPC_EP_PHY_ADDR(ep_descr->bEndpointAddress));
    LPC_USB->USBEpDMADis = LPC_EP_FLAG(ep_descr->bEndpointAddress);
    return 0;
}

/***********************************************************************************
 *******************            Функции для работы с DMA      **********************
 ***********************************************************************************/

int lusb_ll_ep_add_dd(const t_usb_endpoint_descriptor *ep_descr, void* buf,
                      uint32_t length, uint32_t flags) {
    int i, res = LUSB_ERR_SUCCESS;
    uint8_t ep_num = LPC_EP_PHY_ADDR(ep_descr->bEndpointAddress);
    uint32_t nxt, ptr;

#ifdef LUSB_PORT_ZERO_PACKET_ON_CLEAR
    if (f_ep_req_dummy_pack & (1<< ep_num)) {
        f_ep_req_dummy_pack &= ~(1<< ep_num);
        res = lusb_ll_add_dma_descr(ep_descr, f_dummy_buf, 0, 0);
    }
#endif

    if (res==LUSB_ERR_SUCCESS) {
        //поиск незанятого места для дескриптора
        for (i=0; (i < LUSB_DMA_DESCR_NONISO_CNT)
             && ((f_usb_dd_mmap & (1<<i)) != 0); i++)
        {}

        //нет свободного дескр. - ошибка
        if (i>= LUSB_DMA_DESCR_NONISO_CNT)
            res = LUSB_ERR_NO_FREE_DESCR;
    }

    if (res == LUSB_ERR_SUCCESS) {
        t_usb_dd* pDescr;
        bool int_en;

        //инициализация полей добавляемого дескриптора
        f_usb_noiso_dd[i].NextDDPointer = 0;
        f_usb_noiso_dd[i].Control =
                (ep_descr->wMaxPacketSize << LPC_USB_DDCTL_MAX_PACKET_SIZE_Pos) & LPC_USB_DDCTL_MAX_PACKET_SIZE_Msk;
        f_usb_noiso_dd[i].BuffLength = length;
        f_usb_noiso_dd[i].StartAddr = (uint32_t)buf;
        f_usb_noiso_dd[i].Status = 0;
        f_usb_noiso_dd[i].PresentDMACount = 0;

        //установка 1, чтобы указать, что данное место для дескриптора занято
        f_usb_dd_mmap |= (1<<i);

        //запрещаем прерывания usb пока работаем с цепочкой дескрипторов
        int_en = lusb_ll_usbirq_en(false);

        //добавление дескриптора в конец цепочки
        //адрес текущего дескриптора
        nxt = f_udca_init[ep_num];
        //if (nxt == 0)
        //    nxt = f_udca[ep_num];

        if (nxt != 0) {
            //поиск последнего дескриптора в цепочке
            while (nxt!=0) {
                ptr = nxt;
                nxt = *((uint32_t*)ptr);
            }

            //устанавливаем ссылку с последнего на новый
            pDescr = (t_usb_dd*)ptr;
            pDescr->NextDDPointer = (uint32_t)&f_usb_noiso_dd[i];
            pDescr->Control |= LPC_USB_DDCTL_NEXT_DD_VALID_Msk;
        } else {
            /* нет цепочки => устанавливаемый дескриптор - первый */
            f_udca_init[ep_num] = f_udca[ep_num] = (uint32_t)&f_usb_noiso_dd[i];
            //LPC_USB->USBDMARSet = (1<<ep_num);
            //LPC_USB->USBEpDMAEn = (1 << ep_num);

        }
        // обновляем дескрипторы (refetch) (чтобы контроллер usb отследил измениния)
        LPC_USB->USBEpDMADis = 0;
        lusb_ll_usbirq_en(int_en);

        //разрешение DMA, если был остановлен
        if ((LPC_USB->USBEpDMASt & (1 << ep_num)) == 0) {
            LPC_USB->USBEpDMAEn = (1 << ep_num);
        }
    }

    return res;
}


/************************************************************************
 *  получить следующий завершенный, но еще не обработанный стеком
 *  дескриптор DMA для заданной конечной точки.
 *  Если нет таких дескрипторов - возврат 0
 *  nxt_cpl - указывает, есть ли еще такие дескрипторы в цепочке (1 - есть, 0 - нет)
 **********************************************************************/
static t_usb_dd* f_lusb_get_next_descr(uint8_t ep_num, uint8_t* nxt_cpl) {
    t_usb_dd *nxt, *ptr = 0;
    //if (f_udca_init[ep_num]==0)
    //        f_udca_init[ep_num] = f_udca[ep_num];

    ptr = (t_usb_dd*)f_udca_init[ep_num];
    *nxt_cpl = 0;


    if (ptr!=NULL) {
        if (f_ep_dd_processed & (1<<ep_num)) {
            ptr = (t_usb_dd*)ptr->NextDDPointer;
            if (ptr != NULL) {
                 f_udca_init[ep_num] = (uint32_t)ptr;
                 f_ep_dd_processed &= ~(1<<ep_num);
            }
        }
    }

    if (ptr!=NULL) {

        //смотрим - обработан ли текущий дескриптор
        if (ptr->Status & LPC_USB_DDSTAT_DD_RETIRED_Msk) {
            //если да - переходим к следующему
            nxt = (t_usb_dd*)ptr->NextDDPointer;
            if (nxt != 0) {
                f_udca_init[ep_num] = (uint32_t)nxt;
                //cмотрим - обработан ли следующий
                if (nxt->Status & LPC_USB_DDSTAT_DD_RETIRED_Msk)
                   *nxt_cpl = 1;
            } else {
                f_ep_dd_processed |= (1<<ep_num);
            }
        } else {
            ptr = 0;
        }
    }
    return ptr;
}

/**************************************************************************************
 *  Получить состояние выполняющегося в настоящий момент дескриптора для
 *  конечной точки с адресом ep_addr
 *  Если не выполняется DMA-передача, то возвращает ошибку LUSB_ERR_DESCR_NOT_FOUND
 * ************************************************************************************/
int lusb_ll_ep_dd_status(const t_usb_endpoint_descriptor *ep_descr, t_lusb_dd_status* dd_st) {
    uint8_t ep_num = LPC_EP_PHY_ADDR(ep_descr->bEndpointAddress);
    t_usb_dd* pDD;
    pDD = (t_usb_dd*)f_udca[ep_num];
    if (pDD!=0) {
        dd_st->flags = 0;
        dd_st->last_addr = pDD->StartAddr;
        dd_st->length = pDD->BuffLength;
        dd_st->status = (pDD->Status & LPC_USB_DDSTAT_DD_STATUS_Msk) >> LPC_USB_DDSTAT_DD_STATUS_Pos;
        dd_st->trans_cnt = pDD->PresentDMACount;
    }
    return (pDD==0)? LUSB_ERR_DESCR_NOT_FOUND : LUSB_ERR_SUCCESS;
}


void lusb_ll_ep_clr(const t_usb_endpoint_descriptor *ep_descr) {

    uint8_t ep_num = LPC_EP_PHY_ADDR(ep_descr->bEndpointAddress);
    bool int_en = lusb_ll_usbirq_en(false);

    if (LPC_USB->USBEpDMASt & (1<<ep_num)) {
        volatile t_usb_dd *nxt, *ptr = 0;

        lusb_ll_ep_set_stall(ep_descr->bEndpointAddress);
        while (LPC_USB->USBEpDMASt & (1<<ep_num))
            LPC_USB->USBDMARSet = (1<<ep_num);

        LPC_USB->USBEpDMADis = (1<<ep_num);

        lusb_ll_ep_clr_stall(ep_descr->bEndpointAddress);
#ifdef LUSB_PORT_ZERO_PACKET_ON_CLEAR
        /* Для кт на передачу после зачистки DMA добавляем передачу
         * нулевого пакета, так как при запрете EP/STALL или очистке буфера
         * может нарушаиться синхронизация буферов DMA и EP.
         * Это приводит к потере одного следующего пакета. */
        if (ep_descr->bEndpointAddress & USB_EPDIR)
            f_ep_req_dummy_pack |= (1<<ep_num);
#endif


        LPC_USB->USBEoTIntClr    = (1<<ep_num);
        LPC_USB->USBNDDRIntClr   = (1<<ep_num);
        LPC_USB->USBSysErrIntClr = (1<<ep_num);


        ptr = (t_usb_dd*)f_udca_init[ep_num];
        while (ptr!=0) {

            //дескриптор свободен
            f_usb_dd_need_free |= (1UL <<
                      (((uint32_t)ptr - (uint32_t)f_usb_noiso_dd))/sizeof(t_usb_dd));

            nxt = (t_usb_dd*)ptr->NextDDPointer;
            if (nxt != NULL)
                f_udca_init[ep_num] = (uint32_t)nxt;

            ptr->BuffLength = 0;
            ptr->PresentDMACount = 0;
            ptr->Status = 0;
            ptr->Control = 0;
            ptr->NextDDPointer = 0;
            ptr->StartAddr = 0;

            ptr = nxt;
        }

        f_ep_dd_processed |= (1<<ep_num);

    }

    lusb_ll_usbirq_en(int_en);

}







//получение стандартного адреса конечной точки из ее физического номера
#define LPC_USB_ADDR(phy_num) ((((phy_num) & 1) << 7)  | ((phy_num) >> 1))


/*******************************************************************************************
 *  Очистка неиспользуемых блоков памяти под дескрипторы DMA, чтобы можно было их
 *  занять новыми дескрипторами
 *  Выполняется в фоне из lusb_ll_progress
 *  Дескриптор считается не занятым, в случае, если на него не ссылается usb-контроллер
 *  (передача завершена и УЖЕ НАЧАТА передача следующего дескриптора)
 *  Нельзя очищать по завершению передачи - так как usb-контроллер ссылается еще на этот
 *  дескриптор, чтобы найти следующий.
 *  После завершения передачи устанавливается соответствующий бит в f_usb_dd_need_free,
 *  после чего, в данной функции, если на него не ссылается контроллер, он очищается
 ******************************************************************************************/
static void f_lusb_ll_freemem(void) {
    if (f_usb_dd_need_free!=0) {
        bool int_en = lusb_ll_usbirq_en(false);
        for (uint8_t i=0; i < LUSB_DMA_DESCR_NONISO_CNT; i++) {
            //если поставлен запрос на очистку
            if (f_usb_dd_need_free & (1 << i)) {
                uint8_t ep;
                //ищем - ссылается ли на него еще контроллер через f_udca
                for (ep =0; (ep < 32) && !((f_udca[ep]==(uint32_t)&f_usb_noiso_dd[i])
                                           || (f_udca_init[ep]==(uint32_t)&f_usb_noiso_dd[i])); ep++)
                {}

                //если нет - очищаем (снимаем бит в f_usb_dd_mmap)
                if (ep >= 32) {
                    f_usb_dd_need_free &= ~(1UL << i);
                    f_usb_dd_mmap &= ~(1UL << i);
                }
            }
        }
        lusb_ll_usbirq_en(int_en);
    }

}
#endif




/* **********************************************************************************
 * опрос флагов прерывания USB-модуля
 * обработка и вызов требуемых callback-функций lusb_core
 * **********************************************************************************/
static void f_lusb_ll_isr(void) {
    uint32_t irq_st;
    uint8_t i;

#if LUSB_EP_NONCTL_CNT
    if (LPC_SC->USBIntSt & LPC_USB_IntSt_INT_REQ_DMA_Msk) {
        //проверка прерывания по концу передачи дескриптора
        if (LPC_USB->USBDMAIntSt & LPC_USB_DMAIntSt_EOT_Msk) {
#ifdef LUSB_LINK_STATUS_CONTROL
            f_set_led_status(e_LEDSTATE_TXRX_IND);
#endif

            irq_st = LPC_USB->USBEoTIntSt;            
            for (i=0; i < 32; i++) {
                //пришло прерывание о завершении передачи пакета
                if (irq_st & (1<<i)) {
                    uint8_t next_cpl;
                    t_usb_dd* descr;
                    t_lusb_dd_status dd_st;
                    //очищаем прерывание
                    LPC_USB->USBEoTIntClr = (1<<i);
                    //выполняем пока есть завершенные дескрипторы
                    do {
                        //получаем завершенный дескриптор
                        descr = f_lusb_get_next_descr(i, &next_cpl);
                        if (descr!=NULL)  {
                            if (f_core_dd(descr)) {
                                dd_st.flags = 0;
                                dd_st.last_addr = descr->StartAddr;
                                dd_st.length = descr->BuffLength;
                                dd_st.status = (descr->Status & LPC_USB_DDSTAT_DD_STATUS_Msk) >> LPC_USB_DDSTAT_DD_STATUS_Pos;
                                dd_st.trans_cnt = descr->PresentDMACount;
                                //вызываем cb на пришедший дескриптор                                
                                lusb_core_cb_dd_done(LPC_USB_ADDR(i), LUSB_DD_EVENT_EOT, &dd_st);


                            }

                            /* ставим признак, что дескриптор больше не нужен
                               освобождать тут нельзя!! так как DMA-контроллер может его использовать
                              реально очистка идет в lusb_ll_freemem */
                            f_usb_dd_need_free |= (1UL <<
                                        (((uint32_t)descr - (uint32_t)f_usb_noiso_dd))/sizeof(t_usb_dd));
                        }                        
                    } while (next_cpl && (descr!=0));
                }
            }
        }

        if (LPC_USB->USBDMAIntSt & LPC_USB_DMAIntSt_NDDR_Msk) {
            irq_st = LPC_USB->USBNDDRIntSt;
            for (i=0; i < 32; i++) {
                if (irq_st & (1<<i)) {
                    LPC_USB->USBNDDRIntClr = (1<<i);
                    lusb_core_cb_dd_done(LPC_USB_ADDR(i), LUSB_DD_EVENT_NODD, 0);
                }
            }
        }

        if (LPC_USB->USBDMAIntSt & LPC_USB_DMAIntSt_ERR_Msk) {
            irq_st = LPC_USB->USBSysErrIntSt;
            for (i=0; i < 32; i++) {
                if (irq_st & (1<<i)) {
                    LPC_USB->USBSysErrIntClr = (1<<i);
                    lusb_core_cb_dd_done(LPC_USB_ADDR(i), LUSB_DD_EVENT_ERR, 0);
                }
            }
        }
    }
#endif

    irq_st = LPC_USB->USBDevIntSt;

    //прерывание при изменении состояния устройства
    if (irq_st & LPC_USB_DevIntSt_DEV_STAT_Msk) {
        LPC_USB->USBDevIntClr = LPC_USB_DevIntClr_DEV_STAT_Msk;
        //читаем состояние модуля
        uint8_t st = usb_cmd_rd(LPC_USB_CMDCODE_GET_DEVICE_STATUS);
        //произошел сброс шины
        if (st & LPC_USB_CMD_DEVSTATUS_RST_Msk) {
            lusb_ll_reset();
            lusb_core_cb_reset();
        }
        //смена подключения
        if (st & LPC_USB_CMD_DEVSTATUS_CON_CH_Msk) {
            lusb_core_cb_con_changed((st&LPC_USB_CMD_DEVSTATUS_CON_Msk)? 1: 0);
        }
        //переход в suspend
        if (st & LPC_USB_CMD_DEVSTATUS_SUS_CH_Msk) {
            lusb_core_cb_susp_changed((st&LPC_USB_CMD_DEVSTATUS_SUS_Msk)? 1: 0);
        }
    }

    //ошибка USB модуля
    if (irq_st & LPC_USB_DevIntSt_ERR_INT_Msk) {
        usb_cmd_rd(LPC_USB_CMDCODE_GET_ERROR_CODE);
        usb_cmd_rd(LPC_USB_CMDCODE_READ_ERROR_STATUS);
        LPC_USB->USBDevIntClr = LPC_USB_DevIntSt_ERR_INT_Msk;

        /** @todo lusb lpc17xx: Добавить callback на ошибку usb модуля */
    }

    //int для кт при обмене без DMA
    if (irq_st & LPC_USB_DevIntSt_EP_SLOW_Msk) {
        uint32_t ep_st;
        //проверка флагов для всех ep
        for (i=0; (i < 2) && (LPC_USB->USBEpIntSt!=0); i++) {
            if (LPC_USB->USBEpIntSt & LPC_USB_EPTX_FLG(i)) {
#ifdef LUSB_LINK_STATUS_CONTROL
                f_set_led_status(e_LEDSTATE_TXRX_IND);
#endif
                //сбрасываем флаг прерывания (реально - команда ISE)
                LPC_USB->USBDevIntClr = LPC_USB_DevIntSt_CDFULL_Msk;
                LPC_USB->USBEpIntClr = LPC_USB_EPTX_FLG(i);
                while (!(LPC_USB->USBDevIntSt & LPC_USB_DevIntSt_CDFULL_Msk));
                ep_st = LPC_USB->USBCmdData; //получаем статус конечной точки
                lusb_core_cb_packet_tx(USB_EPDIR_IN | i, ep_st);
            }


            if (LPC_USB->USBEpIntSt & LPC_USB_EPRX_FLG(i)) {

#ifdef LUSB_LINK_STATUS_CONTROL
                f_set_led_status(e_LEDSTATE_TXRX_IND);
#endif
                //сбрасываем флаг прерывания (реально - команда ISE)
                LPC_USB->USBDevIntClr = LPC_USB_DevIntSt_CDFULL_Msk;
                LPC_USB->USBEpIntClr = LPC_USB_EPRX_FLG(i);
                while (!(LPC_USB->USBDevIntSt & LPC_USB_DevIntSt_CDFULL_Msk));
                ep_st = LPC_USB->USBCmdData; //получаем статус конечной точки

                lusb_core_cb_packet_rx(i,ep_st);
            }
        }

        LPC_USB->USBDevIntClr = LPC_USB_DevIntSt_EP_SLOW_Msk;
    }
}



#ifdef LUSB_INTERRUPT
#include "lcspec_interrupt.h"
void USB_IRQHandler(void) {
    f_lusb_ll_isr();
}

#include "lcspec_interrupt.h"
void USBActivity_IRQHandler(void) {

}
#endif




/**************************************************************************
 *  Выполнение фоновых задач для порта
 *  очистка памяти для дескрипторов
 *  и отслеживание дисконнекта/конекта к usb-шине
 *  (так как аппаратно коннект не отслеживается...)
 * ************************************************************************ */
void lusb_ll_progress(void) {
    static uint8_t con = 0, con1 = 0;

    //очистка ненужных дескрипторов
#if LUSB_EP_NONCTL_CNT
    f_lusb_ll_freemem();
#endif

    //событие подключения обрабатываем софтовым образом,
    //так как аппаратно - только отключение...
    con1 = LPC_GET_USBV();
    if (con1!=con) {
        con = con1;
        lusb_core_cb_con_changed(con);
#ifdef LUSB_LINK_STATUS_CONTROL
        f_set_led_status(e_LEDSTATE_INIT);
#endif
    }

#ifdef LUSB_LINK_STATUS_CONTROL
    f_led_process();
#endif

#ifndef LUSB_INTERRUPT
    f_lusb_ll_isr();
#endif
}
