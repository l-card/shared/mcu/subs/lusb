#include "lusb.h"
#ifdef LUSB_ACTIVITY_INDICATION

#include "ltimer.h"

static t_ltimer f_tmr;
static enum IND_STATE {
    IND_STATE_INIT,
    IND_STATE_BLINK,
    IND_STATE_GUARD
} f_state = IND_STATE_INIT;

void lusb_activity_indication(void) {
    if (f_state==IND_STATE_INIT) {
        ltimer_set(&f_tmr, LTIMER_MS_TO_CLOCK_TICKS(LUSB_ACTIVITY_IND_BLINK_TOUT));
        lusb_appl_cb_activity_start();
        f_state = IND_STATE_BLINK;
    }
}

void lusb_activity_pull(void) {
    if ((f_state==IND_STATE_BLINK) && ltimer_expired(&f_tmr)) {
        f_state = IND_STATE_GUARD;
        ltimer_set(&f_tmr, LTIMER_MS_TO_CLOCK_TICKS(LUSB_ACTIVITY_IND_GUARD_TOUT));
        lusb_appl_cb_activity_end();
    } else if ((f_state==IND_STATE_GUARD) && ltimer_expired(&f_tmr)) {
        f_state = 0;
    }
}
#endif
