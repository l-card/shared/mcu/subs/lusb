#ifndef LUSB_SOFT_DD_H
#define LUSB_SOFT_DD_H

#include "lusb_usbdefs.h"

void lusb_soft_dd_packet_rx_cb(uint8_t ep_addr, int flags);
void lusb_soft_dd_packet_tx_cb(uint8_t ep_addr, int flags);

void lusb_soft_dd_ep_enable_cb(t_usb_endpoint_descriptor *ep_descr);
void lusb_soft_dd_ep_disable_cb(t_usb_endpoint_descriptor *ep_descr);



int lusb_soft_dd_add(const t_usb_endpoint_descriptor *ep_descr, void* buf,
                     uint32_t length, uint32_t flags);

void lusb_soft_dd_ep_clr(const t_usb_endpoint_descriptor *ep_descr);

int lusb_soft_dd_ep_dd_status(const t_usb_endpoint_descriptor *ep_descr, t_lusb_dd_status* dd_st);

#endif // LUSB_SOFT_DD_H
