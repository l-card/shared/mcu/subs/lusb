/*
 * lusb_descriptros.c
 *
 *  Created on: 06.07.2010
 *      Author: borisov
 *
 *  Файл содержит стандартные дескрипторы USB
 *  (дескриптор устройства, конфигурации (включая интерфейса и конечных точке),
 *  строк
 */



#include "lusb_config_params.h"
#include "lusb_usbdefs.h"
#include "lusb_descriptors.h"
#include "lusb_types.h"


//дескриптор устройства
LUSB_DESCR_SPEC_C t_usb_device_descriptor g_lusb_dev_descr = {
    sizeof(t_usb_device_descriptor),
    USB_DESCRTYPE_DEVICE,
    LUSB_USB_VER,
    LUSB_DEVICE_CLASS,
    LUSB_DEVICE_SUBCLASS,
    LUSB_DEVICE_PROTOCOL,
    LUSB_EP0_PACKET_SIZE,
    LUSB_DEVICE_VENDOR_ID,
    LUSB_DEVICE_PRODUCT_ID,
    LUSB_DEVICE_RELEASE,
    LUSB_STRIND_MANUFACTURER,
    LUSB_STRIND_PRODUCT,
    LUSB_STRIND_SERIAL,
    LUSB_CONFIG_CNT
} ;

#ifdef LUSB_SUPPORT_HIGH_SPEED
LUSB_DESCR_SPEC_C t_usb_device_qualifier_descriptor g_lusb_qual_descr = {
    USB_DEVICE_QUALIFIER_DESCRIPTOR_LENGTH,
    USB_DESCRTYPE_DEVICE_QUALIFIER,
    LUSB_USB_VER,
    LUSB_DEVICE_CLASS,
    LUSB_DEVICE_SUBCLASS,
    LUSB_DEVICE_PROTOCOL,
    LUSB_EP0_PACKET_SIZE,
    LUSB_CONFIG_CNT,
    0
};

#endif



#define LUSB_MAKE_CFG_PTR(name) LUSB_MAKE_CFG_PTR_(name)
#define LUSB_MAKE_CFG_PTR_(name) LUSB_DESCR_SPEC_C t_usb_configuration_descriptor* name##_ptr = \
                        (LUSB_DESCR_SPEC_C t_usb_configuration_descriptor*)&name

/*******************************************************************************************
 ******************** Структура, описывающая конфигурацию устройства  **********************
 ******************************************************************************************/
#include "lcspec_pack_start.h"


#ifdef LUSB_SUPPORT_HIGH_SPEED
    #define LUSB_CURCFG_STRUCT_NAME              g_lusb_cfgs_hs
    #define LUSB_CURCFG_DESCRTYPE                USB_DESCRTYPE_CONFIGURATION
    #define LUSB_CURCFG_SPEED                    LUSB_SPEED_HIGH
    #include "lusb_cfg_descr_gen.inc"

    #define LUSB_CURCFG_STRUCT_NAME              g_lusb_cfgs_hs_other
    #define LUSB_CURCFG_DESCRTYPE                USB_DESCRTYPE_OTHER_SPEED_CONFIGURATION
    #define LUSB_CURCFG_SPEED                    LUSB_SPEED_FULL
    #include "lusb_cfg_descr_gen.inc"

    #define LUSB_CURCFG_STRUCT_NAME              g_lusb_cfgs_fs
    #define LUSB_CURCFG_DESCRTYPE                USB_DESCRTYPE_CONFIGURATION
    #define LUSB_CURCFG_SPEED                    LUSB_SPEED_FULL
    #include "lusb_cfg_descr_gen.inc"

    #define LUSB_CURCFG_STRUCT_NAME              g_lusb_cfgs_fs_other
    #define LUSB_CURCFG_DESCRTYPE                USB_DESCRTYPE_OTHER_SPEED_CONFIGURATION
    #define LUSB_CURCFG_SPEED                    LUSB_SPEED_HIGH
    #include "lusb_cfg_descr_gen.inc"
#else
    #define LUSB_CURCFG_STRUCT_NAME              g_lusb_cfgs
    #define LUSB_CURCFG_DESCRTYPE                USB_DESCRTYPE_CONFIGURATION
    #define LUSB_CURCFG_SPEED                    LUSB_SPEED_FULL
    #include "lusb_cfg_descr_gen.inc"
#endif



/* дескриптор с ID поддерживаемых языков */
struct st_langids {
    uint8_t bLength;
    uint8_t bType;
    uint16_t lang_ids[LUSB_LANGID_CNT];
};

static LUSB_DESCR_SPEC_C struct st_langids f_lusb_langid_descr = {
    2 + 2* LUSB_LANGID_CNT, USB_DESCRTYPE_STRING, {
        USB_LANGID_ENG_USA,
#ifdef LUSB_USE_RUS_STRINGS
        USB_LANGID_RUS,
#endif
    }
};


#ifdef LUSB_DEF_UTF16_STRING

#define LUSB_STR_DESC_ARR   (LUSB_DESCR_SPEC_H uint8_t*)&



/* дескриптор со строкой производителя */
LUSB_STRING_GEN(f_lusb_manufacturer_string, LUSB_STR_MANUFACTURER);
LUSB_STRING_GEN(f_lusb_product_string, LUSB_STR_PRODUCT);
#if !defined LUSB_USE_MAN_GEN_SERIAL && defined LUSB_STR_SERIAL
    LUSB_STRING_GEN(f_lusb_serial_string, LUSB_STR_SERIAL);
#endif

#ifdef LUSB_STR_CONFIG
    LUSB_STRING_GEN(f_lusb_cfg1_string, LUSB_STR_CONFIG);
#endif

#ifdef LUSB_STR_BULK_INTERFACE
    LUSB_STRING_GEN(f_lusb_std_intf_string, LUSB_STR_BULK_INTERFACE);
#endif

#if defined LUSB_USE_MSC_INTERFACE && defined LUSB_STR_MSC_INTERFACE
    LUSB_STRING_GEN(f_lusb_msc_string, LUSB_STR_MSC_INTERFACE);
#endif

#if defined LUSB_USE_IAP_NATIVE_INTERFACE && defined LUSB_STR_IAP_EA_NATIVE_INTERFACE
    LUSB_STRING_GEN(f_lusb_iap_native_string, LUSB_STR_IAP_EA_NATIVE_INTERFACE);
#endif


#ifdef LUSB_USE_RUS_STRINGS
    #ifdef  LUSB_STR_MANUFACTURER_RUS_LEN
        LUSB_STRING_GEN_LEN(f_lusb_manufacturer_string_rus, LUSB_STR_MANUFACTURER_RUS, LUSB_STR_MANUFACTURER_RUS_LEN);
    #else
        LUSB_STRING_GEN(f_lusb_manufacturer_string_rus, LUSB_STR_MANUFACTURER_RUS);
    #endif
    #ifdef LUSB_STR_PRODUCT_RUS_LEN
        LUSB_STRING_GEN_LEN(f_lusb_product_string_rus, LUSB_STR_PRODUCT_RUS, LUSB_STR_PRODUCT_RUS_LEN);
    #else
        LUSB_STRING_GEN(f_lusb_product_string_rus, LUSB_STR_PRODUCT_RUS);
    #endif
#ifdef LUSB_STR_CONFIG_RUS
    #ifdef LUSB_STR_CONFIG_RUS_LEN
        LUSB_STRING_GEN_LEN(f_lusb_cfg1_string_rus, LUSB_STR_CONFIG_RUS, LUSB_STR_CONFIG_RUS_LEN);
     #else
        LUSB_STRING_GEN(f_lusb_cfg1_string_rus, LUSB_STR_CONFIG_RUS);
    #endif
#endif
#ifdef LUSB_STR_BULK_INTERFACE_RUS
    #ifdef LUSB_STR_BULK_INTERFACE_RUS_LEN
        LUSB_STRING_GEN_LEN(f_lusb_std_intf_string_rus, LUSB_STR_BULK_INTERFACE_RUS, LUSB_STR_BULK_INTERFACE_RUS_LEN);
    #else
        LUSB_STRING_GEN(f_lusb_std_intf_string_rus, LUSB_STR_BULK_INTERFACE_RUS);
    #endif
#endif
    #if defined LUSB_USE_MSC_INTERFACE && defined LUSB_STR_MSC_INTERFACE
        #ifdef LUSB_STR_MSC_INTERFACE_LEN
            LUSB_STRING_GEN_LEN(f_lusb_msc_string_rus, LUSB_STR_MSC_INTERFACE_RUS, LUSB_STR_MSC_INTERFACE_LEN);
        #else
            LUSB_STRING_GEN(f_lusb_msc_string_rus, LUSB_STR_MSC_INTERFACE_RUS);
        #endif
    #endif
#endif

#else

#define USB_CFG_WORD(wrd)  wrd&0xFF, (wrd>>8)&0xFF

LUSB_DESCR_SPEC_C static uint8_t f_lusb_manufacturer_string[] = {
    2 + 2*LUSB_STR_MANUFACTURER_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_MANUFACTURER
};

/* дескриптор со строкой продукта */
LUSB_DESCR_SPEC_C static uint8_t f_lusb_product_string[] = {
    2+2*LUSB_STR_PRODUCT_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_PRODUCT
};

#if defined LUSB_STR_SERIAL && !defined LUSB_USE_MAN_GEN_SERIAL

/* дескриптор со строкой серийного номера */
LUSB_DESCR_SPEC_C static uint8_t f_lusb_serial_string[] = {
    2+2*LUSB_STR_SERIAL_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_SERIAL
};
#endif

#ifdef LUSB_STR_CONFIG
/* дескриптор со строкой описания конфигурации 1 */
LUSB_DESCR_SPEC_C static uint8_t f_lusb_cfg1_string[] = {
    2+2*LUSB_STR_CONFIG_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_CONFIG
};
#endif


#if defined LUSB_USE_STDBULK_INTERFACE && defined LUSB_STR_BULK_INTERFACE
/* дескриптор со строкой серийного номера */
LUSB_DESCR_SPEC_C static uint8_t f_lusb_std_intf_string[] = {
    2+2*LUSB_STR_BULK_INTERFACE_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_BULK_INTERFACE
};
#endif

#if defined LUSB_USE_MSC_INTERFACE && defined LUSB_STR_MSC_INTERFACE
LUSB_DESCR_SPEC_C static uint8_t f_lusb_msc_string[] = {
    2+2*LUSB_STR_MSC_INTERFACE_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_MSC_INTERFACE
};
#endif


#ifdef LUSB_USE_RUS_STRINGS
LUSB_DESCR_SPEC_C static uint8_t f_lusb_manufacturer_string_rus[] = {
    2 + 2*LUSB_STR_MANUFACTURER_RUS_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_MANUFACTURER_RUS
};

/* дескриптор со строкой продукта */
LUSB_DESCR_SPEC_C static uint8_t f_lusb_product_string_rus[] = {
    2+2*LUSB_STR_PRODUCT_RUS_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_PRODUCT_RUS
};

#ifdef LUSB_STR_CONFIG_RUS
/* дескриптор со строкой описания конфигурации 1 */
LUSB_DESCR_SPEC_C static uint8_t f_lusb_cfg1_string_rus[] = {
    2+2*LUSB_STR_CONFIG_RUS_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_CONFIG_RUS
};
#endif


#if defined LUSB_USE_STDBULK_INTERFACE && defined LUSB_STR_BULK_INTERFACE
/* дескриптор со строкой серийного номера */
LUSB_DESCR_SPEC_C static uint8_t f_lusb_std_intf_string_rus[] = {
    2+2*LUSB_STR_BULK_INTERFACE_RUS_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_BULK_INTERFACE_RUS
};
#endif

#if defined LUSB_USE_MSC_INTERFACE && defined LUSB_STR_MSC_INTERFACE
LUSB_DESCR_SPEC_C static uint8_t f_lusb_msc_string[] = {
    2+2*LUSB_STR_MSC_INTERFACE_RUS_LEN,
    USB_DESCRTYPE_STRING,
    LUSB_STR_MSC_INTERFACE_RUS
};
#endif



#endif

#define LUSB_STR_DESC_ARR

#endif


LUSB_DESCR_SPEC_C uint16_t *g_lusb_langids = f_lusb_langid_descr.lang_ids;

//массив - содержит адреса всех дескрипторов строк
LUSB_DESCR_SPEC_C uint8_t *g_lusb_string_descrs[LUSB_LANGID_CNT][LUSB_STRDESCR_CNT+1] = {
                            {   (LUSB_DESCR_SPEC_H uint8_t*)&f_lusb_langid_descr,
                                LUSB_STR_DESC_ARR f_lusb_manufacturer_string,
                                LUSB_STR_DESC_ARR f_lusb_product_string,
#ifdef LUSB_STR_CONFIG
                                LUSB_STR_DESC_ARR f_lusb_cfg1_string,
#endif
#if defined LUSB_USE_STDBULK_INTERFACE && defined LUSB_STR_BULK_INTERFACE
                                LUSB_STR_DESC_ARR f_lusb_std_intf_string,
#endif
#if defined LUSB_USE_MSC_INTERFACE && defined LUSB_STR_MSC_INTERFACE
                                LUSB_STR_DESC_ARR f_lusb_msc_string,
#endif
#if defined LUSB_USE_IAP_NATIVE_INTERFACE && defined LUSB_STR_IAP_EA_NATIVE_INTERFACE
                                LUSB_STR_DESC_ARR f_lusb_iap_native_string,
#endif
#ifdef LUSB_STR_SERIAL
    #ifdef LUSB_USE_MAN_GEN_SERIAL
                                NULL
    #else
                                LUSB_STR_DESC_ARR f_lusb_serial_string,
    #endif
#endif
                            },
#ifdef LUSB_USE_RUS_STRINGS
                            {   (LUSB_DESCR_SPEC_H uint8_t*) &f_lusb_langid_descr,
                                LUSB_STR_DESC_ARR f_lusb_manufacturer_string_rus,
                                LUSB_STR_DESC_ARR f_lusb_product_string_rus,
#ifdef LUSB_STR_CONFIG
                                LUSB_STR_DESC_ARR f_lusb_cfg1_string_rus,
#endif
#if defined LUSB_USE_STDBULK_INTERFACE && defined LUSB_STR_BULK_INTERFACE
                                LUSB_STR_DESC_ARR f_lusb_std_intf_string_rus,
#endif
#if defined LUSB_USE_MSC_INTERFACE && defined LUSB_STR_MSC_INTERFACE
                                LUSB_STR_DESC_ARR f_lusb_msc_string_rus,
#endif
#if defined LUSB_USE_IAP_NATIVE_INTERFACE && defined LUSB_STR_IAP_EA_NATIVE_INTERFACE
                                LUSB_STR_DESC_ARR f_lusb_iap_native_string,
#endif
#ifdef LUSB_STR_SERIAL
    #ifdef LUSB_USE_MAN_GEN_SERIAL
                                NULL
    #else
                                LUSB_STR_DESC_ARR f_lusb_serial_string,
    #endif
#endif
                            }
#endif
                        };

#include "lcspec_pack_restore.h"







