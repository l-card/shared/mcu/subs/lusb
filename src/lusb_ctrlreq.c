/*
 * lusb_ctrreq.c
 *
 *  Created on: 13.07.2010
 *      Author: borisov
 *
 *      Файл содержит функции по обработке стандартных запросов USB
 *
 */

#include "lusb.h"
#include "lusb_appl_cb.h"
#include "lusb_descriptors.h"
#ifdef LUSB_USE_SOFT_DD
    #include "lusb_soft_dd.h"
#endif
#ifdef LUSB_USE_MSFT_OS_DESCR
    #include "lusb_msft_os_descr.h"
#endif
#include "lusb_ctrlreq.h"
#include <stddef.h>
#include <string.h>

#if LUSB_EP_NONCTL_CNT
static int f_ep_enable(uint8_t intf, uint8_t ep_idx, const t_usb_endpoint_descriptor *ep_descr) {
    int err = lusb_ll_ep_enable(ep_descr);
    if (!err) {
        g_lusb_state.intf[intf].ep[ep_idx].halted = 0;
        g_lusb_state.intf[intf].ep[ep_idx].dd_cpl_cnt = 0;
        g_lusb_state.intf[intf].ep[ep_idx].dd_req_cnt = 0;
        g_lusb_state.intf[intf].ep[ep_idx].descr = ep_descr;


        lusb_appl_cb_ep_event(intf, ep_idx, LUSB_EP_EVENT_ENABLED);
        lusb_printf(LUSB_LOG_LVL_INFO, "lusb: ep 0x%02X enable\n", ep_descr->bEndpointAddress);
    } else {
        lusb_printf(LUSB_LOG_LVL_ERROR, "lusb: ep 0x%02X enable error!\n", ep_descr->bEndpointAddress);
    }
    return err;
}


static int f_ep_disable(int intf, int ep_idx) {
    int err = 0;

    const t_usb_endpoint_descriptor *ep_descr = g_lusb_state.intf[intf].ep[ep_idx].descr;
    if (lusb_ep_intf_get_dd_in_progress(intf, ep_idx) > 0)
        err = lusb_ep_intf_clear(intf, ep_idx);

    if (!err) {
        err = lusb_ll_ep_disable(ep_descr);
    }

    if (!err) {
        g_lusb_state.intf[intf].ep[ep_idx].descr = NULL;
    }

    if (!err) {
        lusb_appl_cb_ep_event(intf, ep_idx, LUSB_EP_EVENT_DISABLED);
        lusb_printf(LUSB_LOG_LVL_INFO, "lusb: ep 0x%02X disable", ep_descr->bEndpointAddress);
    } else {
        lusb_printf(LUSB_LOG_LVL_ERROR, "lusb: ep 0x%02X disable error!\n", ep_descr->bEndpointAddress);
    }

    return err;
}


static int f_ep_set_stall(uint8_t ep_addr, int stall)  {
    uint8_t intf, ep_idx;
    int res = lusb_ep_idx(ep_addr, &intf, &ep_idx);
    if (res == LUSB_ERR_SUCCESS) {
        bool irq_en = lusb_ll_usbirq_en(false);
        lusb_ep_intf_clear(intf, ep_idx);
        if (stall) {
            lusb_ll_ep_set_stall(ep_addr);
            lusb_appl_cb_ep_event(intf, ep_idx, LUSB_EP_EVENT_HALTED);
            lusb_printf(LUSB_LOG_LVL_INFO, "lusb: ep 0x%02X set stall", ep_addr);
        } else {
            lusb_ll_ep_clr_stall(ep_addr);
            lusb_appl_cb_ep_event(intf, ep_idx, LUSB_EP_EVENT_HALT_CLR);
            lusb_printf(LUSB_LOG_LVL_INFO, "lusb: ep 0x%02X clear stall", ep_addr);
        }
        lusb_ll_usbirq_en(irq_en);
        g_lusb_state.intf[intf].ep[ep_idx].halted = stall;
    }
    return res;
}


#endif


static const t_usb_configuration_descriptor * f_get_cfg_descr(int other) {
    const t_usb_configuration_descriptor* cfg_d = NULL;
#ifdef LUSB_SUPPORT_HIGH_SPEED
    if (lusb_get_speed()==LUSB_SPEED_HIGH) {
        if (!other) {
            cfg_d = LUSB_CONF_HS_DESCR_PTR;
        } else {
            cfg_d = LUSB_CONF_HS_OTHER_DESCR_PTR;
        }
    } else {
        if (!other) {
            cfg_d = LUSB_CONF_FS_DESCR_PTR;
        } else {
            cfg_d = LUSB_CONF_FS_OTHER_DESCR_PTR;
        }
    }
#else
    return LUSB_CONF_DESCR_PTR;
#endif
    return cfg_d;
}


/* Поиск в массиве дескрипторов дескриптора интерфейса с заданными номерами
 *    интерфейса и альтернативных настроек, разрешение и инициализация информации
 *    для конечных точек, соответствующих данному интерфейсу */
static int f_intf_setup(t_usb_common_descriptor** ppdescr, int intf, int altset) {
    t_usb_common_descriptor* pDescr = *ppdescr;
    int fnd = 0;
    int end_parse = 0;
#if LUSB_EP_NONCTL_CNT
    int ep_idx = 0;
#endif

    while ((pDescr->bLength) &&(!end_parse)) {
        switch (pDescr->bDescriptorType) {
            case USB_DESCRTYPE_INTERFACE: {
                    t_usb_interface_descriptor* intf_descr = (t_usb_interface_descriptor*)pDescr;

                    if ((intf_descr->bInterfaceNumber==intf) && (intf_descr->bAlternateSetting == altset)) {
#if LUSB_EP_NONCTL_CNT
                        ep_idx = 0;
#endif
                        g_lusb_state.intf[intf].descr = intf_descr;
                        g_lusb_state.intf[intf].alt_settings = altset;
                        fnd = 1;
                    } else {
                        end_parse = fnd;
                    }
                }
                break;
            case USB_DESCRTYPE_ENDPOINT: {
#if LUSB_EP_NONCTL_CNT
                    if (fnd) {
                        /* настраиваем конечные точки */
                        f_ep_enable(intf, ep_idx++, (const t_usb_endpoint_descriptor*)pDescr);
                    }
#else
                    lusb_printf(LUSB_LOG_LVL_ERROR, "ep descriptor detected, but nonctl eps disabled!\n");
#endif
                }
                break;
            default:
                end_parse = fnd;
                break;
        }
        if (!end_parse)
            pDescr = (t_usb_common_descriptor*)((uint32_t)pDescr + pDescr->bLength);
    }


    *ppdescr = pDescr;
    return 0;
}

/* освобождение ресурсов заданного интерфейса */
static int f_intf_release(unsigned intf) {    
    int err = 0;
#if LUSB_EP_NONCTL_CNT
    uint8_t ep_ind;

    for (ep_ind=0; (ep_ind < g_lusb_state.intf[intf].descr->bNumEndpoints)
         && !err; ep_ind++) {
        f_ep_disable(intf, ep_ind);
    }
#endif

    g_lusb_state.intf[intf].descr = NULL;
    g_lusb_state.intf[intf].alt_settings_cnt = 0;
    g_lusb_state.intf[intf].alt_settings = 0;

    return err;
}

/* освобождение всех интерфейсов текущей конфигурации */
static void f_cur_cfg_release(void) {
    unsigned intf;
    for (intf=0; intf < LUSB_INTERFACE_CNT; intf++) {
        if (g_lusb_state.intf[intf].descr!=NULL) {
            f_intf_release(intf);
        }
    }
}




/****************************************************************************
 *  Функция обрабатывает запрос SET_CONFIGURATION
 *  Находит нужную конфигурацию и парсит ее, настравая конечные точки
 *  Если конфигурация не найдена - возвращает LUSB_ERR_UNSUPPORTED_REQ
 *  Если конфигурация = 0 - перевод в addressed state
 * **************************************************************************/
int lusb_ctrlreq_set_config(int cfg_num) {
    int res = LUSB_ERR_UNSUPPORTED_REQ;
#ifdef LUSB_APPL_CB_DEVSTATE_CHANGED
    uint8_t old_state = g_lusb_state.dev.state;
#endif
    /* cfg_num > 0 => установка рабочей конфигурации */
    if (cfg_num) {
        t_usb_common_descriptor* pDescr;
        int end_parse = 0;
        int repeat = 0;

        /* просматриваем все дескрипторы конфигурации */
        pDescr = (t_usb_common_descriptor*)f_get_cfg_descr(0);
        while ((pDescr->bLength) &&(!end_parse)) {
            repeat = 0;
            switch (pDescr->bDescriptorType)  {
                case USB_DESCRTYPE_CONFIGURATION: {
                    t_usb_configuration_descriptor* cfg_descr = (t_usb_configuration_descriptor*)pDescr;
                    /* ищем дескриптор конфигурации с заданным номером */
                    if (cfg_descr->bConfigurationValue == cfg_num) {
                        unsigned i;
                        /* сбрасываем конфигурацию */
                        f_cur_cfg_release();

                        g_lusb_state.cfg.descr = cfg_descr;
                        g_lusb_state.dev.status = cfg_descr->bmAttributes  & USB_CONFIG_ATTR_SELFPOW;

                        for (i=0; i < LUSB_INTERFACE_CNT; i++) {
                            g_lusb_state.intf[i].alt_settings_cnt = 0;
                        }

                        res = LUSB_ERR_SUCCESS;
                    } else {
                        pDescr = (t_usb_common_descriptor*)(((uint32_t)pDescr) + cfg_descr->wTotalLength);
                        continue;
                    }
                }
                break;
            case USB_DESCRTYPE_INTERFACE: {
                    t_usb_interface_descriptor* intf_descr = (t_usb_interface_descriptor*)pDescr;
                    int intf = intf_descr->bInterfaceNumber;

                    /* При новой конфигурации устанавливаются настройки интерфейса
                     * по-умолчанию (bAlternateSetting == 0). Дескрипторы остальных
                     * настроек используем тут только для подсчета их количества */
                    if (intf_descr->bAlternateSetting == 0) {
                        f_intf_setup(&pDescr, intf, intf_descr->bAlternateSetting);
                        repeat = 1;
                    } else {
                        if (intf_descr->bAlternateSetting > g_lusb_state.intf[intf].alt_settings_cnt) {
                            g_lusb_state.intf[intf].alt_settings_cnt = intf_descr->bAlternateSetting;
                        }
                    }
                }
                break;
            }
            if (!repeat)
                pDescr = (t_usb_common_descriptor*)((uint32_t)pDescr + pDescr->bLength);
        }



        if (res == LUSB_ERR_SUCCESS) {
            lusb_ll_configure(1);
            g_lusb_state.dev.state |= LUSB_DEVSTATE_CONFIGURED;
#ifdef LUSB_USE_MSC_INTERFACE
            lusb_msc_set_config();
#endif
        }
    } else  {
        /* cfg_num = 0 => переход в режим Adressed; */
        g_lusb_state.dev.state &= ~LUSB_DEVSTATE_CONFIGURED;
        f_cur_cfg_release();
        g_lusb_state.cfg.descr = NULL;
        lusb_ll_configure(0);
        res = LUSB_ERR_SUCCESS;
    }

#ifdef LUSB_APPL_CB_DEVSTATE_CHANGED
    if (old_state != g_lusb_state.dev.state)
        lusb_appl_cb_devstate_ch(old_state, g_lusb_state.dev.state);
#endif
    return res;
}

/****************************************************************************
 *  Функция обрабатывает запросы SET_FEATURES/CLEAR_FEATURES                *
 * **************************************************************************/
int lusb_ctrlreq_features(const t_lusb_req* req) {
    int res = LUSB_ERR_UNSUPPORTED_REQ;
    uint8_t feat_set = (req->request == USB_REQ_SET_FEATURES) ? 1 : 0;

    switch (req->req_type&USB_REQ_REQTYPE_RECIPIENT) {
        case USB_REQ_REQTYPE_RECIPIENT_DEVICE:
            /* установка свойства remote wake up */
            if (req->val == USB_FEATURES_DEVICE_RWU) {
                if (feat_set) {
                    g_lusb_state.dev.status |= USB_DEVSTATUS_RWU;
                } else {
                    g_lusb_state.dev.status &= ~USB_DEVSTATUS_RWU;
                }
                res = LUSB_ERR_SUCCESS;
#ifdef LUSB_APPL_CB_ENTER_TEST_MODE
            } else if ((wWal == USB_FEATURES_DEVICE_TESTMODE) && feat_set) {
                res = lusb_appl_cb_enter_test_mode((req->index >> 8)&0xFF);
#endif
            }
            break;
        case USB_REQ_REQTYPE_RECIPIENT_INTERFACE:
            /* нет поддерживаемых свойств интерфейса */
            break;
        case USB_REQ_REQTYPE_RECIPIENT_ENDPOINT:
#if LUSB_EP_NONCTL_CNT
            /* поддерживается только HALT для ep */
            if (req->val == USB_FEATURES_ENDPOINT_HALT) {
                res = f_ep_set_stall(req->index, feat_set);
            }
#endif
            break;
    }
    return res;
}


/****************************************************************************
 *  Функция обрабатывает запрос SET_INTERFACE                               *
 * **************************************************************************/
int lusb_ctrlreq_set_interface(const t_lusb_req *req) {
    int res = LUSB_ERR_UNSUPPORTED_REQ;
    if (g_lusb_state.dev.state & LUSB_DEVSTATE_CONFIGURED) {
        uint8_t intf  = req->index;
        uint8_t altset = req->val;

        /* проверка - существует ли требуемый интерфейс */
        if (intf < g_lusb_state.cfg.descr->bNumInterfaces) {
            /* проверка - есть ли у интерфейса требуемые настройки */
            if (altset <= g_lusb_state.intf[intf].alt_settings_cnt) {
                uint8_t prev_altset = g_lusb_state.intf[intf].alt_settings;

                res = LUSB_ERR_SUCCESS;
                if (altset != prev_altset) {
                    t_usb_common_descriptor *descr = (t_usb_common_descriptor *)g_lusb_state.cfg.descr;
                    res = f_intf_release(intf);
                    if (res == LUSB_ERR_SUCCESS)
                        res = f_intf_setup(&descr, intf, altset);

                    if (res == LUSB_ERR_SUCCESS) {
                        lusb_appl_cb_intf_alteset_ch(intf, prev_altset, altset);
                    }
                }
            }
        }
    }
    return res;
}

/************* запрос SET_DESCRIPTOR не поддерживается ************/
int lusb_ctrlreq_set_descr(const t_lusb_req *req, uint8_t* buf) {
    return LUSB_ERR_UNSUPPORTED_REQ;
}




/****************************************************************************
 *  Функция подготовливает данные на передачу на запрос GET_DESCRIPTOR
 *     Поддерживаются запросы на дескрипторы
 *         DEVICE_DESCRIPTOR
 *         CONFIGURATION_DESCRIPTOR
 *         STRING_DESCRIPTOR
 *     для HS:
 *          DEVICE_QUALIFIER_DESCRIPTOR
 *          OTHER_SPEED_CONFIGURATION_DESCRIPTOR
 * **************************************************************************/
const uint8_t *lusb_ctrlreq_get_descr(const t_lusb_req* req, int* length, uint8_t *tx_buf) {
    const uint8_t *wr_buf = NULL;
    switch(req->val_h){
        case USB_DESCRTYPE_DEVICE:
            wr_buf = (uint8_t*)LUSB_DEV_DESCR_PTR;
            *length = ((t_usb_device_descriptor*)wr_buf)->bLength;
            break;

#ifdef LUSB_SUPPORT_HIGH_SPEED
        case USB_DESCRTYPE_DEVICE_QUALIFIER:
            wr_buf = (uint8_t*)LUSB_QUAL_DESCR_PTR;
            *length = ((t_usb_device_qualifier_descriptor*)wr_buf)->bLength;
            break;
        case USB_DESCRTYPE_OTHER_SPEED_CONFIGURATION:
        case USB_DESCRTYPE_CONFIGURATION: {
                const t_usb_configuration_descriptor* cfg_d = f_get_cfg_descr(
                            req->val_h==USB_DESCRTYPE_OTHER_SPEED_CONFIGURATION);
                wr_buf = (uint8_t*)cfg_d;
                *length = cfg_d->wTotalLength;
            }
            break;
#else
    case USB_DESCRTYPE_CONFIGURATION: {
            const t_usb_configuration_descriptor* cfg_d = f_get_cfg_descr(0);
            wr_buf = (uint8_t*)cfg_d;
            *length = cfg_d->wTotalLength;
        }
        break;
#endif
        case USB_DESCRTYPE_STRING: {
#ifdef LUSB_USE_MSFT_OS_DESCR
                /* строка со специальным индексом и специального формата означает
                   поддержку устройством Microsoft OS Descriptors */
                if (req->val_l == LUSB_MSFT_OS_STRING_DESCR_IDX) {
                    *length = LUSB_MSFT_OS_STRING_DESCR_LENGTH;
                    wr_buf = lusb_msft_os_str_descr;
                } else {
#endif
                    int ind=-1, i;
                    /* 0 строка - массив с lang id (общая для всех языков) */
                    if (req->val_l==0) {
                        ind = 0;
                    } else {
                        /* поиск языка */
                        for (i=0; (i < LUSB_LANGID_CNT) && (ind==-1); i++) {
                            if (g_lusb_langids[i] == req->index)
                                ind = i;
                        }

                        /* проверка, что номер строки в допустимых пределах */
                        if (ind >= 0) {
                            if (req->val_l > LUSB_STRDESCR_CNT)
                                ind = -1;
                        }
                    }

                    /* если все ок - передаем строковый дескриптор */
                    if (ind >= 0) {
#ifdef LUSB_USE_MAN_GEN_SERIAL
                        if (req->val_l == LUSB_STRIND_SERIAL) {
                            const char* str = lusb_app_cb_get_serial();
                            int len = strnlen(str, LUSB_MAX_SERIAL_SIZE);


                            tx_buf[0] = 2 + (len << 1);
                            tx_buf[1] = USB_DESCRTYPE_STRING;
                            for (i=0; i < len; ++i) {
                                tx_buf[2+2*i] = str[i];
                                tx_buf[2+2*i+1] = 0;
                            }
                            *length = tx_buf[0];
                            wr_buf = tx_buf;
                        } else {
#endif
                            wr_buf = (uint8_t*)g_lusb_string_descrs[ind][req->val_l];
                            *length = wr_buf[0];
#ifdef LUSB_USE_MAN_GEN_SERIAL
                        }
#endif
                    }
                }
#ifdef LUSB_USE_MSFT_OS_DESCR
            }
#endif
            break;
    } //switch(req->val_h)

    return wr_buf;
}

/****************************************************************************
 *  Функция подготовливает данные на передачу на запрос GET_STATUS          *
 *  (статус устройства, интерфейса или конечной точки)                      *
 * **************************************************************************/
const uint8_t* lusb_ctrlreq_get_status(const t_lusb_req* req, int* length, uint8_t *tx_buf) {
    uint8_t* wr_buf = NULL;
    switch (req->req_type&USB_REQ_REQTYPE_RECIPIENT) {
        case USB_REQ_REQTYPE_RECIPIENT_DEVICE:
            wr_buf = (uint8_t*)&g_lusb_state.dev.status;
            *length = USB_DEVICE_STATUS_LENGTH;
            break;
        case USB_REQ_REQTYPE_RECIPIENT_INTERFACE:
            //разрешено только при сконфигурированном устройстве
            if (g_lusb_state.dev.state & LUSB_DEVSTATE_CONFIGURED) {
                if (req->index < g_lusb_state.cfg.descr->bNumInterfaces) {
                    wr_buf = tx_buf;
                    wr_buf[0] = 0;
                    wr_buf[1] = 0;
                    *length = USB_INTERFACE_STATUS_LENGTH;
                }
            }
            break;
        case USB_REQ_REQTYPE_RECIPIENT_ENDPOINT: {
                uint16_t status = 0;
                int res = LUSB_ERR_SUCCESS;

                if ((req->index & USB_EPNUM_MSK)!=0) {
#if LUSB_EP_NONCTL_CNT
                    uint8_t intf_idx, ep_idx;
                    res = lusb_ep_idx(req->index, &intf_idx, &ep_idx);
                    if (res==LUSB_ERR_SUCCESS) {
                        uint8_t halted;
                        res = lusb_ep_intf_is_halted(intf_idx, ep_idx, &halted);
                        if ((res==LUSB_ERR_SUCCESS) && halted)
                            status |= USB_EPSTATUS_HALT;
                    }
#else
                    res = LUSB_ERR_EP_INDEX;
#endif
                }


                if (res == LUSB_ERR_SUCCESS) {
                    wr_buf = tx_buf;
                    wr_buf[0] = status & 0xFF;
                    wr_buf[1] = (status >> 8) & 0xFF;
                    *length = USB_ENDPOINT_STATUS_LENGTH;
                }
            }
            break;
    }
    return wr_buf;
}


/****************************************************************************
 *  Функция подготовливает данные на передачу на запрос GET_INTERFACE       *
 * **************************************************************************/
const uint8_t* lusb_ctrlreq_get_interface(const t_lusb_req* req, int* length, uint8_t *tx_buf) {
    uint8_t* wr_buf = NULL;
    uint8_t i;

    /* устройство должно быть сконфигурировано */
    if (g_lusb_state.dev.state & LUSB_DEVSTATE_CONFIGURED) {
        /* ищем интерфейс для которого идет запрос */
        for (i=0; i < g_lusb_state.cfg.descr->bNumInterfaces; i++) {
            if (g_lusb_state.intf[i].descr->bInterfaceNumber == req->index) {
                wr_buf = tx_buf;
                wr_buf[0] = g_lusb_state.intf[i].alt_settings;
                *length = USB_GET_INTERFACE_REQ_LENGTH;
            }
        }
    }
    return wr_buf;
}

/****************************************************************************
 *  Функция подготовливает данные на передачу на запрос GET_CONFIGURATION   *
 * **************************************************************************/
const uint8_t* lusb_ctrlreq_get_configuration(const t_lusb_req* req, int* length, uint8_t *tx_buf) {
    uint8_t* wr_buf = NULL;
    wr_buf = tx_buf;
    wr_buf[0] = (g_lusb_state.dev.state & LUSB_DEVSTATE_CONFIGURED) ?
                g_lusb_state.cfg.descr->bConfigurationValue : 0;
    *length = USB_GETCONFIG_REQ_LENGTH;
    return wr_buf;
}

/****************** SYNCH_FRAME не поддерживается...... ***************/
const uint8_t* lusb_ctrlreq_synch_frame(const t_lusb_req* req, int* length) {
    return NULL;
}



